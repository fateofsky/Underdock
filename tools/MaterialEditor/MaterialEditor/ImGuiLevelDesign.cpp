#include "pch.h"
#include "ImGuiLevelDesign.h"

bool viewInspectorWindow = true;

void ImguiWin32::RunLevelDesign(bool & open)
{
  const int imguiWinFlag =
    ImGuiWindowFlags_::ImGuiWindowFlags_NoResize |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoSavedSettings |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoBringToFrontOnFocus |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
  ImGui::SetNextWindowPos(ImVec2(25, 25));
  ImGui::SetNextWindowSize(ImVec2(900, 700));

  if (ImGui::Begin("Level Design", &open, imguiWinFlag))
  {
    // Inspector window
    const int imguiInspectorWinFlag = ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
    if (ImGui::Begin("Inspector", 0, imguiInspectorWinFlag))
    {

      ImGui::End();
    }

    // Scene


    ImGui::End();
  }
}

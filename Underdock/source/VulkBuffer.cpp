#include "../pch.h"
#include "Engine.h"

void VulkBuffer::CreateBuffer(unsigned size, unsigned copies, VkBufferUsageFlags usage,
                              VkMemoryPropertyFlags memory_property)
{
  VkBufferCreateInfo bufferInfo;
  bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
  bufferInfo.size = size;
  bufferInfo.usage = vBufUsageFlags = usage;
  bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
  bufferInfo.flags = 0;
  bufferInfo.pNext = NULL;
  bufferInfo.queueFamilyIndexCount = 0;
  bufferInfo.pQueueFamilyIndices = NULL;
#ifdef _DEBUG
  VkResult vkRes = vkCreateBuffer(Engine::core::vDevice, &bufferInfo, NULL, &vBuffer);
  assert(vkRes == VK_SUCCESS);
#else
  vkCreateBuffer(Engine::core::vDevice, &bufferInfo, NULL, &vBuffer);
#endif // _DEBUG

  // allocate memory
  VkMemoryRequirements buffer_memReq;
  vkGetBufferMemoryRequirements(Engine::core::vDevice, vBuffer, &buffer_memReq);

  VkMemoryAllocateInfo bufferAllocInfo;
  bufferAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
  vTotalAllocated = buffer_memReq.size * copies;
  vTotalRequested = size * copies;
  vBufferAlignment = buffer_memReq.size;
  bufferAllocInfo.allocationSize = vTotalAllocated;
  bufferAllocInfo.pNext = NULL;

#ifdef _DEBUG
  bool texBufGetType =
    Engine::core::memory_type_from_properties(
      buffer_memReq.memoryTypeBits,
      memory_property,
      &bufferAllocInfo.memoryTypeIndex);
  vkRes = vkAllocateMemory(Engine::core::vDevice, &bufferAllocInfo, NULL, &vBufMem);
  assert(texBufGetType && vkRes == VK_SUCCESS);
#else
    Engine::core::memory_type_from_properties(
      buffer_memReq.memoryTypeBits,
      memory_property,
      &bufferAllocInfo.memoryTypeIndex);
  vkAllocateMemory(Engine::core::vDevice, &bufferAllocInfo, NULL, &vBufMem);
#endif // _DEBUG

}


void VulkBuffer::DestroyBuffer()
{
  // destroy buffer
  vkDestroyBuffer(Engine::core::vDevice, vBuffer, NULL);
  vkFreeMemory(Engine::core::vDevice, vBufMem, NULL);
}


void VulkBuffer::BindBufferMemory()
{
  // bind and map memory
#ifdef _DEBUG
  VkResult vkRes = vkBindBufferMemory(Engine::core::vDevice, vBuffer, vBufMem, 0);
  assert(vkRes == VK_SUCCESS);
#else
  vkBindBufferMemory(Engine::core::vDevice, vBuffer, vBufMem, 0);
#endif // _DEBUG
}

void VulkBuffer::MapMemory(void* dataToMap, unsigned size, VkDeviceSize offset)
{
  vkMapMemory(Engine::core::vDevice, vBufMem, offset, size, 0, &vBufMemData);
  if (dataToMap != nullptr && size > 0)
    memcpy(vBufMemData, dataToMap, size);
  vkUnmapMemory(Engine::core::vDevice, vBufMem);
}

void VulkBuffer::CopyBuffer(VkCommandBuffer cmd, const VulkBuffer& rhsBuffer)
{
  VkBufferCopy copyRegion;
  copyRegion.dstOffset = 0;
  copyRegion.srcOffset = 0;
  copyRegion.size = vTotalRequested;
  vkCmdCopyBuffer(cmd, rhsBuffer.vBuffer, vBuffer, 1, &copyRegion);
}


VkBuffer VulkBuffer::GetBuffer() const
{
  return vBuffer;
}

VkBuffer* VulkBuffer::GetBufferPtr()
{
  return &vBuffer;
}

const VkBuffer* VulkBuffer::GetBufferPtr() const
{
  return &vBuffer;
}

VkDeviceMemory VulkBuffer::GetBufferDevMem() const
{
  return vBufMem;
}

void* VulkBuffer::GetMappedMemoryData()
{
  return vBufMemData;
}

const VkDeviceSize& VulkBuffer::GetTotalAllocatedMemory() const
{
  return vTotalAllocated;
}

const VkDeviceSize& VulkBuffer::GetBufferAlignmentSize() const
{
  return vBufferAlignment;
}


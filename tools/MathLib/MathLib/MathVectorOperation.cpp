/* Start Header
*****************************************************************/
/*!
\file      MathVectorOperation.cpp
\author    
\par       
\date      
\brief

Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the
prior written consent of DigiPen Institute of Technology is prohibited.
*/
/* End Header
*******************************************************************/
#include "stdafx.h"



/*
- 2D vector operations
*/
void vec2Normalize(vec2 &rgResult, const vec2 &rgVector1)
{
	if (vec2Compare(rgVector1, vec2()))
	{
		rgResult = rgVector1;
		return;
	}

	rgResult = rgVector1 / vec2Length(rgVector1);
}

float vec2Length(const vec2 &rgVector1)
{
	return FastSquare(vec2SquareLength(rgVector1));
}

float vec2SquareLength(const vec2 &rgVector1)
{
	return (rgVector1.x_ * rgVector1.x_ + rgVector1.y_ * rgVector1.y_);
}

float vec2istance(const vec2 &rgVector1, const vec2 &rgVector2)
{
	vec2 Temp = rgVector2 - rgVector1;
	return vec2Length(Temp);
}

float vec2SquareDistance(const vec2 &rgVector1, const vec2 &rgVector2)
{
	vec2 Temp = rgVector2 - rgVector1;
	return vec2SquareLength(Temp);
}

float vec2DotProduct(const vec2 &rgVector1, const vec2 &rgVector2)
{
	return (rgVector1.x_ * rgVector2.x_ + rgVector1.y_ * rgVector2.y_);
}

float vec2CrossProduct(const vec2 &rgVector1, const vec2 &rgVector2)
{
	return (rgVector1.x_ * rgVector2.y_ - rgVector1.y_ * rgVector2.x_);
}


/*
- 3D vector operations
*/
void vec3Normalize(vec3 &rgResult, const vec3 &rgVector1)
{
	if (vec3Compare(rgVector1, vec3()))
	{
		rgResult = rgVector1;
		return;
	}

	rgResult = rgVector1 / vec3Length(rgVector1);
}

float vec3Length(const vec3 &rgVector1)
{
	return FastSquare(vec3SquareLength(rgVector1));
}

float vec3SquareLength(const vec3 &rgVector1)
{
	return (rgVector1.x_ * rgVector1.x_ + rgVector1.y_ * rgVector1.y_ + rgVector1.z_ * rgVector1.z_);
}

float vec3istance(const vec3 &rgVector1, const vec3 &rgVector2)
{
	vec3 Temp = rgVector2 - rgVector1;
	return vec3Length(Temp);
}
float vec3SquareDistance(const vec3 &rgVector1, const vec3 &rgVector2)
{
	vec3 Temp = rgVector2 - rgVector1;
	return vec3SquareLength(Temp);
}

float vec3DotProduct(const vec3 &rgVector1, const vec3 &rgVector2)
{
	return (rgVector1.x_ * rgVector2.x_ + rgVector1.y_ * rgVector2.y_ + rgVector1.z_ * rgVector2.z_);
}

vec3 vec3CrossProduct(const vec3 &rgVector1, const vec3 &rgVector2)
{
	vec3 Temp;
	Temp.x_ = rgVector1.y_ * rgVector2.z_ - rgVector2.y_ * rgVector1.z_;
	Temp.y_ = rgVector2.x_ * rgVector1.z_ - rgVector1.x_ * rgVector2.z_;
	Temp.z_ = rgVector1.x_ * rgVector2.y_ - rgVector2.x_ * rgVector1.y_;
	return Temp;
}



/*
- 4D vector operations
*/
vec4 constructPlane(const vec3& p0, const vec3& p1, const vec3& p2)
{
	vec3 a = p2 - p0;
	vec3 b = p1 - p0;
	if (a.sqrMagnitude() == 0) return vec4(0,0,0,0);
	if (b.sqrMagnitude() == 0) return vec4(0, 0, 0, 0);

	vec3 n = vec3CrossProduct(b, a);
	if (n.sqrMagnitude() == 0) return vec4(0, 0, 0, 0);
	n.Normalize();
	float proj = vec3DotProduct(p0, n);
	return vec4(n.x_, n.y_, n.z_, proj);
}

void vec4Normalize(vec4 &rgResult, const vec4 &rgVector1)
{
	if (vec4Compare(rgVector1, vec4()))
	{
		rgResult = rgVector1;
		return;
	}

	rgResult = rgVector1 / vec4Length(rgVector1);
}

float vec4Length(const vec4 &rgVector1)
{
	return FastSquare(vec4SquareLength(rgVector1));
}

float vec4SquareLength(const vec4 &rgVector1)
{
	return (rgVector1.x_ * rgVector1.x_ + rgVector1.y_ * rgVector1.y_ + rgVector1.z_ * rgVector1.z_ + rgVector1.w_ * rgVector1.w_);
}

float vec4Distance(const vec4 &rgVector1, const vec4 &rgVector2)
{
	vec4 Temp = rgVector2 - rgVector1;
	return vec4Length(Temp);
}

float vec4SquareDistance(const vec4 &rgVector1, const vec4 &rgVector2)
{
	vec4 Temp = rgVector2 - rgVector1;
	return vec4SquareLength(Temp);
}

float vec4DotProduct(const vec4 &rgVector1, const vec4 &rgVector2)
{
	return (rgVector1.x_ * rgVector2.x_ + rgVector1.y_ * rgVector2.y_ + rgVector1.z_ * rgVector2.z_ + rgVector1.w_ * rgVector2.w_);
}

vec4 vec4CrossProduct(const vec4& rgVector1, const vec4& rgVector2)
{
	vec3 result(vec3CrossProduct(vec3(rgVector1.x_, rgVector1.y_, rgVector1.z_), vec3(rgVector2.x_, rgVector2.y_, rgVector2.z_)));
	return vec4(result.x_, result.y_, result.z_, 0.0f);
}

vec3 LookAt(const vec3& pos, const vec3& dest, const vec3& up)
{
	vec3 lookingDir = vec3(0, 0, 1);
	vec3 lookAtDir = (dest - pos).normalized();

	/*vec3 v = vec3CrossProduct(lookingDir, lookAtDir);
	
	float c = vec3otProduct(lookingDir, lookAtDir);
	float s = v.magnitude();
	v.Normalize();

	mat4 rotation;

	if (c > 0.0f && (1.0f - c) < EPSILON)
		return vec3();
	else if (c < 0.0f && (-1.0f - c) > -EPSILON)
	{
		vec3 axis;
		if (v.x_ || v.z_)
		{
			axis.x_ = -v.z_;
			axis.z_ = -v.x_;
		}
		else
		{
			axis.x_ = -v.y_;
			axis.y_ = -v.x_;
		}
		
	
		v = axis.normalized();
	
		c = -1.0f;
		s = 0.0f;
	}

	rotation.m2_44[0][0] = c + v.x_ * v.x_ * (1 - c);
	rotation.m2_44[1][0] = v.z_ * s + v.y_ * v.x_ * (1 - c);
	rotation.m2_44[2][0] = -v.y_ * s + v.z_ * v.x_ * (1 - c);
	rotation.m2_44[0][1] = -v.z_ * s + v.x_ * v.y_ * (1 - c);
	rotation.m2_44[1][1] = c + v.y_ * v.y_ * (1 - c);
	rotation.m2_44[2][1] = v.x_ * s + v.z_ * v.y_ * (1 - c);
	rotation.m2_44[0][2] = v.y_ * s + v.x_ * v.z_ * (1 - c);
	rotation.m2_44[1][2] = -v.x_ * s + v.y_ * v.z_ * (1 - c);
	rotation.m2_44[2][2] = c + v.z_ * v.z_ * (1 - c);*/

	// testing
	vec3 f = lookAtDir;
	vec3 u = up;
	vec3 s = vec3CrossProduct(f, u).normalized();
	u = vec3CrossProduct(s, f);

	mat4 rotation;
	rotation.m2_44[0][0] = s.x_;
	rotation.m2_44[1][0] = s.y_;
	rotation.m2_44[2][0] = s.z_;
	rotation.m2_44[0][1] = u.x_;
	rotation.m2_44[1][1] = u.y_;
	rotation.m2_44[2][1] = u.z_;
	rotation.m2_44[0][2] = -f.x_;
	rotation.m2_44[1][2] = -f.y_;
	rotation.m2_44[2][2] = -f.z_;

	float c1, c2, s1;
	vec3 rot;

	rot.x_ = atan2(rotation.m21, rotation.m22);
	c2 = sqrt(rotation.m00 * rotation.m00 + rotation.m10 * rotation.m10);
	rot.y_ = atan2(-rotation.m20, c2);
	s1 = sin(rot.x_);
	c1 = cos(rot.x_);
	rot.z_ = atan2(s1 * rotation.m02 - c1 * rotation.m01, c1 * rotation.m11 - s1 * rotation.m12);

	rot *= TODEGREE;

	return rot;
}

bool vec2Compare(const vec2& lhs, const vec2& rhs)
{
  return MATH_OPS_FLOATCOMPARE(lhs.x_, rhs.x_) && MATH_OPS_FLOATCOMPARE(lhs.y_, rhs.y_);
}

bool vec3Compare(const vec3 & lhs, const vec3 & rhs)
{
  return MATH_OPS_FLOATCOMPARE(lhs.x_, rhs.x_) &&
         MATH_OPS_FLOATCOMPARE(lhs.y_, rhs.y_) &&
         MATH_OPS_FLOATCOMPARE(lhs.z_, rhs.z_);
}

bool vec4Compare(const vec4 & lhs, const vec4 & rhs)
{
  return MATH_OPS_FLOATCOMPARE(lhs.w_, rhs.w_) &&
         MATH_OPS_FLOATCOMPARE(lhs.x_, rhs.x_) && 
         MATH_OPS_FLOATCOMPARE(lhs.y_, rhs.y_) &&
         MATH_OPS_FLOATCOMPARE(lhs.z_, rhs.z_);
}

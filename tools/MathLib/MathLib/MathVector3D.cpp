/* Start Header
*****************************************************************/
/*!
\file      MathVector3D.cpp
\author    
\par       
\date      
\brief

Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the
prior written consent of DigiPen Institute of Technology is prohibited.
*/
/* End Header
*******************************************************************/
#include "stdafx.h"

///////////
// vec3 //--------------------------------------------------------------
///////////
/* -----------------------------*/
/* ----- Member Functions ----- */
/* -----------------------------*/
vec3::vec3(int x_, int y_, int z_)
	: x_{ (float)x_ }, y_{ (float)y_ }, z_{ (float)z_ } {}

vec3::vec3(float x_, float y_, float z_)
	: x_{ x_ }, y_{ y_ }, z_{ z_ } {}

vec3::vec3(const vec3& v, float z_)
	: x_{ v.x_ }, y_{ v.y_ }, z_{ z_ } {}

vec3::vec3(const vec3& v, float y_, float z_)
	: x_{ v.x_ }, y_{ y_ }, z_{ z_ } {}

vec3::vec3(const vec2& v, float z_)
	: x_{ v.x_ }, y_{ v.y_ }, z_{ z_ } {}

vec3::vec3(const vec4& v)
	: x_{ v.x_ }, y_{ v.y_ }, z_{ v.z_ } {}

vec3::vec3(const std::string& str)
{
	size_t comma1 = str.find(',');
	size_t comma2 = str.find(',', comma1 + 1);
	x_ = (decltype(x_))std::stod(str);
	if (comma1 != std::string::npos)
	{
		y_ = (decltype(y_))std::atof((&str.front()) + comma1 + 1);
		if (comma1 != std::string::npos)
			z_ = (decltype(z_))std::atof((&str.front()) + comma2 + 1);
	}
	else
	{
		y_ = 0;
		z_ = 0;
	}
}

vec3::operator std::string() const
{
	return std::to_string(x_) + ", " + std::to_string(y_) + ", " + std::to_string(z_);
}

vec3& vec3::operator+=(const vec3& b)
{
	*this = *this + b;
	return *this;
}

vec3& vec3::operator-=(const vec3& b)
{
	*this = *this - b;
	return *this;
}

vec3& vec3::operator*=(const float& s)
{
	*this = *this * s;
	return *this;
}

vec3& vec3::operator*=(const vec3& b)
{
	*this = *this * b;
	return *this;
}

vec3& vec3::operator/=(const float& s)
{
	assert(s != 0.0f);
	*this = *this / s;
	return *this;
}

vec3& vec3::operator/=(const vec3& b)
{
	assert(b.x_ != 0.0f);
	assert(b.y_ != 0.0f);
	assert(b.z_ != 0.0f);
	*this = *this / b;
	return *this;
}

vec3& vec3::operator=(const float rhs[])
{
	x_ = rhs[0];
	y_ = rhs[1];
	z_ = rhs[2];
	return *this;
}

vec3 & vec3::operator=(const vec4 & rhs)
{
	x_ = rhs.x_;
	y_ = rhs.y_;
	z_ = rhs.z_;
	return *this;
}

vec3& vec3::operator++()
{
	++x_;
	++y_;
	++z_;
	return *this;
}

vec3 vec3::operator++(int)
{
	vec3 Temp(x_, y_, z_);
	x_++;
	y_++;
	z_++;
	return Temp;
}

vec3& vec3::operator--()
{
	--x_;
	--y_;
	--z_;
	return *this;
}

vec3 vec3::operator--(int)
{
	vec3 Temp(x_, y_, z_);
	x_--;
	y_--;
	z_--;
	return Temp;
}

float& vec3::operator[](int i)
{
	return ar[i];
}

const float& vec3::operator[](int i) const
{
	return ar[i];
}


float vec3::magnitude() const
{
	return FastSquare(sqrMagnitude());
}

float vec3::sqrMagnitude() const
{
	return (x_ * x_ + y_ * y_ + z_ * z_);
}

vec3 vec3::normalized() const
{
	vec3 norm;
	vec3Normalize(norm, *this);
	return norm;
}

float vec3::manhattanDistance() const
{
	return abs(x_) + abs(y_) + abs(z_);
}

void vec3::Normalize()
{
	vec3Normalize(*this, *this);
}

/* ---------------------------------*/
/* ----- Non-Member Functions ----- */
/* ---------------------------------*/
vec3 operator+(const vec3& a, const vec3& b)
{
	return vec3(a.x_ + b.x_, a.y_ + b.y_, a.z_ + b.z_);
}

vec3 operator+(const vec3& a)
{
	return a;
}

vec3 operator-(const vec3& a, const vec3& b)
{
	return vec3(a.x_ - b.x_, a.y_ - b.y_, a.z_ - b.z_);
}

vec3 operator-(const vec3& a)
{
	return vec3(-a.x_, -a.y_, -a.z_);
}

vec3 operator*(const vec3& a, const float& s)
{
	return vec3(a.x_ * s, a.y_ * s, a.z_ * s);
}

vec3 operator*(const float& s, const vec3& a)
{
	return a * s;
}

vec3 operator*(const vec3& a, const vec3& b)
{
	vec3 tmp;
	tmp.x_ = a.x_ * b.x_;
	tmp.y_ = a.y_ * b.y_;
	tmp.z_ = a.z_ * b.z_;
	return tmp;
}

vec3 operator/(const vec3& a, const float& s)
{
	assert(s != 0.0f);
	return vec3(a.x_ / s, a.y_ / s, a.z_ / s);
}

vec3 operator/(const vec3& a, const vec3& b)
{
	assert(b.x_ != 0.0f);
	assert(b.y_ != 0.0f);
	assert(b.z_ != 0.0f);
	vec3 tmp;
	tmp.x_ = a.x_ / b.x_;
	tmp.y_ = a.y_ / b.y_;
	tmp.z_ = a.z_ / b.z_;
	return tmp;
}

vec3 operator%(const vec3& a, const float& s)
{
	return vec3(fmodf(a.x_, s), fmodf(a.y_, s), fmodf(a.z_, s));
}

//bool operator==(const vec3& a, const vec3& b)
//{
//	return (a.x_ == b.x_ && a.y_ == b.y_ && a.z_ == b.z_);
//}
//
//bool operator!=(const vec3& a, const vec3& b)
//{
//	return !(a == b);
//}

std::ostream & operator<<(std::ostream & os, const vec3 & rhs)
{
	std::cout << "(" << rhs.x_ << ", " << rhs.y_ << ", " << rhs.z_ << ")";
	return os;
}
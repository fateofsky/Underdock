#include "pch.h"
#include "ModelComponent.h"

ModelComponent::ModelComponent(Gameobject* owner, const unsigned & _id)
  : ComponentBase(owner, _id)
{
  ComponentBase::InsertParam(0, &m_modelBufferIndex);
}

ModelComponent::~ModelComponent()
{ }

void ModelComponent::SetModelIndex(const unsigned& modelBufferIndex)
{
  m_modelBufferIndex.GetData() = modelBufferIndex;
}

const unsigned& ModelComponent::GetModelIndex() const
{
  return m_modelBufferIndex.GetData();
}

void ModelComponent::Init()
{
}

void ModelComponent::PostInit()
{
}

void ModelComponent::Update()
{
}

void ModelComponent::PreRender()
{ }

void ModelComponent::PostUpdate()
{
}

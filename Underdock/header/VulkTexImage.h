#pragma once
#include <vulkan\vulkan.h>
#include "stb_image.h"
#include "VulkBuffer.h"

class VulkTexImage
{
public:
  bool Load2DTextureImage(const char* file, 
                          VkFormat fmt = VK_FORMAT_R8G8B8A8_UNORM,
                          VkImageAspectFlags flgs = VK_IMAGE_ASPECT_COLOR_BIT);
  void CreateAttachment(const unsigned& width, const unsigned& height,
                        VkFormat fmt = VK_FORMAT_R8G8B8A8_UNORM, VkImageUsageFlags usage = VkImageUsageFlagBits::VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
                        VkImageAspectFlags flags = VK_IMAGE_ASPECT_COLOR_BIT);
  void CreateShaderSampler(VkFilter minFilter, VkFilter magFilter, VkSamplerAddressMode uvwMode);
  void DestroyImage();

  void _InitializeImage(VkCommandBuffer cmdBuf);
  void _Cleanup();

  VkImage         GetTextureImage() const;
  VkImageView     GetTextureImageView() const;
  VkDeviceMemory  GetTextureImageDevMem() const;
  VkFormat        GetTextureImageFormat() const;
  VkSampler       GetTextureImageSampler() const;

private:

  struct VulkImageProperties
  {
    unsigned _width;
    unsigned _height;
    unsigned _depth;
    unsigned _channels;
  };

  VkImage             vTexImg;
  VkImageView         vTexImgView;
  VkDeviceMemory      vTexImgMem;
  VkSampler           vTexSampler;
  VkFormat            vTexImageFormat;
  VkDeviceSize        vTexImageDeviceSize;
  VulkImageProperties vProp;

  // temporary
  VulkBuffer          vStagingBuffer;
};

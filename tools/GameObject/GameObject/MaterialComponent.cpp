#include "pch.h"
#include "MaterialComponent.h"

MaterialComponent::MaterialComponent(Gameobject* owner, const unsigned & _id)
  : ComponentBase(owner, _id)
{
  ComponentBase::InsertParam(1, &m_pushForRender);
  ComponentBase::InsertParam(2, &m_taggedGlobalInstance);
  ComponentBase::InsertParam(3, &m_taggedSharedInstance);
  ComponentBase::InsertParam(4, &m_descSetNumber);
  ComponentBase::InsertParam(5, &m_matPipelineIndex);
  ComponentBase::InsertParam(6, &m_textureList);
  ComponentBase::InsertParam(7, &m_attachmentTextureList);
  ComponentBase::InsertParam(8, &m_type);
}

MaterialComponent::~MaterialComponent()
{ 
  //m_materialDataSets.GetData().DeleteAllData();
}

void MaterialComponent::Init()
{
  // TODO: Check for push for render and register into graphics manager
}

void MaterialComponent::PostInit()
{
}

void MaterialComponent::Update()
{
}

void MaterialComponent::PreRender()
{
  // TODO: Push all data in m_materialDataSets into renderables
}

void MaterialComponent::PostUpdate()
{
}

void MaterialComponent::SetDescriptorSetNumber(const unsigned& setNum)
{
  m_descSetNumber.GetData() = setNum;
}

void MaterialComponent::SetMaterialPipeline(const unsigned& matPipeIndex)
{
  m_matPipelineIndex.GetData() = matPipeIndex;
}


void MaterialComponent::PushTextureList(const unsigned& slotIndex, const unsigned & texID, const unsigned& interfaceBlockID)
{
  m_textureList.GetData(slotIndex)._textureID = texID;
  m_textureList.GetData(slotIndex)._interfaceBlockID = interfaceBlockID;
  m_textureList.GetData(slotIndex)._renderTargetID = std::numeric_limits<unsigned>::max();
}

void MaterialComponent::SetAttachmentListSize(const unsigned& size)
{
  m_attachmentTextureList.SetDataSize(size, Renderables::_TextureRefIBPair{ 0, 0 });
}

void MaterialComponent::PushAttachmentTextureList(const unsigned& slotIndex, const unsigned& outputAttachmentLocation, 
                                                  const unsigned& interfaceBlockID, const unsigned& renderTargetID)
{
  m_attachmentTextureList.GetData(slotIndex)._textureID = outputAttachmentLocation;
  m_attachmentTextureList.GetData(slotIndex)._interfaceBlockID = interfaceBlockID;
  m_attachmentTextureList.GetData(slotIndex)._renderTargetID = renderTargetID;
}

void MaterialComponent::SetTextureListSize(const unsigned& size)
{
  m_textureList.SetDataSize(size, Renderables::_TextureRefIBPair{ 0, 0 });
}

const unsigned& MaterialComponent::GetDescriptorSetNumber() const
{
  return m_descSetNumber.GetData();
}

const unsigned& MaterialComponent::GetMaterialPipelineIndex() const
{
  return m_matPipelineIndex.GetData();
}


void MaterialComponent::SetResourceAccessibilityType(Renderables::_ResourceType type)
{
  m_type.GetData() = type;
}

Renderables::_ResourceType  MaterialComponent::GetResourceAccessibilityType() const
{
  return m_type.GetData();
}

void MaterialComponent::SetGlobalResourceTag(const unsigned& instanceID)
{
  m_taggedGlobalInstance.GetData() = instanceID;
}

void MaterialComponent::SetSharedResourceTag(const unsigned& instanceID)
{
  m_taggedSharedInstance.GetData() = instanceID;
}

const unsigned& MaterialComponent::GetGlobalResourceTag() const
{
  return m_taggedGlobalInstance.GetData();
}

const unsigned& MaterialComponent::GetSharedResourceTag() const
{
  return m_taggedSharedInstance.GetData();
}

void MaterialComponent::SetData(const void* data, const unsigned& dataSize, const unsigned& interfaceID, const uint64_t& offset, const unsigned& frameNum)
{
#ifdef _DEBUG
  if (interfaceID >= m_renderable.m_interfaceBlockAccess.size())
  {
    return;
  }
#endif // _DEBUG
  Renderables::_renderablesInterfaceBlock& ib = m_renderable.m_interfaceBlockAccess[interfaceID];
  memcpy_s(ib._uniformDataPtrList[frameNum] + offset, ib._size, data, dataSize);
}

//
//MaterialComponent::_MaterialIntBlock::_MaterialData::_MaterialData()
//  : _baseType(-1), _baseSize(0), _dataPtr(nullptr)
//{ }
//
//MaterialComponent::_MaterialIntBlock::_MaterialData::_MaterialData(const int& baseType, const unsigned& baseSize, void* ptr)
//  : _baseType(baseType), _baseSize(baseSize), _dataPtr(ptr)
//{ }
//
//void MaterialComponent::_MaterialIntBlock::Serialize(MemoryStream& ofs)
//{
//  ofs.Write(_matBlockData.size());
//  for (auto& matData : _matBlockData)
//  {
//    ofs.Write(matData._baseType);
//    ofs.Write(matData._baseSize);
//    ofs.Write(matData._dataPtr, matData._baseSize);
//  }
//}
//
//void MaterialComponent::_MaterialIntBlock::Deserialize(MemoryStream& ifs)
//{
//  size_t listSize;
//  ifs.Read(&listSize);
//  _matBlockData.resize(listSize);
//  for (auto& elem : _matBlockData)
//  {
//    ifs.Read(&elem._baseType);
//    ifs.Read(&elem._baseSize);
//    ifs.Read(elem._dataPtr, elem._baseSize);
//  }
//}
//
//void MaterialComponent::_MaterialDataSets::Serialize(MemoryStream& ofs)
//{
//  ofs.Write(_allMatBlocks.size());
//  for (auto& block : _allMatBlocks)
//    block.Serialize(ofs);
//}
//
//void MaterialComponent::_MaterialDataSets::Deserialize(MemoryStream& ifs)
//{
//  size_t listSize;
//  ifs.Read(&listSize);
//  _allMatBlocks.resize(listSize);
//  for (auto& block : _allMatBlocks)
//    block.Deserialize(ifs);
//}
//
//void MaterialComponent::_MaterialDataSets::DeleteData(const unsigned & interfaceID, const unsigned & index)
//{
//#ifdef _DEBUG
//  if (interfaceID >= _allMatBlocks.size())
//  {
//    std::cout << "Error: interfaceID: " << interfaceID << " is out of bound when attempting to delete data\n";
//    assert(false);
//  }
//  else
//  {
//    if (index >= _allMatBlocks[interfaceID]._matBlockData.size())
//    {
//      std::cout << "Error: index: " << index << " is out of bound when attempting to delete data\n";
//      assert(false);
//    }
//  }
//#endif // _DEBUG
//
//  free(_allMatBlocks[interfaceID]._matBlockData[index]._dataPtr);
//}
//
//void MaterialComponent::_MaterialDataSets::DeleteAllData()
//{
//  for (auto& matBlk : _allMatBlocks)
//  {
//    for (auto& blkData : matBlk._matBlockData)
//      if (blkData._dataPtr) free(blkData._dataPtr);
//    matBlk._matBlockData.clear();
//  }
//  _allMatBlocks.clear();
//}
//
//const int& MaterialComponent::_MaterialDataSets::GetBaseType(const unsigned& interfaceID, const unsigned& index) const
//{
//#ifdef _DEBUG
//  if (interfaceID >= _allMatBlocks.size())
//  {
//    std::cout << "Error: interfaceID: " << interfaceID << " is out of bound when attempting to get base type\n";
//    assert(false);
//  }
//  else
//  {
//    if (index >= _allMatBlocks[interfaceID]._matBlockData.size())
//    {
//      std::cout << "Error: index: " << index << " is out of bound when attempting to get base type\n";
//      assert(false);
//    }
//  }
//#endif // _DEBUG
//
//  return _allMatBlocks[interfaceID]._matBlockData[index]._baseType;
//}
//
//const unsigned& MaterialComponent::_MaterialDataSets::GetBaseSize(const unsigned& interfaceID, const unsigned& index) const
//{
//#ifdef _DEBUG
//  if (interfaceID >= _allMatBlocks.size())
//  {
//    std::cout << "Error: interfaceID: " << interfaceID << " is out of bound when attempting to get base size\n";
//    assert(false);
//  }
//  else
//  {
//    if (index >= _allMatBlocks[interfaceID]._matBlockData.size())
//    {
//      std::cout << "Error: index: " << index << " is out of bound when attempting to get base size\n";
//      assert(false);
//    }
//  }
//#endif // _DEBUG
//
//  return _allMatBlocks[interfaceID]._matBlockData[index]._baseSize;
//}
//

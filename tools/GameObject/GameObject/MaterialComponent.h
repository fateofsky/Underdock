#define PARSECOMP
#undef PARSECOMP
#pragma once
#include "ComponentBase.h"
#include "Renderables.h"

class ComponentBase;


class MaterialComponent : public ComponentBase, public _SINGLE
{
  friend class GraphicsManager;
public:


  MaterialComponent(Gameobject* owner, const unsigned& _id);
  virtual ~MaterialComponent();

  virtual void Init() override;
  virtual void PostInit() override;
  virtual void Update() override;
  virtual void PreRender() override;
  virtual void PostUpdate() override;

  void SetDescriptorSetNumber(const unsigned& setNum);
  void SetMaterialPipeline(const unsigned& matPipeIndex);
  void SetTextureListSize(const unsigned& size);
  void PushTextureList(const unsigned& slotIndex, const unsigned& texID, const unsigned& interfaceBlockID);
  void SetAttachmentListSize(const unsigned& size);
  void PushAttachmentTextureList(const unsigned& slotIndex, const unsigned& outputAttachmentLocation, 
                                 const unsigned& interfaceBlockID, const unsigned& renderTargetID);


  const unsigned& GetDescriptorSetNumber() const;
  const unsigned& GetMaterialPipelineIndex() const;
  void SetResourceAccessibilityType(Renderables::_ResourceType type);
  Renderables::_ResourceType GetResourceAccessibilityType() const;

  void SetGlobalResourceTag(const unsigned& instanceID);
  void SetSharedResourceTag(const unsigned& instanceID);
  const unsigned& GetGlobalResourceTag() const;
  const unsigned& GetSharedResourceTag() const;

  void SetData(const void* data, const unsigned& dataSize, const unsigned& interfaceID, 
               const uint64_t& offset, const unsigned& frameNum);


private:
  Renderables                                    m_renderable;

  // serializable
  DataParam<bool>                                m_pushForRender;
  DataParam<unsigned>                            m_taggedGlobalInstance;
  DataParam<unsigned>                            m_taggedSharedInstance;
  DataParam<unsigned>                            m_descSetNumber;
  DataParam<unsigned>                            m_matPipelineIndex;
  DataParamList<Renderables::_TextureRefIBPair>  m_textureList;
  DataParamList<Renderables::_TextureRefIBPair>  m_attachmentTextureList;

  // resources layout
  DataParam<Renderables::_ResourceType> m_type;
};
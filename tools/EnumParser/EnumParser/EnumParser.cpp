#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <stdio.h>
#include <iostream>
#include <xmmintrin.h>
#include <strsafe.h>
#include <direct.h>
#include "atlstr.h"
#include <vector>
#include <fstream>
#include <sstream>

#define MAX_LOADSTRING 100
#define MAXPATHMINE 512
#define GetWorkingDirectory _getcwd

// Global Variables:
HWND  hwnd;                              // Handle to a window
HDC hdc;                                 //Hardware Device Context
HGLRC hrc;                               //rendering context
HINSTANCE hInst;                         // current instance
WCHAR szTitle[MAX_LOADSTRING];           // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];     // the main window class name

namespace vld
{
  using uchar = unsigned char;
  using ucharP = unsigned char*;
  using vecStr = std::vector<std::string>;
  using Str = std::string;
}

using namespace vld;

int main(int argc, char *argv[])
{
  std::cout << " -- Component Enum parser -- \n";
  if (argc < 4)
  {
    std::cout << "Usage: <input folder> <ComponentRTTI file> <ComponentRTTIFunc file>";
    return -1;
  }

  // get working directory
  vecStr           vecOfValidFile;
  WIN32_FIND_DATA  ffd;
  TCHAR            szDir[MAXPATHMINE]{ 0 };
  HANDLE           hFind{ INVALID_HANDLE_VALUE };
  Str              outputFile;
  std::vector<Str> ComponentNameVec;
  Str              stdDirStr;

  // copy file directory over
  _tcscpy(szDir, A2T(argv[1]));
  StringCchCopy(szDir, MAXPATHMINE, szDir);
  StringCchCat(szDir, MAX_PATH, TEXT("\\*"));
  stdDirStr = argv[1];

  // find all the files with valid files
  hFind = FindFirstFile(szDir, &ffd);
  FindNextFile(hFind, &ffd); // skip the ..
  ComponentNameVec.emplace_back("none");
  while (FindNextFile(hFind, &ffd) != 0)
  {
    Str dirstr = ffd.cFileName;

    size_t strPos = dirstr.find_first_of(".");
    if (strPos >= dirstr.size()) continue;

    std::ifstream opTest(stdDirStr + "\\" + dirstr);
    if (opTest.is_open())
    {
      std::string firstLine;
      std::getline(opTest, firstLine);
      if (firstLine != "#define PARSECOMP")
      {
        opTest.close();
        continue;
      }
    }

    // Find only header
    Str isItHeader = dirstr.substr(strPos);
    if (isItHeader != ".h" && isItHeader != ".hpp") continue;

    dirstr = dirstr.substr(0, dirstr.find_first_of("."));
    ComponentNameVec.emplace_back(dirstr);
  }


  /*
    -- Write to rtti
  */
  std::ofstream ofs(argv[2], std::ofstream::trunc);
  if (!ofs.is_open())
  {
    std::cout << "Error : fail to open " << argv[2] << std::endl;
    return -1;
  }
  
  std::cout << "Now parsing RTTI...\n\n";

  // component enum
  ofs << "#pragma once\n\nenum ComponentEnum\n{\n";
  for (unsigned i = 0; i < ComponentNameVec.size(); ++i)
  {
    std::cout << " -> " << ComponentNameVec[i].c_str() << std::endl;
    ofs << "  " << ("C_" + ComponentNameVec[i]);

    if (i < (ComponentNameVec.size() - 1))
      ofs << ",\n";
  }
  ofs << "\n};\n\n";


  // forward declare
  std::cout << "\nCreating RTTI type class...\n";
  ofs << "class Gameobject;\n";
  for (unsigned i = 1; i < ComponentNameVec.size(); ++i)
    ofs << "class " << ComponentNameVec[i] << ";\n";


  // rtti namespace
  std::cout << "\nCreating RTTI utilities...\n";
  ofs << "\nnamespace rtti\n{\n";
  ofs << "  template<typename T>\n  inline ComponentEnum DeriveTypeID()\n   {  return ComponentEnum::C_none;  }\n\n";
  for (unsigned i = 1; i < ComponentNameVec.size(); ++i)
  {
    Str tmpStr = ("C_" + ComponentNameVec[i]);
    ofs << "  template<>\n  inline ComponentEnum DeriveTypeID<" << ComponentNameVec[i] << ">()\n   {  return ComponentEnum::" << tmpStr << ";  }\n\n";
  }

  // Functions to print enum to strings
  ofs << "  inline const char* GetComponentEnumString(ComponentEnum val)\n  {\n    switch (val)\n    {\n";
  for (unsigned i = 1; i < ComponentNameVec.size(); ++i)
  {
    Str tmpStr = ("C_" + ComponentNameVec[i]);
    ofs << "    case " << tmpStr << ": return \"" << ComponentNameVec[i] << "\";\n";
  }
  ofs << "    default: return \"[NOT A COMPONENT]\";\n";
  ofs << "    }\n  }\n";

  ofs << "}\n";
  ofs.close();


  /*
    -- Write back to rtti functions
  */
  std::ofstream ofsFunc(argv[3], std::ofstream::trunc);
  if (!ofsFunc.is_open())
  {
    std::cout << "Error : fail to open " << argv[3] << std::endl;
    return -1;
  }
  std::cout << "Now parsing RTTI Functions...\n\n";


  // starting file
  ofsFunc << "#pragma once\n#include \"Gameobject.h\"\n";

  // loop include
  for (unsigned i = 1; i < ComponentNameVec.size(); ++i)
    ofsFunc << "#include \"" << ComponentNameVec[i] << ".h\"\n";

  // rttiFunc namespace
  std::cout << "\nCreating RTTI utility functions...\n";
  ofsFunc << "\nnamespace rttiFnc\n{\n";

  // Add component to gameobject
  ofsFunc << "  template <typename T>\n";
  ofsFunc << "  inline void AddComponent(Gameobject* obj, ComponentEnum enumType, unsigned ID)\n  {\n    switch (enumType)\n    {\n";
  for (unsigned i = 1; i < ComponentNameVec.size(); ++i)
  {
    Str tmpStr = ("C_" + ComponentNameVec[i]);
    ofsFunc << "    case " << tmpStr << ": obj->AddComponent_ID<" << ComponentNameVec[i] << ">(ID); break;\n";
  }
  ofsFunc << "    }\n  }\n\n";

  ofsFunc << "}\n";
  ofsFunc.close();

  std::cout << "\n-- Parsing complete --\n";

  return 1;
}

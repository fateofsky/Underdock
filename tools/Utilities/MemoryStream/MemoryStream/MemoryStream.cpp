#include "MemoryStream.h"
#include <fstream>

#define MEM_STREAM_DEFAULT_ALLOC_SIZE 1024

const char spaceVal = ' ';
const char tlideVal = '~';


MemoryStream::MemoryStream()
  : _bufferIter(nullptr), _streamBeg(nullptr),
    _streamEnd(nullptr), _maxAllocation(MEM_STREAM_DEFAULT_ALLOC_SIZE), _dataSize(0)
{ }

MemoryStream::~MemoryStream()
{
  ClearStream();
}

void MemoryStream::BeginWrite(const unsigned allocSize)
{
#ifdef _DEBUG
  if (_bufferIter != nullptr)
  {
    std::cout << "Error: BeginWrite is already called or MemoryStream is used for reading\n";
    return;
  }
#endif // _DEBUG

  _maxAllocation = allocSize == 0 ? _maxAllocation : allocSize;
  _bufferIter = new char[_maxAllocation];
  _streamBeg = _bufferIter;
  _streamEnd = _bufferIter + _maxAllocation;
}

bool MemoryStream::Flush(const std::string & outputFile)
{
  std::ofstream ofs(outputFile, std::ios::binary | std::ios::trunc);
#ifdef _DEBUG
  if (!ofs.is_open())
  {
    std::cout << "Error: Unable to save into " << outputFile << std::endl;
    return false;
  }
#endif // _DEBUG

  ofs.write(_streamBeg, _bufferIter - _streamBeg);
  ofs.close();
  return true;
}


void MemoryStream::ReallocateMemoryStream(uint64_t newSize)
{
  // double size allocation policy
  while (true)
  {
    _maxAllocation <<= 1;
    if (_maxAllocation >= newSize) break;
  }
  char* tmpBuff = new char[_maxAllocation];
  __int64 offset = _bufferIter - _streamBeg;
  unsigned oldSize = _maxAllocation >> 1;

  // copy old data
  memcpy_s(tmpBuff, oldSize, _streamBeg, oldSize);

  // clean up old memory location
  delete _streamBeg;

  // reassign memory
  _streamBeg = tmpBuff;
  _streamEnd = tmpBuff + _maxAllocation;
  _bufferIter = tmpBuff + offset;
}

bool MemoryStream::FileRead(const std::string & inputFile)
{
  std::ifstream ifs(inputFile, std::ios::binary | std::ios::in | std::ios::ate);
#ifdef _DEBUG
  if (!ifs.is_open())
  {
    ifs.close();
    std::cout << "Error: Unable open file " << inputFile << std::endl;
    return false;
  }
#endif // _DEBUG

  // find out the size
  _dataSize = ifs.tellg();
  ifs.seekg(0, ifs.beg);
  
  // initialize local stream
  _streamBeg = new char[_dataSize];
  _bufferIter = _streamBeg;
  _streamEnd = _streamBeg + _dataSize;
  ifs.read(_streamBeg, _dataSize);

  // clean up
  ifs.close();
  return true;
}

void MemoryStream::Reset()
{
  _bufferIter = _streamBeg;
}

void MemoryStream::ClearStream()
{
  if (_streamBeg != nullptr)
    delete[] _streamBeg;

  _bufferIter = _streamBeg = _streamEnd = nullptr;
}

char* MemoryStream::GetStreamedData(size_t& retDataSize)
{
  retDataSize = _dataSize;
  return _streamBeg;
}

void MemoryStream::Read(void* dst, unsigned size)
{
  #ifdef _DEBUG
  if (_bufferIter == nullptr || _streamEnd == nullptr || _streamBeg == nullptr ||
    _bufferIter + size > _streamEnd)
  {
    std::cout << "Error: Unable to read memory, buffer iterator out of bound or inaccessable memory\n";
    return;
  }
  #endif // _DEBUG

  memcpy_s(dst, size, _bufferIter, size);
  _bufferIter += size;
}


void MemoryStream::Read(void* dst, size_t size)
{
  #ifdef _DEBUG
  if (_bufferIter == nullptr || _streamEnd == nullptr || _streamBeg == nullptr ||
    _bufferIter + size > _streamEnd)
  {
    std::cout << "Error: Unable to read memory, buffer iterator out of bound or inaccessable memory\n";
    return;
  }
  #endif // _DEBUG

  memcpy_s(dst, size, _bufferIter, size);
  _bufferIter += size;
}

// write with determine size
void MemoryStream::Write(const void* data, unsigned size)
{
#ifdef _DEBUG
  if (_bufferIter == nullptr)
  {
    std::cout << "Error: Unable to write memory,  buffer not initialize for writing\n";
    return;
  }
#endif // _DEBUG

  // increase capacity when buffer is full
  if (_bufferIter + size > _streamEnd)
  {
    // compute new size to check
    uint64_t newSize = (_streamEnd - _streamBeg) + size;
    ReallocateMemoryStream(newSize);
  }

  memcpy_s(_bufferIter, size, data, size);
  _bufferIter += size;
}


void MemoryStream::Write(const void* data, size_t size)
{
#ifdef _DEBUG
  if (_bufferIter == nullptr)
  {
    std::cout << "Error: Unable to write memory,  buffer not initialize for writing\n";
    return;
  }
#endif // _DEBUG

  // increase capacity when buffer is full
  if (_bufferIter + size > _streamEnd)
  {
    // compute new size to check
    uint64_t newSize = (_streamEnd - _streamBeg) + size;
    ReallocateMemoryStream(newSize);
  }

  memcpy_s(_bufferIter, size, data, size);
  _bufferIter += size;
}


void MemoryStream::WriteAsString(const char* str)
{
#ifdef _DEBUG
  if (_bufferIter == nullptr)
  {
    std::cout << "Error: Unable to write memory,  buffer not initialize for writing\n";
    return;
  }
#endif // _DEBUG

  size_t _strSize = strlen(str) + 1;
  Write(&_strSize, sizeof(size_t));
  Write(str, _strSize);
}

void MemoryStream::ReadAsString(std::string& str)
{
#ifdef _DEBUG
  if (_bufferIter == nullptr || _streamEnd == nullptr || _streamBeg == nullptr)
  {
    std::cout << "Error: Unable to read as string, buffer iterator out of bound or inaccessable memory\n";
    return;
  }
#endif // _DEBUG

  size_t _strSize = 0;
  Read(&_strSize, sizeof(size_t));

#ifdef _DEBUG
  if (_bufferIter + _strSize > _streamEnd )
  {
    std::cout << "Error: Unable to read as string, access violation\n";
    return;
  }
#endif // _DEBUG

  str = _bufferIter;
  _bufferIter += _strSize;
}

void MemoryStream::ReadAsCStyleText(std::string& str)
{
#ifdef _DEBUG
  if (_bufferIter == nullptr || _streamEnd == nullptr || _streamBeg == nullptr)
  {
    std::cout << "Error: Unable to read as string, buffer iterator out of bound or inaccessable memory\n";
    return;
  }
#endif // _DEBUG

  char* tmpBufferIter = _bufferIter;
  char* tmpStringEndingIter = nullptr;
  //str = _bufferIter;
  //size_t strLength = strlen(_bufferIter);

  // find the breaking point
  int phase = 0;
  while (true)
  {
    if (phase == 0 && 
        _bufferIter != _streamEnd &&
        strcmp(_bufferIter, &spaceVal) >= 0 &&
        strcmp(_bufferIter, &tlideVal) <= 0)
    {
      ++_bufferIter;
    }
    else
    {
      if (phase == 0)
        tmpStringEndingIter = _bufferIter;
      
      // find the next string sequence
      else if (_bufferIter != _streamEnd &&
               strcmp(_bufferIter, &spaceVal) < 0 ||
               strcmp(_bufferIter, &tlideVal) > 0)
      {
        ++_bufferIter;
      }
      else break;
      phase = 1;
    }
  }

  str = std::string(tmpBufferIter, tmpStringEndingIter - tmpBufferIter);
}

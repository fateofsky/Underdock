#pragma once
#include "VulkBuffer.h"
#include "VulkTexImage.h"
#include "VulkPipeline.h"
#include "MaterialComponent.h"
#include "ModelComponent.h"
#include "FileUtilityManager.h"

using RenderableMap = std::map<unsigned, std::vector<Renderables*>>;  // < set number, list of renderables >
using PipelineMap = std::map<unsigned, RenderableMap>;                // < Pipeline Index, set of renderable map >

class GraphicsManager
{
public:
  static GraphicsManager* GetInstance();
  static void Destroy();

  void LoadResources();
  void Init();
  //void CreateMaterialSetData(MaterialComponent* comp);
  bool RegisterMaterial(MaterialComponent* comp, VulkUtils::VulkRenderingTree& renderingTree);
  bool RegisterModel(ModelComponent* comp);

  //void Render();
  const vec4& GetClearColor() const;
  void SetClearColor(const vec4& color);

  VulkBuffer& GetVulkVertexBuffer();
  VulkBuffer& GetVulkIndexBuffer();
  VulkBuffer& GetVulkQuadVertexBuffer();
  VulkBuffer& GetVulkQuadIndexBuffer();

  const VulkPipeline& GetPipeline(const unsigned& index) const;
  VulkPipeline& GetPipeline(const unsigned& index);

  const VulkUtils::_VulkModelInfo& GetModelInfo(const unsigned& index) const;
  VulkUtils::_VulkModelInfo& GetModelInfo(const unsigned& index);

  const VulkRenderTarget& GetRenderTarget(const unsigned& index) const;
  VulkRenderTarget& GetRenderTarget(const unsigned& index);


  void DrawMesh(const VkCommandBuffer& cmdBuf, const unsigned& indexSize, const unsigned& indexOffset, const unsigned& meshOffset);
  void DrawQuad(const VkCommandBuffer& cmdBuf);

private:
  GraphicsManager();
  static GraphicsManager*                 m_instance;

  void _AllocateMainVertexAndIndexBuffer();
  //void _BindMainStaticBuffers();

  std::vector<Renderables*>               m_allRenderables;    // all renderables (for backend compiling)
  VulkBuffer                              m_vBuffer;           // combine static vulkan vertex buffer
  VulkBuffer                              m_iBuffer;           // combine static vulkan index buffer
  VulkBuffer                              m_quadVBuffer;       // quad vertex buffer
  VulkBuffer                              m_quadIBuffer;       // quad index buffer
  std::vector<VulkPipeline>               m_vPipelineList;     // list of vulkan pipeline
  std::vector<VulkTexImage>               m_vTextureList;      // list of vulkan textures
  std::vector<VulkRenderTarget>           m_vRenderTargetList; // list of vulkan render target
  std::vector<VulkUtils::_VulkModelInfo>  m_vModelInfoList;    // list of vulkan model information
  VulkUtils::VulkVBStorage                m_vBufferStorage;    // list of data buffer for static models

  VulkUtils::_VulkModelInfo               m_quadInfo;          // render quad info

  vec4                                    m_clearColor = vec4(.2f, .2f, .2f, 1.f);
};

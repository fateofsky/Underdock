#pragma once
#include "MaterialLayout.h"
#include <vector>
#include <string>

class ShaderBuilder
{
public:
  static ShaderBuilder* GetInstance();
  static void Initialize();
  static void Destroy();

  bool BuildShader_OGL(MaterialLayout* layout, bool debugPrint = false);
  bool ExportPipelineBinary_Vulkan(MaterialLayout* layout, const std::string& saveOutput, bool RawShader = false);
  bool ExportRenderPass_Vulkan(MaterialLayout* layout, const std::string& saveOutput);


private:

  struct SPIR_V_OBJ
  {
    size_t _dataSize;
    char*  _data;
  };

  ShaderBuilder();
  static ShaderBuilder* m_instance;
};
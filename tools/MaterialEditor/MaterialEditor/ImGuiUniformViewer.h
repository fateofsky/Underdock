#pragma once
#include "imgui.h"
#include "MaterialLayout.h"
#include "OGLStringElement.h"

namespace ImguiWin32
{
  extern void RunUniformViewer(bool& open, MaterialLayout* layout);
  extern void ShowUniformEditor(const std::string& name, UniformResourceBase* data);
  extern void RenameUniformData(bool& open, const std::string* resourceName, MaterialLayout* layout);
  extern void ResetResourceNameBuffer();
}
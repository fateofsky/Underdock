  #pragma once

/*
    PREDEFINED MACROS
*/

#define VK_USE_PLATFORM_WIN32_KHR
#define MAX_SCREEN_WIDTH       GetSystemMetrics(SM_CXSCREEN)
#define MAX_SCREEN_HEIGHT      GetSystemMetrics(SM_CYSCREEN)
#define DEFAULT_SCREEN_WIDTH   1280
#define DEFAULT_SCREEN_HEIGHT  768
#define APP_TITLE              "Underdock"
#define APP_VERSION            0
#define VK_APP_MAJOR           1
#define VK_APP_MID             1
#define VK_APP_MINOR           0
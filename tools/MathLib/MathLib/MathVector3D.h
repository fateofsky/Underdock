/* Start Header
*****************************************************************/
/*!
\file      MathVector3D.h
\author    
\par       
\date      
\brief

Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the
prior written consent of DigiPen Institute of Technology is prohibited.
*/
/* End Header
*******************************************************************/
#pragma once
#include "MathLib.h"

union vec2;
union vec4;

///////////
// vec3 //--------------------------------------------------------------
///////////
union vec3
{
	struct
	{
		float x_;
		float y_;
		float z_;
	};

	enum Format
	{
		XYZ, ZYX
	};

	float ar[3];

	vec3(int x, int y, int z);
	vec3(float x = 0.0f, float y = 0.0f, float z = 0.0f);
	vec3(const vec3& v, float z_);
	vec3(const vec3& v, float y_, float z_);
	vec3(const vec2& v, float z = 0.0f);
	vec3(const vec4& v);
	vec3(const std::string& str);

	operator std::string() const;
	vec3& operator+=(const vec3& b);
	vec3& operator-=(const vec3& b);
	vec3& operator*=(const float& s);
	vec3& operator*=(const vec3& b);
	vec3& operator/=(const float& s);
	vec3& operator/=(const vec3& b);
	vec3& operator=(const float rhs[]);
	vec3& operator=(const vec4& rhs);
	vec3& operator++();
	vec3 operator++(int);
	vec3& operator--();
	vec3 operator--(int);
	float& operator[](int i);
	const float& operator[](int i) const;

	// Properties
	float magnitude() const;
	float sqrMagnitude() const;
	vec3 normalized() const;
	float manhattanDistance() const;

	// Method
	void Normalize();
};

 vec3 operator+(const vec3& a, const vec3& b);
 vec3 operator+(const vec3& a);
 vec3 operator-(const vec3& a, const vec3& b);
 vec3 operator-(const vec3& a);
 vec3 operator*(const vec3& a, const float& s);
 vec3 operator*(const float& s, const vec3& a);
 vec3 operator*(const vec3& a, const vec3& b);
 vec3 operator/(const vec3& a, const float& s);
 vec3 operator/(const vec3& a, const vec3& b);
 vec3 operator%(const vec3& a, const float& s);
 std::ostream & operator<<(std::ostream & os, const vec3 & rhs);
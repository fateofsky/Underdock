/* Start Header
*****************************************************************/
/*!
\file      MathVector2D.cpp
\author    
\par       
\date      
\brief

Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the
prior written consent of DigiPen Institute of Technology is prohibited.
*/
/* End Header
*******************************************************************/
#include "stdafx.h"

///////////
// vec2 //--------------------------------------------------------------
///////////
/* -----------------------------*/
/* ----- Member Functions ----- */
/* -----------------------------*/
vec2::vec2(int x_, int y_)
	: x_{ (float)x_ }, y_{ (float)y_ } {}

vec2::vec2(float x_, float y_)
	: x_{ x_ }, y_{ y_ } {}


vec2::vec2(const vec3& v)
	: x_{ v.x_ }, y_{ v.y_ } {}

vec2::vec2(const vec4& v)
	: x_{ v.x_ }, y_{ v.y_ } {}

vec2::vec2(const std::string& str)
{
	size_t comma = str.find(',');
	x_ = std::stof(str);
	if (comma != std::string::npos) 
		y_ = std::atof(&str.front() + comma + 1);
	else y_ = 0;
}

vec2::operator std::string() const
{
	return std::to_string(x_) + ", " + std::to_string(y_);
}

vec2& vec2::operator+=(const vec2& b)
{
	*this = *this + b;
	return *this;
}

vec2& vec2::operator-=(const vec2& b)
{
	*this = *this - b;
	return *this;
}

vec2& vec2::operator*=(const float& s)
{
	*this = *this * s;
	return *this;
}

vec2& vec2::operator*=(const vec2& b)
{
	*this = *this * b;
	return *this;
}

vec2& vec2::operator/=(const float& s)
{
	assert(s != 0.0f);
	*this = *this / s;
	return *this;
}

vec2& vec2::operator/=(const vec2& b)
{
	assert(b.x_ != 0.0f);
	assert(b.y_ != 0.0f);
	*this = *this / b;
	return *this;
}

vec2& vec2::operator=(const float rhs[])
{
	x_ = rhs[0];
	y_ = rhs[1];
	return *this;
}

vec2& vec2::operator++()
{
	++x_;
	++y_;
	return *this;
}

vec2 vec2::operator++(int)
{
	vec2 Temp(x_, y_);
	x_++;
	y_++;
	return Temp;
}

vec2& vec2::operator--()
{
	--x_;
	--y_;
	return *this;
}

vec2 vec2::operator--(int)
{
	vec2 Temp(x_, y_);
	x_--;
	y_--;
	return Temp;
}

float& vec2::operator[](int i)
{
	return ar[i];
}

const float& vec2::operator[](int i) const
{
	return ar[i];
}


float vec2::magnitude() const
{
	return FastSquare(sqrMagnitude());
}

float vec2::sqrMagnitude() const
{
	return (x_ * x_ + y_ * y_);
}

vec2 vec2::normalized() const
{
	vec2 norm;
	vec2Normalize(norm, *this);
	return norm;
}

void vec2::Normalize()
{
	vec2Normalize(*this, *this);
}

/* ---------------------------------*/
/* ----- Non-Member Functions ----- */
/* ---------------------------------*/
vec2 operator+(const vec2& a, const vec2& b)
{
	return vec2(a.x_ + b.x_, a.y_ + b.y_);
}

vec2 operator+(const vec2& a)
{
	return a;
}

vec2 operator-(const vec2& a, const vec2& b)
{
	return vec2(a.x_ - b.x_, a.y_ - b.y_);
}

vec2 operator-(const vec2& a)
{
	return vec2(-a.x_, -a.y_);
}

vec2 operator*(const vec2& a, const float& s)
{
	return vec2(a.x_ * s, a.y_ * s);
}

vec2 operator*(const float& s, const vec2& a)
{
	return a * s;
}

vec2 operator*(const vec2& a, const vec2& b)
{
	vec2 tmp;
	tmp.x_ = a.x_ * b.x_;
	tmp.y_ = a.y_ * b.y_;
	return tmp;
}

vec2 operator/(const vec2& a, const float& s)
{
	assert(s != 0.0f);
	return vec2(a.x_ / s, a.y_ / s);
}

vec2 operator/(const vec2& a, const vec2& b)
{
	assert(b.x_ != 0.0f);
	assert(b.y_ != 0.0f);

	vec2 tmp;
	tmp.x_ = a.x_ / b.x_;
	tmp.y_ = a.y_ / b.y_;
	return tmp;
}

vec2 operator%(const vec2& a, const float& s)
{
	return vec2(fmodf(a.x_, s), fmodf(a.y_, s));
}

//bool operator==(const vec2& a, const vec2& b)
//{
//	return (a.x_ == b.x_ && a.y_ == b.y_);
//}
//
//bool operator!=(const vec2& a, const vec2& b)
//{
//	return !(a == b);
//}

std::ostream & operator<<(std::ostream & os, const vec2 & rhs)
{
	std::cout << "(" << rhs.x_ << "," << rhs.y_ << ")";
	return os;
}
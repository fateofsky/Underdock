#pragma once
#include "GameobjectInclude.h"
#include "MathLib.h"
#include "InputManager.h"
#include "GraphicsManager.h"
#include <vector>

class AppManager
{
public:
  
  enum GAMELOOP_STATE
  {
    LOAD,
    INIT,
    UPDATE
  };

  static AppManager* GetInstance();
  static void Destroy();
  void RegisterEntityForInit(ComponentBase* compEntity);
  void RegisterEntityForUpdate(ComponentBase* compEntity);
  //void RegisterPreRenderEntity(ComponentBase* compEntity);
  void RegisterMainCamera(GameobjectHandle& cameraObj);
  void Run();

  const float& GetAppTime();

private:
  AppManager();
  static AppManager*               m_instance;
  GAMELOOP_STATE                   m_state;
  std::vector<ComponentBase*>      m_entitiesInitVec;
  std::vector<ComponentBase*>      m_entitiesUpdateVec;
  //std::vector<ComponentBase*>      m_entitiesPreRenderVec;
  ComponentHandle<CameraComponent> m_mainCameraHandle;

  float                            m_appTime;
};
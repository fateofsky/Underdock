#pragma once
#include "MemoryStream.h"
#include <vector>

enum _PARAM_TYPE
{
  RAWDATA,
  GAMEOBJECT,
  COMPONENT,
  RAWDATALIST,
  RAWDATACDS
};

class ParameterBase
{
public:
  ParameterBase(_PARAM_TYPE type);

  virtual void Serialize(MemoryStream& ofs) = 0;
  virtual void Deserialize(MemoryStream& ifs) = 0;
  _PARAM_TYPE GetType() const;

protected:
    const _PARAM_TYPE m_type;
};


template <typename T>
class DataParam : public ParameterBase
{
public:
  DataParam()
    : ParameterBase(_PARAM_TYPE::RAWDATA)
  { }

  T& GetData()
  { return _data; }

  const T& GetData() const
  {
    return _data;
  }

  virtual void Serialize(MemoryStream& ofs) override
  {
    //ofs.Write(m_type);
    unsigned szT = sizeof(T);
    ofs.Write(szT);
    ofs.Write(_data);
  }

  virtual void Deserialize(MemoryStream& ifs) override
  {
    unsigned szT;
    ifs.Read(&szT);
    ifs.Read(&_data, szT);
  }

private:
  T _data;
};

template <typename T>
class DataParamList : public ParameterBase
{
public:
  DataParamList()
    : ParameterBase(_PARAM_TYPE::RAWDATALIST)
  { }

  void SetDataSize(size_t size, const T& dataVal)
  {
    _dataList.resize(size, dataVal);
  }

  void SetDataSize(size_t size)
  {
    _dataList.resize(size);
  }

  void AddData(const T& data)
  {
    _dataList.push_back(data);
  }

  T& GetData(const unsigned& index)
  {
    return _dataList[index];
  }

  const T& GetData(const unsigned& index) const
  {
    return _dataList[index];
  }

  const std::vector<T>& GetWholeList() const
  {
    return _dataList;
  }


  virtual void Serialize(MemoryStream& ofs) override
  {
    ofs.Write(_dataList.size());
    const unsigned szT = sizeof(T);
    ofs.Write(szT);
    for(auto& elem : _dataList)
      ofs.Write(elem);
  }

  virtual void Deserialize(MemoryStream& ifs) override
  {
    size_t dataListSize;
    ifs.Read(&dataListSize);
    unsigned typeSize;
    ifs.Read(&typeSize);
    SetDataSize(dataListSize);
    for (size_t dataIndex = 0; dataIndex < dataListSize; ++dataIndex)
      ifs.Read(&_dataList[dataIndex], typeSize);
  }

private:
  std::vector<T> _dataList;
};

/*
  CDS: CUSTOM DATA SERIALIZATION / DESERIALIZATION
  Use: Type T is a user defined class with Serialize and Deserialize function implemented
*/
template <typename T>
class DataParamCDS : public ParameterBase
{
public:
  DataParamCDS()
    : ParameterBase(_PARAM_TYPE::RAWDATACDS)
  { }

  virtual void Serialize(MemoryStream& ofs) override
  {
    _data.Serialize(ofs);
  }
  
  virtual void Deserialize(MemoryStream& ifs) override
  {
    _data.Deserialize(ifs);
  }

  T& GetData()
  {
    return _data;
  }

private:
  T _data;
};


class GameObjectParam : public ParameterBase
{
public:
  GameObjectParam();
  virtual void Serialize(MemoryStream& ofs) override;
  virtual void Deserialize(MemoryStream& ifs) override;
  unsigned& GetID();

private:
  unsigned    m_id;
};


class ComponentParam : public ParameterBase
{
public:
  ComponentParam();
  virtual void Serialize(MemoryStream& ofs) override;
  virtual void Deserialize(MemoryStream& ifs) override;
  unsigned& GetOwner();
  unsigned& GetID();

private:
  unsigned  m_owner;
  unsigned  m_id;
};
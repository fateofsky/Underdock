#include "pch.h"
#include "DemoLevel2.h"

namespace Demo2
{
  GameobjectHandle CameraObj;
  GameobjectHandle textureObj;
  GameobjectHandle LightObj;
  GameobjectHandle SamplerTexture;
  GameobjectHandle treeObjs[9];
  
  GameobjectHandle globalWaterObjSet0;
  GameobjectHandle sharedWaterObjSet1;
  GameobjectHandle waterObj;
  
  AppManager* app = nullptr;
  GraphicsManager* gm = nullptr;
}

VulkUtils::VulkRenderingTree DemoLevel2::_level2RenderingTree_DeferredPass;
VulkUtils::VulkRenderingTree DemoLevel2::_level2RenderingTree_LightPass;
void DemoLevel2::LoadLevel()
{
  GameobjectManager* mgr = GameobjectManager::GetInstance();
  Demo2::app = AppManager::GetInstance();
  Demo2::gm = GraphicsManager::GetInstance();

  Demo2::CameraObj.MoveGameobjectHandleOver(mgr->GetGameobjectHandle(mgr->CreateGameobject()));
  if (Demo2::CameraObj.IsValid())
  {
    auto transform = Demo2::CameraObj.GetData()->AddComponent_hc<Transform>();
    if (transform.IsValid())
    {
      auto transformPtr = transform.GetComponent();
      transformPtr->SetPosition(vec3(3.f, 5.f, 3.f));
      transformPtr->SetScale(vec3(1, 1, 1));
      transformPtr->SetRotation(vec3(0.f, 0.f, 0.f));

      Demo2::app->RegisterEntityForInit(transformPtr);
    }

    auto camComp = Demo2::CameraObj.GetData()->AddComponent_hc<CameraComponent>();
    if (camComp.IsValid())
    {
      auto camCompPtr = camComp.GetComponent();
      camCompPtr->SetNearPlane(0.1f);
      camCompPtr->SetFarPlane(100.f);
      camCompPtr->SetFOV(TORADIAN * 60.f);
      camCompPtr->SetAspectRatio((float)Engine::core::windowsWidth / (float)Engine::core::windowsHeight);
      camCompPtr->SetLookAt(vec3(-3.f, -5.f, -3.f));
      camCompPtr->SetOrthogonal(false);

      Demo2::app->RegisterEntityForInit(camComp.GetComponent());
    }

    auto matComp = Demo2::CameraObj.GetData()->AddComponent_hc<MaterialComponent>();
    if (matComp.IsValid())
    {
      auto matCompPtr = matComp.GetComponent();
      matCompPtr->SetMaterialPipeline(0);
      matCompPtr->SetDescriptorSetNumber(0);
      matCompPtr->SetResourceAccessibilityType(Renderables::_ResourceType::GLOBAL);

      Demo2::gm->RegisterMaterial(matCompPtr, _level2RenderingTree_DeferredPass);
      //RegisterPreRenderEntity(matCompPtr);
      //RegisterEntity(matComp.GetComponent());
    }

    Demo2::app->RegisterMainCamera(Demo2::CameraObj);
  }

  // Test Try to populate shared resources
  Demo2::textureObj.MoveGameobjectHandleOver(mgr->GetGameobjectHandle(mgr->CreateGameobject()));
  if (Demo2::textureObj.IsValid())
  {
    auto matComp = Demo2::textureObj.GetData()->AddComponent_hc<MaterialComponent>();
    if (matComp.IsValid())
    {
      auto matCompPtr = matComp.GetComponent();
      matCompPtr->SetMaterialPipeline(0);
      matCompPtr->SetDescriptorSetNumber(1);
      matCompPtr->SetResourceAccessibilityType(Renderables::_ResourceType::SHARED);
      matCompPtr->SetGlobalResourceTag(0);  // SHARED RESOURCE TYPE REQUIRES BOTH GLOBAL RESOURCE TAG

      matCompPtr->SetTextureListSize(2);
      matCompPtr->PushTextureList(0, 0, 0);
      matCompPtr->PushTextureList(1, 1, 1);
      Demo2::gm->RegisterMaterial(matCompPtr, _level2RenderingTree_DeferredPass);
    }
  }


  // Test: Try to populate tree branches
  const float originX = -2.f;
  const float originZ = -2.f;
  //const float originX = 0.f;
  //const float originZ = 0.f;
  for (int height = 0; height < 3; ++height)
  {
    for (int width = 0; width < 3; ++width)
    {
      float currX = width * 2;
      float currZ = height * 2;
      int whIndex = width + height * 3;
      Demo2::treeObjs[whIndex].MoveGameobjectHandleOver(mgr->GetGameobjectHandle(mgr->CreateGameobject()));
      auto transform = Demo2::treeObjs[whIndex].GetData()->AddComponent_hc<Transform>();
      if (transform.IsValid())
      {
        auto transformPtr = transform.GetComponent();
        transformPtr->SetPosition(vec3(originX + currX, 0.f, originZ + currZ));
        transformPtr->SetScale(vec3(1, 1, 1));
        transformPtr->SetRotation(vec3(0, 90, 0));

        Demo2::app->RegisterEntityForInit(transformPtr);
      }

      auto matComp = Demo2::treeObjs[whIndex].GetData()->AddComponent_hc<MaterialComponent>();
      if (matComp.IsValid())
      {
        auto matCompPtr = matComp.GetComponent();
        matCompPtr->SetMaterialPipeline(0);
        matCompPtr->SetDescriptorSetNumber(2); // set number 2
        matCompPtr->SetResourceAccessibilityType(Renderables::_ResourceType::INSTANCE);
        matCompPtr->SetGlobalResourceTag(0);  // INSTANCE RESOURCE TYPE REQUIRES BOTH GLOBAL RESOURCE TAG & SHARED RESOURCE TAG
        matCompPtr->SetSharedResourceTag(0); // ~

        Demo2::gm->RegisterMaterial(matCompPtr, _level2RenderingTree_DeferredPass);
      }

      auto modelComponent = Demo2::treeObjs[whIndex].GetData()->AddComponent_hc<ModelComponent>();
      if (modelComponent.IsValid())
      {
        modelComponent.GetComponent()->SetModelIndex(0);
        Demo2::gm->RegisterModel(modelComponent.GetComponent());
        //RegisterEntity(modelComponent.GetComponent());
      }
    }
  }


  // water plane
  Demo2::globalWaterObjSet0.MoveGameobjectHandleOver(mgr->GetGameobjectHandle(mgr->CreateGameobject()));
  if (Demo2::globalWaterObjSet0.IsValid())
  {
    auto matComp = Demo2::globalWaterObjSet0.GetData()->AddComponent_hc<MaterialComponent>();
    if (matComp.IsValid())
    {
      auto matCompPtr = matComp.GetComponent();
      matCompPtr->SetMaterialPipeline(1);
      matCompPtr->SetDescriptorSetNumber(0); // set number 0
      matCompPtr->SetResourceAccessibilityType(Renderables::_ResourceType::GLOBAL);

      Demo2::gm->RegisterMaterial(matCompPtr, _level2RenderingTree_DeferredPass);
    }
  }

  Demo2::sharedWaterObjSet1.MoveGameobjectHandleOver(mgr->GetGameobjectHandle(mgr->CreateGameobject()));
  if (Demo2::sharedWaterObjSet1.IsValid())
  {
    auto matComp = Demo2::sharedWaterObjSet1.GetData()->AddComponent_hc<MaterialComponent>();
    if (matComp.IsValid())
    {
      auto matCompPtr = matComp.GetComponent();
      matCompPtr->SetMaterialPipeline(1);
      matCompPtr->SetDescriptorSetNumber(1); // set number 1
      matCompPtr->SetResourceAccessibilityType(Renderables::_ResourceType::SHARED);
      matCompPtr->SetGlobalResourceTag(1);  // INSTANCE RESOURCE TYPE REQUIRES BOTH GLOBAL RESOURCE TAG & SHARED RESOURCE TAG
      matCompPtr->SetTextureListSize(1);
      matCompPtr->PushTextureList(0, 2, 0);

      Demo2::gm->RegisterMaterial(matCompPtr, _level2RenderingTree_DeferredPass);
    }
  }


  Demo2::waterObj.MoveGameobjectHandleOver(mgr->GetGameobjectHandle(mgr->CreateGameobject()));
  if (Demo2::waterObj.IsValid())
  {
    auto transform = Demo2::waterObj.GetData()->AddComponent_hc<Transform>();
    if (transform.IsValid())
    {
      auto transformPtr = transform.GetComponent();
      transformPtr->SetPosition(vec3(.0f, -0.5f, 0.f));
      transformPtr->SetScale(vec3(10, 1, 10));
      transformPtr->SetRotation(vec3(0, 0, 0));

      Demo2::app->RegisterEntityForInit(transformPtr);
    }

    auto matComp = Demo2::waterObj.GetData()->AddComponent_hc<MaterialComponent>();
    if (matComp.IsValid())
    {
      auto matCompPtr = matComp.GetComponent();
      matCompPtr->SetMaterialPipeline(1);
      matCompPtr->SetDescriptorSetNumber(2); // set number 2
      matCompPtr->SetResourceAccessibilityType(Renderables::_ResourceType::INSTANCE);
      matCompPtr->SetGlobalResourceTag(1);  // INSTANCE RESOURCE TYPE REQUIRES BOTH GLOBAL RESOURCE TAG & SHARED RESOURCE TAG
      matCompPtr->SetSharedResourceTag(0); // ~

      Demo2::gm->RegisterMaterial(matCompPtr, _level2RenderingTree_DeferredPass);
    }

    auto modelComponent = Demo2::waterObj.GetData()->AddComponent_hc<ModelComponent>();
    if (modelComponent.IsValid())
    {
      modelComponent.GetComponent()->SetModelIndex(2);
      Demo2::gm->RegisterModel(modelComponent.GetComponent());
    }
  }


  // G-buffer texture
  Demo2::SamplerTexture.MoveGameobjectHandleOver(mgr->GetGameobjectHandle(mgr->CreateGameobject()));
  if (Demo2::SamplerTexture.IsValid())
  {
    auto matComp = Demo2::SamplerTexture.GetData()->AddComponent_hc<MaterialComponent>();
    if (matComp.IsValid())
    {
      auto matCompPtr = matComp.GetComponent();
      matCompPtr->SetMaterialPipeline(2);
      matCompPtr->SetDescriptorSetNumber(0);
      matCompPtr->SetResourceAccessibilityType(Renderables::_ResourceType::GLOBAL);

      // set attachment sampler from render pass/target 0 output
      matCompPtr->SetAttachmentListSize(2);
      matCompPtr->PushAttachmentTextureList(0, 1, 0, 0);
      matCompPtr->PushAttachmentTextureList(1, 2, 1, 0);

      Demo2::gm->RegisterMaterial(matCompPtr, _level2RenderingTree_LightPass);
    }
  }

  // Create light object
  Demo2::LightObj.MoveGameobjectHandleOver(mgr->GetGameobjectHandle(mgr->CreateGameobject()));
  if (Demo2::LightObj.IsValid())
  {
    auto transform = Demo2::LightObj.GetData()->AddComponent_hc<Transform>();
    if (transform.IsValid())
    {
      auto transformPtr = transform.GetComponent();
      transformPtr->SetPosition(vec3(5.f, 5.f, 5.f));
      transformPtr->SetScale(vec3(1, 1, 1));
      transformPtr->SetRotation(vec3(0.f, 0.f, 0.f));

      Demo2::app->RegisterEntityForInit(transformPtr);
    }

    auto camComp = Demo2::LightObj.GetData()->AddComponent_hc<CameraComponent>();
    if (camComp.IsValid())
    {
      auto camCompPtr = camComp.GetComponent();
      camCompPtr->SetNearPlane(0.1f);
      camCompPtr->SetFarPlane(100.f);
      camCompPtr->SetFOV(TORADIAN * 60.f);
      camCompPtr->SetAspectRatio((float)Engine::core::windowsWidth / (float)Engine::core::windowsHeight);
      camCompPtr->SetLookAt(vec3(-5.f, -5.f, -5.f));
      camCompPtr->SetOrthogonal(true);

      Demo2::app->RegisterEntityForInit(camComp.GetComponent());
    }

    auto matComp = Demo2::LightObj.GetData()->AddComponent_hc<MaterialComponent>();
    if (matComp.IsValid())
    {
      auto matCompPtr = matComp.GetComponent();
      matCompPtr->SetMaterialPipeline(2);
      matCompPtr->SetDescriptorSetNumber(1);
      matCompPtr->SetResourceAccessibilityType(Renderables::_ResourceType::SHARED);
      Demo2::gm->RegisterMaterial(matCompPtr, _level2RenderingTree_LightPass);
    }
  }
}

void DemoLevel2::InitLevel()
{
}

void DemoLevel2::UpdateLevel()
{
  mat4 vpMtx = Demo2::CameraObj.GetData()->GetFirstOfComponentHandle<CameraComponent>().GetComponent()->GetViewProjMatrix_Transpose();
  Demo2::CameraObj.GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&vpMtx, sizeof(mat4), 0, 0, Engine::swpch::vCurFrame);
  for (int i = 0; i < 9; ++i)
  {
    const mat4 trans = Demo2::treeObjs[i].GetData()->GetFirstOfComponentHandle<Transform>().GetComponent()->GetTransformMtx_Transpose();
    const mat4 rotation = Demo2::treeObjs[i].GetData()->GetFirstOfComponentHandle<Transform>().GetComponent()->GetRotationMtx_Transpose();
    const vec4 colorBump(1.3f, 1.25f, 1.f, .35f);
    Demo2::treeObjs[i].GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&rotation, sizeof(mat4), 0, 0, Engine::swpch::vCurFrame);
    Demo2::treeObjs[i].GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&trans, sizeof(mat4), 0, sizeof(mat4), Engine::swpch::vCurFrame);
    Demo2::treeObjs[i].GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&colorBump, sizeof(vec4), 1, 0, Engine::swpch::vCurFrame);
  }

  // set 0
  float warmedTime = Demo2::app->GetAppTime() + 10.f;
  Demo2::globalWaterObjSet0.GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&warmedTime, sizeof(float), 0, 0, Engine::swpch::vCurFrame);
  Demo2::globalWaterObjSet0.GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&vpMtx, sizeof(mat4), 0, sizeof(vec4), Engine::swpch::vCurFrame);

  // set2
  const mat4 trans = Demo2::waterObj.GetData()->GetFirstOfComponentHandle<Transform>().GetComponent()->GetTransformMtx_Transpose();
  const mat4 rotationMtx = Demo2::waterObj.GetData()->GetFirstOfComponentHandle<Transform>().GetComponent()->GetRotationMtx_Transpose();
  const vec2 waveSpeedClamp(1.f, .1f);
  const vec4 colorBumpDamping(1.f, 1.1f, 1.05f, .25f);
  Demo2::waterObj.GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&rotationMtx, sizeof(mat4), 0, 0, Engine::swpch::vCurFrame);
  Demo2::waterObj.GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&trans, sizeof(mat4), 0, sizeof(mat4), Engine::swpch::vCurFrame);
  Demo2::waterObj.GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&waveSpeedClamp, sizeof(vec2), 0, sizeof(mat4) << 1, Engine::swpch::vCurFrame);
  Demo2::waterObj.GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&colorBumpDamping, sizeof(vec4), 1, 0, Engine::swpch::vCurFrame);


  // Light variable
  vec4 lightColor(1.5f, 1.5f, 1.5f, 0.f);
  vec4 lightDir(-1.f, -1.f, -1.f, 0.f);
  mat4 vMtx = Demo2::LightObj.GetData()->GetFirstOfComponentHandle<CameraComponent>().GetComponent()->GetViewMatrix_Transpose();
  Demo2::LightObj.GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&lightColor, sizeof(vec4), 0,                 0, Engine::swpch::vCurFrame);
  Demo2::LightObj.GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&lightDir,   sizeof(vec4), 0,      sizeof(vec4), Engine::swpch::vCurFrame);
  //Demo2::LightObj.GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&vMtx,     sizeof(mat4), 0, sizeof(vec4) << 1, Engine::swpch::vCurFrame);

}

void DemoLevel2::RenderLevel()
{
  const vec4& clearColor = Demo2::gm->GetClearColor();
  const VulkBuffer& vBuffer = Demo2::gm->GetVulkVertexBuffer();
  const VulkBuffer& iBuffer = Demo2::gm->GetVulkIndexBuffer();
  const VulkBuffer& quadVBuffer = Demo2::gm->GetVulkQuadVertexBuffer();
  const VulkBuffer& quadIBuffer = Demo2::gm->GetVulkQuadIndexBuffer();
  VulkRenderTarget& rt = Demo2::gm->GetRenderTarget(0);
  
  Engine::core::SwapChainImageAcquire();
  Engine::core::StartRecord();
  
  // Pass 1
  rt.BeginRenderTargetPass();
  
  // glviewport + buffer
  Engine::core::SetViewportScissor(Engine::swpch::vCurFrame);
  const static VkDeviceSize v_Offsets[1] = { 0 };
  vkCmdBindVertexBuffers(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame], 0, 1, vBuffer.GetBufferPtr(), v_Offsets);
  vkCmdBindIndexBuffer(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame], iBuffer.GetBuffer(), 0, VK_INDEX_TYPE_UINT32);

  for (auto& globalNode : _level2RenderingTree_DeferredPass._globalNodes)
  {
    VulkPipeline& pipeline = Demo2::gm->GetPipeline(globalNode._globalRenderable->m_matPipelineIndex);
    pipeline.BindPipeline();
    pipeline.BindDescriptorSet(globalNode._globalRenderable->m_descSetNumber, globalNode._globalRenderable->m_descInstanceNumber);

    // for each shared resources (set 1)
    for (auto& shrResource : globalNode._sharedNodes)
    {
      pipeline.BindDescriptorSet(shrResource._sharedRenderable->m_descSetNumber, shrResource._sharedRenderable->m_descInstanceNumber);

      // for each instances
      for (auto& instance : shrResource._instanceNodes)
      {
        // bind and draw if model exist
        pipeline.BindDescriptorSet(instance._renderable->m_descSetNumber, instance._renderable->m_descInstanceNumber);
        if (instance._renderable->m_modelIndex != (std::numeric_limits<unsigned>::max)())
        {
          auto& modelInfo = Demo2::gm->GetModelInfo(instance._renderable->m_modelIndex);
          for (auto& mesh : modelInfo._meshInfoList)
            Demo2::gm->DrawMesh(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame], mesh._indexSize, mesh._indicesOffset, mesh._meshOffset);
        }
      }
    }

  }
  rt.EndRenderTargetPass();

  // Implicit synchronization using subpass dependencies

  // main pass
  Engine::core::BeginMainRenderPass(Engine::swpch::vRenderpass, Engine::swpch::vFBVec[Engine::swpch::vCurFrame], &clearColor.x_);
  
  // reset viewport and rebind buffers
  Engine::core::SetViewportScissor(Engine::swpch::vCurFrame);
  vkCmdBindVertexBuffers(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame], 0, 1, quadVBuffer.GetBufferPtr(), v_Offsets);
  vkCmdBindIndexBuffer(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame], quadIBuffer.GetBuffer(), 0, VK_INDEX_TYPE_UINT32);

  for (auto& globalNode : _level2RenderingTree_LightPass._globalNodes)
  {
    // bind attachment sampler
    VulkPipeline& pipeline = Demo2::gm->GetPipeline(globalNode._globalRenderable->m_matPipelineIndex);
    pipeline.BindPipeline();
    pipeline.BindDescriptorSet(globalNode._globalRenderable->m_descSetNumber, globalNode._globalRenderable->m_descInstanceNumber);

    // bind lighting properties
    for (auto& shrResource : globalNode._sharedNodes)
    {
      pipeline.BindDescriptorSet(shrResource._sharedRenderable->m_descSetNumber, shrResource._sharedRenderable->m_descInstanceNumber);
      Demo2::gm->DrawQuad(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame]);
    }
    //Demo2::gm->DrawQuad(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame]);
  }
  Engine::core::EndMainRenderPass();


  Engine::core::EndRecord();

  // submit
  Engine::core::SwapChainImageSubmitAndPresent();
}

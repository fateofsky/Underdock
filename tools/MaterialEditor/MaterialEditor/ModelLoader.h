#pragma once
#include <string>
#include <fStream>
#include "Model.h"
#include "CommonVBLayout.h"
#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"
#include "OGLBaseType.h"

namespace ModelUtils
{
  // utilities
  bool ImportModel(const std::string& file, Model* model, bool normalizeVertex = false);
  std::vector<vPack> PackData(const Model& mod, const vBufferLayout& bLayout);
  void FlattenMesh(const NodeHierarchy& node, std::vector<const Mesh*>& result);
  unsigned ComputePerVertexPackSize(const vBufferLayout& layout);
  unsigned ComputeElementOffset(OGL_BASETYPE type);

  // serialize Model

}
#include "pch.h"
#include "Shader.h"
#include <algorithm>

/*
  Debug output
*/
char infoLog[1024]{ 0 };
bool DebugShaderProcess(GLuint shader, int type, const char* note)
{
  GLint testShader = 0;
  if (type == GL_COMPILE_STATUS)
  {
    glGetShaderInfoLog(shader, 1024, NULL, infoLog);
    glGetShaderiv(shader, GL_COMPILE_STATUS, &testShader);
  }
  else if (type == GL_LINK_STATUS)
  {
    GLsizei len;
    glGetProgramInfoLog(shader, 1024, &len, infoLog);
    glGetProgramiv(shader, GL_LINK_STATUS, &testShader);
  }

  if (!testShader) {
    std::cout << note << std::endl;
    std::cout << infoLog << std::endl;
    return false;
  }

  return true;
}

Shader::Shader() : m_sdrObjVec(), m_sdrPrg(0)
{ }

void Shader::Destroy()
{
  for (auto& sdrObj : m_sdrObjVec)
    glDeleteShader(sdrObj.sdrObj);
  DeleteProgram();
}

void Shader::CopyFrom(const Shader& sdr)
{
  m_sdrObjVec = sdr.m_sdrObjVec;
  m_sdrPrg = sdr.m_sdrPrg;
}

bool Shader::LoadShaderFile(const std::string& filePath)
{
  std::ifstream ifs(filePath, std::ios::binary | std::ios::ate);
  if (!ifs.good()) {
    std::cout << "Error: Failed to load shader file: " << filePath << std::endl;
    return false;
  }

  std::string fileP = filePath;
  size_t extPos = fileP.find_last_of('.');
  if (extPos == std::string::npos)
  {
    std::cout << "Error: Unknown file extension type\n";
    return false;
  }
  std::string extString = fileP.substr(extPos + 1);

  // determine shader type
  SDROBJ_TYPE_PAIR pair;
  if (extString == "vert")
  {
    pair.sdrObj = glCreateShader(GL_VERTEX_SHADER);
    pair.sdrType = ShaderType::VERTEXSDR;
  }
  else if (extString == "tessctrl")
  {
    pair.sdrObj = glCreateShader(GL_TESS_CONTROL_SHADER);
    pair.sdrType = ShaderType::TESSCTRLSDR;
  }
  else if (extString == "tesseval")
  {
    pair.sdrObj = glCreateShader(GL_TESS_EVALUATION_SHADER);
    pair.sdrType = ShaderType::TESSEVALSDR;
  }
  else if (extString == "geom")
  {
    pair.sdrObj = glCreateShader(GL_GEOMETRY_SHADER);
    pair.sdrType = ShaderType::GEOMSDR;
  }
  else if (extString == "frag")
  {
    pair.sdrObj = glCreateShader(GL_FRAGMENT_SHADER);
    pair.sdrType = ShaderType::FRAGSDR;
  }
  else if (extString == "comp")
  {
    pair.sdrObj = glCreateShader(GL_COMPUTE_SHADER);
    pair.sdrType = ShaderType::COMPUTESDR;
  }
  else
  {
    std::cout << "Error: Unknown shader type\n";
    return false;
  }

  size_t size = ifs.tellg();
  char* data = new char[size + 1];
  ifs.seekg(ifs.beg);
  ifs.read(data, size);
  data[size] = '\0'; // important

  // try compile shader
  glShaderSource(pair.sdrObj, 1, &data, NULL);
  glCompileShader(pair.sdrObj);
  if (!DebugShaderProcess(pair.sdrObj, GL_COMPILE_STATUS, (fileP + ": \n").c_str()))
  {
    glDeleteShader(pair.sdrObj);
    return false;
  }

  m_sdrObjVec.push_back(pair);
  return true;
}


bool Shader::CreateShader(const char* shaderSrc, ShaderType type)
{
  // Early rejection
  for (auto& elem : m_sdrObjVec)
  {
    if (elem.sdrType == type)
    {
      std::cout << "Error: Shader type " << type << " already created\n";
      return false;
    }
  }

  SDROBJ_TYPE_PAIR pair;
  pair.sdrType = type;

  switch (type)
  {
  case Shader::VERTEXSDR:
    pair.sdrObj = glCreateShader(GL_VERTEX_SHADER);
    break;

  case Shader::TESSCTRLSDR:
    pair.sdrObj = glCreateShader(GL_TESS_CONTROL_SHADER);
    break;

  case Shader::TESSEVALSDR:
    pair.sdrObj = glCreateShader(GL_TESS_EVALUATION_SHADER);
    break;

  case Shader::GEOMSDR:
    pair.sdrObj = glCreateShader(GL_GEOMETRY_SHADER);
    break;

  case Shader::FRAGSDR:
    pair.sdrObj = glCreateShader(GL_FRAGMENT_SHADER);
    break;

  case Shader::COMPUTESDR:
    pair.sdrObj = glCreateShader(GL_COMPUTE_SHADER);
    break;

  }

  glShaderSource(pair.sdrObj, 1, &shaderSrc, NULL);
  glCompileShader(pair.sdrObj);
  if (!DebugShaderProcess(pair.sdrObj, GL_COMPILE_STATUS, "ShaderBuild"))
  {
    glDeleteShader(pair.sdrObj);
    return false;
  }

  m_sdrObjVec.push_back(pair);
  return true;
}

void Shader::DeleteShader(ShaderType type)
{
  m_sdrObjVec.erase(std::remove_if(m_sdrObjVec.begin(), m_sdrObjVec.end(), [&type](const SDROBJ_TYPE_PAIR& elemType)->bool
  {
    return elemType.sdrType == type;
  }));
}


bool Shader::CreateProgram()
{
  m_sdrPrg = glCreateProgram();

  for (auto& elem : m_sdrObjVec)
    glAttachShader(m_sdrPrg, elem.sdrObj);

  glLinkProgram(m_sdrPrg);
  if (!DebugShaderProcess(m_sdrPrg, GL_LINK_STATUS, "Link: \n"))
  {
    for (auto& elem : m_sdrObjVec)
      glDetachShader(m_sdrPrg, elem.sdrObj);
    glDeleteProgram(m_sdrPrg);
    return false;
  }

  return true;
}

void Shader::DeleteProgram()
{
  glDeleteProgram(m_sdrPrg);
}

void Shader::UseProgram()
{
  glUseProgram(m_sdrPrg);
}

GLuint Shader::GetProgram()
{
  return m_sdrPrg;
}

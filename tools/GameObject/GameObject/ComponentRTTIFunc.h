#pragma once
#include "Gameobject.h"
#include "CameraComponent.h"
#include "MaterialComponent.h"
#include "ModelComponent.h"
#include "Transform.h"

namespace rttiFnc
{
  template <typename T>
  inline void AddComponent(Gameobject* obj, ComponentEnum enumType, unsigned ID)
  {
    switch (enumType)
    {
    case C_CameraComponent: obj->AddComponent_ID<CameraComponent>(ID); break;
    case C_MaterialComponent: obj->AddComponent_ID<MaterialComponent>(ID); break;
    case C_ModelComponent: obj->AddComponent_ID<ModelComponent>(ID); break;
    case C_Transform: obj->AddComponent_ID<Transform>(ID); break;
    }
  }

}

#define PARSECOMP
#undef PARSECOMP
#pragma once
#include "ComponentBase.h"
#include "MaterialComponent.h"
#include "../../MathLib/MathLib/MathLib.h"

class ComponentBase;

class Transform : public ComponentBase, public _SINGLE
{
  friend class Gameobject;

  public:
    Transform(Gameobject* owner, const unsigned& _id);
    virtual ~Transform();
    virtual void Init() override;
    virtual void PostInit() override;
    virtual void Update() override;
    virtual void PreRender() override;
    virtual void PostUpdate() override;

    const vec3& GetPosition() const;
    const vec3& GetScale() const;
    const vec3& GetRotation() const;

    void SetPosition(const vec3& pos);
    void SetScale(const vec3& scale);
    void SetRotation(const vec3& rotate);

    const mat4& GetTransformMtx() const;
    mat4 GetTransformMtx_Transpose() const;

    const mat4& GetTranslationMtx() const;
    mat4 GetTranslationMtx_Transpose() const;

    const mat4& GetRotationMtx() const;
    mat4 GetRotationMtx_Transpose() const;

    const mat4& GetScaleMtx() const;
    mat4 GetScaleMtx_Transpose() const;

    bool GetRequiredUpdate() const;

  private:
    DataParam<vec3> m_position;
    DataParam<vec3> m_scale;
    DataParam<vec3> m_rotation;
    mat4            m_transformation;
    mat4            m_translationMtx;
    mat4            m_rotationMtx;
    mat4            m_scaleMtx;

    ComponentHandle<MaterialComponent> m_matHandle;
    bool _update = false;
};

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file

#ifndef PCH_H
#define PCH_H

#define _CRT_SECURE_NO_WARNINGS
#define MAX_SCREEN_WIDTH         GetSystemMetrics(SM_CXSCREEN)
#define MAX_SCREEN_HEIGHT        GetSystemMetrics(SM_CYSCREEN)
#define DEFAULT_SCREEN_WIDTH     1280
#define DEFAULT_SCREEN_HEIGHT    768
#define DEFAULT_SCREEN_WIDTH_F   1280.f
#define DEFAULT_SCREEN_HEIGHT_F  768.f
#define DEFAULT_DT               0.0166666f
#define APP_TITLE              "Material Editor"

// TODO: add headers that you want to pre-compile here
#include <Windows.h>
#include <iostream>
#include <fstream>
#include <string>

#endif //PCH_H

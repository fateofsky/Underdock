#include "pch.h"
#include "GameobjectManager.h"
#include "MemoryStream.h"

GameobjectManager* GameobjectManager::m_instance = nullptr;
GameobjectManager::GameobjectManager()
  : m_gameObjectMap()
{ }

Gameobject* GameobjectManager::GetGameobject(const unsigned& objID)
{
  return m_gameObjectMap.find(objID) != m_gameObjectMap.end() ? m_gameObjectMap[objID] : nullptr;
}


GameobjectManager* GameobjectManager::GetInstance()
{
  return m_instance == nullptr ? m_instance = new GameobjectManager() : m_instance;
}

GameobjectHandle GameobjectManager::GetGameobjectHandle(const unsigned & objID)
{
  char* flag = EntityTracker::GetInstance()->GetGameobjectFlag(objID);
  return GameobjectHandle(flag, objID);
}

unsigned GameobjectManager::CreateGameobject()
{
  unsigned objId = EntityTracker::GetInstance()->GenerateID();
  m_gameObjectMap.insert(std::pair<unsigned, Gameobject*>(objId, new Gameobject(objId)));
  return objId;
}

void GameobjectManager::CreateGameobject(unsigned ObjID)
{
#ifdef _DEBUG
  unsigned currID = EntityTracker::GetInstance()->GetCurrentID();
  bool idCheck = ObjID < currID || !EntityTracker::GetInstance()->IsUnuseID(ObjID);
  if (idCheck)
  {
    std::cout << "Error: Unable to create gameobject, possible duplicate of Gameobject ID\n";
    assert(false);
  }
  else
#endif // _DEBUG
    m_gameObjectMap.insert(std::pair<unsigned, Gameobject*>(ObjID, new Gameobject(ObjID)));
}

void GameobjectManager::DestroyGameobject(const unsigned & objID)
{
  EntityTracker* etc = EntityTracker::GetInstance();
  etc->SetGameobjectInvalid(objID);
  if (m_gameObjectMap.find(objID) != m_gameObjectMap.end())
    m_gameObjectMap.erase(objID);
}

void GameobjectManager::DestroyGameobject(const GameobjectHandle & objHC)
{
  EntityTracker* etc = EntityTracker::GetInstance();
  unsigned objID = objHC.GetID();
  etc->SetGameobjectInvalid(objID);
  if (m_gameObjectMap.find(objID) != m_gameObjectMap.end())
    m_gameObjectMap.erase(objID);
}

void GameobjectManager::DestroyAllGameobject()
{
  EntityTracker* etc = EntityTracker::GetInstance();
  for (auto& elem : m_gameObjectMap)
    etc->SetGameobjectInvalid(elem.first);
  m_gameObjectMap.clear();
}

void GameobjectManager::Serialize(const std::string & file)
{
  MemoryStream memStr;
  memStr.BeginWrite();

  memStr.Write(m_gameObjectMap.size());                            // Number of gameobjects
  memStr.Write(EntityTracker::GetInstance()->GetCurrentID());      // Max ID

  for (auto& go : m_gameObjectMap)
  {
    // serialize ID + number of component
    memStr.Write(go.first);                                        // Gameobject ID
    memStr.Write(go.second->m_multiComponentMap.size());           // Number of unique components

    for (auto& comp : go.second->m_multiComponentMap)
    {
      // serialize component type and number of that type
      memStr.Write(comp.first);                                    // Component type
      memStr.Write(comp.second.size());                            // Number of components

      for (auto& realComp : comp.second)
      {
        // serialize component ID + number of parameters
        memStr.Write(realComp.first);                              // Component ID
        memStr.Write(realComp.second->m_paramCollection.size());   // Number of serializable data parameters

        for (auto& paramData : realComp.second->m_paramCollection)
        {
          memStr.Write(paramData.second->GetType());               // type of data
          memStr.Write(paramData.first);                           // Uniform location
          paramData.second->Serialize(memStr);                     // raw data
        }
      }
    }
  }

  memStr.Flush(file);
}

void GameobjectManager::Deserialize(const std::string & file)
{
  MemoryStream memLd;
  memLd.FileRead(file);

  size_t gameObjSize;
  unsigned maxEntityID;
  memLd.Read(&gameObjSize);                               // Read gameobject size
  Gameobject* newSerlObj = new Gameobject[gameObjSize];
  memLd.Read(&maxEntityID);                               // Read max entity ID
  EntityTracker::GetInstance()->SetMaxID(0);              // init for deserialization

  for (unsigned i = 0; i < gameObjSize; ++i)
  {
    unsigned goID;
    memLd.Read(&goID);                                    // Read gameobject ID
    newSerlObj[i].SetHashId(goID);

    size_t multiCompSize;
    memLd.Read(&multiCompSize);                           // Read number of unique component

    for (size_t j = 0; j < multiCompSize; ++j)
    {
      ComponentEnum cE;
      memLd.Read(&cE);                                    // Read component enum type
      size_t compSize;
      memLd.Read(&compSize);                              // Read number of components

      for (size_t k = 0; k < compSize; ++k)
      {
        unsigned compID;
        memLd.Read(&compID);                              // Read component ID
        rttiFnc::AddComponent<Gameobject>(&newSerlObj[i], cE, compID);

        // Add component here so the parameters are setted up
        ComponentBase* addedCompBase = newSerlObj[i].GetComponentBase(cE, compID);
        assert(addedCompBase != nullptr);
        
        size_t compParamSize;
        memLd.Read(&compParamSize);                       // Read number of serializable data parameters

        for (size_t l = 0; l < compParamSize; ++l)
        {
          _PARAM_TYPE pType;
          memLd.Read(&pType);                            // Read types of data
          unsigned char uniLoc;
          memLd.Read(&uniLoc);                           // Read uniform location
          
          ParameterBase* paramB = addedCompBase->GetParamBase(uniLoc);
          paramB->Deserialize(memLd);

          /*
            ** EXTREMELY VOLATILE
          */
          //switch (pType)
          //{
          //case RAWDATA:
          //{
          //  DataParam<char>* tmpDataParamPtr = reinterpret_cast<DataParam<char>*>(paramB);
          //  void* _data = &tmpDataParamPtr->GetData();
          //  unsigned szT;
          //  memLd.Read(&szT);
          //  memLd.Read(_data, szT);
          //} break;

          //case GAMEOBJECT:
          //{
          //  GameObjectParam* tmpGoParamPtr = reinterpret_cast<GameObjectParam*>(paramB);
          //  void* _data = &tmpGoParamPtr->GetID();
          //  memLd.Read(_data, sizeof(unsigned));
          //} break;

          //case COMPONENT:
          //{
          //  ComponentParam* tmpCompParamPtr = reinterpret_cast<ComponentParam*>(paramB);
          //  void* _data = &tmpCompParamPtr->GetOwner();
          //  memLd.Read(_data, sizeof(unsigned) << 1);
          //} break;

          //case RAWDATALIST:
          //{
          //  DataParamList<char>* tmpDataListParamPtr = reinterpret_cast<DataParamList<char>*>(paramB);

          //  size_t dataListSize;
          //  memLd.Read(&dataListSize);
          //  unsigned typeSize;
          //  memLd.Read(&typeSize);
          //  tmpDataListParamPtr->SetDataSize(dataListSize, 0);
          //  for (size_t dataIndex = 0; dataIndex < dataListSize; ++dataIndex)
          //  {
          //    void* _data = &tmpDataListParamPtr->GetData((unsigned)dataIndex);
          //    memLd.Read(_data, typeSize);
          //  }

          //} break;

          //default: assert(false); break;
          //}

        }
      }
    }
  }

  // post initialize maxEntityID
  EntityTracker::GetInstance()->SetMaxID(maxEntityID);
}



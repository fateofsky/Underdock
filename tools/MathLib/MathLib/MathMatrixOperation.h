/* Start Header
*****************************************************************/
/*!
\file      MathMatrixOperation.h
\author
\par
\date
\brief

Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the
prior written consent of DigiPen Institute of Technology is prohibited.
*/
/* End Header
*******************************************************************/
#pragma once
#include "MathLib.h"

////////////////
// MatrixUtil //--------------------------------------------------------------
////////////////
mat3  mat3Zero();
mat3  mat3Identity();
void  mat3Translate(mat3& out, const vec2& trans);
void  mat3Translate(mat3& out, float x, float y);
void  mat3Scale(mat3& out, const vec2& scale);
void  mat3Scale(mat3& out, float x, float y);
void  mat3RotRad(mat3& out, float angle);
void  mat3RotDeg(mat3& out, float angle);
void  mat3Transpose(mat3& out, const mat3& mtx);
void  mat3Inverse(mat3& out, const mat3& mtx, float* determinant);

mat4  mat4Zero();
mat4  mat4Identity();
void  mat4Translate(mat4& out, const vec3& trans);
void  mat4Translate(mat4& out, float x, float y, float z);
void  mat4Scale(mat4& out, const vec3& scale);
void  mat4Scale(mat4& out, float x, float y, float z);
void  mat4RotXRad(mat4& out, float angle);
void  mat4RotXDeg(mat4& out, float angle);
void  mat4RotYRad(mat4& out, float angle);
void  mat4RotYDeg(mat4& out, float angle);
void  mat4RotZRad(mat4& out, float angle);
void  mat4RotZDeg(mat4& out, float angle);
void  mat4RotRad(mat4& out, float xAngle, float yAngle, float zAngle);
void  mat4RotDeg(mat4& out, float xAngle, float yAngle, float zAngle);
void  mat4Transpose(mat4& out, const mat4& mtx);
void  mat4Inverse(mat4& out, const mat4& mtx, float* determinant);

//Changes angle domain from -180->180 to 0->360
void  correctAngleDomains(vec3& rotation);
//Prevents axis flip caused by decomposing rotation matrix back to angles,
//does so by comparing angles to angles from old euler angles values
void  orientRotationToOldRotation(vec3& rotation, const vec3& oldRotation);
void  mtxToTransformations(const mat4& in, vec3* rotation, vec3* scale, vec3* translation, vec3* oldRotation = nullptr);
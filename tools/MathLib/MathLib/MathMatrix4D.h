/* Start Header
*****************************************************************/
/*!
\file      MathMatrix4D.h
\author    
\par       
\date      
\brief

Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the
prior written consent of DigiPen Institute of Technology is prohibited.
*/
/* End Header
*******************************************************************/
#pragma once
#include "MathLib.h"
#include <xmmintrin.h>

#define mat4_LEN 16
#define mat4_ROW 4

union mat3;

union mat4
{
	struct
	{
		float m00, m01, m02, m03;
		float m10, m11, m12, m13;
		float m20, m21, m22, m23;
		float m30, m31, m32, m33;
	};
	__m128 m128[mat4_ROW];
	float m_44[mat4_LEN];
	float m2_44[mat4_ROW][mat4_ROW];

	mat4();
	mat4(const float * pArr);
	mat4(float _00, float _01, float _02, float _03,
		  float _10, float _11, float _12, float _13,
		  float _20, float _21, float _22, float _23,
		  float _30, float _31, float _32, float _33);
	mat4(const mat3& mat3);
	mat4(const std::string& str);

	operator std::string() const;
	mat4& operator=(const mat3 &rhs);
	mat4& operator=(const mat4& rhs);
	mat4& operator*=(const mat4& rhs);
	mat4& operator+=(const mat4& rhs);
	mat4& operator-=(const mat4& rhs);
	mat4& operator=(const float* arr44);
	float& operator[](int i);
	float operator[](int i) const;

	// Properties
	mat4 inversed() const;
	mat4 transposed() const;
	const float * get() const;

	// Methods
	void Identity();
	void Zero();
	void Inverse();
	void Transpose();
};

// Matrix scaling
mat4 operator*(const mat4& lhs, const float& rhs);
mat4 operator*(const mat4 &lhs, const mat4 &rhs);
mat4 operator+(const mat4 &lhs, const mat4 &rhs);
mat4 operator-(const mat4 &lhs, const mat4 &rhs);
vec3 operator*(const mat4 &pMtx, const vec3& vec3);
vec4 operator*(const mat4 &pMtx, const vec4& vec4);
bool operator==(const mat4 &lhs, const mat4 &rhs);
bool operator!=(const mat4 &lhs, const mat4 &rhs);
std::ostream& operator<<(std::ostream& os, const mat4& mat4);
#define PARSECOMP
#undef PARSECOMP
#pragma once
#include "ComponentBase.h"
#include "MaterialComponent.h"
#include "../../MathLib/MathLib/MathLib.h"

class ComponentBase;

class CameraComponent : public ComponentBase
{
  friend class Gameobject;

public:
  CameraComponent(Gameobject* owner, const unsigned& _id);
  virtual ~CameraComponent();
  virtual void Init() override;
  virtual void PostInit() override;
  virtual void Update() override;
  virtual void PreRender() override;
  virtual void PostUpdate() override;

  void SetOrthogonal(bool state);
  bool IsOrthogonal() const;


  // Utilities / setup
  void SetFOV(const float& FOV);     // IN RADIAN
  const float& GetFOV() const;       // ~
  void SetNearPlane(const float& nearPlane);
  const float& GetNearPlane() const;
  void SetFarPlane(const float& farPlane);
  const float& GetFarPlane() const;
  void SetAspectRatio(const float& aspectR);
  const float& GetAspectRatio() const;

  void SetLookAt(const vec3& direction);
  const vec3& GetLookAt() const;

  const mat4& GetViewMatrix() const;
  const mat4& GetProjMatrix() const;
  const mat4& GetViewProjMatrix() const;

  mat4 GetViewMatrix_Transpose() const;
  mat4 GetProjMatrix_Transpose() const;
  mat4 GetViewProjMatrix_Transpose() const;

  bool GetRequiredUpdate() const;

  private:
    void _ComputeMatrices();

    mat4 m_viewMat;
    mat4 m_projMat;
    mat4 m_viewProjMat;

    DataParam<float> m_near;
    DataParam<float> m_far;
    DataParam<float> m_fov;
    DataParam<float> m_aspect;
    DataParam<vec3>  m_lookAt;
    DataParam<bool>  m_orthorgonal;

    ComponentHandle<MaterialComponent> m_matHandle;
    ComponentHandle<Transform>         m_transformHandle;
    bool _update = false;
};

#pragma once
#include "Model.h"
#include "VertexBuffer.h"
#include "PipelineLayout.h"


struct MaterialLayout
{
  using BindMap = std::map<std::string, GLint>;
  void Destroy();
  void AttachShader(const std::string& name);
  void AttachTexture(const std::string& name, Shader::ShaderType pipelineStage);
  void AttachModel(const std::string& name);
  void AttachIndices(const std::vector<unsigned>& indexList);
  void AttachVertexBuffer(const std::string& name);
  void SetLayoutName(const std::string& name);
  void InsertVBufferLayout(vBufferLayout::vBufferLayoutType type);
  void RemoveVBufferLayout(vBufferLayout::vBufferLayoutType type);

  int  AddUniform(const std::string& name, OGL_BASETYPE baseType, 
                  UNIFORM_USAGE usage, Shader::ShaderType pipelineStage, unsigned group);

  void RemoveUniform(const std::string& name);
  void RemoveTexture(const std::string& name);

  void DetachShader();
  void DetachModel();

  void BindAll();
  void UnbindAll();
  void Render();

  // Expensive (only use for deserialization)
  UniformResourceBase* GetUniform(const std::string& name);

  // direct resources reference
  Shader*             m_shader;
  Model*              m_model;

  PipelineLayout      m_resourceLayout; // info on vertex input goes here
  vBufferLayout       m_vBufferLayout;  // info on actual vertex buffer layout

  // misc tool
  std::string         m_layoutName;
  std::string         m_shaderName;
  std::string         m_modelName;

  // internal
  std::string        m_saveDataLocation;

  // indirect resources (no serialize)
  const std::vector<VertexBuffer>* m_vbList;
  std::vector<unsigned>            m_drawIndexList;
  BindMap                          m_bindUniformMap;

};
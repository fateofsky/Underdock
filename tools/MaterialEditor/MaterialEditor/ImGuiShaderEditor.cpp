#include "pch.h"
#include "ImGuiShaderEditor.h"

// Local variables
std::string codeBase;


void ImguiWin32::RunShaderEditor(bool& open, MaterialLayout* layout, Shader::ShaderType stage, bool& firstTime)
{
  const int imguiWinFlag =
    ImGuiWindowFlags_::ImGuiWindowFlags_NoResize |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
  ImGui::SetNextWindowSize(ImVec2(600, 580));

  if (ImGui::Begin("                                    Shader Editor", &open, imguiWinFlag))
  {
    ImGui::Text(oglStrElem::uniformStageItems[stage].name);
    ImGui::Separator();
    ImGui::Spacing();
    ImGui::Spacing();

    // Assign the shader code into the editor
    PipelineLayout::pipelineStageMap& stageMap = layout->m_resourceLayout.GetPipelineStageMap();
    if (firstTime)
    {
      codeBase = stageMap[stage].shaderMainCode;
      firstTime = false;
    }

    // Edit Shader
    ImGui::InputTextMultiline("##ShaderEdit", &codeBase, ImVec2(550, 450));

    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();

    if (ImGui::Button("Save##ShaderEditSave", ImVec2(70, 30)))
    {
      stageMap[stage].shaderMainCode = codeBase;
      open = false;
    }
    ImGui::SameLine();
    if (ImGui::Button("Cancel##ShaderEditCancel", ImVec2(70, 30)))
      open = false;

    ImGui::End();
  }
}

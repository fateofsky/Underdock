#pragma once
#include <string>
#include <iostream>

class MemoryStream
{
public:
  MemoryStream();
  ~MemoryStream();

  // non copyable
  MemoryStream(const MemoryStream&) = delete;
  MemoryStream(const MemoryStream&&) = delete;

  // file writing
  void BeginWrite(const unsigned allocSize = 0); // MUST CALL WRITING / FLUSHING
  bool Flush(const std::string& outputFile);

  // file reading
  bool FileRead(const std::string& inputFile);

  // clear / destruction
  void Reset();
  void ClearStream();

  /*
    Utilities
  */
  char* GetStreamedData(size_t& retDataSize);    // read the whole file
  void Read(void* dst, unsigned size);           // read with determine size
  void Read(void* dst, size_t size);             // read with determine size (size_t is 8 bytes on x64 OS)
  void ReadAsString(std::string& str);           // specially reading strings
  void ReadAsCStyleText(std::string& str);       // read as C style strings (oeprator >> or getline)


  template <typename T>                          // memory read pointer
  void Read(T* dst)
  {
    unsigned size = sizeof(T);

#ifdef _DEBUG
    if (_bufferIter == nullptr || _streamEnd == nullptr || _streamBeg == nullptr || 
        _bufferIter + size > _streamEnd)
    {
      std::cout << "Error: Unable to read memory, buffer iterator out of bound or inaccessable memory\n";
      return;
    }
#endif // _DEBUG

    memcpy_s(dst, size, _bufferIter, size);
    _bufferIter += size;
  }


  void Write(const void* data, unsigned size);   // write with determine size
  void Write(const void* data, size_t size);     // write with determine size (size_t is 8 bytes on x64 OS)
  void WriteAsString(const char* str);           // specially writing strings

  template <typename T>                          // memory write pointer
  void Write(T* data)
  {
    unsigned size = sizeof(T);

#ifdef _DEBUG
    if (_bufferIter == nullptr)
    {
      std::cout << "Error: Unable to write memory,  buffer not initialize for writing\n";
      return;
    }
#endif // _DEBUG

    // increase capacity when buffer is full
    if (_bufferIter + size > _streamEnd)
    {
      // compute new size to check
      uint64_t newSize = (_streamEnd - _streamBeg) + size;
      ReallocateMemoryStream(newSize);
    }

    memcpy_s(_bufferIter, size, data, size);
    _bufferIter += size;
  }

  template <typename T>                          // memory write reference
  void Write(const T& data)
  {
    unsigned size = sizeof(T);

#ifdef _DEBUG
    if (_bufferIter == nullptr)
    {
      std::cout << "Error: Unable to write memory,  buffer not initialize for writing\n";
      return;
    }
#endif // _DEBUG

    // increase capacity when buffer is full
    if (_bufferIter + size > _streamEnd)
    {
      // compute new size to check
      uint64_t newSize = (_streamEnd - _streamBeg) + size;
      ReallocateMemoryStream(newSize);
    }

    memcpy_s(_bufferIter, size, &data, size);
    _bufferIter += size;
  }


private:
  void ReallocateMemoryStream(uint64_t newSizeToCheck);

  char*           _bufferIter;
  char*           _streamBeg;
  char*           _streamEnd;
  unsigned        _maxAllocation;
  size_t          _dataSize;
};
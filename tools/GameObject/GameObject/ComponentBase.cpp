#include "pch.h"
#include "ComponentBase.h"

ComponentBase::ComponentBase(Gameobject * owner, const unsigned& _id)
  : Entity(_id), m_owner(owner) {}

void ComponentBase::InsertParam(unsigned char locId, ParameterBase * paramB)
{
#ifdef _DEBUG
  if (m_paramCollection.find(locId) != m_paramCollection.end())
  {
    std::cout << "Error: parameter location " << (int)locId << " is already being used!\n";
    assert(false);
  }
#endif // _DEBUG

  m_paramCollection[locId] = paramB;
}

ComponentBase::~ComponentBase()
{ }

Gameobject* ComponentBase::GetOwner()
{
  return m_owner;
}

ParameterBase* ComponentBase::GetParamBase(unsigned char uniLoc)
{
#ifdef _DEBUG
  if (m_paramCollection.find(uniLoc) == m_paramCollection.end())
  {
    std::cout << "Error: Unable to find uniform location " << (int)uniLoc << std::endl;
    return nullptr;
  }
#endif // _DEBUG

  return m_paramCollection[uniLoc];
}


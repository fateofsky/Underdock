#include "pch.h"
#include "ResourceManager.h"
#include "glm/gtc/matrix_transform.hpp"
#include "ModelLoader.h"

/*
  FILE UTILITIES
*/
std::string ConvertSlash(const std::string& input)
{
  std::string output = input;
  std::replace(output.begin(), output.end(), '\\', '/');
  return output;
}

/////////////////////////////////////////////////////////////////
ResourceManager* ResourceManager::m_instance = nullptr;
ResourceManager::ResourceManager()
{ }

void ResourceManager::Initialize()
{
  m_instance = new ResourceManager();
  m_instance->ChangeCameraPosition(glm::vec3(0, 0, 5.0f)); // default
  m_instance->m_objectTransformation = glm::mat4(1.f);
}

void ResourceManager::Destroy()
{
  for (auto& elem : m_instance->m_vBufferMap)  // destroy vertex buffer
    for(auto& buf : elem.second)
      buf.Destroy();

  for (auto& elem : m_instance->m_textureMap)  // destroy texture
    elem.second.DestroyTexture();

  for (auto& elem : m_instance->m_vShaderMap)  // destroy shader program
    elem.second.Destroy();

  delete m_instance;
}

ResourceManager * ResourceManager::GetInstance()
{
  return m_instance;
}

void ResourceManager::ChangeCameraPosition(const glm::vec3 & pos)
{
  m_camPosition = pos;
  m_scheduledRefresh = true;
}

void ResourceManager::ChangeFOV(float degree)
{
  m_fov = glm::radians(degree);
  m_scheduledRefresh = true;
}

void ResourceManager::ChangeAspect(float asp)
{
  m_aspect = asp;
  m_scheduledRefresh = true;
}

void ResourceManager::ChangeNear(float nearPlane)
{
  m_near = nearPlane;
  m_scheduledRefresh = true;
}

void ResourceManager::ChangeFar(float farPlane)
{
  m_far = farPlane;
  m_scheduledRefresh = true;
}


void ResourceManager::UpdateMatrix()
{
  if (m_scheduledRefresh)
  {
    // OpenGL convention
    m_camView = glm::lookAt(
      m_camPosition,          // camera is at (x, x, x), in world space
      glm::vec3(0, 0, 0),     // and looks at the origin
      glm::vec3(0, 1, 0)      // head is up (set to 0,-1, 0 to look upside-down)
    );

    m_camProj = glm::perspective(glm::radians(m_fov), m_aspect, m_near, m_far);
    m_camViewProj = m_camProj * m_camView;
    m_scheduledRefresh = false;
  }
}

const std::vector<vPack>* ResourceManager::GetVertexBufferData(const std::string & name)
{
  return m_BufferPackMap.find(name) != m_BufferPackMap.end() ? &m_BufferPackMap[name] : nullptr;
}

const std::vector<VertexBuffer>* ResourceManager::GetVertexBuffer(const std::string& name, const unsigned& index)
{
  return m_vBufferMap.find(name) != m_vBufferMap.end() ? &m_vBufferMap[name] : nullptr;
}

Texture* ResourceManager::GetTexture(const std::string& name)
{
  return m_textureMap.find(name) != m_textureMap.end() ? &m_textureMap[name] : nullptr;
}

Shader* ResourceManager::GetShader(const std::string& name)
{
  return m_vShaderMap.find(name) != m_vShaderMap.end() ? &m_vShaderMap[name] : nullptr;
}

Model* ResourceManager::GetModel(const std::string& name)
{
  return m_modelMap.find(name) != m_modelMap.end() ? &m_modelMap[name] : nullptr;
}

float* ResourceManager::GetViewProjMtx()
{
  return &m_camViewProj[0][0];
}

float* ResourceManager::GetViewMtx()
{
  return &m_camView[0][0];
}

float* ResourceManager::GetProjMtx()
{
  return &m_camProj[0][0];
}

float* ResourceManager::GetObjTransformMtx()
{
  return &m_objectTransformation[0][0];
}

glm::mat4& ResourceManager::GetViewMtx_Full()
{
  return m_camView;
}

glm::mat4& ResourceManager::GetProjMtx_Full()
{
  return m_camProj;
}

glm::mat4& ResourceManager::GetVP_Full()
{
  return m_camViewProj;
}

glm::mat4& ResourceManager::GetObjTransformMtx_Full()
{
  return m_objectTransformation;
}

std::vector<unsigned> ResourceManager::CreateVertexBuffer(const std::string& modelName, const std::string& bufferName, const vBufferLayout& bufferLayout)
{
  if (m_modelMap.find(modelName) == m_modelMap.end())
  {
    std::cout << "Error: Unable to find model named " << modelName << " to create the vertex buffer\n";
    return std::vector<unsigned>();
  }
  else if (m_vBufferMap.find(bufferName) != m_vBufferMap.end())
  {
    for(auto& buf : m_vBufferMap[bufferName])
      buf.Destroy();
    m_vBufferMap[bufferName].clear();
  }

  // create vertex buffer and pack
  std::vector<vPack> vBufPack = ModelUtils::PackData(m_modelMap[modelName], bufferLayout);
  std::vector<unsigned> meshIndices;
  for (size_t i = 0; i < vBufPack.size(); ++i)
  {
    VertexBuffer vBuf;
    vBuf.Create(vBufPack[i]._Data.size(),    &vBufPack[i]._Data[0],
                vBufPack[i]._Indices.size(), &vBufPack[i]._Indices[0], bufferLayout);
    m_vBufferMap[bufferName].push_back(vBuf);
    meshIndices.push_back((unsigned)(vBufPack[i]._Indices.size() / sizeof(unsigned)));
  }
  m_BufferPackMap[bufferName] = vBufPack;

  return meshIndices;
}

std::vector<unsigned> ResourceManager::ComputeVertexBufferIndexList(const std::string& modelName, const std::string& bufferName)
{
  if (m_modelMap.find(modelName) == m_modelMap.end())
  {
    std::cout << "Error: Unable to find model named " << modelName << " to create the vertex buffer\n";
    return std::vector<unsigned>();
  }

  // Recompute buffer pack
  std::vector<unsigned> meshIndices;
  std::vector<vPack>& vBufferPack = m_BufferPackMap[bufferName];
  for (size_t i = 0; i < vBufferPack.size(); ++i)
    meshIndices.push_back((unsigned)(vBufferPack[i]._Indices.size() / sizeof(unsigned)));

  return meshIndices;
}

bool ResourceManager::LoadModel(const std::string& file, bool normalizeVertex)
{
  // convert every back slash to forward slash
  std::string newFile = ConvertSlash(file);
 
  Model model;
  bool _loadModel = ModelUtils::ImportModel(newFile, &model, normalizeVertex);

  // POLICY: COMPLETE REPLACEMENT OF MODEL IF MODEL EXISTED
  if (_loadModel)
  {
    std::string fName = newFile.substr(newFile.find_last_of('/') + 1);
    m_modelMap[fName] = model;
  }
  
  return _loadModel;
}

bool ResourceManager::LoadTexture(const std::string& file)
{
  // convert every back slash to forward slash
  std::string newFile = ConvertSlash(file);
  std::string fName = newFile.substr(newFile.find_last_of('/') + 1);

  // clean up old texture if new on loaded successfully
  bool toDestroy = m_textureMap.find(fName) != m_textureMap.end();
  Texture tDestroy = toDestroy ? m_textureMap[fName] : Texture();

  Texture texture;
  bool _loadTex = texture.CreateTexture(newFile);
  if (_loadTex)
  {
    m_textureMap[fName] = texture;
    if (toDestroy) tDestroy.DestroyTexture();
  }

  return _loadTex;
}

bool ResourceManager::LoadTexture(const std::string & file, std::string & textureName)
{
  // convert every back slash to forward slash
  std::string newFile = ConvertSlash(file);
  std::string fName = newFile.substr(newFile.find_last_of('/') + 1);

  // clean up old texture if new on loaded successfully
  bool toDestroy = m_textureMap.find(fName) != m_textureMap.end();
  Texture tDestroy = toDestroy ? m_textureMap[fName] : Texture();

  Texture texture;
  bool _loadTex = texture.CreateTexture(newFile);
  if (_loadTex)
  {
    m_textureMap[fName] = texture;
    if (toDestroy) tDestroy.DestroyTexture();
    textureName = fName;
  }

  return _loadTex;
}

bool ResourceManager::LoadShader(const std::string& file, const std::string& sdrName)
{
  bool _loadShader;
  if (m_vShaderMap.find(sdrName) != m_vShaderMap.end())
    _loadShader = m_vShaderMap[sdrName].LoadShaderFile(file);
  else
  {
    Shader loadedShader;
    _loadShader = loadedShader.LoadShaderFile(file);
    if(_loadShader)
      m_vShaderMap[sdrName] = loadedShader;
  }

  return _loadShader;
}

bool ResourceManager::CreateEmptyShader(const std::string& sdrName)
{
  if (m_vShaderMap.find(sdrName) != m_vShaderMap.end()) return false;
  m_vShaderMap[sdrName];
  return true;
}

bool ResourceManager::ReplaceShaderContent(const std::string& sdrName, const Shader& newSdr)
{
  if (m_vShaderMap.find(sdrName) == m_vShaderMap.end()) return false;
  m_vShaderMap[sdrName].CopyFrom(newSdr);
  return true;
}

bool ResourceManager::CreateShaderProgram(const std::string& sdrName)
{
  if (m_vShaderMap.find(sdrName) != m_vShaderMap.end())
    return m_vShaderMap[sdrName].CreateProgram();

  std::cout << "Error: Shader name " << sdrName << " cannot be found and link\n";
  return false;
}

std::map<std::string, Texture>& ResourceManager::GetAllTextures()
{
  return m_textureMap;
}

std::map<std::string, Model>& ResourceManager::GetAllModel()
{
  return m_modelMap;
}

#pragma once
#include "GameobjectInclude.h"
#include "MathLib.h"
#include "Engine.h"
#include "AppManager.h"

class DemoLevel
{
public:
  static void LoadLevel();
  static void InitLevel();
  static void UpdateLevel();
  static void RenderLevel();

private:
  static VulkUtils::VulkRenderingTree _levelRenderingTree;
};
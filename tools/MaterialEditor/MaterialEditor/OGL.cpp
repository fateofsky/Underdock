#include "pch.h"
#include "OGL.h"

void ogl::InitContext(HDC& hdc, HGLRC& hrc)
{
  // create opengl context
  HGLRC tempContext = wglCreateContext(hdc);
  wglMakeCurrent(hdc, tempContext);

  int attribs[] =
  {
  WGL_CONTEXT_MAJOR_VERSION_ARB, 4,
  WGL_CONTEXT_MINOR_VERSION_ARB, 5,
  WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_DEBUG_BIT_ARB,               // I suggest using debug context in order to know whats really happening and easily catch bugs
  WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB, // nProfile = WGL_CONTEXT_CORE_PROFILE_BIT_ARB or WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB
  0
  };
  PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB = NULL;
  wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress("wglCreateContextAttribsARB");
  if (wglCreateContextAttribsARB != NULL)
  {
    hrc = wglCreateContextAttribsARB(hdc, 0, attribs);
    wglMakeCurrent(NULL, NULL);
    wglDeleteContext(tempContext);
    wglMakeCurrent(hdc, hrc);
  }


  GLenum error{ glewInit() };
  if (error != GLEW_OK) {
    MessageBox(NULL, "GLEW init Fail!", "Error", NULL);
    throw;
  }

  // Get OpenGL version
  int glV[2]{ -1, -1 };
  glGetIntegerv(GL_MAJOR_VERSION, &glV[0]);
  glGetIntegerv(GL_MINOR_VERSION, &glV[1]);

  // Debug print out
  std::cout << "OpenGL v" << glV[0] << "." << glV[1] << "\n";
  std::cout << "System GPU" << "\n";
  std::cout << " -> Graphic card    : " << (char *)glGetString(GL_VENDOR) << "\n";
  std::cout << " -> Model           : " << (char *)glGetString(GL_RENDERER) << "\n" << "\n";
}


VertexBuffer ogl::CreateVertexBuffer(void* vertexDataPack, GLuint vSize, void* indicesData,
                                     GLuint iSize, const vBufferLayout& layout, GLuint elemSi)
{
  VertexBuffer vBuf;

  // create element and vertex buffer
  glGenVertexArrays(1, &vBuf.m_vao);
  glGenBuffers(1, &vBuf.m_vbo);
  glGenBuffers(1, &vBuf.m_ebo);

  // bind buffers
  glBindVertexArray(vBuf.m_vao);
  glBindBuffer(GL_ARRAY_BUFFER, vBuf.m_vbo);
  glBufferData(GL_ARRAY_BUFFER, vSize, vertexDataPack, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vBuf.m_ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, iSize, indicesData, GL_STATIC_DRAW);

  unsigned indexCount = 0;
  GLuint64 offSetCount = 0;
  for (auto& elem : layout.layout)
  {
    glEnableVertexAttribArray(indexCount);

    switch (elem)
    {
    case vBufferLayout::VERTEX_POS:
    case vBufferLayout::VERTEX_NORMAL:
    case vBufferLayout::VERTEX_TANGENT:
    case vBufferLayout::VERTEX_BITANGENT:
      glVertexAttribPointer(indexCount, 3, GL_FLOAT, GL_FALSE, elemSi, (GLvoid*)offSetCount);
      offSetCount += 3 * sizeof(float);
      break;

    case vBufferLayout::VERTEX_BONE_ID:
      glVertexAttribPointer(indexCount, 4, GL_INT, GL_FALSE, elemSi, (GLvoid*)offSetCount);
      offSetCount += 4 * sizeof(unsigned);
      break;

    case vBufferLayout::VERTEX_BONE_WEIGHT:
      glVertexAttribPointer(indexCount, 4, GL_FLOAT, GL_FALSE, elemSi, (GLvoid*)offSetCount);
      offSetCount += 4 * sizeof(float);
      break;

    case vBufferLayout::TEX_COORDUV0:
    case vBufferLayout::TEX_COORDUV1:
    case vBufferLayout::TEX_COORDUV2:
    case vBufferLayout::TEX_COORDUV3:
      glVertexAttribPointer(indexCount, 2, GL_FLOAT, GL_FALSE, elemSi, (GLvoid*)offSetCount);
      offSetCount += 2 * sizeof(float);
      break;
    }

    ++indexCount;
  }

  glBindVertexArray(0);

  return vBuf;
}

void ogl::DeleteVertexBuffer(const VertexBuffer & vb)
{
  glDeleteBuffers(1, &vb.m_vbo);
  glDeleteBuffers(1, &vb.m_ebo);
  glDeleteVertexArrays(1, &vb.m_vao);
}



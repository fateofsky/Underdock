#include "../pch.h"
#include "VulkPipeline.h"
#include "Engine.h"

VulkPipeline::VulkPipeline()
  : m_hasRenderTarget(false),
    m_vUniformBuffer(),
    m_setPool(),
    m_pipelineLayout(),
    m_pipeline(),
    m_renderTargetPtr(nullptr)
{ }

void VulkPipeline::LoadPipeline_1(const std::string & file, VulkRenderTarget* vrt)
{
  MemoryStream inFile;
  if (!inFile.FileRead(file)) return;

  m_renderTargetPtr = vrt;

  // Initialize default shader stage info
  VkPipelineShaderStageCreateInfo sdrStageInfo;
  std::vector<VkPipelineShaderStageCreateInfo> sdrStageInfoList;
  sdrStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  sdrStageInfo.pNext = NULL;
  sdrStageInfo.pSpecializationInfo = NULL;
  sdrStageInfo.flags = 0;
  sdrStageInfo.pName = "main";

  // READ SHADER (SPIR-V)s
  size_t shaderStageSize = 0;
  inFile.Read(&shaderStageSize);
  for (size_t i = 0; i < shaderStageSize; ++i)
  {
    int stage;
    inFile.Read(&stage);
    VulkUtils::ShaderStage sdrStage = (VulkUtils::ShaderStage)stage;
    VkShaderModuleCreateInfo& sdrInfo = m_shaderMap[sdrStage]._vkSdrInfo;
    sdrInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    sdrInfo.pNext = NULL;
    sdrInfo.flags = 0;

    inFile.Read(&sdrInfo.codeSize);
    char* tmpData = new char[sdrInfo.codeSize];
    inFile.Read(tmpData, sdrInfo.codeSize);
    sdrInfo.pCode = reinterpret_cast<uint32_t*>(tmpData);

#ifdef _DEBUG
    VkResult vkRes = vkCreateShaderModule(Engine::core::vDevice, &sdrInfo, NULL, &m_shaderMap[sdrStage]._vkSdrModule);
    assert(vkRes == VK_SUCCESS);
#else
    vkCreateShaderModule(Engine::core::vDevice, &sdrInfo, NULL, &m_shaderMap[sdrStage]._vkSdrModule);
#endif // _DEBUG

    delete[] tmpData;
    sdrStageInfo.stage = VulkUtils::DetermineShaderStage(sdrStage);
    sdrStageInfo.module = m_shaderMap[sdrStage]._vkSdrModule;
    sdrStageInfoList.push_back(sdrStageInfo);
  }

  // READ DESCRIPTOR SETS
  size_t descSetLayoutCount;
  inFile.Read(&descSetLayoutCount);
  for (size_t i = 0; i < descSetLayoutCount; ++i)
  {
    unsigned groupSet;
    inFile.Read(&groupSet);
    size_t uniqueDescriptorCount;
    inFile.Read(&uniqueDescriptorCount);

    for (size_t j = 0; j < uniqueDescriptorCount; ++j)
    {
      VulkUtils::VulkInterfaceBlockInfo descStr;
      inFile.Read(&descStr._vkGroupBinding);
      inFile.Read(&descStr._vkBlockSize);
      descStr._vkBlockStride = 0;

      int stage;
      inFile.Read(&stage);
      descStr._vkStage = VulkUtils::ShaderStage(stage);

      size_t layoutTypeListSize;
      inFile.Read(&layoutTypeListSize);
      descStr._vkBaseTypeLayout.resize(layoutTypeListSize);
      for (size_t k = 0; k < layoutTypeListSize; ++k)
      {
        int readType;
        inFile.Read(&readType);
        descStr._vkBaseTypeLayout[k] = VulkUtils::VK_BASE_TYPE(readType);
      }

      m_descSetMap[groupSet]._setInterfaceBlocks.push_back(descStr);
    }
  }

  // Vertex input state
  size_t inputVertSize;
  inFile.Read(&inputVertSize);
  if (m_shaderMap.find(VulkUtils::ShaderStage::VERTEX) != m_shaderMap.end())
  {
    for (size_t i = 0; i < inputVertSize; ++i)
    {
      int inputType;
      inFile.Read(&inputType);
      m_inputBaseTypeList.push_back(VulkUtils::VK_BASE_TYPE(inputType));
    }
  }

  VkPipelineDynamicStateCreateInfo       dyInfo;  // generated on reading file
  VkPipelineVertexInputStateCreateInfo   viInfo;  // generated on reading file
  VkPipelineInputAssemblyStateCreateInfo inputAssemblyState;
  VkPipelineRasterizationStateCreateInfo rasterState;
  VkPipelineColorBlendStateCreateInfo    colorBlendState;
  VkPipelineDepthStencilStateCreateInfo  depthStencilState;
  VkPipelineViewportStateCreateInfo      vpInfo;  // generated on reading file
  VkPipelineMultisampleStateCreateInfo   msInfo;  // ~

  // Read vkDynamicState items
  std::vector<VkDynamicState> tmpDStateVec;
  size_t dStateVecSize;
  inFile.Read(&dStateVecSize);
  tmpDStateVec.resize(dStateVecSize);
  for (size_t i = 0; i < dStateVecSize; ++i)
    inFile.Read(&tmpDStateVec[i]);
  dyInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
  dyInfo.pNext = NULL;
  dyInfo.dynamicStateCount = (uint32_t)dStateVecSize;
  dyInfo.pDynamicStates = tmpDStateVec.data();
  dyInfo.flags = 0;


  // Read VkVertexInputBindingDescription && VkVertexInputAttributeDescription
  VkVertexInputBindingDescription viBinding;
  std::vector<VkVertexInputAttributeDescription> tmpAttDescList;
  size_t attDescListSize;
  inFile.Read(&viBinding);        // VkVertexInputBindingDescription
  inFile.Read(&attDescListSize);  // VkVertexInputAttributeDescription
  tmpAttDescList.resize(attDescListSize);
  for (size_t i = 0; i < attDescListSize; ++i)
    inFile.Read(&tmpAttDescList[i]);
  viInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
  viInfo.pNext = NULL;
  viInfo.flags = 0;
  viInfo.vertexBindingDescriptionCount = 1;
  viInfo.pVertexBindingDescriptions = &viBinding;
  viInfo.vertexAttributeDescriptionCount = (uint32_t)attDescListSize;
  viInfo.pVertexAttributeDescriptions = tmpAttDescList.data();


  // read VkPipelineInputAssemblyStateCreateInfo
  inFile.Read(&inputAssemblyState);


  // read VkPipelineRasterizationStateCreateInfo
  inFile.Read(&rasterState);


  // read VkPipelineColorBlendAttachmentState & assign VkPipelineColorBlendStateCreateInfo
  std::vector<VkPipelineColorBlendAttachmentState> cbAttStateList;
  size_t cbAttStateSize;
  inFile.Read(&cbAttStateSize);
  cbAttStateList.resize(cbAttStateSize);
  for (size_t i = 0; i < cbAttStateSize; ++i)
    inFile.Read(&cbAttStateList[i]);
  inFile.Read(&colorBlendState);
  colorBlendState.attachmentCount = (uint32_t)cbAttStateSize;
  colorBlendState.pAttachments = cbAttStateList.data();


  // read VkPipelineDepthStencilStateCreateInfo
  inFile.Read(&depthStencilState);


  // read viewport state (default)
  inFile.Read(&vpInfo);


  // read multi sample state (default)
  inFile.Read(&msInfo);


  // CREATE LAYOUT
  std::vector<VkDescriptorSetLayout> tmpLayoutVec;
  for (auto& set : m_descSetMap)
  {
    std::vector<VkDescriptorSetLayoutBinding> dslBindings;
    for (auto& block : set.second._setInterfaceBlocks)
    {
      VkDescriptorSetLayoutBinding layoutBindings;
      VkDescriptorType dType =
        block._vkBaseTypeLayout.size() == 0 && block._vkBlockSize == 0 ?
        VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER :
        VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
      layoutBindings.binding = block._vkGroupBinding;
      layoutBindings.descriptorCount = 1;
      layoutBindings.descriptorType = dType;
      layoutBindings.pImmutableSamplers = NULL;
      layoutBindings.stageFlags = VulkUtils::DetermineShaderStage(block._vkStage);
      dslBindings.push_back(layoutBindings);
    }

    // Descriptor set layout create info
    VkDescriptorSetLayoutCreateInfo descripLayoutInfo;
    descripLayoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descripLayoutInfo.bindingCount = (uint32_t)dslBindings.size();
    descripLayoutInfo.pBindings = dslBindings.data();
    descripLayoutInfo.pNext = NULL;
    descripLayoutInfo.flags = 0;
#ifdef _DEBUG
    VkResult vkRes = vkCreateDescriptorSetLayout(Engine::core::vDevice, &descripLayoutInfo, NULL, &set.second._vkDSLayout);
    assert(vkRes == VK_SUCCESS);
#else
    vkCreateDescriptorSetLayout(Engine::core::vDevice, &descripLayoutInfo, NULL, &set.second._vkDSLayout);
#endif // _DEBUG

    // compile layout
    tmpLayoutVec.push_back(set.second._vkDSLayout);
  }

  // create pipeline layout
  VkPipelineLayoutCreateInfo pPipeLayoutInfo;
  pPipeLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
  pPipeLayoutInfo.pNext = NULL;
  pPipeLayoutInfo.pushConstantRangeCount = 0;
  pPipeLayoutInfo.pPushConstantRanges = NULL;
  pPipeLayoutInfo.setLayoutCount = (uint32_t)tmpLayoutVec.size();
  pPipeLayoutInfo.pSetLayouts = tmpLayoutVec.data();
  pPipeLayoutInfo.flags = 0;
#ifdef _DEBUG
  VkResult vkRes = vkCreatePipelineLayout(Engine::core::vDevice, &pPipeLayoutInfo, NULL, &m_pipelineLayout);
  assert(vkRes == VK_SUCCESS);
#else
  vkCreatePipelineLayout(Engine::core::vDevice, &pPipeLayoutInfo, NULL, &m_pipelineLayout);
#endif // _DEBUG

  // Create Graphics Pipeline
  VkGraphicsPipelineCreateInfo gPipe;
  gPipe.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
  gPipe.pNext = NULL;
  gPipe.layout = m_pipelineLayout;
  gPipe.basePipelineHandle = VK_NULL_HANDLE;
  gPipe.basePipelineIndex = 0;
  gPipe.flags = 0;
  gPipe.pVertexInputState = &viInfo;
  gPipe.pInputAssemblyState = &inputAssemblyState;
  gPipe.pRasterizationState = &rasterState;
  gPipe.pColorBlendState = &colorBlendState;
  gPipe.pTessellationState = NULL;
  gPipe.pMultisampleState = &msInfo;
  gPipe.pDynamicState = &dyInfo;
  gPipe.pViewportState = &vpInfo;
  gPipe.pDepthStencilState = &depthStencilState;
  gPipe.stageCount = (uint32_t)shaderStageSize;
  gPipe.pStages = sdrStageInfoList.data();
  gPipe.renderPass = m_renderTargetPtr ? m_renderTargetPtr->m_renderPass: Engine::swpch::vRenderpass;
  gPipe.subpass = 0;
#ifdef _DEBUG
  vkRes = vkCreateGraphicsPipelines(Engine::core::vDevice, VK_NULL_HANDLE, 1, &gPipe, NULL, &m_pipeline);
  assert(vkRes == VK_SUCCESS);
#else
  vkCreateGraphicsPipelines(Engine::core::vDevice, VK_NULL_HANDLE, 1, &gPipe, NULL, &m_pipeline);
#endif // _DEBUG
}


void VulkPipeline::Destroy()
{
  // remove pipeline layout, descriptor sets and descriptor pool
  vkDestroyPipelineLayout(Engine::core::vDevice, m_pipelineLayout, NULL);

  for (auto& desLayout : m_descSetMap)
    vkDestroyDescriptorSetLayout(Engine::core::vDevice, desLayout.second._vkDSLayout, NULL);

  vkDestroyDescriptorPool(Engine::core::vDevice, m_setPool, NULL);

  for (auto& sdr : m_shaderMap)
    vkDestroyShaderModule(Engine::core::vDevice, sdr.second._vkSdrModule, NULL);

  m_descSetMap.clear();
  m_shaderMap.clear();
}

void VulkPipeline::BindPipeline()
{
  vkCmdBindPipeline(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame], VK_PIPELINE_BIND_POINT_GRAPHICS, m_pipeline);
}

void VulkPipeline::BindDescriptorSet(const unsigned& set, const unsigned& instanceID)
{
#ifdef _DEBUG
  if (m_descSetMap.find(set) != m_descSetMap.end())
  {
    m_descSetMap[set].GetSetInstance(instanceID).BindDescriptorSet_Graphics(Engine::swpch::vCurFrame, set, m_pipelineLayout);
  }
  else
  {
    std::cout << "Error: Descriptor set " << set << " not found\n";
    assert(false);
  }

#else
  m_descSetMap[set].GetSetInstance(instanceID).BindDescriptorSet_Graphics(Engine::swpch::vCurFrame, set, m_pipelineLayout);
#endif // _DEBUG

}


int VulkPipeline::RegisterDescriptor(const unsigned & setNumber)
{
  if (m_descSetMap.find(setNumber) == m_descSetMap.end())
    return -1;
  ++m_descSetMap[setNumber]._refCount;
  m_descSetMap[setNumber]._setInstance.push_back(VulkUtils::DescriptorSetInst{});
  return (int)m_descSetMap[setNumber]._setInstance.size() - 1;
}


void VulkPipeline::CompileRegisteredDescriptor()
{
  // precompute memory alignment
  VkBuffer testBuffer;
  VkBufferCreateInfo uniBufInfo;
  uniBufInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
  uniBufInfo.pNext = NULL;
  uniBufInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
  uniBufInfo.size = 512; // random magic number
  uniBufInfo.queueFamilyIndexCount = 0;
  uniBufInfo.pQueueFamilyIndices = NULL;
  uniBufInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
  uniBufInfo.flags = 0;

#ifdef _DEBUG
  VkResult vkRes = vkCreateBuffer(Engine::core::vDevice, &uniBufInfo, NULL, &testBuffer);
  assert(vkRes == VK_SUCCESS);
#else
  vkCreateBuffer(Engine::core::vDevice, &uniBufInfo, NULL, &testBuffer);
#endif // _DEBUG

  // compute alignment
  VkMemoryRequirements memReqForAlignment;
  vkGetBufferMemoryRequirements(Engine::core::vDevice, testBuffer, &memReqForAlignment);
  vkDestroyBuffer(Engine::core::vDevice, testBuffer, NULL);


  // compute total VkDescriptorPoolSize list
  std::vector<VkDescriptorPoolSize> typeCountList;
  unsigned totalNumberOfSets = 0;
  for (auto& set : m_descSetMap)
  {
    totalNumberOfSets += set.second._refCount;
    for (size_t i = 0; i < set.second._refCount; ++i)
    {
      for (auto& block : set.second._setInterfaceBlocks)
      {
        VkDescriptorPoolSize typeCount;
        typeCount.descriptorCount = 1;
        typeCount.type =
          block._vkBaseTypeLayout.size() == 0 && block._vkBlockSize == 0 ?
          VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER :
          VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        typeCountList.push_back(typeCount);
      }
    }
  }

  if (totalNumberOfSets > 0)
  {
    // Create descriptor pool
    VkDescriptorPoolCreateInfo desPoolCreateInfo;
    desPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    desPoolCreateInfo.maxSets = totalNumberOfSets * Engine::swpch::vNumSCImgs;
    desPoolCreateInfo.poolSizeCount = (uint32_t)typeCountList.size();  // size in typeCount
    desPoolCreateInfo.pPoolSizes = typeCountList.data();
    desPoolCreateInfo.pNext = NULL;
    desPoolCreateInfo.flags = 0;
#ifdef  _DEBUG
    vkRes = vkCreateDescriptorPool(Engine::core::vDevice, &desPoolCreateInfo, NULL, &m_setPool);
    assert(vkRes == VK_SUCCESS);
#else
    vkCreateDescriptorPool(Engine::core::vDevice, &desPoolCreateInfo, NULL, &m_setPool);
#endif //  _DEBUG

    // allocate descriptor based on created pool
    VkDeviceSize setOffset = 0;
    for (auto& set : m_descSetMap)
    {
      size_t totalNumberOfSetsWithInstances = Engine::swpch::vNumSCImgs * set.second._refCount;
      std::vector<VkDescriptorSet> tmpSetCollection(totalNumberOfSetsWithInstances, VkDescriptorSet{});
      std::vector<VkDescriptorSetLayout> desLayoutTmpVec(totalNumberOfSetsWithInstances, set.second._vkDSLayout);

      if (totalNumberOfSetsWithInstances > 0)
      {
        VkDescriptorSetAllocateInfo desPoolAllocInfo;
        desPoolAllocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        desPoolAllocInfo.pNext = NULL;
        desPoolAllocInfo.descriptorPool = m_setPool;
        desPoolAllocInfo.descriptorSetCount = (uint32_t)totalNumberOfSetsWithInstances; // might need to change
        desPoolAllocInfo.pSetLayouts = desLayoutTmpVec.data();
#ifdef _DEBUG
        vkRes = vkAllocateDescriptorSets(Engine::core::vDevice, &desPoolAllocInfo, tmpSetCollection.data());
        assert(vkRes == VK_SUCCESS);
#else
        vkAllocateDescriptorSets(Engine::core::vDevice, &desPoolAllocInfo, tmpSetCollection.data());
#endif // _DEBUG

        int descCounter = 0;
        for (auto& inst : set.second._setInstance)
        {
          for (auto& descSet : inst._set)
            descSet = tmpSetCollection[descCounter++];
        }
      }


      // compute descriptor block size + paddings
      VkDeviceSize cumulativeBlockSize = 0;
      VkDeviceSize trueCumulativeBlockSize = 0;
      for (auto& block : set.second._setInterfaceBlocks)
      {
        
        VkDeviceSize blockSize = (VkDeviceSize)block._vkBlockSize;
        if (blockSize > 0 && blockSize <= memReqForAlignment.alignment)
        {
          block._vkBlockStride = (unsigned)memReqForAlignment.alignment;
        }
        else if (blockSize > memReqForAlignment.alignment)
        {
          VkDeviceSize multiplesOfAlignment = blockSize / memReqForAlignment.alignment;
          VkDeviceSize checkback = multiplesOfAlignment * memReqForAlignment.alignment;
          block._vkBlockStride = checkback < blockSize ? (unsigned)(checkback + memReqForAlignment.alignment) : (unsigned)checkback;
        }
        else continue; // skip combined image sampler

        block._vkTrueBlockStride = (unsigned)(block._vkBlockStride * Engine::swpch::vNumSCImgs);
        cumulativeBlockSize += block._vkBlockStride;
        trueCumulativeBlockSize += block._vkTrueBlockStride;
      }

      // set them at last
      set.second._setStride = (unsigned)cumulativeBlockSize;
      set.second._trueSetStride = (unsigned)(trueCumulativeBlockSize);
      set.second._setStartingOffset = (unsigned)setOffset;
      setOffset += (VkDeviceSize)set.second._trueSetStride * set.second._refCount;
    }

    // allocate buffer
    if (setOffset > 0)
    {
      m_vUniformBuffer.CreateBuffer((unsigned)setOffset, 1,
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
      m_vUniformBuffer.MapMemory(nullptr, (unsigned)setOffset, 0);
      m_vUniformBuffer.BindBufferMemory();
    }
    else
    {
      std::cout << "Info: No memory allocated for this pipeline\n";
    }
  }
  else
  {
    std::cout << "Warning: No sets found in the pipeline\n";
  }
}

VulkUtils::VulkDescSetLayout* VulkPipeline::GetLayout(const unsigned & setNumber)
{
  return m_descSetMap.find(setNumber) != m_descSetMap.end() ? &m_descSetMap[setNumber] : nullptr;
}

void* VulkPipeline::GetBufferMemory()
{
  return m_vUniformBuffer.GetMappedMemoryData();
}

const VulkBuffer & VulkPipeline::GetVulkBufferObj() const
{
  return m_vUniformBuffer;
}

VulkRenderTarget* VulkPipeline::GetRenderTarget()
{
  return m_renderTargetPtr;
}


//VulkUtils::DescriptorSetInst* VulkPipeline::GetDescriptorSetInst(const unsigned & setNumber, const int & index)
//{
//  if (m_descSetMap.find(setNumber) == m_descSetMap.end())
//    return nullptr;
//  return &m_descSetMap[setNumber]._setInstance[index];
//}


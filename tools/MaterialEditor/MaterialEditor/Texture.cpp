#include "pch.h"
#include "Texture.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

Texture::Texture()
  : m_texture(0), m_texWidth(0), m_texHeight(0),
    m_texChannel(0), m_groupSet(0)
{ }

bool Texture::CreateTexture(const char* file)
{
  stbi_uc* ImgData = stbi_load(file, &m_texWidth, &m_texHeight, &m_texChannel, STBI_rgb_alpha);

  if (ImgData == nullptr)
  {
    std::cout << "Error: Uable to load texture from file " << file << std::endl;
    return false;
  }

  glGenTextures(1, &m_texture);
  glBindTexture(GL_TEXTURE_2D, m_texture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, m_texWidth, m_texHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, ImgData);

  // texture parameters
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glGenerateMipmap(GL_TEXTURE_2D);


  // Unbind and clear memory
  glBindTexture(GL_TEXTURE_2D, 0);
  stbi_image_free(ImgData);
  return true;
}

bool Texture::CreateTexture(const std::string & file)
{
  return CreateTexture(file.c_str());
}


void Texture::DestroyTexture()
{
  glDeleteTextures(1, &m_texture);
}

void Texture::BindTexture(GLint location)
{
  glActiveTexture(GL_TEXTURE0 + location);
  glUniform1i(location, location);
  glBindTexture(GL_TEXTURE_2D, m_texture);
}

const GLuint& Texture::GetTexture() const
{
  return m_texture;
}

void Texture::ChangeGroup(const int & newGroup)
{
  m_groupSet = newGroup;
}

int Texture::GetGroup() const
{
  return m_groupSet;
}

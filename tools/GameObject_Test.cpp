#include "pch.h"
#include "Transform.h"
#include "CameraComponent.h"
#include "GameobjectManager.h"

bool Test1(unsigned loopC)
{
  std::cout << " 1: Testing add and remove " << loopC << " times...\n";
  GameobjectManager* inst = GameobjectManager::GetInstance();
  GameobjectHandle obj = inst->GetGameobjectHandle(inst->CreateGameobject());
  GameobjectHandle obj2 = inst->GetGameobjectHandle(inst->CreateGameobject());

  // Mass test
  for (unsigned i = 0; i < loopC; ++i)
  {
    if (!obj.IsValid() || !obj2.IsValid()) continue;

    unsigned ref0 = obj.GetData()->AddComponent_ref<Transform>();
    unsigned ref1 = obj.GetData()->AddComponent_ref<CameraComponent>();
    unsigned ref2 = obj.GetData()->AddComponent_ref<CameraComponent>();
    unsigned ref3 = obj.GetData()->AddComponent_ref<CameraComponent>();
    ComponentHandle<CameraComponent> testGetHCCamera = obj.GetData()->AddComponent_hc<CameraComponent>();

    ComponentHandle<CameraComponent> cc = obj.GetData()->GetComponentHandle<CameraComponent>(ref1);
    obj.GetData()->ResetHandle(cc, ref1);

    ComponentHandle<CameraComponent> newCC;
    obj.GetData()->ResetHandle(newCC, ref2);

    ComponentHandle<CameraComponent> newCC2;
    obj.GetData()->ResetHandle(newCC2, ref3);
    //cc = obj->GetComponentHandle<CameraComponent>(); MUST FAIL TO COMPILE

    {
      ComponentHandle<Transform> tc = obj.GetData()->GetComponentHandle<Transform>(ref0);
      obj.GetData()->DeleteComponent(tc);

      if (tc.IsValid())
        return false;
    }

    obj.GetData()->DeleteComponent(cc);
    obj.GetData()->DeleteComponent(ref2);  // remove using reference number
    obj.GetData()->DeleteComponent(newCC2);
    obj.GetData()->DeleteComponent(testGetHCCamera);

    if (cc.IsValid() ||
        newCC.IsValid() ||
        newCC2.IsValid() ||
        testGetHCCamera.IsValid())
      return false;
  }
  return true;
}

bool Test2()
{
  std::cout << " 2: Testing add and remove by reference and component...\n";
  GameobjectManager* inst = GameobjectManager::GetInstance();
  GameobjectHandle obj2 = inst->GetGameobjectHandle(inst->CreateGameobject());

  obj2.GetData()->AddComponent_ref<Transform>();
  obj2.GetData()->AddComponent_ref<CameraComponent>();
  obj2.GetData()->AddComponent_ref<CameraComponent>();
  obj2.GetData()->AddComponent_ref<CameraComponent>();
  auto hcccc = obj2.GetData()->AddComponent_hc<CameraComponent>();
  obj2.GetData()->ClearAllComponent();

  return (!hcccc.IsValid());
}

bool Test3()
{
  std::cout << " 3: Testing delete all Gameobject...\n";
  GameobjectManager::GetInstance()->DestroyAllGameobject();
  return true;
}

bool Test4()
{
  std::cout << " 4: Testing delete Gameobject with component reference...\n";
  GameobjectManager* inst = GameobjectManager::GetInstance();
  GameobjectHandle obj2 = inst->GetGameobjectHandle(inst->CreateGameobject());

  auto Thcccc = obj2.GetData()->AddComponent_hc<Transform>();
  obj2.GetData()->AddComponent_ref<CameraComponent>();
  obj2.GetData()->AddComponent_ref<CameraComponent>();
  obj2.GetData()->AddComponent_ref<CameraComponent>();
  auto hcccc = obj2.GetData()->AddComponent_hc<CameraComponent>();

  inst->DestroyGameobject(obj2);

  return !hcccc.IsValid() || !Thcccc.IsValid();
}


bool Test5()
{
  std::cout << " 5: Testing delete Gameobject with Gameobject reference...\n";
  GameobjectManager* inst = GameobjectManager::GetInstance();
  GameobjectHandle obj2 = inst->GetGameobjectHandle(inst->CreateGameobject());
  inst->DestroyGameobject(obj2);
  return true;
}

bool Test6()
{
  std::cout << " 5: Testing creating bunch of Gameobject with Gameobject reference...\n";
  GameobjectManager* inst = GameobjectManager::GetInstance();
  for (unsigned i = 0; i < 10; ++i)
    inst->CreateGameobject();
  inst->DestroyAllGameobject();
  return true;
}

int main()
{
  std::string passString(" -- Test Pass --\n\n");
  std::string failString(" -- Test Pass --\n\n");

  std::cout << " Gameobject Test --\n\n";

  // Gameobject addition component deletion
  std::cout << (Test1(1000) ? passString : failString);
  std::cout << (Test2() ? passString : failString);

  // Gameobject deletion
  std::cout << (Test3() ? passString : failString);

  // Gameobject addtion & deletion W/ Component reference
  std::cout << (Test4() ? passString : failString);

  // Gameobject addtion & deletion W/ Gameobject reference
  std::cout << (Test5() ? passString : failString);

  // Gameobject addtional bunch
  std::cout << (Test6() ? passString : failString);

  std::cout << " Test completed --\n\n";

// Debug hash
  EntityTracker::GetInstance()->RunDebugHashTable();

  return 1;
}

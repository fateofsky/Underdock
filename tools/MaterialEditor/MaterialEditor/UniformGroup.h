#pragma once
#include <string>
#include "UniformResource.h"

class UniformGroup
{
public:

  enum _MemoryLayout
  {
    NONE,
    STD140,  // uniform block
    STD430   // storage block
  };

  UniformGroup();
  int                       InsertUniform(const std::string& uniformName);
  int                       RemoveUniform(const std::string& uniformName);
  size_t                    GetGroupCount() const;
  std::vector<std::string>& GetGroupList();
  void                      ChangeGroup(unsigned group);
  unsigned                  GetGroup() const;
  void                      ChangeMemoryLayout(_MemoryLayout mLayout);
  _MemoryLayout             GetMemoryLayout() const;

private:
  std::vector<std::string> m_groups;
  unsigned                 m_groupSet;
  _MemoryLayout            m_memoryLayout;
};
/* Start Header
*****************************************************************/
/*!
\file      MathMatrix3D.h
\author    
\par       
\date      
\brief

Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the
prior written consent of DigiPen Institute of Technology is prohibited.
*/
/* End Header
*******************************************************************/
#pragma once
#include "MathLib.h"

#define mat3_LEN 9
#define mat3_ROW 3

union mat4;

///////////
// mat3 //--------------------------------------------------------------
///////////
union mat3
{
	struct
	{
		float m00, m01, m02;
		float m10, m11, m12;
		float m20, m21, m22;
	};
	float m_33[mat3_LEN];
	float m2_33[mat3_ROW][mat3_ROW];	// [row][col]

	mat3();
	mat3(const float *pArr);
	mat3(float _00, float _01, float _02,
		  float _10, float _11, float _12,
		  float _20, float _21, float _22);
	mat3(const mat4& mat4);
	mat3(const std::string& str);

	operator std::string() const;
	mat3& operator=(const mat3 &rhs);
	mat3& operator=(const mat4 &rhs);
	mat3& operator*=(const mat3 &rhs);
	mat3& operator+=(const mat3& rhs);
	mat3& operator-=(const mat3& rhs);
	mat3& operator=(const float* arr33);
	float& operator[](int i);
	const float& operator[](int i) const;

	// Properties
	mat3 inversed() const;
	mat3 transposed() const;
	const float * get() const;

	// Methods
	void Identity();
	void Zero();
	void Inverse();
	void Transpose();
};

mat3 operator*(const mat3& lhs, const float& rhs);
mat3 operator*(const mat3 &lhs, const mat3 &rhs);
mat3 operator+(const mat3& a, const mat3& b);
mat3 operator-(const mat3& a, const mat3& b);
vec2 operator*(const mat3 &pMtx, const vec2 &rhs);
vec3 operator*(const mat3 &pMtx, const vec3 &rhs);
std::ostream& operator<<(std::ostream & os, const mat3 & rhs);
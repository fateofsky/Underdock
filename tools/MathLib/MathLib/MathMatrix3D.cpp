/* Start Header
*****************************************************************/
/*!
\file      MathMatrix3D.cpp
\author    
\par       
\date      
\brief

Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the
prior written consent of DigiPen Institute of Technology is prohibited.
*/
/* End Header
*******************************************************************/
#include "stdafx.h"

///////////
// mat3 //--------------------------------------------------------------
///////////
/* -----------------------------*/
/* ----- Member Functions ----- */
/* -----------------------------*/
#pragma warning(suppress: 26495) //unitiaizlied member, but its a union so...
mat3::mat3()
	: m_33{ 1.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 1.0f }
{}

mat3::mat3(const float *pArr)
{
	std::copy(pArr, pArr + mat3_LEN, m_33);
}

mat3::mat3(float _00, float _01, float _02,
			 float _10, float _11, float _12,
			 float _20, float _21, float _22)
	: m00{ _00 }, m01{ _01 }, m02{ _02 },
	  m10{ _10 }, m11{ _11 }, m12{ _12 },
	  m20{ _20 }, m21{ _21 }, m22{ _22 }
{}

mat3::mat3(const mat4& mat4)
{
	m00 = mat4.m00;
	m01 = mat4.m01;
	m02 = mat4.m02;

	m10 = mat4.m10;
	m11 = mat4.m11;
	m12 = mat4.m12;

	m20 = mat4.m20;
	m21 = mat4.m21;
	m22 = mat4.m22;
}
#pragma warning(suppress: 26495) //unitiaizlied member, but its a union so...
mat3::mat3(const std::string& str)
{
	size_t comma = 0;
	for (float& elem : m_33)
	{
		if (comma != std::string::npos)
		{
			elem = std::atof(&str.front() + comma);
			comma = str.find(',', comma + 1) + 1;
		}
		else elem = 0;
	}
}

mat3::operator std::string() const
{
	std::string out;
	for (const float& elem : m_33)
	{
		out += std::to_string(elem) + ", ";
	}
	out.pop_back();
	out.pop_back();
	return out;
}

mat3& mat3::operator=(const mat3 &mtx)
{
	std::copy(mtx.m_33, mtx.m_33 + mat3_LEN, m_33);
	return *this;
}

mat3& mat3::operator=(const mat4 &mtx)
{
	m00 = mtx.m00;
	m01 = mtx.m01;
	m02 = mtx.m02;

	m10 = mtx.m10;
	m11 = mtx.m11;
	m12 = mtx.m12;

	m20 = mtx.m20;
	m21 = mtx.m21;
	m22 = mtx.m22;
	return *this;
}

mat3& mat3::operator*=(const mat3 &rhs)
{
	*this = *this * rhs;
	return *this;
}

mat3& mat3::operator+=(const mat3& b)
{
	*this = *this + b;
	return *this;
}

mat3& mat3::operator-=(const mat3& b)
{
	*this = *this - b;
	return *this;
}

mat3& mat3::operator=(const float* arr33)
{
	for (int i = 0; i < mat3_LEN; i++)
		m_33[i] = arr33[i];
	return *this;
}

float& mat3::operator[](int i)
{
	return m_33[i];
}

const float& mat3::operator[](int i) const
{
	return m_33[i];
}

mat3 mat3::inversed() const
{
	mat3 inv;
	mat3Inverse(inv, *this, nullptr);
	return inv;
}

mat3 mat3::transposed() const
{
	mat3 trans;
	mat3Transpose(trans, *this);
	return trans;
}

const float * mat3::get() const
{
	return m_33;
}

void mat3::Identity()
{
	*this = mat3();
}

void mat3::Zero()
{
	*this = mat3Zero();
}

void mat3::Inverse()
{
	mat3Inverse(*this, *this, nullptr);
}

void mat3::Transpose()
{
	mat3Transpose(*this, *this);
}

mat3 operator*(const mat3 & lhs, const float & rhs)
{
	mat3 tmp(lhs);
	tmp[0] *= rhs;
	tmp[1] *= rhs;
	tmp[2] *= rhs;
	tmp[3] *= rhs;
	tmp[4] *= rhs;
	tmp[5] *= rhs;
	tmp[6] *= rhs;
	tmp[7] *= rhs;
	tmp[8] *= rhs;
	return tmp;
}

/* ---------------------------------*/
/* ----- Non-Member Functions ----- */
/* ---------------------------------*/
mat3 operator*(const mat3 &lhs, const mat3 &rhs)
{
	mat3 mt;
	mt.m00 = lhs.m00 * rhs.m00 + lhs.m01 * rhs.m10 + lhs.m02 * rhs.m20;
	mt.m01 = lhs.m00 * rhs.m01 + lhs.m01 * rhs.m11 + lhs.m02 * rhs.m21;
	mt.m02 = lhs.m00 * rhs.m02 + lhs.m01 * rhs.m12 + lhs.m02 * rhs.m22;
	mt.m10 = lhs.m10 * rhs.m00 + lhs.m11 * rhs.m10 + lhs.m12 * rhs.m20;
	mt.m11 = lhs.m10 * rhs.m01 + lhs.m11 * rhs.m11 + lhs.m12 * rhs.m21;
	mt.m12 = lhs.m10 * rhs.m02 + lhs.m11 * rhs.m12 + lhs.m12 * rhs.m22;
	mt.m20 = lhs.m20 * rhs.m00 + lhs.m21 * rhs.m10 + lhs.m22 * rhs.m20;
	mt.m21 = lhs.m20 * rhs.m01 + lhs.m21 * rhs.m11 + lhs.m22 * rhs.m21;
	mt.m22 = lhs.m20 * rhs.m02 + lhs.m21 * rhs.m12 + lhs.m22 * rhs.m22;
	return mt;
}

mat3 operator+(const mat3& a, const mat3& b)
{
	mat3 sum;
	for (size_t i = 0; i < mat3_LEN; ++i) sum.m_33[i] = a.m_33[i] + b.m_33[i];
	return sum;
}

mat3 operator-(const mat3& a, const mat3& b)
{
	mat3 sum;
	for (size_t i = 0; i < mat3_LEN; ++i) sum.m_33[i] = a.m_33[i] - b.m_33[i];
	return sum;
}

vec2 operator *(const mat3 &pMtx, const vec2 &rhs)
{
	return vec2(pMtx.m00 * rhs.x_ + pMtx.m01 * rhs.y_ + pMtx.m02,
				 pMtx.m10 * rhs.x_ + pMtx.m11 * rhs.y_ + pMtx.m12);
}

vec3 operator *(const mat3 &pMtx, const vec3 &rhs)
{
	return vec3(pMtx.m00 * rhs.x_ + pMtx.m01 * rhs.y_ + pMtx.m02 * rhs.z_,
				 pMtx.m10 * rhs.x_ + pMtx.m11 * rhs.y_ + pMtx.m12 * rhs.z_,
				 pMtx.m20 * rhs.x_ + pMtx.m21 * rhs.y_ + pMtx.m22 * rhs.z_);
}

std::ostream & operator<<(std::ostream & os, const mat3 & rhs)
{
	std::cout << "|" << rhs.m00 << "," << rhs.m01 << "," << rhs.m02 << "|\n";
	std::cout << "|" << rhs.m10 << "," << rhs.m11 << "," << rhs.m12 << "|\n";
	std::cout << "|" << rhs.m20 << "," << rhs.m21 << "," << rhs.m22 << "|";
	return os;
}
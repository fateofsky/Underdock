#pragma once
#include <string>

class FileUtilityManager
{
public:
  static std::string GetResourcesFilePath();
  static std::string GetImageFilePath(const std::string& imageFile);
  static std::string GetModelFilePath(const std::string& modelFile);
  static std::string GetPipelineFilePath(const std::string& pipelineFile);
  static std::string GetRendertargetFilePath(const std::string& rtFile);

private:
  static const std::string m_imageLoc;
  static const std::string m_modelLoc;
  static const std::string m_pipelineLoc;
  static const std::string m_rendertargetLoc;
};
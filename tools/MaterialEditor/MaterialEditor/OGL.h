#pragma once
#include "OGLHeader.h"
#include "VertexBuffer.h"
#include "CommonVBLayout.h"

namespace ogl
{
  void InitContext(HDC& hdc, HGLRC& hrc);
  VertexBuffer CreateVertexBuffer(void* vertexDataPack, GLuint vSize, void* indicesData, 
                                  GLuint iSize, const vBufferLayout& layout, GLuint elemSize);
  void DeleteVertexBuffer(const VertexBuffer& vb);
}
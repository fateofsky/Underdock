#include "pch.h"
#include <algorithm>    // std::remove_if
#include "MaterialLayout.h"
#include "ResourceManager.h"

void MaterialLayout::Destroy()
{
  m_resourceLayout.Destroy();

  m_shader = nullptr;
  m_model = nullptr;

  m_layoutName = m_shaderName = m_modelName = "";
}

void MaterialLayout::AttachShader(const std::string& name)
{
  m_shader = ResourceManager::GetInstance()->GetShader(name);
  m_shaderName = name;
}

void MaterialLayout::AttachTexture(const std::string & name, Shader::ShaderType pipelineStage)
{
  Texture* texture = ResourceManager::GetInstance()->GetTexture(name);
  m_resourceLayout.AddTextureToStage(name, pipelineStage, texture);
}

void MaterialLayout::AttachModel(const std::string & name)
{
  m_model = ResourceManager::GetInstance()->GetModel(name);
  m_modelName = name;
}

void MaterialLayout::AttachIndices(const std::vector<unsigned>& indexList)
{
  m_drawIndexList = indexList;
}

void MaterialLayout::AttachVertexBuffer(const std::string& name)
{
  m_vbList = ResourceManager::GetInstance()->GetVertexBuffer(name);
}


void MaterialLayout::SetLayoutName(const std::string & name)
{
  m_layoutName = name;
}

void MaterialLayout::InsertVBufferLayout(vBufferLayout::vBufferLayoutType type)
{
  m_vBufferLayout.layout.push_back(type);
}

void MaterialLayout::RemoveVBufferLayout(vBufferLayout::vBufferLayoutType type)
{
  m_vBufferLayout.layout.erase(std::remove_if(m_vBufferLayout.layout.begin(), m_vBufferLayout.layout.end(), [&type](const vBufferLayout::vBufferLayoutType& elemType)->bool
    {
      return elemType == type;
    }));
}

int MaterialLayout::AddUniform(const std::string & name, OGL_BASETYPE baseType, 
                               UNIFORM_USAGE usage, Shader::ShaderType pipelineStage, unsigned group)
{
  if (m_resourceLayout.UniformExist(name)) return 1;
  UniformResourceBase* _base = UniformResourceBase::CreateUniform(baseType, usage, group);
  return m_resourceLayout.AddUniformToStage(name, pipelineStage, _base);
}

void MaterialLayout::RemoveUniform(const std::string& name)
{
  m_resourceLayout.RemoveUniform(name);
}

void MaterialLayout::DetachShader()
{
  m_shader = nullptr;
  m_shaderName = "";
}

void MaterialLayout::RemoveTexture(const std::string& name)
{
  m_resourceLayout.RemoveTexture(name);
}

void MaterialLayout::DetachModel()
{
  m_model = nullptr;
  m_modelName = "";
}

void MaterialLayout::BindAll()
{
  m_resourceLayout.BindPipeline(m_bindUniformMap);
}

void MaterialLayout::UnbindAll()
{
  m_resourceLayout.UnbindPipeline();
}

void MaterialLayout::Render()
{
  size_t indCount = m_drawIndexList.size();
  for (size_t i = 0; i < indCount; ++i)
    m_vbList->operator[](i).DrawBuffer(m_drawIndexList[i]);
}

UniformResourceBase* MaterialLayout::GetUniform(const std::string & name)
{
  return m_resourceLayout.GetUniform(name);

}

#define PARSECOMP
#undef PARSECOMP
#pragma once
#include "ComponentBase.h"
#include "../../MathLib/MathLib/MathLib.h"

class ComponentBase;

class ModelComponent : public ComponentBase, public _SINGLE
{
public:
  ModelComponent(Gameobject* owner, const unsigned& _id);
  virtual ~ModelComponent();
  void            SetModelIndex(const unsigned& modelBufferIndex);
  const unsigned& GetModelIndex() const;

  virtual void Init() override;
  virtual void PostInit() override;
  virtual void Update() override;
  virtual void PreRender() override;
  virtual void PostUpdate() override;

private:
  DataParam<unsigned> m_modelBufferIndex;
};
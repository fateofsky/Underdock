#pragma once

enum ComponentEnum
{
  C_none,
  C_CameraComponent,
  C_MaterialComponent,
  C_ModelComponent,
  C_Transform
};

class Gameobject;
class CameraComponent;
class MaterialComponent;
class ModelComponent;
class Transform;

namespace rtti
{
  template<typename T>
  inline ComponentEnum DeriveTypeID()
   {  return ComponentEnum::C_none;  }

  template<>
  inline ComponentEnum DeriveTypeID<CameraComponent>()
   {  return ComponentEnum::C_CameraComponent;  }

  template<>
  inline ComponentEnum DeriveTypeID<MaterialComponent>()
   {  return ComponentEnum::C_MaterialComponent;  }

  template<>
  inline ComponentEnum DeriveTypeID<ModelComponent>()
   {  return ComponentEnum::C_ModelComponent;  }

  template<>
  inline ComponentEnum DeriveTypeID<Transform>()
   {  return ComponentEnum::C_Transform;  }

  inline const char* GetComponentEnumString(ComponentEnum val)
  {
    switch (val)
    {
    case C_CameraComponent: return "CameraComponent";
    case C_MaterialComponent: return "MaterialComponent";
    case C_ModelComponent: return "ModelComponent";
    case C_Transform: return "Transform";
    default: return "[NOT A COMPONENT]";
    }
  }
}

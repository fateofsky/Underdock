#pragma once
#include "GL/glew.h"
#include "GL/wglew.h"
#include "CommonVBLayout.h"


struct VertexBuffer
{
  GLuint		 m_vao = 0;
  GLuint		 m_vbo = 0;
  GLuint		 m_ebo = 0;

  void Create(GLsizeiptr dataSize, const void* data,
              GLsizeiptr indicesSize, const void* indicesData,
              const vBufferLayout& bufLayout);
  //void ResetAttribPointerLayout(const vBufferLayout& bufLayout) const;
  void Destroy();
  void DrawBuffer(unsigned size);
  void DrawBuffer(unsigned size) const;
};
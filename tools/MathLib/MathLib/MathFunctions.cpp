#include "stdafx.h"
#include "MathFunctions.h"

vec3 ProjectPointOnPlane(const vec3& point, const vec3& normal, float planeDistance)
{
  vec3 _pt = point;
  float projDist = vec3DotProduct(point, normal);
  vec3 projPt = projDist * normal;
  vec3 offsetVec = projPt - planeDistance * normal;
  _pt -= offsetVec;
  return _pt;
}

FrustumIntersectionType::Type PlaneSphere(const vec4& plane,
  const vec3& sphereCenter, float sphereRadius)
{
  vec3 proj = ProjectPointOnPlane(sphereCenter, vec3(plane.x_, plane.y_, plane.z_), plane.w_);
  vec3 offset = sphereCenter - proj;
  if (offset.sqrMagnitude() <= sphereRadius * sphereRadius) return FrustumIntersectionType::Overlaps;
  if (vec3DotProduct(offset, vec3(plane.x_, plane.y_, plane.z_)) > 0)return FrustumIntersectionType::Inside;
  else return FrustumIntersectionType::Outside;
}

FrustumIntersectionType::Type PlaneAabb(const vec4& plane,
  const vec3& aabbMin, const vec3& aabbMax)
{
  vec3 aabbExtents = 0.5f * (aabbMax - aabbMin);
  vec3 aabbCenter = aabbMin + aabbExtents;
  vec3 u = vec3(std::copysign(aabbExtents.x_, plane.x_), std::copysign(aabbExtents.y_, plane.y_), std::copysign(aabbExtents.z_, plane.z_));
  float d = vec3DotProduct(vec3(plane.x_, plane.y_, plane.z_), u);
  return PlaneSphere(plane, aabbCenter, d);
}

FrustumIntersectionType::Type FrustumAabb(const vec4 planes[5],
  const vec3& aabbMin, const vec3& aabbMax)
{
  FrustumIntersectionType::Type types[6];
  FrustumIntersectionType::Type type = FrustumIntersectionType::Type::Inside;
  for (unsigned i = 0; i < 5; ++i)
  {
    types[i] = PlaneAabb(planes[i], aabbMin, aabbMax);
  }
  for (unsigned i = 0; i < 5; ++i)
  {
    if (types[i] == FrustumIntersectionType::Type::Overlaps && type != FrustumIntersectionType::Type::Outside) type = FrustumIntersectionType::Type::Overlaps;
    else if (types[i] == FrustumIntersectionType::Type::Outside) type = FrustumIntersectionType::Type::Outside;
  }
  return type;
}

bool AabbAabb(const vec3& aabbMin0, const vec3& aabbMax0,
  const vec3& aabbMin1, const vec3& aabbMax1)
{
  /******Student:Assignment1******/
  if ((aabbMax0.x_ < aabbMin1.x_ || aabbMax1.x_ < aabbMin0.x_) ||
    (aabbMax0.y_ < aabbMin1.y_ || aabbMax1.y_ < aabbMin0.y_) ||
    (aabbMax0.z_ < aabbMin1.z_ || aabbMax1.z_ < aabbMin0.z_))
    return false;
  return true;
}


mat4 Math::Orthogonal(float left, float right, float bottom, float top, float nearZ, float farZ)
{
  mat4 projectionMatrix;
  projectionMatrix[0] = 2.0f / (right - left);
  projectionMatrix[5] = 2.0f / (top - bottom);
  projectionMatrix[3] = -(right + left) / (right - left);
  projectionMatrix[7] = -(top + bottom) / (top - bottom);

  projectionMatrix[10] = -1.0f / (farZ - nearZ);
  projectionMatrix[11] = -nearZ / (farZ - nearZ);
  return projectionMatrix;
}

mat4 Math::Perspective(float fov, float aspect, float nearZ, float farZ)
{
  float tanHalfFov = tan(fov / 2.0f);
  //mat4 projectionMatrix;
  //projectionMatrix[0] = 2.0f * dist / (aspect * tanHalfFov);
  //projectionMatrix[5] = 2.0f * dist / tanHalfFov;
  //projectionMatrix[11] = -2.0f * (farZ * nearZ) / (farZ - nearZ);
  //projectionMatrix[10] = farZ / (nearZ - farZ);
  //projectionMatrix[14] = -1.0f;
  //projectionMatrix[15] = 0.0f;
  //return projectionMatrix;
  
  mat4 projectionMatrix;
  projectionMatrix[0]        = 1.f / (aspect * tanHalfFov);
  projectionMatrix[5]        = 1.f / tanHalfFov;
  projectionMatrix[10]       = (nearZ + farZ) / (nearZ - farZ);
  projectionMatrix[11]       = 2.0f * (nearZ * farZ) / (nearZ - farZ);
  projectionMatrix[14]       = -1.0f;
  projectionMatrix[15]       = 0.0f;
  return projectionMatrix;
  
}

vec3 Math::CatmullRom(const vec3& v1, const vec3& v2, const vec3& v3, const vec3& v4, float s)
{
  return
    v1 * (-0.5f * s * s * s + s * s - 0.5f * s) +
    v2 * (1.5f * s * s * s + -2.5f * s * s + 1.0f) +
    v3 * (-1.5f * s * s * s + 2.0f * s * s + 0.5f * s) +
    v4 * (0.5f * s * s * s - 0.5f * s * s);
}

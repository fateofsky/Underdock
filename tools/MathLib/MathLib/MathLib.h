/* Start Header
*****************************************************************/
/*!
\file      MathLib.h
\author    
\par       
\date      
\brief

Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the
prior written consent of DigiPen Institute of Technology is prohibited.
*/
/* End Header
*******************************************************************/
#pragma once

// Disable conversion warning of double to float
#pragma warning(disable:4244)
#pragma warning(disable:4201)

#include "MathVector2D.h"
#include "MathVector3D.h"
#include "MathVector4D.h"
#include "MathMatrix3D.h"
#include "MathMatrix4D.h"
#include "MathQuaternion.h"
#include "MathFunctions.h"
#include "MathMatrixOperation.h"
#include "MathVectorOperation.h"


// Other C Run Time
#include <immintrin.h>
#include <string>
#include <assert.h>


// Fast Inverse
extern float FastInvSquare(float x);
extern float FastSquare(float x);


// MATH RELATED DEFINES
#define TORADIAN               PI / 180.0f
#define TODEGREE               180.0f / PI
#define EPSILON                0.0005f
#define PI                     3.14159265358979323846f
#define HALFPI                 1.57079633f
#define TWOPI                  6.28318530718
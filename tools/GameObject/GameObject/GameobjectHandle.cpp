#include "pch.h"
#include "GameobjectHandle.h"
#include "GameobjectManager.h"

GameobjectHandle::GameobjectHandle()
  : m_flag(NULL), m_objReference((std::numeric_limits<unsigned>::max)())
{ }

GameobjectHandle::GameobjectHandle(char * _flag, const unsigned & _ref)
  : m_flag(_flag), m_objReference(_ref)
{ }

GameobjectHandle::~GameobjectHandle()
{
  DestroyInfo();
}

void GameobjectHandle::MoveGameobjectHandleOver(GameobjectHandle&& rRef)
{
  DestroyInfo();
  m_flag              = rRef.m_flag;
  m_objReference      = rRef.m_objReference;
  rRef.m_flag         = NULL;
  rRef.m_objReference = (std::numeric_limits<unsigned>::max)();

}

bool GameobjectHandle::IsValid() const
{
  return m_flag && (*m_flag & COMPONENT_VALID);
}

const unsigned& GameobjectHandle::GetID() const
{
  return m_objReference;
}

Gameobject* GameobjectHandle::GetData()
{
  return m_flag && (*m_flag & COMPONENT_VALID) ? GameobjectManager::GetInstance()->GetGameobject(m_objReference) : nullptr;
}

void GameobjectHandle::DestroyInfo()
{
  EntityTracker::GetInstance()->RemoveGameobjectReference(m_objReference);
  m_objReference = MAX_UNSIGNED;
  m_flag = nullptr;
}

/* Start Header
*****************************************************************/
/*!
\file      MathVectorOperation.h
\author   
\par      
\date     
\brief

Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the
prior written consent of DigiPen Institute of Technology is prohibited.
*/
/* End Header
*******************************************************************/
#pragma once
#include "MathLib.h"

////////////////
// VectorUtil //--------------------------------------------------------------
////////////////

#define MATH_OPS_COMPARE_EPSILON 0.0001f
#define MATH_OPS_FLOATCOMPARE(X, Y) (abs(X - Y) < MATH_OPS_COMPARE_EPSILON)

void  vec2Normalize(vec2 &rgResult, const vec2 &rgVector1);
float vec2Length(const vec2 &rgVector1);
float vec2SquareLength(const vec2 &rgVector1);
float vec2istance(const vec2 &rgVector1, const vec2 &rgVector2);
float vec2SquareDistance(const vec2 &rgVector1, const vec2 &rgVector2);
float vec2DotProduct(const vec2 &rgVector1, const vec2 &rgVector2);
float vec2CrossProduct(const vec2 &rgVector1, const vec2 &rgVector2);


void  vec3Normalize(vec3 &rgResult, const vec3 &rgVector1);
float vec3Length(const vec3 &rgVector1);
float vec3SquareLength(const vec3 &rgVector1);
float vec3istance(const vec3 &rgVector1, const vec3 &rgVector2);
float vec3SquareDistance(const vec3 &rgVector1, const vec3 &rgVector2);
float vec3DotProduct(const vec3 &rgVector1, const vec3 &rgVector2);
vec3 vec3CrossProduct(const vec3 &rgVector1, const vec3 &rgVector2);


vec4 constructPlane(const vec3& p0, const vec3& p1, const vec3& p2);
void  vec4Normalize(vec4 &rgResult, const vec4 &rgVector1);
float vec4Length(const vec4 &rgVector1);
float vec4SquareLength(const vec4 &rgVector1);
float vec4Distance(const vec4 &rgVector1, const vec4 &rgVector2);
float vec4SquareDistance(const vec4 &rgVector1, const vec4 &rgVector2);
float vec4DotProduct(const vec4 &rgVector1, const vec4 &rgVector2);
vec4 vec4CrossProduct(const vec4 &rgVector1, const vec4 &rgVector2);

vec3 LookAt(const vec3& pos, const vec3& dest, const vec3& up = vec3(0, 1 , 0));

bool vec2Compare(const vec2& lhs, const vec2& rhs);
bool vec3Compare(const vec3& lhs, const vec3& rhs);
bool vec4Compare(const vec4& lhs, const vec4& rhs);
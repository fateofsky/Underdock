#include "pch.h"
#include "GraphicsManager.h"
#include "Gameobject.h"
#include "Engine.h"
#include <thread>

GraphicsManager* GraphicsManager::m_instance = nullptr;

GraphicsManager * GraphicsManager::GetInstance()
{
  return m_instance == nullptr ? m_instance = new GraphicsManager() : m_instance;
}

void GraphicsManager::Destroy()
{
  // wait for GPU to idle before destroying vulkan
  vkDeviceWaitIdle(Engine::core::vDevice);

  // clear pipeline
  for (auto& pipeline : m_instance->m_vPipelineList)
    pipeline.Destroy();

  // clear texture
  for (auto& texture : m_instance->m_vTextureList)
    texture.DestroyImage();

  m_instance->m_vBuffer.DestroyBuffer();
  m_instance->m_iBuffer.DestroyBuffer();

  delete m_instance;
}

void GraphicsManager::LoadResources()
{
  MemoryStream resourceFile;
  resourceFile.FileRead(FileUtilityManager::GetResourcesFilePath());

  // texture load
  std::string numberStr;
  resourceFile.ReadAsCStyleText(numberStr);
  int numberOfTextures = std::stoi(numberStr);
  for (int i = 0; i < numberOfTextures; ++i)
  {
    std::string textureName;
    resourceFile.ReadAsCStyleText(textureName);
    VulkTexImage vulkImages;
    vulkImages.Load2DTextureImage(FileUtilityManager::GetImageFilePath(textureName).c_str());
    vulkImages.CreateShaderSampler(VK_FILTER_LINEAR, VK_FILTER_LINEAR, VK_SAMPLER_ADDRESS_MODE_REPEAT); // standard default
    m_vTextureList.push_back(vulkImages);
  }

  // model load
  resourceFile.ReadAsCStyleText(numberStr);
  int numberOfModels = std::stoi(numberStr);
  for (int i = 0; i < numberOfModels; ++i)
  {
    std::string modelName;
    resourceFile.ReadAsCStyleText(modelName);
    VulkUtils::_VulkModelInfo modelInfo;
    modelInfo.LoadModelBuffer_1(FileUtilityManager::GetModelFilePath(modelName).c_str());
    m_vModelInfoList.push_back(modelInfo);
  }

  // render target load
  resourceFile.ReadAsCStyleText(numberStr);
  int numberOfRenderTargets = std::stoi(numberStr);
  for (int i = 0; i < numberOfRenderTargets; ++i)
  {
    std::string renderTargetName;
    resourceFile.ReadAsCStyleText(renderTargetName);
    VulkRenderTarget rt;
    rt.LoadRenderTarget_1(FileUtilityManager::GetRendertargetFilePath(renderTargetName).c_str());
    m_vRenderTargetList.push_back(rt);
  }

  resourceFile.ReadAsCStyleText(numberStr);
  int numberOfPipeline = std::stoi(numberStr);
  for (int i = 0; i < numberOfPipeline; ++i)
  {
    std::string pipelineName;
    resourceFile.ReadAsCStyleText(pipelineName);

    std::string rtIndexStr;
    resourceFile.ReadAsCStyleText(rtIndexStr);
    int rtIndex = std::stoi(rtIndexStr);
    VulkRenderTarget* rtPtr = rtIndex == 0 ? nullptr : &m_vRenderTargetList[rtIndex-1];
    VulkPipeline pp;
    pp.LoadPipeline_1(FileUtilityManager::GetPipelineFilePath(pipelineName).c_str(), rtPtr);
    m_vPipelineList.push_back(pp);
  }

}

void GraphicsManager::Init()
{
  // Init vulkan images
  VkCommandBuffer cmd = Engine::utils::beginSingleTimeCommands();
  for (auto& tex : m_vTextureList)
    tex._InitializeImage(cmd);
  Engine::utils::endSingleTimeCommands(cmd);
  for (auto& tex : m_vTextureList)
    tex._Cleanup();

  // Init pipeline
  for (auto& pipeline : m_vPipelineList)
    pipeline.CompileRegisteredDescriptor();

  // Init model
  m_quadInfo.CreateQuad();
  m_vBufferStorage.CompileModel(m_vModelInfoList);
  _AllocateMainVertexAndIndexBuffer();
  //m_quadInfo = m_vModelInfoList.back();
  //m_vModelInfoList.pop_back();

  // build all renderables with the correct pointer
  std::vector<VkWriteDescriptorSet> globalWriteSetList;
  for (auto& renderable : m_allRenderables)
  {
    VulkPipeline& pipeline = m_vPipelineList[renderable->m_matPipelineIndex];
    VulkUtils::VulkDescSetLayout* pipelineLayout = pipeline.GetLayout(renderable->m_descSetNumber);
    VulkUtils::DescriptorSetInst& setInst = pipelineLayout->_setInstance[renderable->m_descInstanceNumber];
    uint8_t* memDataPtr = reinterpret_cast<uint8_t*>(pipeline.GetBufferMemory());

    unsigned blockIndex = 0;
    for (auto& block : renderable->m_interfaceBlockAccess)
    {
      unsigned swpChFrameIndex = 0;
      for (auto& ptr : block._uniformDataPtrList)
      {
        VulkRenderTarget* renderTargetPtr = (block._isAttachmentSampler) ? &m_vRenderTargetList[block._renderTargetRef] : nullptr;

        // compute write image Sampler
        if (block._size == 0)
        {
          const VulkTexImage& vTexImgObkj = (renderTargetPtr == nullptr) ?
                                             m_vTextureList[block._textureRef] : 
                                             renderTargetPtr->m_outputAttachmentPerFrame[swpChFrameIndex].m_vOutputAttachment[block._textureRef];
           VkSampler imgSampler = (renderTargetPtr == nullptr) ? vTexImgObkj.GetTextureImageSampler() : renderTargetPtr->GetAttachmentSampler();

          globalWriteSetList.push_back(
            pipelineLayout->ComputeWriteSet_CombineTextureSampler(setInst._set[swpChFrameIndex], vTexImgObkj.GetTextureImageView(), imgSampler, blockIndex)
          );
        }

        // compute write uniform buffer
        else
        {
          unsigned offset;
          ptr = pipelineLayout->ComputeMemoryLocation(memDataPtr, offset,
                                                      renderable->m_descInstanceNumber,
                                                      blockIndex, swpChFrameIndex);

          globalWriteSetList.push_back(
            pipelineLayout->ComputeWriteSet_UniformBuffer(setInst._set[swpChFrameIndex], pipeline.GetVulkBufferObj().GetBuffer(), offset, block._size, blockIndex)
          );
        }

        ++swpChFrameIndex;
      }

      ++blockIndex;
    }
  }

  // Write sets
  vkUpdateDescriptorSets(Engine::core::vDevice, (uint32_t)globalWriteSetList.size(), globalWriteSetList.data(), 0, NULL);

  // clear memory
  for (auto& writeSet : globalWriteSetList)
  {
    if (writeSet.pBufferInfo)
      delete writeSet.pBufferInfo;
    else if (writeSet.pImageInfo)
      delete writeSet.pImageInfo;
  }
}


bool GraphicsManager::RegisterMaterial(MaterialComponent* comp, VulkUtils::VulkRenderingTree& renderingTree)
{
  unsigned pipelineIndex = comp->GetMaterialPipelineIndex();
  unsigned descSetNumber = comp->GetDescriptorSetNumber();
  if (pipelineIndex < m_vPipelineList.size())
  {
    VulkPipeline* pipeline = &m_vPipelineList[pipelineIndex];
    comp->m_renderable.m_matPipelineIndex = pipelineIndex;
    comp->m_renderable.m_descSetNumber = descSetNumber;
    comp->m_renderable.m_descInstanceNumber = pipeline->RegisterDescriptor(descSetNumber);
    comp->m_renderable.m_rType = comp->m_type.GetData();

    VulkUtils::VulkDescSetLayout* layout = pipeline->GetLayout(descSetNumber);
    comp->m_renderable.m_interfaceBlockAccess.resize(layout->_setInterfaceBlocks.size());
    unsigned interfaceIndex = 0;
    for (auto& blockAccess : comp->m_renderable.m_interfaceBlockAccess)
    {
      blockAccess._uniformDataPtrList.resize(Engine::swpch::vNumSCImgs);
      blockAccess._size = layout->_setInterfaceBlocks[interfaceIndex]._vkBlockSize;
      
      // assign texture
      auto& texList = comp->m_textureList.GetWholeList();
      for (auto& texPair : texList)
      {
        if (texPair._interfaceBlockID == interfaceIndex)
        {
          blockAccess._textureRef = texPair._textureID;
          break;
        }
      }

      // assign sampler texture
      auto& attachmentList = comp->m_attachmentTextureList.GetWholeList();
      for (auto& texPair : attachmentList)
      {
        if (texPair._interfaceBlockID == interfaceIndex)
        {
          blockAccess._textureRef = texPair._textureID;
          blockAccess._renderTargetRef = texPair._renderTargetID;
          blockAccess._isAttachmentSampler = true;
          break;
        }
      }

      ++interfaceIndex;
    }

    m_allRenderables.push_back(&comp->m_renderable);

    // attached to render tree
    switch (comp->m_renderable.m_rType)
    {
    case Renderables::_ResourceType::GLOBAL:
      renderingTree.CreateGlobalNode(&comp->m_renderable);
      break;
    case Renderables::_ResourceType::SHARED:
      renderingTree.CreateSharedNode(&comp->m_renderable, comp->GetGlobalResourceTag());
      break;
    case Renderables::_ResourceType::INSTANCE:
      renderingTree.CreateInstanceNode(&comp->m_renderable, comp->GetGlobalResourceTag(), comp->GetSharedResourceTag());
      break;
    }

    return true;
  }
  else return false;
}


bool GraphicsManager::RegisterModel(ModelComponent* comp)
{
  ComponentHandle<MaterialComponent> matComp = comp->GetOwner()->GetFirstOfComponentHandle<MaterialComponent>();
  if (matComp.IsValid())
  {
    const unsigned& modelIndex = comp->GetModelIndex();
    if (modelIndex < m_vModelInfoList.size())
    {
      matComp.GetComponent()->m_renderable.m_modelIndex = modelIndex;
    }

    return true;
  }

  std::cout << "Error: Object doesnt have a Material Component attached.\n";
  return false;
}

const vec4& GraphicsManager::GetClearColor() const
{
  return m_clearColor;
}

void GraphicsManager::SetClearColor(const vec4& color)
{
  m_clearColor = color;
}

VulkBuffer& GraphicsManager::GetVulkVertexBuffer()
{
  return m_vBuffer;
}

VulkBuffer& GraphicsManager::GetVulkIndexBuffer()
{
  return m_iBuffer;
}

VulkBuffer& GraphicsManager::GetVulkQuadVertexBuffer()
{
  return m_quadVBuffer;
}

VulkBuffer& GraphicsManager::GetVulkQuadIndexBuffer()
{
  return m_quadIBuffer;
}

const VulkPipeline& GraphicsManager::GetPipeline(const unsigned& index) const
{
#ifdef _DEBUG
  if (index >= m_vPipelineList.size()) throw -1;
#endif // _DEBUG

  return m_vPipelineList[index];
}

VulkPipeline& GraphicsManager::GetPipeline(const unsigned& index)
{
#ifdef _DEBUG
  if (index >= m_vPipelineList.size()) throw -1;
#endif // _DEBUG

  return m_vPipelineList[index];
}

const VulkUtils::_VulkModelInfo& GraphicsManager::GetModelInfo(const unsigned& index) const
{
#ifdef _DEBUG
  if (index >= m_vModelInfoList.size()) throw -1;
#endif // _DEBUG

  return m_vModelInfoList[index];
}

VulkUtils::_VulkModelInfo& GraphicsManager::GetModelInfo(const unsigned& index)
{
#ifdef _DEBUG
  if (index >= m_vModelInfoList.size()) throw -1;
#endif // _DEBUG

  return m_vModelInfoList[index];
}

const VulkRenderTarget& GraphicsManager::GetRenderTarget(const unsigned& index) const
{
  return m_vRenderTargetList[index];
}

VulkRenderTarget& GraphicsManager::GetRenderTarget(const unsigned& index)
{
  return m_vRenderTargetList[index];
}


GraphicsManager::GraphicsManager()
  : m_vBuffer(), m_iBuffer()
{ }

void GraphicsManager::_AllocateMainVertexAndIndexBuffer()
{
  // create staging vertex buffer & index buffer
  VulkBuffer stagingVertexBuffer;
  VulkBuffer stagingIndexBuffer;
  stagingVertexBuffer.CreateBuffer((unsigned)m_vBufferStorage._fullMeshData.size(), 1,
                                   VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                                   VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
  stagingVertexBuffer.BindBufferMemory();
  stagingVertexBuffer.MapMemory(m_vBufferStorage._fullMeshData.data(), (unsigned)m_vBufferStorage._fullMeshData.size());
  stagingIndexBuffer.CreateBuffer((unsigned)m_vBufferStorage._fullIndicesData.size(), 1,
                                   VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                                   VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
  stagingIndexBuffer.BindBufferMemory();
  stagingIndexBuffer.MapMemory(m_vBufferStorage._fullIndicesData.data(), (unsigned)m_vBufferStorage._fullIndicesData.size());


  // create staging vertex buffer & index buffer
  VulkBuffer stagingQuadVBuffer;
  VulkBuffer stagingQuadIBuffer;
  stagingQuadVBuffer.CreateBuffer((unsigned)m_quadInfo._meshInfoList[0]._tmpMeshData.size(), 1,
                                   VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                                   VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
  stagingQuadVBuffer.BindBufferMemory();
  stagingQuadVBuffer.MapMemory(m_quadInfo._meshInfoList[0]._tmpMeshData.data(), (unsigned)m_quadInfo._meshInfoList[0]._tmpMeshData.size());
  stagingQuadIBuffer.CreateBuffer((unsigned)m_quadInfo._meshInfoList[0]._tmpIndicesData.size(), 1,
                                   VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                                   VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
  stagingQuadIBuffer.BindBufferMemory();
  stagingQuadIBuffer.MapMemory(m_quadInfo._meshInfoList[0]._tmpIndicesData.data(), (unsigned)m_quadInfo._meshInfoList[0]._tmpIndicesData.size());


  // Create GPU visible vertex and index buffer
  m_vBuffer.CreateBuffer((unsigned)m_vBufferStorage._fullMeshData.size(), 1,
                         VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                         VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
  m_vBuffer.BindBufferMemory();
  m_iBuffer.CreateBuffer((unsigned)m_vBufferStorage._fullIndicesData.size(), 1,
                         VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                         VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
  m_iBuffer.BindBufferMemory();

  // Create GPU visible quad buffer
  m_quadVBuffer.CreateBuffer((unsigned)m_quadInfo._meshInfoList[0]._tmpMeshData.size(), 1,
                             VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                             VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
  m_quadVBuffer.BindBufferMemory();
  m_quadIBuffer.CreateBuffer((unsigned)m_quadInfo._meshInfoList[0]._tmpIndicesData.size(), 1,
                         VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                         VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
  m_quadIBuffer.BindBufferMemory();

  // copy into GPU visible only buffer
  VkCommandBuffer cmd = Engine::utils::beginSingleTimeCommands();
  m_vBuffer.CopyBuffer(cmd, stagingVertexBuffer);
  m_iBuffer.CopyBuffer(cmd, stagingIndexBuffer);
  m_quadVBuffer.CopyBuffer(cmd, stagingQuadVBuffer);
  m_quadIBuffer.CopyBuffer(cmd, stagingQuadIBuffer);
  Engine::utils::endSingleTimeCommands(cmd);

  stagingVertexBuffer.DestroyBuffer();
  stagingIndexBuffer.DestroyBuffer();
  stagingQuadVBuffer.DestroyBuffer();
  stagingQuadIBuffer.DestroyBuffer();
}

void GraphicsManager::DrawMesh(const VkCommandBuffer& cmdBuf, const unsigned& indexSize, const unsigned& indexOffset, const unsigned& meshOffset)
{
  vkCmdDrawIndexed(cmdBuf, indexSize, 1, indexOffset, meshOffset, 0);
}

void GraphicsManager::DrawQuad(const VkCommandBuffer& cmdBuf)
{
  vkCmdDrawIndexed(cmdBuf, m_quadInfo._meshInfoList[0]._indexSize, 1, 0, 0, 0);
}

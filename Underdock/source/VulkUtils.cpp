#include "../pch.h"
#include "VulkUtils.h"
#include "Engine.h"

VulkUtils::DescriptorSetInst::DescriptorSetInst()
  :_set(Engine::swpch::vNumSCImgs)
{ }

void VulkUtils::DescriptorSetInst::BindDescriptorSet_Graphics(const unsigned& curFrame, const unsigned& setNumber, VkPipelineLayout pLayout)
{
  vkCmdBindDescriptorSets(Engine::swpch::vCmdBuffVec[curFrame], VK_PIPELINE_BIND_POINT_GRAPHICS,
    pLayout, setNumber, 1, &_set[curFrame], 0, NULL);
}

VulkUtils::VulkDescSetLayout::VulkDescSetLayout()
  : _refCount(0), _setInstance(), _setInterfaceBlocks()
{ }

uint8_t* VulkUtils::VulkDescSetLayout::ComputeMemoryLocation(uint8_t* bufMem, unsigned& offsetCompute, const unsigned& instIndex,
                                                             const unsigned& blockID, const unsigned& frameIndex)
{
  // compute blockStride offset
  unsigned blockStrideBasedOnBlockID = 0;
  unsigned blockCountId = 0;
  for (auto& block : _setInterfaceBlocks)
  {
    if (blockCountId == blockID) break;
    blockStrideBasedOnBlockID += block._vkTrueBlockStride;
    ++blockCountId;
  }

  offsetCompute = _setStartingOffset + (_trueSetStride * instIndex) + blockStrideBasedOnBlockID;
  offsetCompute += frameIndex * _setInterfaceBlocks[blockID]._vkBlockStride;
  return bufMem + offsetCompute;
}

VkWriteDescriptorSet VulkUtils::VulkDescSetLayout::ComputeWriteSet_UniformBuffer(VkDescriptorSet vSet, VkBuffer vBuf, VkDeviceSize offset, VkDeviceSize range, uint32_t binding)
{
  VkDescriptorBufferInfo* uniBufDesInfoPtr = new VkDescriptorBufferInfo();
  uniBufDesInfoPtr->buffer = vBuf;
  uniBufDesInfoPtr->offset = offset;
  uniBufDesInfoPtr->range = range;

  VkWriteDescriptorSet writeSet;
  writeSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
  writeSet.dstSet = vSet;
  writeSet.descriptorCount = 1;
  writeSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
  writeSet.pBufferInfo = uniBufDesInfoPtr;
  writeSet.dstBinding = binding;
  writeSet.dstArrayElement = 0;
  writeSet.pNext = NULL;
  writeSet.pTexelBufferView = NULL;
  writeSet.pImageInfo = NULL;

  return writeSet;
}


VkWriteDescriptorSet VulkUtils::VulkDescSetLayout::ComputeWriteSet_CombineTextureSampler(VkDescriptorSet vSet, VkImageView vImgView, VkSampler vImgSampler, uint32_t binding)
{
  VkDescriptorImageInfo* imgBufDesInfoPtr = new VkDescriptorImageInfo();
  imgBufDesInfoPtr->imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
  imgBufDesInfoPtr->imageView = vImgView;
  imgBufDesInfoPtr->sampler = vImgSampler;

  VkWriteDescriptorSet writeSet;
  writeSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
  writeSet.dstSet = vSet;
  writeSet.descriptorCount = 1;
  writeSet.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
  writeSet.dstBinding = binding;
  writeSet.dstArrayElement = 0;
  writeSet.pNext = NULL;
  writeSet.pTexelBufferView = NULL;
  writeSet.pBufferInfo = NULL;
  writeSet.pImageInfo = imgBufDesInfoPtr;

  return writeSet;
}

VulkUtils::DescriptorSetInst& VulkUtils::VulkDescSetLayout::GetSetInstance(const unsigned& instanceID)
{
#ifdef _DEBUG
  if (instanceID >= _setInstance.size())
  {
    assert(false);
    return _setInstance[instanceID];
  }
#endif // _DEBUG

  return _setInstance[instanceID];
}


VkShaderStageFlagBits VulkUtils::DetermineShaderStage(VulkUtils::ShaderStage stage)
{
  switch (stage)
  {
  case VulkUtils::VERTEX:
    return VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT;

  case VulkUtils::TESSELLATION_CTRL:
    return VkShaderStageFlagBits::VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;

  case VulkUtils::TESSELLATION_EVAL:
    return VkShaderStageFlagBits::VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;

  case VulkUtils::GEOMETRY:
    return VkShaderStageFlagBits::VK_SHADER_STAGE_GEOMETRY_BIT;

  case VulkUtils::FRAGMENT:
    return VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT;

  case VulkUtils::COMPUTE:
    return VkShaderStageFlagBits::VK_SHADER_STAGE_COMPUTE_BIT;

    /*
      NVIDIA SPECIFICS
    */
  case VulkUtils::RAYGEN:
    return VkShaderStageFlagBits::VK_SHADER_STAGE_RAYGEN_BIT_NV;

  case VulkUtils::ANY_HIT:
    return VkShaderStageFlagBits::VK_SHADER_STAGE_ANY_HIT_BIT_NV;

  case VulkUtils::CLOSEST_HIT:
    return VkShaderStageFlagBits::VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV;

  case VulkUtils::MISS:
    return VkShaderStageFlagBits::VK_SHADER_STAGE_MISS_BIT_NV;

  case VulkUtils::INTERSECTION:
    return VkShaderStageFlagBits::VK_SHADER_STAGE_INTERSECTION_BIT_NV;

  case VulkUtils::CALLABLE:
    return VkShaderStageFlagBits::VK_SHADER_STAGE_CALLABLE_BIT_NV;

  case VulkUtils::TASK:
    return VkShaderStageFlagBits::VK_SHADER_STAGE_TASK_BIT_NV;

  case VulkUtils::MESH:
    return VkShaderStageFlagBits::VK_SHADER_STAGE_MESH_BIT_NV;

  default:
    return VkShaderStageFlagBits::VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM;
  }
}

unsigned VulkUtils::ComputeSizeOfVkBaseType(VK_BASE_TYPE baseType)
{
  switch (baseType)
  {
  case VulkUtils::NONE_TYPE:
    return 0;

  case VulkUtils::INT_TYPE:
    return sizeof(int);

  case VulkUtils::INT4_TYPE:
    return sizeof(int) << 2;

  case VulkUtils::FLOAT_TYPE:
    return sizeof(float);

  case VulkUtils::BOOL_TYPE:
    return sizeof(bool);

  case VulkUtils::VEC2_TYPE:
    return sizeof(float) << 1;

  case VulkUtils::VEC3_TYPE:
    return sizeof(float) * 3;

  case VulkUtils::VEC4_TYPE:
    return sizeof(float) << 2;

  case VulkUtils::MAT3_TYPE:
    return sizeof(float) * 9;

  case VulkUtils::MAT4_TYPE:
    return sizeof(float) << 4;
  }

  return 0;
}

void VulkUtils::_VulkModelInfo::LoadModelBuffer_1(const std::string & file)
{
  MemoryStream inFile;
  if (!inFile.FileRead(file)) return;

  size_t sizeOfMesh;
  inFile.Read(&sizeOfMesh);
  _meshInfoList.resize(sizeOfMesh);

  for (size_t i = 0; i < _meshInfoList.size(); ++i)
  {
    // vertex size
    inFile.Read(&_meshInfoList[i]._vertexSize);

    size_t vbDataSize;
    inFile.Read(&vbDataSize);
    _meshInfoList[i]._tmpMeshData.resize(vbDataSize);
    inFile.Read(_meshInfoList[i]._tmpMeshData.data(), vbDataSize);

    size_t vbIndexSize;
    inFile.Read(&vbIndexSize);
    _meshInfoList[i]._tmpIndicesData.resize(vbIndexSize);
    inFile.Read(_meshInfoList[i]._tmpIndicesData.data(), vbIndexSize);
    _meshInfoList[i]._indexSize = (unsigned)_meshInfoList[i]._tmpIndicesData.size() / sizeof(unsigned);
  }
}

void VulkUtils::_VulkModelInfo::CreateQuad()
{
  const float quadVertices[]
  {
    // positions                // texture Coords
    -1.f, -1.f, 0.f, 1.f,    0.f, 0.f,
     1.f, -1.f, 0.f, 1.f,    1.f, 0.f,
     1.f,  1.f, 0.f, 1.f,    1.f, 1.f,
    -1.f,  1.f, 0.f, 1.f,    0.f, 1.f,
  };

  const unsigned quadIndex[]
  {
    0, 1, 2,
    0, 2, 3
  };

  _meshInfoList.resize(1);

  const size_t quadSize = sizeof(quadVertices);
  _meshInfoList[0]._tmpMeshData.resize(quadSize);
  memcpy_s(_meshInfoList[0]._tmpMeshData.data(), quadSize, quadVertices, quadSize);

  const size_t indexSize = sizeof(quadIndex);
  _meshInfoList[0]._tmpIndicesData.resize(indexSize);
  memcpy_s(_meshInfoList[0]._tmpIndicesData.data(), indexSize, quadIndex, indexSize);

  _meshInfoList[0]._indexSize = 6;
  _meshInfoList[0]._vertexSize = 4;
}

void VulkUtils::VulkVBStorage::CompileModel(std::vector<VulkUtils::_VulkModelInfo>& modelList)
{
  // compute total size required for index and mesh buffer
  unsigned totalMeshAllocSize = 0;
  unsigned totalIndicesAllocSize = 0;
  unsigned meshVertexOffset = 0;
  unsigned meshIndexOffset = 0;
  for (auto& model : modelList)
  {
    for (auto& mesh : model._meshInfoList)
    {
      mesh._meshOffset       = meshVertexOffset;
      mesh._indicesOffset    = meshIndexOffset;
      totalMeshAllocSize    += (unsigned)mesh._tmpMeshData.size();    //_computedAllocMeshSize;
      totalIndicesAllocSize += (unsigned)mesh._tmpIndicesData.size(); // mesh._computedAllocIndicesSize;

      meshVertexOffset      += mesh._vertexSize;
      meshIndexOffset       += mesh._indexSize;
    }
  }

  // extract data from mesh + update it offset based on correct alignment
  _fullMeshData.resize(totalMeshAllocSize, 0);
  _fullIndicesData.resize(totalIndicesAllocSize, 0);

  // **thread here if you want
  size_t VertexDataOffset = 0;
  size_t IndexDataOffset = 0;
  for (auto& model : modelList)
  {
    for (auto& mesh : model._meshInfoList)
    {
      memcpy_s(_fullMeshData.data() + VertexDataOffset, mesh._tmpMeshData.size(), mesh._tmpMeshData.data(), mesh._tmpMeshData.size());
      memcpy_s(_fullIndicesData.data() + IndexDataOffset, mesh._tmpIndicesData.size(), mesh._tmpIndicesData.data(), mesh._tmpIndicesData.size());

      VertexDataOffset += mesh._tmpMeshData.size();
      IndexDataOffset += mesh._tmpIndicesData.size();
    }
  }
}

void VulkUtils::VulkRenderingTree::CreateGlobalNode(Renderables* rend)
{
  _globalNodes.push_back(_GlobalNode{ rend });
}

void VulkUtils::VulkRenderingTree::CreateSharedNode(Renderables* rend, const unsigned& globalIndex)
{
  // Find global node
  _GlobalNode* gb;
  if (globalIndex >= _globalNodes.size())
  {
    _globalNodes.push_back(_GlobalNode{ nullptr });
#ifdef _DEBUG
    std::cout << "Warning: No global node found with the index " << globalIndex << std::endl;
#endif // _DEBUG

    gb = &_globalNodes.back();
  }
  else gb = &_globalNodes[globalIndex];
  
  gb->_sharedNodes.push_back(_GlobalNode::_SharedNode{ rend });
}

void VulkUtils::VulkRenderingTree::CreateInstanceNode(Renderables* rend, const unsigned& globalIndex, const unsigned& sharedIndex)
{
  // Find global node
  _GlobalNode* gb;
  if (globalIndex >= _globalNodes.size())
  {
    _globalNodes.push_back(_GlobalNode{ nullptr });
#ifdef _DEBUG
    std::cout << "Info: No global node found with the index " << globalIndex << ". Creating empty global node" << std::endl;
#endif // _DEBUG

    gb = &_globalNodes.back();
  }
  else gb = &_globalNodes[globalIndex];

  // Find shared node
  _GlobalNode::_SharedNode* sh;
  if (sharedIndex >= gb->_sharedNodes.size())
  {
    gb->_sharedNodes.push_back(_GlobalNode::_SharedNode{ nullptr });
#ifdef _DEBUG
    std::cout << "Info: No shared node found with the index " << sharedIndex << ". Creating empty shared node" << std::endl;
#endif // _DEBUG

    sh = &gb->_sharedNodes.back();
  }
  else sh = &gb->_sharedNodes[sharedIndex];

  sh->_instanceNodes.push_back(_GlobalNode::_SharedNode::_InstanceNode{ rend });
}

/* Start Header
*****************************************************************/
/*!
\file      MathVector2D.h
\author    
\par       
\date      
\brief

Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the
prior written consent of DigiPen Institute of Technology is prohibited.
*/
/* End Header
*******************************************************************/
#pragma once
#include "MathLib.h"
#include <string>

union vec3;
union vec4;

///////////
// vec2 //--------------------------------------------------------------
///////////
union vec2
{
	struct
	{
		float x_;
		float y_;
	};
	

	float ar[2];

	vec2(int x, int y);
	vec2(float x = 0.0f, float y = 0.0f);
	vec2(const vec3& v);
	vec2(const vec4& v);
	vec2(const std::string& str);

	operator std::string() const;
	vec2& operator+=(const vec2& b);
	vec2& operator-=(const vec2& b);
	vec2& operator*=(const float& s);
	vec2& operator*=(const vec2& b);
	vec2& operator/=(const float& s);
	vec2& operator/=(const vec2& b);
	vec2& operator=(const float rhs[]);
	vec2& operator++();
	vec2 operator++(int);
	vec2& operator--();
	vec2 operator--(int);
	float& operator[](int i);
	const float& operator[](int i) const;

	// Properties
	float magnitude() const;
	float sqrMagnitude() const;
	vec2 normalized() const;

	// Method
	void Normalize();
};

 vec2 operator+(const vec2& a, const vec2& b);
 vec2 operator+(const vec2& a);
 vec2 operator-(const vec2& a, const vec2& b);
 vec2 operator-(const vec2& a);
 vec2 operator*(const vec2& a, const float& s);
 vec2 operator*(const float& s, const vec2& a);
 vec2 operator*(const vec2& a, const vec2& b);
 vec2 operator/(const vec2& a, const float& s);
 vec2 operator/(const vec2& a, const vec2& b);
 vec2 operator%(const vec2& a, const float& s);
 std::ostream & operator<<(std::ostream & os, const vec2 & rhs);
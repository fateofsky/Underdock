#include "pch.h"
#include "FileUtilityManager.h"

const std::string FileUtilityManager::m_imageLoc        = "data/images/";
const std::string FileUtilityManager::m_modelLoc        = "data/models/";
const std::string FileUtilityManager::m_pipelineLoc     = "data/pipelines/";
const std::string FileUtilityManager::m_rendertargetLoc = "data/rendertargets/";

std::string FileUtilityManager::GetResourcesFilePath()
{
  return "data/Resources.txt";
}

std::string FileUtilityManager::GetImageFilePath(const std::string& imageFile)
{
  return m_imageLoc + imageFile;
}

std::string FileUtilityManager::GetModelFilePath(const std::string& modelFile)
{
  return m_modelLoc + modelFile;
}

std::string FileUtilityManager::GetPipelineFilePath(const std::string& pipelineFile)
{
  return m_pipelineLoc + pipelineFile;
}

std::string FileUtilityManager::GetRendertargetFilePath(const std::string& rtFile)
{
  return m_rendertargetLoc + rtFile;
}

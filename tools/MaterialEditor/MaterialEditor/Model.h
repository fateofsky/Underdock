#pragma once
#include <vector>
#include <map>
#include "glm/glm.hpp"


struct Faces
{
  unsigned index[3];
  glm::vec3 fNormal;
};


struct BoneWeight
{
  float weight[4];
};

struct BoneIndex
{
  unsigned index[4];
};

struct Mesh
{
  // bufferable properties
  std::vector<glm::vec4>   vertices;
  std::vector<glm::vec4>   vNormals;
  std::vector<glm::vec4>   vTangent;
  std::vector<glm::vec4>   vBiTangent;
  std::vector<BoneIndex>   boneIndex;
  std::vector<BoneWeight>  boneWeight;
  std::map<unsigned, std::vector<glm::vec2>> uvCoordSets;

  // misc properties
  std::vector<Faces>       faces;
  unsigned numberOfUvs;
};

struct NodeHierarchy
{
  unsigned            m_nodeIndex = 0;
  glm::mat4           m_transform;
  std::vector<Mesh>   m_meshes;
  std::vector<NodeHierarchy> m_children;
};

struct Model
{
  NodeHierarchy m_root;
};
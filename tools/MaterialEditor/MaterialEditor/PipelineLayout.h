#pragma once
#include <vulkan/vulkan.h>
#include <map>
#include "Shader.h"
#include "UniformGroup.h"
#include "Texture.h"
#include "CommonVBLayout.h"
#include "LayoutMessages.h"

using ResourcesMap    = std::map<std::string, UniformResourceBase*>;
using TextureMap      = std::map<std::string, Texture*>;
using UniformGroupMap = std::map<std::string, UniformGroup>;
class PipelineLayout
{
  struct _pipelineStage
  {
    std::vector<OGL_BASETYPE> baseInput;
    std::vector<OGL_BASETYPE> baseOutput;
    TextureMap                inputTextures;    // sampler2D
    ResourcesMap              inputUniforms;    // uniform data
    UniformGroupMap           uniformGroups;
    std::string               shaderMainCode;

    void DestroyUniformData();
  };

public:
  PipelineLayout();
  void Destroy();
  int AddStages(Shader::ShaderType shaderStage);
  int AddVertexInputToVertexStage(const vBufferLayout& layout); // ADDING WILL REPLACE THE EXISTING ONE IF ANY
  int AddInputToStage(Shader::ShaderType shaderStage, OGL_BASETYPE type);
  int AddOutputToStage(Shader::ShaderType shaderStage, OGL_BASETYPE type);
  int AddUniformToStage(const std::string& name, Shader::ShaderType shaderStage, UniformResourceBase* base);
  int AddTextureToStage(const std::string& name, Shader::ShaderType shaderStage, Texture* tex);
  int AddShaderCodeToStage(Shader::ShaderType shaderStage, const std::string& sdrCode);

  bool UniformExist(const std::string name);
  bool TextureExist(const std::string name);
  void RenameUniform(const std::string oldName, const std::string newName);

  int RemoveStages(Shader::ShaderType shaderStage);
  int RemoveInputFromStage(Shader::ShaderType shaderStage, const int& index);
  int RemoveOutputFromStage(Shader::ShaderType shaderStage, const int& index);
  int RemoveUniform(const std::string& name);
  int RemoveTexture(const std::string& name);

  void BindPipeline(const std::map<std::string, GLint>& nameMapBind);
  void UnbindPipeline();

  bool FindNextStage(Shader::ShaderType currentStage, Shader::ShaderType& result);
  bool FindPreviousStage(Shader::ShaderType currentStage, Shader::ShaderType& result);

  int AddToGroup(Shader::ShaderType stage, const std::string& groupName, 
                 const std::string& uniformName, unsigned group, UniformGroup::_MemoryLayout mLayout);
  int RemoveFromGroup(Shader::ShaderType stage, const std::string& groupName, const std::string& uniformName);
  int RenameUniformGroup(Shader::ShaderType stage, const std::string& groupName, const std::string& newGroupName);
  int RemoveGroup(Shader::ShaderType stage, const std::string& groupName);
  
  void ChangeHasDepth(bool target);
  bool GetHasDepth() const;

  using pipelineStageMap = std::map<Shader::ShaderType, _pipelineStage>;
  using _pipelineStage = _pipelineStage;
  pipelineStageMap& GetPipelineStageMap();
  UniformResourceBase* GetUniform(const std::string& name);

  // Vulkan API Usage
  VkPipelineInputAssemblyStateCreateInfo& GetInputAssemblyInfo();
  VkPipelineRasterizationStateCreateInfo& GetRasterizationStateInfo();
  VkPipelineColorBlendStateCreateInfo&    GetColorBlendAttStateInfo();
  VkPipelineDepthStencilStateCreateInfo&  GetDepthStencilStateInfo();

  std::vector<VkDynamicState>&                      GetDynamicStateList();
  std::vector<VkPipelineColorBlendAttachmentState>& GetColorBlendAttStateList();
  static VkPipelineColorBlendAttachmentState CreateDefaultColorBlendAttachmentState();

  std::vector<VkFormat> ComputeListOfVkFormats(const std::vector<OGL_BASETYPE>& oglTypeList);

  void AddDependencies();
  void RemoveDependencies(const unsigned& index);
  const std::vector<VkSubpassDependency>& GetDependenciesList() const;
  std::vector<VkSubpassDependency>& GetDependenciesList_Writable();
  bool GetEnableDepthToTexture() const;
  void SetEnableDepthToTexture(bool state);

  const std::vector<VkFormat>& GetMRTOutputList() const;
  std::vector<VkFormat>& GetMRTOutputList_Writable();
  
  const std::vector<VkImageUsageFlags>& GetMRTOutputUsageList() const;
  std::vector<VkImageUsageFlags>& GetMRTOutputUsageList_Writable();

  void SetMessageFlag(const unsigned& toSet);
  bool TestMessageFlag(const unsigned& toTest);

  int* GetRenderTargetDimension();
  void SetRenderTargetDimension(int width, int height);

private:
  unsigned                                     m_flags;
  int                                          m_renderTargetDimension[2];
  bool                                         m_hasDepth;
  bool                                         m_enableDepthToTexture; // create depth sampler + finalLayout to shader read only

  // Vulkan raster info
  VkPipelineInputAssemblyStateCreateInfo           m_inputAssemblyCreateInfo;
  VkPipelineRasterizationStateCreateInfo           m_rasterCreateInfo;
  VkPipelineColorBlendStateCreateInfo              m_cBlendStateCreateInfo;
  VkPipelineDepthStencilStateCreateInfo            m_depthStencilStateCreateInfo;
  
  std::map<Shader::ShaderType, _pipelineStage>     m_pipelineStages;
  std::vector<VkDynamicState>                      m_dynamicStateList;
  std::vector<VkPipelineColorBlendAttachmentState> m_cBlendAttStateList;
  std::vector<VkSubpassDependency>                 m_dependencies;
  std::vector<VkFormat>                            m_mrtOutputFormat;
  std::vector<VkImageUsageFlags>                   m_mrtOutputUsage;

  // render pass info
  
  // viewport and multisample default
};
#pragma once
#include "EntityTracker.h"
#include "Gameobject.h"

class Gameobject;

class GameobjectHandle
{
  friend class Gameobject;

  public:
    GameobjectHandle();
    GameobjectHandle(char* _flag, const unsigned& _ref);
    ~GameobjectHandle();

    void MoveGameobjectHandleOver(GameobjectHandle&& rRef);
    bool IsValid() const;
    const unsigned& GetID() const;
    Gameobject* GetData();
    void DestroyInfo();

  private:
    GameobjectHandle(const GameobjectHandle&) = delete;    // non construction-copyable
    GameobjectHandle& operator=(const GameobjectHandle&) = delete;

    char*    m_flag;          // validity flag
    unsigned m_objReference;  // reference to gameobject ID
};
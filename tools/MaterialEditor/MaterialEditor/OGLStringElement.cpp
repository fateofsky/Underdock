#include "pch.h"
#include "OGLStringElement.h"

const std::vector<oglStrElem::itemPair<OGL_BASETYPE>> oglStrElem::dataTypeItems
{ 
  oglStrElem::itemPair<OGL_BASETYPE>{ "int",   OGL_BASETYPE::INT_TYPE   },
  oglStrElem::itemPair<OGL_BASETYPE>{ "int4",  OGL_BASETYPE::INT4_TYPE  },
  oglStrElem::itemPair<OGL_BASETYPE>{ "float", OGL_BASETYPE::FLOAT_TYPE },
  oglStrElem::itemPair<OGL_BASETYPE>{ "bool",  OGL_BASETYPE::BOOL_TYPE  },
  oglStrElem::itemPair<OGL_BASETYPE>{ "vec2",  OGL_BASETYPE::VEC2_TYPE  },
  oglStrElem::itemPair<OGL_BASETYPE>{ "vec3",  OGL_BASETYPE::VEC3_TYPE  },
  oglStrElem::itemPair<OGL_BASETYPE>{ "vec4",  OGL_BASETYPE::VEC4_TYPE  },
  oglStrElem::itemPair<OGL_BASETYPE>{ "mat3",  OGL_BASETYPE::MAT3_TYPE  },
  oglStrElem::itemPair<OGL_BASETYPE>{ "mat4",  OGL_BASETYPE::MAT4_TYPE  }
};
const std::vector<oglStrElem::itemPair<UNIFORM_USAGE>> oglStrElem::uniformUsageItems
{
  oglStrElem::itemPair<UNIFORM_USAGE>{ "Camera View",              UNIFORM_USAGE::CAM_VIEW     },
  oglStrElem::itemPair<UNIFORM_USAGE>{ "Camera Projection",        UNIFORM_USAGE::CAM_PROJ     },
  oglStrElem::itemPair<UNIFORM_USAGE>{ "Camera View Projection",   UNIFORM_USAGE::CAM_VIEWPROJ },
  oglStrElem::itemPair<UNIFORM_USAGE>{ "Transformation",           UNIFORM_USAGE::TRANSFORM    },
  oglStrElem::itemPair<UNIFORM_USAGE>{ "User Input",               UNIFORM_USAGE::USER_INPUT   },
  oglStrElem::itemPair<UNIFORM_USAGE>{ "Driver Time",              UNIFORM_USAGE::DRIVER_TIME  }
};
const std::vector<oglStrElem::itemPair<Shader::ShaderType>> oglStrElem::uniformStageItems
{
  oglStrElem::itemPair<Shader::ShaderType>{ "Vertex",                  Shader::ShaderType::VERTEXSDR   },
  oglStrElem::itemPair<Shader::ShaderType>{ "Tessellation Control",    Shader::ShaderType::TESSCTRLSDR },
  oglStrElem::itemPair<Shader::ShaderType>{ "Tessellation Evaluation", Shader::ShaderType::TESSEVALSDR },
  oglStrElem::itemPair<Shader::ShaderType>{ "Geometry",                Shader::ShaderType::GEOMSDR     },
  oglStrElem::itemPair<Shader::ShaderType>{ "Fragment",                Shader::ShaderType::FRAGSDR     }
};

const std::vector<oglStrElem::itemPair<Shader::ShaderType>> oglStrElem::uniformStageExtItems
{
  oglStrElem::itemPair<Shader::ShaderType>{ ".vert",    Shader::ShaderType::VERTEXSDR   },
  oglStrElem::itemPair<Shader::ShaderType>{ ".tesc",    Shader::ShaderType::TESSCTRLSDR },
  oglStrElem::itemPair<Shader::ShaderType>{ ".tese",    Shader::ShaderType::TESSEVALSDR },
  oglStrElem::itemPair<Shader::ShaderType>{ ".geom",    Shader::ShaderType::GEOMSDR     },
  oglStrElem::itemPair<Shader::ShaderType>{ ".frag",    Shader::ShaderType::FRAGSDR     }
};

const std::vector<oglStrElem::itemPair<vBufferLayout::vBufferLayoutType>> oglStrElem::bufferVertexInputItems
{
  oglStrElem::itemPair<vBufferLayout::vBufferLayoutType>{ "Vertex Position",     vBufferLayout::vBufferLayoutType::VERTEX_POS         },
  oglStrElem::itemPair<vBufferLayout::vBufferLayoutType>{ "Vertex Normal",       vBufferLayout::vBufferLayoutType::VERTEX_NORMAL      },
  oglStrElem::itemPair<vBufferLayout::vBufferLayoutType>{ "Vertex Tangent",      vBufferLayout::vBufferLayoutType::VERTEX_TANGENT     },
  oglStrElem::itemPair<vBufferLayout::vBufferLayoutType>{ "Vertex Bitangent",    vBufferLayout::vBufferLayoutType::VERTEX_BITANGENT   },
  oglStrElem::itemPair<vBufferLayout::vBufferLayoutType>{ "Vertex Bone ID",      vBufferLayout::vBufferLayoutType::VERTEX_BONE_ID     },
  oglStrElem::itemPair<vBufferLayout::vBufferLayoutType>{ "Vertex Bone Weight",  vBufferLayout::vBufferLayoutType::VERTEX_BONE_WEIGHT },
  oglStrElem::itemPair<vBufferLayout::vBufferLayoutType>{ "UV Coord 0",          vBufferLayout::vBufferLayoutType::TEX_COORDUV0       },
  oglStrElem::itemPair<vBufferLayout::vBufferLayoutType>{ "UV Coord 1",          vBufferLayout::vBufferLayoutType::TEX_COORDUV1       },
  oglStrElem::itemPair<vBufferLayout::vBufferLayoutType>{ "UV Coord 2",          vBufferLayout::vBufferLayoutType::TEX_COORDUV2       },
  oglStrElem::itemPair<vBufferLayout::vBufferLayoutType>{ "UV Coord 3",          vBufferLayout::vBufferLayoutType::TEX_COORDUV3       }
};


const std::vector<oglStrElem::itemPair<int>> oglStrElem::integerItems
{
  oglStrElem::itemPair<int>{ "0",   0  },
  oglStrElem::itemPair<int>{ "1",   1  },
  oglStrElem::itemPair<int>{ "2",   2  },
  oglStrElem::itemPair<int>{ "3",   3  },
  oglStrElem::itemPair<int>{ "4",   4  },
  oglStrElem::itemPair<int>{ "5",   5  },
  oglStrElem::itemPair<int>{ "6",   6  },
  oglStrElem::itemPair<int>{ "7",   7  },
  oglStrElem::itemPair<int>{ "8",   8  },
  oglStrElem::itemPair<int>{ "9",   9  },
  oglStrElem::itemPair<int>{ "10",  10 },
  oglStrElem::itemPair<int>{ "11",  11 },
  oglStrElem::itemPair<int>{ "12",  12 },
  oglStrElem::itemPair<int>{ "13",  13 },
  oglStrElem::itemPair<int>{ "14",  14 },
  oglStrElem::itemPair<int>{ "15",  15 },
  oglStrElem::itemPair<int>{ "16",  16 },
  oglStrElem::itemPair<int>{ "17",  17 },
  oglStrElem::itemPair<int>{ "18",  18 },
  oglStrElem::itemPair<int>{ "19",  19 },
  oglStrElem::itemPair<int>{ "20",  20 },
  oglStrElem::itemPair<int>{ "21",  21 },
  oglStrElem::itemPair<int>{ "22",  22 },
  oglStrElem::itemPair<int>{ "23",  23 },
  oglStrElem::itemPair<int>{ "24",  24 },
  oglStrElem::itemPair<int>{ "25",  25 },
  oglStrElem::itemPair<int>{ "26",  26 },
  oglStrElem::itemPair<int>{ "27",  27 },
  oglStrElem::itemPair<int>{ "28",  28 },
  oglStrElem::itemPair<int>{ "29",  29 },
  oglStrElem::itemPair<int>{ "30",  30 },
  oglStrElem::itemPair<int>{ "31",  31 }
};

const std::vector<oglStrElem::itemPair<UniformGroup::_MemoryLayout>> oglStrElem::uniMemLayoutItems
{
  oglStrElem::itemPair<UniformGroup::_MemoryLayout>{ "None",   UniformGroup::_MemoryLayout::NONE   },
  oglStrElem::itemPair<UniformGroup::_MemoryLayout>{ "std140", UniformGroup::_MemoryLayout::STD140 },
  oglStrElem::itemPair<UniformGroup::_MemoryLayout>{ "std430", UniformGroup::_MemoryLayout::STD430 }
};

const std::vector<oglStrElem::itemPair<VkDynamicState>> oglStrElem::dynamicStateItems
{
  oglStrElem::itemPair<VkDynamicState>{ "Viewport",              VkDynamicState::VK_DYNAMIC_STATE_VIEWPORT },
  oglStrElem::itemPair<VkDynamicState>{ "Scissor",               VkDynamicState::VK_DYNAMIC_STATE_SCISSOR },
  oglStrElem::itemPair<VkDynamicState>{ "Line Width",            VkDynamicState::VK_DYNAMIC_STATE_LINE_WIDTH },
  oglStrElem::itemPair<VkDynamicState>{ "Depth Bias",            VkDynamicState::VK_DYNAMIC_STATE_DEPTH_BIAS },
  oglStrElem::itemPair<VkDynamicState>{ "Blend Constants",       VkDynamicState::VK_DYNAMIC_STATE_BLEND_CONSTANTS },
  oglStrElem::itemPair<VkDynamicState>{ "Depth Bound",           VkDynamicState::VK_DYNAMIC_STATE_DEPTH_BOUNDS },
  oglStrElem::itemPair<VkDynamicState>{ "Stencil Compare Mask",  VkDynamicState::VK_DYNAMIC_STATE_STENCIL_COMPARE_MASK },
  oglStrElem::itemPair<VkDynamicState>{ "Stencil Write Mask",    VkDynamicState::VK_DYNAMIC_STATE_STENCIL_WRITE_MASK },
  oglStrElem::itemPair<VkDynamicState>{ "Stencil Reference",     VkDynamicState::VK_DYNAMIC_STATE_STENCIL_REFERENCE }
};

const std::vector<oglStrElem::itemPair<VkPrimitiveTopology>> oglStrElem::primitiveTopItems
{
  oglStrElem::itemPair<VkPrimitiveTopology>{ "Point List",                  VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_POINT_LIST },
  oglStrElem::itemPair<VkPrimitiveTopology>{ "Line List",                   VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_LINE_LIST },
  oglStrElem::itemPair<VkPrimitiveTopology>{ "Line Strip",                  VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_LINE_STRIP },
  oglStrElem::itemPair<VkPrimitiveTopology>{ "Triangle List",               VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST },
  oglStrElem::itemPair<VkPrimitiveTopology>{ "Triangle Strip",              VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP },
  oglStrElem::itemPair<VkPrimitiveTopology>{ "Triangle Fan",                VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN },
  oglStrElem::itemPair<VkPrimitiveTopology>{ "Line List w/ Adjacency",      VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY },
  oglStrElem::itemPair<VkPrimitiveTopology>{ "Line Strip w/ Adjacency",     VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY },
  oglStrElem::itemPair<VkPrimitiveTopology>{ "Triangle List w/ Adjacency",  VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY },
  oglStrElem::itemPair<VkPrimitiveTopology>{ "Triangle Strip w/ Adjacency", VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY },
  oglStrElem::itemPair<VkPrimitiveTopology>{ "Patch List",                  VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_PATCH_LIST }
};

const std::vector<oglStrElem::itemPair<VkPolygonMode>> oglStrElem::polyModeItems
{
  oglStrElem::itemPair<VkPolygonMode>{ "Fill",   VkPolygonMode::VK_POLYGON_MODE_FILL },
  oglStrElem::itemPair<VkPolygonMode>{ "Line",   VkPolygonMode::VK_POLYGON_MODE_LINE },
  oglStrElem::itemPair<VkPolygonMode>{ "Point",  VkPolygonMode::VK_POLYGON_MODE_POINT }
};

const std::vector<oglStrElem::itemPair<VkCullModeFlagBits>> oglStrElem::cullModeFlagItems
{
  oglStrElem::itemPair<VkCullModeFlagBits>{ "None",         VkCullModeFlagBits::VK_CULL_MODE_NONE },
  oglStrElem::itemPair<VkCullModeFlagBits>{ "Front",        VkCullModeFlagBits::VK_CULL_MODE_FRONT_BIT },
  oglStrElem::itemPair<VkCullModeFlagBits>{ "Back",         VkCullModeFlagBits::VK_CULL_MODE_BACK_BIT },
  oglStrElem::itemPair<VkCullModeFlagBits>{ "Front & Back", VkCullModeFlagBits::VK_CULL_MODE_FRONT_AND_BACK }
};

const std::vector<oglStrElem::itemPair<VkFrontFace>> oglStrElem::frontFaceItems
{
  oglStrElem::itemPair<VkFrontFace>{ "CCW", VkFrontFace::VK_FRONT_FACE_COUNTER_CLOCKWISE },
  oglStrElem::itemPair<VkFrontFace>{ "CW",  VkFrontFace::VK_FRONT_FACE_CLOCKWISE }
};

const std::vector<oglStrElem::itemPair<VkBlendOp>> oglStrElem::blendOpsItems
{
  oglStrElem::itemPair<VkBlendOp>{ "Add",              VkBlendOp::VK_BLEND_OP_ADD },
  oglStrElem::itemPair<VkBlendOp>{ "Subtract",         VkBlendOp::VK_BLEND_OP_SUBTRACT },
  oglStrElem::itemPair<VkBlendOp>{ "Reverse Subtract", VkBlendOp::VK_BLEND_OP_REVERSE_SUBTRACT },
  oglStrElem::itemPair<VkBlendOp>{ "Min",              VkBlendOp::VK_BLEND_OP_MIN },
  oglStrElem::itemPair<VkBlendOp>{ "Max",              VkBlendOp::VK_BLEND_OP_MAX }
};

const std::vector<oglStrElem::itemPair<VkBlendFactor>> oglStrElem::blendFactorItems
{
  oglStrElem::itemPair<VkBlendFactor>{ "Zero",                     VkBlendFactor::VK_BLEND_FACTOR_ZERO },
  oglStrElem::itemPair<VkBlendFactor>{ "One",                      VkBlendFactor::VK_BLEND_FACTOR_ONE },
  oglStrElem::itemPair<VkBlendFactor>{ "Src Color",                VkBlendFactor::VK_BLEND_FACTOR_SRC_COLOR },
  oglStrElem::itemPair<VkBlendFactor>{ "One Minus Src Color",      VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR },
  oglStrElem::itemPair<VkBlendFactor>{ "Dst Color",                VkBlendFactor::VK_BLEND_FACTOR_DST_COLOR },
  oglStrElem::itemPair<VkBlendFactor>{ "One Minus Dst Color",      VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR },
  oglStrElem::itemPair<VkBlendFactor>{ "Src Alpha",                VkBlendFactor::VK_BLEND_FACTOR_SRC_ALPHA },
  oglStrElem::itemPair<VkBlendFactor>{ "One Minus Src Alpha",      VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA },
  oglStrElem::itemPair<VkBlendFactor>{ "Dst Alpha",                VkBlendFactor::VK_BLEND_FACTOR_DST_ALPHA },
  oglStrElem::itemPair<VkBlendFactor>{ "One Minus Dst Alpha",      VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA },
  oglStrElem::itemPair<VkBlendFactor>{ "Constant Color",           VkBlendFactor::VK_BLEND_FACTOR_CONSTANT_COLOR },
  oglStrElem::itemPair<VkBlendFactor>{ "One Minus Constant Color", VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR },
  oglStrElem::itemPair<VkBlendFactor>{ "Const Alpha",              VkBlendFactor::VK_BLEND_FACTOR_CONSTANT_ALPHA },
  oglStrElem::itemPair<VkBlendFactor>{ "One Minus Const Alpha",    VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA },
  oglStrElem::itemPair<VkBlendFactor>{ "Src Alpha Saturate",       VkBlendFactor::VK_BLEND_FACTOR_SRC_ALPHA_SATURATE },
  oglStrElem::itemPair<VkBlendFactor>{ "Src1 Color",               VkBlendFactor::VK_BLEND_FACTOR_SRC1_COLOR },
  oglStrElem::itemPair<VkBlendFactor>{ "One Minus Src1 Color",     VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR },
  oglStrElem::itemPair<VkBlendFactor>{ "Src1 Alpha",               VkBlendFactor::VK_BLEND_FACTOR_SRC1_ALPHA },
  oglStrElem::itemPair<VkBlendFactor>{ "One Minus Src1 Alpha",     VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA }
};

const std::vector<oglStrElem::itemPair<VkLogicOp>> oglStrElem::logicOpsItems
{
  oglStrElem::itemPair<VkLogicOp>{ "CLear",         VkLogicOp::VK_LOGIC_OP_CLEAR },
  oglStrElem::itemPair<VkLogicOp>{ "And",           VkLogicOp::VK_LOGIC_OP_AND },
  oglStrElem::itemPair<VkLogicOp>{ "And Reverse",   VkLogicOp::VK_LOGIC_OP_AND_REVERSE },
  oglStrElem::itemPair<VkLogicOp>{ "Copy",          VkLogicOp::VK_LOGIC_OP_COPY },
  oglStrElem::itemPair<VkLogicOp>{ "And Inverted",  VkLogicOp::VK_LOGIC_OP_AND_INVERTED },
  oglStrElem::itemPair<VkLogicOp>{ "No Op",         VkLogicOp::VK_LOGIC_OP_NO_OP },
  oglStrElem::itemPair<VkLogicOp>{ "Xor",           VkLogicOp::VK_LOGIC_OP_XOR },
  oglStrElem::itemPair<VkLogicOp>{ "Or",            VkLogicOp::VK_LOGIC_OP_OR },
  oglStrElem::itemPair<VkLogicOp>{ "Nor",           VkLogicOp::VK_LOGIC_OP_NOR },
  oglStrElem::itemPair<VkLogicOp>{ "Equivalent",    VkLogicOp::VK_LOGIC_OP_EQUIVALENT },
  oglStrElem::itemPair<VkLogicOp>{ "Invert",        VkLogicOp::VK_LOGIC_OP_INVERT },
  oglStrElem::itemPair<VkLogicOp>{ "Or Reverse",    VkLogicOp::VK_LOGIC_OP_OR_REVERSE },
  oglStrElem::itemPair<VkLogicOp>{ "Copy Inverted", VkLogicOp::VK_LOGIC_OP_COPY_INVERTED },
  oglStrElem::itemPair<VkLogicOp>{ "Or Inverteded", VkLogicOp::VK_LOGIC_OP_OR_INVERTED },
  oglStrElem::itemPair<VkLogicOp>{ "Nand",          VkLogicOp::VK_LOGIC_OP_NAND },
  oglStrElem::itemPair<VkLogicOp>{ "Set",           VkLogicOp::VK_LOGIC_OP_SET }
};

const std::vector<oglStrElem::itemPair<VkCompareOp>> oglStrElem::compareOpsItems
{
  oglStrElem::itemPair<VkCompareOp>{ "Never",  VkCompareOp::VK_COMPARE_OP_NEVER },
  oglStrElem::itemPair<VkCompareOp>{ "<",      VkCompareOp::VK_COMPARE_OP_LESS },
  oglStrElem::itemPair<VkCompareOp>{ "==",     VkCompareOp::VK_COMPARE_OP_EQUAL },
  oglStrElem::itemPair<VkCompareOp>{ "<=",     VkCompareOp::VK_COMPARE_OP_LESS_OR_EQUAL },
  oglStrElem::itemPair<VkCompareOp>{ ">",      VkCompareOp::VK_COMPARE_OP_GREATER },
  oglStrElem::itemPair<VkCompareOp>{ "!=",     VkCompareOp::VK_COMPARE_OP_NOT_EQUAL },
  oglStrElem::itemPair<VkCompareOp>{ ">=",     VkCompareOp::VK_COMPARE_OP_GREATER_OR_EQUAL },
  oglStrElem::itemPair<VkCompareOp>{ "Always", VkCompareOp::VK_COMPARE_OP_ALWAYS }
};

const std::vector<oglStrElem::itemPair<VkStencilOp>> oglStrElem::stencilOpsItems
{
  oglStrElem::itemPair<VkStencilOp>{ "Keep",     VkStencilOp::VK_STENCIL_OP_KEEP },
  oglStrElem::itemPair<VkStencilOp>{ "Zero",     VkStencilOp::VK_STENCIL_OP_ZERO },
  oglStrElem::itemPair<VkStencilOp>{ "Replace",  VkStencilOp::VK_STENCIL_OP_REPLACE },
  oglStrElem::itemPair<VkStencilOp>{ "++ Clamp", VkStencilOp::VK_STENCIL_OP_INCREMENT_AND_CLAMP },
  oglStrElem::itemPair<VkStencilOp>{ "-- Clamp", VkStencilOp::VK_STENCIL_OP_DECREMENT_AND_CLAMP },
  oglStrElem::itemPair<VkStencilOp>{ "Invert",   VkStencilOp::VK_STENCIL_OP_INVERT },
  oglStrElem::itemPair<VkStencilOp>{ "++ Wrap",  VkStencilOp::VK_STENCIL_OP_INCREMENT_AND_WRAP },
  oglStrElem::itemPair<VkStencilOp>{ "-- Wrap",  VkStencilOp::VK_STENCIL_OP_DECREMENT_AND_WRAP }
};


const std::vector<oglStrElem::itemPair<VkPipelineStageFlagBits>> oglStrElem::pipelineStageFlagItems
{
  oglStrElem::itemPair<VkPipelineStageFlagBits>{ "Top of the Pipe",     VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT },
  oglStrElem::itemPair<VkPipelineStageFlagBits>{ "Draw Indirect",       VkPipelineStageFlagBits::VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT },
  oglStrElem::itemPair<VkPipelineStageFlagBits>{ "Vertex Input",        VkPipelineStageFlagBits::VK_PIPELINE_STAGE_VERTEX_INPUT_BIT },
  oglStrElem::itemPair<VkPipelineStageFlagBits>{ "Vertex Sdr",          VkPipelineStageFlagBits::VK_PIPELINE_STAGE_VERTEX_SHADER_BIT },
  oglStrElem::itemPair<VkPipelineStageFlagBits>{ "Tess Control Sdr",    VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT },
  oglStrElem::itemPair<VkPipelineStageFlagBits>{ "Tess Eval Sdr",       VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT },
  oglStrElem::itemPair<VkPipelineStageFlagBits>{ "Geometry Sdr",        VkPipelineStageFlagBits::VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT },
  oglStrElem::itemPair<VkPipelineStageFlagBits>{ "Fragment Sdr",        VkPipelineStageFlagBits::VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT },
  oglStrElem::itemPair<VkPipelineStageFlagBits>{ "Early Frag Tests",    VkPipelineStageFlagBits::VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT },
  oglStrElem::itemPair<VkPipelineStageFlagBits>{ "Late Frag Tests",     VkPipelineStageFlagBits::VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT },
  oglStrElem::itemPair<VkPipelineStageFlagBits>{ "Color Att Output",    VkPipelineStageFlagBits::VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT },
  oglStrElem::itemPair<VkPipelineStageFlagBits>{ "Compute Sdr",         VkPipelineStageFlagBits::VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT },
  oglStrElem::itemPair<VkPipelineStageFlagBits>{ "Transfer",            VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TRANSFER_BIT },
  oglStrElem::itemPair<VkPipelineStageFlagBits>{ "Bottom of the Pipe",  VkPipelineStageFlagBits::VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT },
  oglStrElem::itemPair<VkPipelineStageFlagBits>{ "Host",                VkPipelineStageFlagBits::VK_PIPELINE_STAGE_HOST_BIT },
  oglStrElem::itemPair<VkPipelineStageFlagBits>{ "All Graphics",        VkPipelineStageFlagBits::VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT },
  oglStrElem::itemPair<VkPipelineStageFlagBits>{ "All Commands",        VkPipelineStageFlagBits::VK_PIPELINE_STAGE_ALL_COMMANDS_BIT }
};

const std::vector<oglStrElem::itemPair<VkAccessFlagBits>> oglStrElem::accessFlagItems
{
  oglStrElem::itemPair<VkAccessFlagBits>{ "Draw Indirect",           VkAccessFlagBits::VK_ACCESS_INDIRECT_COMMAND_READ_BIT },
  oglStrElem::itemPair<VkAccessFlagBits>{ "Index Read",              VkAccessFlagBits::VK_ACCESS_INDEX_READ_BIT },
  oglStrElem::itemPair<VkAccessFlagBits>{ "Vertex Attribute Read",   VkAccessFlagBits::VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT },
  oglStrElem::itemPair<VkAccessFlagBits>{ "Uniform Read",            VkAccessFlagBits::VK_ACCESS_UNIFORM_READ_BIT },
  oglStrElem::itemPair<VkAccessFlagBits>{ "Input Att Read",          VkAccessFlagBits::VK_ACCESS_INPUT_ATTACHMENT_READ_BIT },
  oglStrElem::itemPair<VkAccessFlagBits>{ "Shader Read",             VkAccessFlagBits::VK_ACCESS_SHADER_READ_BIT },
  oglStrElem::itemPair<VkAccessFlagBits>{ "Shader Write",            VkAccessFlagBits::VK_ACCESS_SHADER_WRITE_BIT },
  oglStrElem::itemPair<VkAccessFlagBits>{ "Color Att Read",          VkAccessFlagBits::VK_ACCESS_COLOR_ATTACHMENT_READ_BIT },
  oglStrElem::itemPair<VkAccessFlagBits>{ "Color Att Write",         VkAccessFlagBits::VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT },
  oglStrElem::itemPair<VkAccessFlagBits>{ "Depth Stencil Att Read",  VkAccessFlagBits::VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT },
  oglStrElem::itemPair<VkAccessFlagBits>{ "Depth Stencil Att Write", VkAccessFlagBits::VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT },
  oglStrElem::itemPair<VkAccessFlagBits>{ "Transfer Read",           VkAccessFlagBits::VK_ACCESS_TRANSFER_READ_BIT },
  oglStrElem::itemPair<VkAccessFlagBits>{ "Transfer Write",          VkAccessFlagBits::VK_ACCESS_TRANSFER_WRITE_BIT },
  oglStrElem::itemPair<VkAccessFlagBits>{ "Host Read",               VkAccessFlagBits::VK_ACCESS_HOST_READ_BIT },
  oglStrElem::itemPair<VkAccessFlagBits>{ "Host Write",              VkAccessFlagBits::VK_ACCESS_HOST_WRITE_BIT },
  oglStrElem::itemPair<VkAccessFlagBits>{ "Memory Read",             VkAccessFlagBits::VK_ACCESS_MEMORY_READ_BIT },
  oglStrElem::itemPair<VkAccessFlagBits>{ "Memory Write",            VkAccessFlagBits::VK_ACCESS_MEMORY_WRITE_BIT }
};


const std::vector<oglStrElem::itemPair<VkDependencyFlagBits>> oglStrElem::dependencyFlagItems
{
  oglStrElem::itemPair<VkDependencyFlagBits>{ "By Region",           VkDependencyFlagBits::VK_DEPENDENCY_BY_REGION_BIT },
  oglStrElem::itemPair<VkDependencyFlagBits>{ "Device Group",        VkDependencyFlagBits::VK_DEPENDENCY_DEVICE_GROUP_BIT },
  oglStrElem::itemPair<VkDependencyFlagBits>{ "View Local",          VkDependencyFlagBits::VK_DEPENDENCY_VIEW_LOCAL_BIT },
  oglStrElem::itemPair<VkDependencyFlagBits>{ "View Local KHR",      VkDependencyFlagBits::VK_DEPENDENCY_VIEW_LOCAL_BIT_KHR },
  oglStrElem::itemPair<VkDependencyFlagBits>{ "Device Group KHR",    VkDependencyFlagBits::VK_DEPENDENCY_DEVICE_GROUP_BIT_KHR }
};

const std::vector<oglStrElem::itemPair<VkImageUsageFlagBits>> oglStrElem::imageUsageFlagItems
{
  oglStrElem::itemPair<VkImageUsageFlagBits>{ "Transfer Src",           VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSFER_SRC_BIT },
  oglStrElem::itemPair<VkImageUsageFlagBits>{ "Transfer Dst",        VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSFER_DST_BIT},
  oglStrElem::itemPair<VkImageUsageFlagBits>{ "Sample",          VkImageUsageFlagBits::VK_IMAGE_USAGE_SAMPLED_BIT },
  oglStrElem::itemPair<VkImageUsageFlagBits>{ "Storage",      VkImageUsageFlagBits::VK_IMAGE_USAGE_STORAGE_BIT },
  oglStrElem::itemPair<VkImageUsageFlagBits>{ "Color Att",    VkImageUsageFlagBits::VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT },
  oglStrElem::itemPair<VkImageUsageFlagBits>{ "Depth Stencil Att",    VkImageUsageFlagBits::VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT },
  oglStrElem::itemPair<VkImageUsageFlagBits>{ "Transient Att",    VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT },
  oglStrElem::itemPair<VkImageUsageFlagBits>{ "Input Att",    VkImageUsageFlagBits::VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT }
};

const std::map<uint32_t, const char*> oglStrElem::subpassIndexItems
{
  { VK_SUBPASS_EXTERNAL, "Subpass External" },
  { 0,                   "0" },
  { 1,                   "1" },
  { 2,                   "2" },
  { 3,                   "3" },
  { 4,                   "4" },
  { 5,                   "5" },
  { 6,                   "6" },
  { 7,                   "7" },
  { 8,                   "8" },
  { 9,                   "9" }
};


const std::map<VkFormat, const char*> oglStrElem::commonImgFormatItems
{
  { VkFormat::VK_FORMAT_UNDEFINED,           "-Undefined-"            },
  { VkFormat::VK_FORMAT_R8G8B8A8_SNORM,      "R8 G8 B8 A8 SNORM"      },
  { VkFormat::VK_FORMAT_R8G8B8A8_UNORM,      "R8 G8 B8 A8 UNORM"      },
  { VkFormat::VK_FORMAT_R16G16B16A16_SNORM,  "R16 G16 B16 A16 SNORM"  },
  { VkFormat::VK_FORMAT_R16G16B16A16_UNORM,  "R16 G16 B16 A16 UNORM"  },
  { VkFormat::VK_FORMAT_R16G16B16A16_SFLOAT, "R16 G16 B16 A16 SFLOAT" },
  { VkFormat::VK_FORMAT_R32G32B32A32_SFLOAT, "R32 G32 B32 A32 SFLOAT" },
  { VkFormat::VK_FORMAT_R64G64B64A64_SFLOAT, "R34 G64 B64 A64 SFLOAT" },
};



OGL_BASETYPE oglStrElem::Find_OGL_BASETYPE(const std::string & name)
{
  for (auto& baseType : oglStrElem::dataTypeItems)
    if (baseType.name == name) return baseType.item;

  throw 1;
  return OGL_BASETYPE::INT_TYPE;
}

UNIFORM_USAGE oglStrElem::Find_UNIFORM_USAGE(const std::string & name)
{
  for (auto& uniform : oglStrElem::uniformUsageItems)
    if (uniform.name == name) return uniform.item;

  throw 1;
  return UNIFORM_USAGE::CAM_PROJ;
}

Shader::ShaderType oglStrElem::Find_ShaderType(const std::string & name)
{
  for (auto& stage : oglStrElem::uniformStageItems)
    if (stage.name == name) return stage.item;

  throw 1;
  return Shader::ShaderType::COMPUTESDR;
}

vBufferLayout::vBufferLayoutType oglStrElem::Find_vBufferLayoutType(const std::string & name)
{
  for (auto& vIn : oglStrElem::bufferVertexInputItems)
    if (vIn.name == name) return vIn.item;

  throw 1;
  return vBufferLayout::vBufferLayoutType::VERTEX_POS;
}

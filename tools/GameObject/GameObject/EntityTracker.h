#pragma once
#include <assert.h>
#include "ComponentRTTI.h"
#include <map>

class EntityTracker
{
  friend class GameobjectManager;

public:
  static EntityTracker* GetInstance();
  
  // Gameobject tracker
  void  AddGameobjectHash(const unsigned& objRef);
  char* GetGameobjectFlag(const unsigned& objRef);
  void  RemoveGameobjectReference(const unsigned& objRef);
  bool  HasGameObjectReference(const unsigned& objRef);
  void  SetGameobjectInvalid(const unsigned& objRef);

  // Component tracker
  void  AddComponentHash(const unsigned& objRef, const ComponentEnum& compId, const unsigned& compRef);
  char* GetComponentFlag(const unsigned& objRef, const ComponentEnum& compId, const unsigned& compRef);
  void  RemoveComponentReference(const unsigned& objRef, const ComponentEnum& compId, const unsigned& compRef);
  bool  HasComponentReference(const unsigned& objRef, const ComponentEnum& compId, const unsigned& compRef);
  void  SetComponentInvalid(const unsigned& objRef, const ComponentEnum& compId, const unsigned& compRef);

  // Debug
  void RunDebugHashTable();

  unsigned GenerateID();
  unsigned GetCurrentID();
  bool     IsUnuseID(const unsigned& ID) const;

private:
  EntityTracker();
  static EntityTracker* m_instance;
  void     SetMaxID(const unsigned& maxID);

  struct _flagReference
  {
    unsigned _ref  = 0;
    char     _flag = 0;
  };

  struct _objComponentHash
  {
    unsigned                                                    _ref = 0;    // number of reference to gameobject
    char                                                        _valid = 0;  // check to see if gameobject is valid
    std::map<ComponentEnum, std::map<unsigned, _flagReference>> _compHash;   // component hash for this game object
  };

  unsigned                              m_IDTracker;
  std::map<unsigned, _objComponentHash> m_gameobjectHash;

#ifdef _DEBUG
  unsigned m_NumberOfGameObjectsCreated           = 0;
  unsigned m_NumberOfGameObjectReferenceCreated   = 0;
  unsigned m_NumberOfGameObjectsDestroyed         = 0;
  unsigned m_NumberOfGameObjectReferenceDestroyed = 0;
  unsigned m_NumberOfComponentsCreated            = 0;
  unsigned m_NumberOfComponentReferenceCreated    = 0;
  unsigned m_NumberOfComponentsDestroyed          = 0;
  unsigned m_NumberOfComponentReferenceDestroyed  = 0;
#endif // _DEBUG


};
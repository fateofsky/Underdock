#pragma once
#include <Windows.h>
#include "VulkBuffer.h"
#include "VulkTexImage.h"
#include "VulkPipeline.h"
#include <fstream>

namespace Engine
{
  extern bool Init(HINSTANCE instanceH);
  extern void Run();
  extern void DestroyEngine();
  extern bool _quit;

  /*
    ENGINE CORE
  */
  namespace core
  {
    extern LRESULT CALLBACK WinProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
    extern bool memory_type_from_properties(uint32_t typeBits, VkFlags requirements_mask, uint32_t *typeIndex);

    void SwapChainImageAcquire();
    void SwapChainImageSubmitAndPresent();
    void StartRecord();
    void BeginMainRenderPass(VkRenderPass rp, VkFramebuffer fb, const float* clearColor, int primaryBuffer = 1,
                             const float& depthVal = 1.f, const uint32_t& stencilVal = 0);
    void EndMainRenderPass();
    void EndRecord();

    /*
      Utils
    */
    void SetViewportScissor(const unsigned& bufferIndex);

    extern HDC                           hdc;                   // hardware device context
    extern HWND                          hwnd;                  // handle to a window
    extern VkQueue                       vGraphicsQue;          // Graphics queue from the device
    extern VkQueue                       vPresentQue;           // Present queue from the device
    extern std::vector<VkPhysicalDevice> vPdVec;                // list of vulkan physical device (GPU)
    extern VkInstance                    vInst;                 // vulkan instance
    extern VkSurfaceKHR                  vSurfKHR;              // vulkan windows surface
    extern VkDevice                      vDevice;               // vulkan logical device
    extern VkViewport                    vViewport;             // vulkan viewport (need for setting viewport like glViewport())
    extern VkRect2D                      vScissor;              // vulkan Scissor, use for clipping)
    extern unsigned                      vGraphicQueueFamIndex; // index to the graphic queue
    extern unsigned                      vPresentQueueFamIndex; // index to the present queue
    extern unsigned                      windowsWidth;          // width
    extern unsigned                      windowsHeight;         // Height
  }

  /*
    SWAPCHAINS
  */
  namespace swpch
  {
    bool                                InitSwapchains(bool initDepth = true);
    void                                DestroySwapchains();

    // Swapchains and screen buffers variables
    extern VkSwapchainKHR                vSwapchain;       // vulkan swapchain
    extern VkImage                       vDepthImg;        // vulkan depth image object
    extern VkImageView                   vDepthImgView;    // vulkan depth image view
    extern VkRenderPass                  vRenderpass;      // vulkan renderpass
    extern std::vector<VkImageView>      vScImgViewVec;    // list of vulkan color image views
    extern std::vector<VkFramebuffer>    vFBVec;           // vulkan frame buffers (tied to number of swap chain images)
    extern std::vector<VkSemaphore>      vImgAcSemaVec;    // vulkan image acquiring semaphore
    extern std::vector<VkSemaphore>      vImgDoneSemaVec;  // vulkan image acquiring semaphore
    extern std::vector<VkFence>          vRenderFenceVec;  // rendering fence for command queue submission
    extern std::vector<VkCommandBuffer>  vCmdBuffVec;      // vulkan command buffer vector
    extern VkCommandPool                 vCmdPool;         // vulkan command pool (to allocate command buffer)
    extern VkFormat                      vFmt;             // vulkan swapchains color image format
    extern VkFormat                      vDepthFmt;        // vulkan swapchains depth image format
    extern unsigned                      vFBIndex;         // index of the current framebuffer to render
    extern unsigned                      vCurFrame;        // index of the current frame it is now (MIGHT be different from vFBIndex)
    extern unsigned                      vNumSCImgs;       // number of swap chain images
    extern bool                          vHasDepth;        // has depth buffer
  }


  namespace utils
  {
    VkCommandBuffer beginSingleTimeCommands();
    void endSingleTimeCommands(const VkCommandBuffer& commandBuffer);
    void copyVkBuffers(VkCommandBuffer cmdBuf, VkBuffer srcBuf, VkBuffer desBuf, VkDeviceSize bufSize);
    void copyVkBufferToImage(VkCommandBuffer cmdBuf, VkBuffer srcBuf, VkImage desImg, VkImageAspectFlags flg, unsigned width, unsigned height);
    void ImageTransition_Undef_Transfer(VkCommandBuffer cmdBuf, VkImage image, VkImageAspectFlags flgs = VK_IMAGE_ASPECT_COLOR_BIT);
    void ImageTransition_Transfer_FragRead(VkCommandBuffer cmdBuf, VkImage image, VkImageAspectFlags flgs = VK_IMAGE_ASPECT_COLOR_BIT);
  }

}



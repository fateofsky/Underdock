#include "pch.h"
#include "AppManager.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include "ImGuiPackage.h"
#include "WindowSystem.h"

const std::string _DEFAULT_LAYOUT_NAME = "DefaultLayout.meml";
const std::string _DEFAULT_MODEL_NAME = "_default.fbx";
const std::string _DEFAULT_VS_NAME = "_default.vert";
const std::string _DEFAULT_FS_NAME = "_default.frag";
const std::string _DEFAULT_TEXTURE_NAME = "_default.png";
const std::string _DEFAULT_CAMVIEW_NAME = "ViewMat";
const std::string _DEFAULT_CAMPROJ_NAME = "ProjectionMat";
const std::string _DEFAULT_CAMVIEWPROJ_NAME = "ViewProjectionMat";
const std::string _DEFAULT_TRANSFORM_NAME = "TransformationMat";
const ImGuiHoveredFlags hvFlags = ImGuiHoveredFlags_::ImGuiHoveredFlags_AnyWindow;

/*
  IMGUI Global Controls
*/
ImTextureID                  globalViewTexID;
bool                         ViewMaterialBool = false;
bool                         ViewModelBool = false;
bool                         ViewTextureBool = false;
bool                         ViewUniformBool = false;
bool                         ViewPipelineBool = false;
bool                         ViewTextureLibraryBool = false;
bool                         ViewLevelDesign = false;
bool                         RenameUniformBool = false;
bool                         DeleteUniform = false;
bool                         DeleteTexture = false;
const std::string* ResourceName = nullptr;
std::string                  selectedItemToDelete;

/*
  Local variables
*/
const unsigned sizeOfBufferLayoutType = sizeof(vBufferLayout::vBufferLayoutType);
const unsigned sizeOfShaderType = sizeof(Shader::ShaderType);
const unsigned sizeOfBaseType = sizeof(OGL_BASETYPE);
const unsigned sizeOfUsageType = sizeof(UNIFORM_USAGE);

//float CosInterpolateFloat(float start, float end, float dt)
//{
//  float cosDT = 1.f - cosf(dt);
//  std::cout << cosDT << std::endl;
//  return start + (end - start) * cosDT;
//}

AppManager* AppManager::m_instance = nullptr;

void AppManager::OpenProject()
{
  std::string fileLoc = WinSystem::OpenFile("Material Layout (.meml)\0*.meml\0All Files\0*.*\0");
  if (fileLoc.size())
  {
    DeSerialize(fileLoc);

    //std::ofstream recOpen("_RecentlyOpen", std::ios::out | std::ios::trunc);
    //if (recOpen.is_open())
    //{
    //  recOpen << m_openFileName;
    //  recOpen.close();
    //}

    std::ofstream projLoc("_ProjectLocation", std::ios::out | std::ios::trunc);
    if (projLoc.is_open())
    {
      projLoc.write(fileLoc.c_str(), fileLoc.size());
      projLoc.close();
    }
  }
}

void AppManager::ImportModel()
{
  std::string fileLoc = WinSystem::OpenFile("FBX\0*.fbx\0DAE\0*.dae\0All Files\0*.*\0");
  if (fileLoc.size())
  {
    std::string NewModelName = fileLoc.substr(fileLoc.find_last_of('\\') + 1);

    // load model
    Model* addModel = ResourceManager::GetInstance()->GetModel(NewModelName);
    if (addModel == nullptr)
    {
      m_curMatLayout->m_modelName = NewModelName;
      bool _modLoadRes = ResourceManager::GetInstance()->LoadModel(fileLoc, true);
      m_curMatLayout->AttachModel(m_curMatLayout->m_modelName);
      if (!_modLoadRes) throw 1;
    }
    else m_curMatLayout->m_model = addModel;

    // create vertex buffer &  use back initial vBufferLayout
    std::vector<unsigned> indexList = ResourceManager::GetInstance()->CreateVertexBuffer(m_curMatLayout->m_modelName,
      m_curMatLayout->m_modelName + "#" + m_curMatLayout->m_layoutName,
      m_curMatLayout->m_vBufferLayout);
    m_curMatLayout->AttachIndices(indexList);
    m_curMatLayout->AttachVertexBuffer(m_curMatLayout->m_modelName + "#" + m_curMatLayout->m_layoutName);
  }
}

AppManager::AppManager()
  : m_driverTime(0.f), m_loadedDefault(false)
{ }

void AppManager::Initialize()
{
  m_instance = new AppManager();
}

void AppManager::Destroy()
{
  for (auto& elem : m_instance->m_materialLayouts)
    elem.second.Destroy();
  delete m_instance;
}

AppManager* AppManager::GetInstance()
{
  return m_instance;
}

void AppManager::LoadDefault()
{
  DeSerialize(_DEFAULT_SAVES_LOC + _DEFAULT_LAYOUT_NAME);
}

bool AppManager::LoadProjectLocation()
{
  std::ifstream ifs("_ProjectLocation", std::ios::in | std::ios::binary | std::ios::ate);
  if (!ifs.is_open()) throw 1;

  std::streampos fileSize = ifs.tellg();
  size_t realFileSize = size_t(fileSize);
  if (realFileSize != 0)
  {
    std::string filePath;
    filePath.resize(realFileSize);
    ifs.seekg(ifs.beg);
    ifs.read(&filePath[0], realFileSize);
    if (DeSerialize(filePath))
    {
      m_openFilePath = filePath;
      ifs.close();
      return true;
    }
  }

  return false;
}

//bool AppManager::LoadRecentlyOpen()
//{
//  std::ifstream recOpen("_RecentlyOpen", std::ios::in | std::ios::ate);
//  if (recOpen.is_open())
//  {
//    std::streampos recSize = recOpen.tellg();
//    if (size_t(recSize) != 0)
//    {
//      recOpen.seekg(recOpen.beg);
//      std::string tmpName;
//      recOpen >> tmpName;
//      if (DeSerialize(m_saveDataLocation + tmpName))
//      {
//        m_openFileName = tmpName;
//        recOpen.close();
//        return true;
//      }
//    }
//  }
//
//  recOpen.close();
//  return false;
//}

void AppManager::InitializeSettings()
{
  /*
    Default camera settings
  */
  ResourceManager* rm = ResourceManager::GetInstance();
  m_camZPos = 70.f;
  m_camZStrength = 3.0f;
  rm->ChangeNear(0.1f);
  rm->ChangeFar(10000.f);
  rm->ChangeAspect(DEFAULT_SCREEN_WIDTH_F / DEFAULT_SCREEN_HEIGHT_F);
  rm->ChangeCameraPosition(glm::vec3(0.f, 0.f, m_camZPos));
  rm->ChangeFOV(60.0f);
  m_eulerAngle = glm::vec2(0, 0);
  m_camSpeed = 0.5f;
}

bool AppManager::Serialize(const std::string& fileLoc, const std::string& newFileName)
{
  if (m_curMatLayout == nullptr) return false;
  //m_openFileName = (m_openFileName.size() < 1) ? m_curMatLayout->m_layoutName : m_openFileName;

  MemoryStream outFile;
  outFile.BeginWrite();

  // shader name & model name
  outFile.WriteAsString(("sdr#" + (newFileName.size() ? newFileName : m_curMatLayout->m_layoutName)).c_str());
  outFile.WriteAsString(m_curMatLayout->m_modelName.c_str());

  // save vbuffer layout
  outFile.Write(m_curMatLayout->m_vBufferLayout.layout.size());
  for (auto& vBuff : m_curMatLayout->m_vBufferLayout.layout)
    outFile.Write(&oglStrElem::bufferVertexInputItems[vBuff].item, sizeOfBufferLayoutType);

  // save pipeline stages
  PipelineLayout::pipelineStageMap& stageMap = m_curMatLayout->m_resourceLayout.GetPipelineStageMap();
  outFile.Write(stageMap.size());
  for (auto& stage : stageMap)
  {
    // save stage type
    outFile.Write(&stage.first, sizeOfShaderType);

    // number of base input
    outFile.Write(stage.second.baseInput.size());
    for (auto& baseInput : stage.second.baseInput)
      outFile.Write(&baseInput, sizeOfBaseType);

    // number of base output
    outFile.Write(stage.second.baseOutput.size());
    for (auto& baseOutput : stage.second.baseOutput)
      outFile.Write(&baseOutput, sizeOfBaseType);

    // number of textures
    outFile.Write(stage.second.inputTextures.size());
    for (auto& texture : stage.second.inputTextures)
    {
      outFile.Write(texture.second->GetGroup());                 // grouping / set number
      outFile.WriteAsString(texture.first.c_str());              // texture name
    }

    // number of uniforms
    outFile.Write(stage.second.inputUniforms.size());
    for (auto& uniform : stage.second.inputUniforms)
    {
      outFile.WriteAsString(uniform.first.c_str());              // name of the uniform
      UNIFORM_USAGE tmpUsageType = uniform.second->GetUsage();   // usage of the uniform
      outFile.Write(&tmpUsageType, sizeOfUsageType);
      OGL_BASETYPE  tmpBaseType = uniform.second->GetType();    // type of the uniform
      outFile.Write(&tmpBaseType, sizeOfBaseType);
      outFile.Write(uniform.second->GetGroup());                 // group sets
      uniform.second->Serialize(outFile);                        // uniform data
    }

    // save uniform group
    outFile.Write(stage.second.uniformGroups.size());   // number of groups
    for (auto& group : stage.second.uniformGroups)
    {
      outFile.WriteAsString(group.first.c_str());       // group name
      outFile.Write(group.second.GetGroup());           // group set
      outFile.Write(group.second.GetMemoryLayout());    // group memory layout
      outFile.Write(group.second.GetGroupCount());      // grouped uniform count
      auto& groupList = group.second.GetGroupList();
      for (auto& _uniform : groupList)
        outFile.WriteAsString(_uniform.c_str());        // uniform name
    }

    // save shader code
    outFile.Write(stage.second.shaderMainCode.size());
    if (stage.second.shaderMainCode.size())
      outFile.WriteAsString(stage.second.shaderMainCode.c_str());
  }

  bool hasRenderpassDepth = m_curMatLayout->m_resourceLayout.GetHasDepth();
  outFile.Write(hasRenderpassDepth);

  // write dimensions
  outFile.Write(m_curMatLayout->m_resourceLayout.GetRenderTargetDimension(), sizeof(int) << 1);

  // write enable depth to texture
  outFile.Write(m_curMatLayout->m_resourceLayout.GetEnableDepthToTexture());

  // write dependencies
  const auto& depenList = m_curMatLayout->m_resourceLayout.GetDependenciesList();
  outFile.Write(depenList.size());
  for (auto& dependency : depenList)
    outFile.Write(dependency);

  // write attachment output format (no need size)
  const auto& mrtOutputFormatList = m_curMatLayout->m_resourceLayout.GetMRTOutputList();
  for (auto& outputFormat : mrtOutputFormatList)
    outFile.Write(outputFormat);


  // write image attachment usage (no need size)
  const auto& mrtOutputUsageList = m_curMatLayout->m_resourceLayout.GetMRTOutputUsageList();
  for (auto& outputUsage : mrtOutputUsageList)
    outFile.Write(outputUsage);

  /*
    Export Vulkan graphics pipeline information
  */
  VkPipelineInputAssemblyStateCreateInfo& inputAssemblyState = m_curMatLayout->m_resourceLayout.GetInputAssemblyInfo();
  VkPipelineRasterizationStateCreateInfo& rasterState = m_curMatLayout->m_resourceLayout.GetRasterizationStateInfo();
  VkPipelineColorBlendStateCreateInfo& colorBlendState = m_curMatLayout->m_resourceLayout.GetColorBlendAttStateInfo();
  VkPipelineDepthStencilStateCreateInfo& depthStencilState = m_curMatLayout->m_resourceLayout.GetDepthStencilStateInfo();
  std::vector<VkDynamicState>& dynamicStateList = m_curMatLayout->m_resourceLayout.GetDynamicStateList();
  std::vector<VkPipelineColorBlendAttachmentState>& cbAttStateList = m_curMatLayout->m_resourceLayout.GetColorBlendAttStateList();

  // write VkDynamicState
  outFile.Write(dynamicStateList.size());
  for (auto& dState : dynamicStateList)
    outFile.Write(dState);

  // write VkPipelineInputAssemblyStateCreateInfo
  outFile.Write(inputAssemblyState);

  // write VkPipelineRasterizationStateCreateInfo
  outFile.Write(rasterState);

  // write list of VkPipelineColorBlendAttachmentState
  outFile.Write(cbAttStateList.size());
  for (auto& state : cbAttStateList)
    outFile.Write(state);

  // write VkPipelineColorBlendStateCreateInfo
  outFile.Write(colorBlendState);

  // write VkPipelineDepthStencilStateCreateInfo
  outFile.Write(depthStencilState);

  outFile.Flush(fileLoc);


  // write save data location to _projectlocation
  std::ofstream ofs("_ProjectLocation", std::ios::out | std::ios::binary | std::ios::trunc);
  if (!ofs.is_open()) throw 1;
  ofs.write(m_openFilePath.c_str(), m_openFilePath.size());
  ofs.close();

  return true;
}

bool AppManager::DeSerialize(const std::string& file)
{
  size_t namePos = file.find_last_of('\\') + 1;
  std::string strData = file.substr(namePos);
  if (m_materialLayouts.find(strData) != m_materialLayouts.end())
  {
    m_curMatLayout = &m_materialLayouts[strData];
    m_openFilePath = file;
    return true;
  }
  m_openFilePath = file;

  ResourceManager* rm = ResourceManager::GetInstance();
  MemoryStream inputFile;
  if (inputFile.FileRead(file) == false) return false;

  // dissect file for it material layout name
  m_materialLayouts[strData] = MaterialLayout();
  m_curMatLayout = &m_materialLayouts[strData];
  m_curMatLayout->SetLayoutName(strData);
  m_curMatLayout->m_saveDataLocation = file;

  // load shader and model name
  inputFile.ReadAsString(m_curMatLayout->m_shaderName); // * WARNING until this point the shader is not loaded
  inputFile.ReadAsString(m_curMatLayout->m_modelName);

  // load model
  Model* addModel = rm->GetModel(m_curMatLayout->m_modelName);
  if (addModel == nullptr)
  {
    bool _modLoadRes = rm->LoadModel(_DEFAULT_MODEL_LOC + m_curMatLayout->m_modelName, true);
    m_curMatLayout->AttachModel(m_curMatLayout->m_modelName);
    if (!_modLoadRes) throw 1;
  }
  else m_curMatLayout->m_model = addModel;


  // load vBufferLayout
  size_t dataSize;
  vBufferLayout::vBufferLayoutType typeOfBuffer;
  inputFile.Read(&dataSize);
  for (size_t i = 0; i < dataSize; ++i)
  {
    inputFile.Read(&typeOfBuffer);
    m_curMatLayout->InsertVBufferLayout(typeOfBuffer);
  }
  // create vertex buffer
  std::vector<unsigned> indexList = rm->CreateVertexBuffer(m_curMatLayout->m_modelName,
    m_curMatLayout->m_modelName + "#" + m_curMatLayout->m_layoutName,
    m_curMatLayout->m_vBufferLayout);
  m_curMatLayout->AttachIndices(indexList);
  m_curMatLayout->AttachVertexBuffer(m_curMatLayout->m_modelName + "#" + m_curMatLayout->m_layoutName);


  // load pipeline stages
  size_t pipelineSize;
  inputFile.Read(&pipelineSize);
  for (size_t i = 0; i < pipelineSize; ++i)
  {
    // load stage type
    Shader::ShaderType stageType;
    inputFile.Read(&stageType);
    m_curMatLayout->m_resourceLayout.AddStages(stageType);

    // load input
    inputFile.Read(&dataSize);
    for (size_t j = 0; j < dataSize; ++j)
    {
      OGL_BASETYPE baseType;
      inputFile.Read(&baseType);
      m_curMatLayout->m_resourceLayout.AddInputToStage(stageType, baseType);
    }

    // load output
    inputFile.Read(&dataSize);
    for (size_t j = 0; j < dataSize; ++j)
    {
      OGL_BASETYPE baseType;
      inputFile.Read(&baseType);
      m_curMatLayout->m_resourceLayout.AddOutputToStage(stageType, baseType);
    }

    // load texture
    inputFile.Read(&dataSize);
    for (size_t j = 0; j < dataSize; ++j)
    {
      int textureGroupSet;
      inputFile.Read(&textureGroupSet);
      inputFile.ReadAsString(strData);
      if (rm->GetTexture(strData) == nullptr)
      {
        bool _loadTex = rm->LoadTexture(_DEFAULT_TEXTURE_LOC + strData);
        rm->GetTexture(strData)->ChangeGroup(textureGroupSet);
        if (!_loadTex) throw 1;
      }
      m_curMatLayout->AttachTexture(strData, stageType);
    }

    // load uniforms
    inputFile.Read(&dataSize);
    for (size_t j = 0; j < dataSize; ++j)
    {
      std::string uniformName;
      inputFile.ReadAsString(uniformName);

      UNIFORM_USAGE usageType;
      inputFile.Read(&usageType);

      OGL_BASETYPE baseType;
      inputFile.Read(&baseType);

      unsigned groupSet;
      inputFile.Read(&groupSet);

      m_curMatLayout->AddUniform(uniformName, baseType, usageType, stageType, groupSet);
      UniformResourceBase* addedUniform = m_curMatLayout->GetUniform(uniformName);
      addedUniform->DeSerialize(inputFile);
    }

    // load uniform group
    size_t uniformGroupSize;
    inputFile.Read(&uniformGroupSize);
    for (size_t j = 0; j < uniformGroupSize; ++j)
    {
      std::string groupName;
      inputFile.ReadAsString(groupName);
      unsigned groupSet;
      inputFile.Read(&groupSet);
      UniformGroup::_MemoryLayout mLayout;
      inputFile.Read(&mLayout);
      size_t groupCount;
      inputFile.Read(&groupCount);
      for (size_t k = 0; k < groupCount; ++k)
      {
        std::string uniformName;
        inputFile.ReadAsString(uniformName);
        m_curMatLayout->m_resourceLayout.AddToGroup(stageType, groupName, uniformName, groupSet, mLayout);
      }
    }

    // load shader code
    inputFile.Read(&dataSize);
    if (dataSize)
    {
      inputFile.ReadAsString(strData);
      m_curMatLayout->m_resourceLayout.AddShaderCodeToStage(stageType, strData);
    }
  }

  // ouput state
  bool hasDepth;
  inputFile.Read(&hasDepth);
  m_curMatLayout->m_resourceLayout.ChangeHasDepth(hasDepth);

  int dimension[2];
  // read dimensions
  inputFile.Read(dimension, sizeof(int) << 1);
  m_curMatLayout->m_resourceLayout.SetRenderTargetDimension(dimension[0], dimension[1]);

  // read enable depth to texture
  bool enableDepthToTexture;
  inputFile.Read(&enableDepthToTexture);
  m_curMatLayout->m_resourceLayout.SetEnableDepthToTexture(enableDepthToTexture);

  // read dependencies
  auto& depenList = m_curMatLayout->m_resourceLayout.GetDependenciesList_Writable();
  size_t depenSize;
  inputFile.Read(&depenSize);
  depenList.resize(depenSize);
  for (size_t i = 0; i < depenSize; ++i)
    inputFile.Read(&depenList[i]);

  // read attachment output format
  auto& mrtOutputFormatList = m_curMatLayout->m_resourceLayout.GetMRTOutputList_Writable();
  size_t mrtOutputSize = mrtOutputFormatList.size();
  for (size_t i = 0; i < mrtOutputSize; ++i)
    inputFile.Read(&mrtOutputFormatList[i]);

  // read attachment output usage
  auto& mrtOutputUsageList = m_curMatLayout->m_resourceLayout.GetMRTOutputUsageList_Writable();
  size_t mrtOutputUsageSize = mrtOutputUsageList.size();
  for (size_t i = 0; i < mrtOutputUsageSize; ++i)
    inputFile.Read(&mrtOutputUsageList[i]);


  VkPipelineInputAssemblyStateCreateInfo& inputAssemblyState = m_curMatLayout->m_resourceLayout.GetInputAssemblyInfo();
  VkPipelineRasterizationStateCreateInfo& rasterState = m_curMatLayout->m_resourceLayout.GetRasterizationStateInfo();
  VkPipelineColorBlendStateCreateInfo& colorBlendState = m_curMatLayout->m_resourceLayout.GetColorBlendAttStateInfo();
  VkPipelineDepthStencilStateCreateInfo& depthStencilState = m_curMatLayout->m_resourceLayout.GetDepthStencilStateInfo();
  std::vector<VkDynamicState>& dynamicStateList = m_curMatLayout->m_resourceLayout.GetDynamicStateList();
  std::vector<VkPipelineColorBlendAttachmentState>& cbAttStateList = m_curMatLayout->m_resourceLayout.GetColorBlendAttStateList();

  // read VkDynamicState
  size_t dynamicSize;
  inputFile.Read(&dynamicSize);
  dynamicStateList.resize(dynamicSize);
  for (size_t i = 0; i < dynamicSize; ++i)
    inputFile.Read(&dynamicStateList[i]);

  // read VkPipelineInputAssemblyStateCreateInfo
  inputFile.Read(&inputAssemblyState);

  // read VkPipelineRasterizationStateCreateInfo
  inputFile.Read(&rasterState);

  // write list of VkPipelineColorBlendAttachmentState
  size_t cbAttStateSize;
  inputFile.Read(&cbAttStateSize);
  cbAttStateList.resize(cbAttStateSize);
  for (size_t i = 0; i < cbAttStateSize; ++i)
    inputFile.Read(&cbAttStateList[i]);

  // read VkPipelineColorBlendStateCreateInfo
  inputFile.Read(&colorBlendState);

  // read VkPipelineDepthStencilStateCreateInfo
  inputFile.Read(&depthStencilState);

  ShaderBuilder::GetInstance()->BuildShader_OGL(m_curMatLayout);
  return true;
}

void AppManager::Update()
{
  ResourceManager* rm = ResourceManager::GetInstance();

  // Camera controls
  if (ImGui::IsMouseDown(0) && ImGui::IsKeyDown(VK_CONTROL) && !ImGui::IsWindowHovered(hvFlags))
  {
    m_eulerAngle.x += (float)m_mouseDelta[0] * m_camSpeed;
    m_eulerAngle.y += (float)m_mouseDelta[1] * m_camSpeed;
    m_eulerAngle.x = m_eulerAngle.x >= 360.f ? 0.0f : m_eulerAngle.x < 0.0f ? 359.99f : m_eulerAngle.x;
    m_eulerAngle.y = m_eulerAngle.y >= 360.f ? 0.0f : m_eulerAngle.y < 0.0f ? 359.99f : m_eulerAngle.y;
  }
  rm->GetObjTransformMtx_Full() = glm::eulerAngleYXY(glm::radians(m_eulerAngle.x), glm::radians(m_eulerAngle.y), 0.f);

  if (m_scrollDir != 0)
  {
    m_camZPos -= m_camZStrength * m_scrollDir;
    rm->ChangeCameraPosition(glm::vec3(0.f, 0.f, m_camZPos));
  }
  rm->UpdateMatrix();
}

void AppManager::Render()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glClearColor(.2f, .2f, .2f, .2f);
  glEnable(GL_DEPTH_TEST);

  // use shader
  m_curMatLayout->m_shader->UseProgram();

  // bind textures + uniforms
  m_curMatLayout->BindAll();

  m_curMatLayout->Render();
  m_curMatLayout->UnbindAll();
}

void AppManager::LastUpdate()
{
  m_scrollDir = 0;
  m_driverTime += DEFAULT_DT;
}

void AppManager::SetNewMousePosition(int x, int y)
{
  m_mouseDelta[0] = x - m_oldPos[0];
  m_mouseDelta[1] = y - m_oldPos[1];

  m_oldPos[0] = x;
  m_oldPos[1] = y;
}

void AppManager::InitializeMouse(int x, int y)
{
  m_oldPos[0] = x;
  m_oldPos[1] = y;
}

int* AppManager::GetMouseDelta()
{
  return m_mouseDelta;
}

void AppManager::ScrollCam(int dir)
{
  if (!ImGui::IsWindowHovered(hvFlags))
    m_scrollDir = dir;
}

const float& AppManager::GetDriverTime() const
{
  return m_driverTime;
}

MaterialLayoutMap& AppManager::GetAllMaterialLayouts()
{
  return m_materialLayouts;
}

void AppManager::SetSaveDataLocationFromMaterial(const std::string& filePathLoc)
{
  m_openFilePath = filePathLoc;
}

void AppManager::SetMaterialFromLayoutMap(MaterialLayout* layout)
{
  m_curMatLayout = layout;
  ShaderBuilder::GetInstance()->BuildShader_OGL(m_curMatLayout);
}

void AppManager::EditorUpdate()
{
  ImguiWin32::NewFrame();

  /*
    Menu Bar
  */
  {
    if (ImGui::BeginMainMenuBar())
    {
      if (ImGui::BeginMenu("Material"))
      {
        if (ImGui::MenuItem("Library..."))
        {
          ViewMaterialBool = true;
        }

        //if (ImGui::MenuItem("Import..."))
        //{
        //  OpenProject();
        //}

        if (ImGui::MenuItem("Save"))
        {
          if (m_loadedDefault)
          {
          }
          else
            Serialize(m_openFilePath);
        }

        if (ImGui::MenuItem("Save As..."))
        {
          std::string fileLoc = WinSystem::SaveFileAs("Material Layout (.meml)\0*.meml\0All Files\0*.*\0", ".meml");
          std::string newSaveFileName = fileLoc.substr(fileLoc.find_last_of('\\') + 1);
          if (fileLoc.size())
            Serialize(fileLoc, newSaveFileName);
        }

        if (ImGui::BeginMenu("Export"))
        {
          if (ImGui::MenuItem("Vulkan shader..."))
          {
            // Invoke windows save dialog
            std::string fileLoc = WinSystem::SaveFileAs("Vulkan Shader (.vksdr)\0*.vksdr\0All Files\0*.*\0", ".vksdr");
            if (fileLoc.size())
              ShaderBuilder::GetInstance()->ExportPipelineBinary_Vulkan(m_curMatLayout, fileLoc, true);
          }

          if (ImGui::MenuItem("OpenGL shader..."))
          {
            // TODO: Export raw opengl shader
          }

          if (ImGui::MenuItem("Pipeline..."))
          {
            // Invoke windows save dialog
            std::string fileLoc = WinSystem::SaveFileAs("Pipeline Binary (.pbin)\0*.pbin\0All Files\0*.*\0", ".pbin");
            if (fileLoc.size())
              ShaderBuilder::GetInstance()->ExportPipelineBinary_Vulkan(m_curMatLayout, fileLoc);
          }

          if (ImGui::MenuItem("Render Pass..."))
          {
            // Invoke windows save dialog
            std::string fileLoc = WinSystem::SaveFileAs("Render Target Binary (.rtb)\0*.rtb\0All Files\0*.*\0", ".rtb");
            if (fileLoc.size())
              ShaderBuilder::GetInstance()->ExportRenderPass_Vulkan(m_curMatLayout, fileLoc);
          }

          ImGui::EndMenu();
        }

        ImGui::Spacing();
        ImGui::Separator();

        if (ImGui::MenuItem("Exit"))
        {
          ::SendMessage(WinSystem::hwnd, WM_CLOSE, NULL, NULL);
        }

        ImGui::EndMenu();
      }


      // Models
      if (ImGui::BeginMenu("Model"))
      {
        if (ImGui::MenuItem("Library..."))
        {
          ViewModelBool = true;
        }

        //if (ImGui::MenuItem("Import..."))
        //{
        //  ImportModel();
        //}

        ImGui::EndMenu();
      }


      // Textures
      if (ImGui::BeginMenu("Textures"))
      {
        if (ImGui::MenuItem("Library..."))
        {
          ViewTextureLibraryBool = true;
        }
        ImGui::EndMenu();
      }

      // Uniforms
      if (ImGui::BeginMenu("Uniforms"))
      {
        if (ImGui::MenuItem("Add..."))
        {
          ViewUniformBool = true;
          ImguiWin32::ResetResourceNameBuffer();
        }

        ImGui::EndMenu();
      }

      // Shaders
      if (ImGui::BeginMenu("Shader"))
      {
        if (ImGui::MenuItem("Material Pipeline..."))
        {
          ViewPipelineBool = true;
        }

        ImGui::EndMenu();
      }

      // Level
      if (ImGui::BeginMenu("Level Design"))
      {
        if (ImGui::MenuItem("View..."))
        {
          ViewLevelDesign = true;
        }
        ImGui::EndMenu();
      }


      ///*
      //  RUN SHORT CUTS
      //*/
      //ImGuiIO& io = ImGui::GetIO();

      //if (io.KeyCtrl && ImGui::IsKeyDown('O'))
      //{
      //  OpenProject();
      //}

      //// Save...
      //if (io.KeyCtrl && ImGui::IsKeyPressed('S'))
      //{
      //  Serialize(m_saveDataLocation + m_openFileName);
      //}

      //// Save as
      //if (io.KeyShift && io.KeyCtrl && ImGui::IsKeyPressed('S'))
      //{
      //    std::string fileLoc = WinSystem::SaveFileAs("Material Layout (.meml)\0*.meml\0All Files\0*.*\0", ".meml");
      //    if (fileLoc.size())
      //      Serialize(fileLoc);
      //}

      ImGui::EndMainMenuBar();
    }
  }

  {
    const int imguiWinFlag =
      ImGuiWindowFlags_::ImGuiWindowFlags_NoResize |
      ImGuiWindowFlags_::ImGuiWindowFlags_NoSavedSettings |
      ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
    ImGui::SetNextWindowPos(ImVec2(1280 - 317, 19));
    ImGui::SetNextWindowSize(ImVec2(300, 708));

    if (ImGui::Begin("             Material Editor", 0, imguiWinFlag))
    {
      // Format layout name
      ImGui::Text(("Layout: " + m_curMatLayout->m_layoutName).c_str());
      ImGui::Separator();
      ImGui::Spacing();
      ImGui::Spacing();

      ImGui::Text(("Model    : " + m_curMatLayout->m_modelName).c_str());
      ImGui::Spacing();
      ImGui::Spacing();
      ImGui::Spacing();
      ImGui::Spacing();

      // List all uniform input and it type
      ImGui::Text("                Resources");
      ImGui::Separator();
      ImGui::Spacing();
      ImGui::Spacing();

      PipelineLayout::pipelineStageMap& stageMap = m_curMatLayout->m_resourceLayout.GetPipelineStageMap();
      for (auto& stage : stageMap)
      {
        // Shader pipeline group
        ImGui::TextColored(ImVec4(1.f, 0.f, 0.f, 1.f), oglStrElem::uniformStageItems[stage.first].name);
        ImGui::Spacing();

        // Show uniform in their respective column
        for (auto& uniform : stage.second.inputUniforms)
        {
          ImguiWin32::ShowUniformEditor(uniform.first, uniform.second);
          std::string trailingName = "##" + uniform.first;

          if (ImGui::Button(("Rename" + trailingName).c_str(), ImVec2(50, 20)))
          {
            ResourceName = &uniform.first;
            RenameUniformBool = true;
            ImguiWin32::ResetResourceNameBuffer();
          }
          ImGui::SameLine();
          if (ImGui::Button(("Delete" + trailingName).c_str(), ImVec2(50, 20)))
          {
            selectedItemToDelete = uniform.first;
            DeleteUniform = true;
          }

          ImGui::Spacing();
          ImGui::Spacing();
        }

        // Show textures
        for (auto& texture : stage.second.inputTextures)
        {
          std::string trailingName = "##" + texture.first;
          unsigned long long upCastTextureID = texture.second->GetTexture(); // promote then read
          if (ImGui::ImageButton(ImTextureID(upCastTextureID), ImVec2(50, 50)))
          {
            globalViewTexID = ImTextureID(upCastTextureID);
            ViewTextureBool = true;
          }

          ImGui::Text(texture.first.c_str());
          ImGui::SameLine();
          if (ImGui::Button(("Delete" + trailingName).c_str(), ImVec2(50, 20)))
          {
            selectedItemToDelete = texture.first;
            DeleteTexture = true;
          }

          ImGui::Spacing();
          ImGui::Spacing();
        }

        ImGui::Spacing();
        ImGui::Spacing();
      }

      ImGui::End();
    }
  }
  if (ViewTextureBool)
    ImguiWin32::RunTextureViewer(ImTextureID(globalViewTexID), ViewTextureBool);
  if (ViewUniformBool)
    ImguiWin32::RunUniformViewer(ViewUniformBool, m_curMatLayout);
  if (RenameUniformBool)
    ImguiWin32::RenameUniformData(RenameUniformBool, ResourceName, m_curMatLayout);
  if (ViewPipelineBool)
    ImguiWin32::RunPipelineViewer(ViewPipelineBool, m_curMatLayout);
  if (ViewTextureLibraryBool)
    ImguiWin32::RunTextureLibraryViewer(ViewTextureLibraryBool, m_curMatLayout);
  if (ViewMaterialBool)
    ImguiWin32::RunMaterialViewer(ViewMaterialBool);
  if (ViewModelBool)
    ImguiWin32::RunModelViewer(ViewModelBool, m_curMatLayout);
  if (ViewLevelDesign)
    ImguiWin32::RunLevelDesign(ViewLevelDesign);

  // Delete resources
  if (DeleteUniform)
  {
    m_curMatLayout->RemoveUniform(selectedItemToDelete);
    selectedItemToDelete = "";
    DeleteUniform = false;
  }
  if (DeleteTexture)
  {
    m_curMatLayout->RemoveTexture(selectedItemToDelete);
    selectedItemToDelete = "";
    DeleteTexture = false;
  }
}

void AppManager::EditorRender()
{
  // IMGUI Render
  ImguiWin32::Render();
}

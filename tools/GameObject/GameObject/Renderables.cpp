#include "pch.h"
#include "Renderables.h"
#include <limits>

Renderables::Renderables()
  : m_descInstanceNumber(std::numeric_limits<int>::min()),
    m_descSetNumber(std::numeric_limits<unsigned>::max()),
    m_matPipelineIndex(std::numeric_limits<unsigned>::max()),
    m_modelIndex(std::numeric_limits<unsigned>::max()),
    m_rType(_ResourceType::GLOBAL),
    m_interfaceBlockAccess()
{ }

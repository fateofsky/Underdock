#version 450 core

// attribute input
layout (location = 0) in vec4 position;
layout (location = 1) in vec2 uvCoord;

// Default uniform binding
layout (location = 0) uniform mat4 view;
layout (location = 1) uniform mat4 proj;
layout (location = 2) uniform mat4 viewProj;

// uniform input (*VULKAN: change to binding)
layout (location = 3) uniform mat4 model;

// const input

// output down the pipeline
layout (location = 0) out vec2 uvOut;

void main()
{
  uvOut = uvCoord;
  gl_Position = viewProj * model * position;
}
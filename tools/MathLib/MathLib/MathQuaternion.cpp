/* Start Header
*****************************************************************/
/*!
\file      MathQuaternion.cpp
\author    Loo Yan Wen, yanwen.loo, 390005715
\par       yanwen.loo@digipen.edu
\date      Dec 08 2017
\brief

Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the
prior written consent of DigiPen Institute of Technology is prohibited.
*/
/* End Header
*******************************************************************/
#include "stdafx.h"

quat::quat(float x_, float y_, float z_, float w_) : x(x_), y(y_), z(z_), w(w_)
{}

#pragma warning(suppress: 26495) //unitialized member, initialized by Set call
quat::quat(const float& radians, const vec3 & axis)
{
	Set(axis, radians);
}

quat::quat(const vec4 & rhs)
	: w(rhs.w_), x(rhs.x_), y(rhs.y_), z(rhs.z_)
{ }

quat::quat(const std::string& str)
{
	size_t comma1 = str.find(',');
	size_t comma2 = str.find(',', comma1 + 1);
	size_t comma3 = str.find(',', comma2 + 1);
	x = std::stof(str);
	if (comma1 != std::string::npos)
	{
		y = std::stof(&str.front() + comma1);
		if (comma2 != std::string::npos)
		{
			z = std::stof(&str.front() + comma2);
			if (comma3 != std::string::npos)
			{
				w = std::stof(&str.front() + comma3);
			}
			else w = 0;
		}
		else z = 0, w = 0;
	}
	else  y = 0, z = 0, w = 0;
}

void quat::Set(const vec3 & axis, float radians)
{
	float halfAngle = 0.5f * radians;
	float s = sinf(halfAngle);
	x = s * axis.x_;
	y = s * axis.y_;
	z = s * axis.z_;
	w = cosf(halfAngle);
}

void quat::GetAxisAngle(vec3 & axis, float & radians) const
{
	radians = 2.0f * acosf(w);

	float l = FastInvSquare(1.0f - w * w);
	float lCheck = 1.0f / l;

	if (lCheck == 0.0f)
		axis = vec3(0.0f, 0.0f, 0.0f);
	else
		axis = vec3(x * l, y * l, z * l);

	/*radians = 2.0f * acosf(w);

	float l = 1.0f / FastSquare(1.0f - w * w);
	float lCheck = 1.0f / l;

	axis = vec3(x * l, y * l, z * l);

	if (std::isnan(axis.x_)) axis.x_ = 0;
	if (std::isnan(axis.y_)) axis.y_ = 0;
	if (std::isnan(axis.z_)) axis.z_ = 0;

	if (isnan(radians))
		radians = 0;*/
}

vec3 quat::GetEulerAngles(vec3::Format format) const
{
	vec3 rotation;

	switch (format)
	{
	case vec3::XYZ:
	{
		float r11 = 2.0f * (w * x - y * z);
		float r12 = w * w - x * x - y * y + z * z;
		float r21 = 2.0f * (x*z + w * y);
		float r31 = 2.0f * (w * z - x * y);
		float r32 = w * w + x * x - y * y - z * z;

		rotation.x_ = atan2(r11, r12);
		rotation.y_ = asin(r21);
		rotation.z_ = atan2(r31, r32);
		break;
	}
	case vec3::ZYX:
	{
		float sinr_cosp = 2.0f * (w * x + y * z);
		float cosr_cosp = 1.0f - 2.0f * (x * x + y * y);
		rotation.x_ = atan2(sinr_cosp, cosr_cosp);

		float sinp = 2.0f * (w * y - z * x);
		if (fabs(sinp) >= 1.0f)
			rotation.y_ = copysign(PI / 2.0f, sinp);
		else
			rotation.y_ = asin(sinp);

		float siny_cosp = 2.0f * (w * z + x * y);
		float cosy_cosp = 1.0f - 2.0f * (y * y + z * z);
		rotation.z_ = atan2(siny_cosp, cosy_cosp);
		break;
	}
	default:
		throw "Please add your case";
		break;
	}

	return rotation * TODEGREE;
}

void quat::Inversed()
{
	x = -x;
	y = -y;
	z = -z;
}

quat quat::Inverse() const
{
	return quat(-x, -y, -z, w);
}

quat quat::InverseAll() const
{
	return quat(-x, -y, -z, -w);
}

void quat::Integrate(const vec3 & dv, float dt)
{
	quat q(dv.x_ * dt, dv.y_ * dt, dv.z_ * dt, 0.0f);

	q *= *this;

	x += q.x * 0.5f;
	y += q.y * 0.5f;
	z += q.z * 0.5f;
	w += q.w * 0.5f;

	NormalizeQuat(*this, *this);
}


quat::operator std::string() const
{
	return std::to_string(x) + ", " + std::to_string(y) + ", " +
		std::to_string(z) + ", " + std::to_string(w);
}

const quat quat::operator*(const quat & rhs) const
{
	return quat(
		w * rhs.x + x * rhs.w + y * rhs.z - z * rhs.y,
		w * rhs.y + y * rhs.w + z * rhs.x - x * rhs.z,
		w * rhs.z + z * rhs.w + x * rhs.y - y * rhs.x,
		w * rhs.w - x * rhs.x - y * rhs.y - z * rhs.z
	);
	//return quat(
	//w * rhs.x + x * rhs.w - y * rhs.z - z * rhs.y, 
	//w * rhs.y - x * rhs.z + y * rhs.w - z * rhs.x, 
	//w * rhs.z + x * rhs.y - y * rhs.x + z * rhs.w, 
	//w * rhs.w - x * rhs.x - y * rhs.y - z * rhs.z);
}

quat quat::operator*=(const quat & rhs)
{
	//quat temp(
	//  w * rhs.x + x * rhs.w + y * rhs.z - z * rhs.y,
	//  w * rhs.y + y * rhs.w + z * rhs.x - x * rhs.z,
	//  w * rhs.z + z * rhs.w + x * rhs.y - y * rhs.x,
	//  w * rhs.w - x * rhs.x - y * rhs.y - z * rhs.z
	//);
	//
	//*this = temp;
	//return *this;
	*this = *this * rhs;
	return *this;
}

const mat3 quat::ToMat3() const
{
	mat3 c;

	float q0q0 = w * w;
	float q1q1 = x * x;
	float q2q2 = y * y;
	float q3q3 = z * z;

	float q1q3 = x * z;
	float q1q2 = x * y;
	float q0q1 = w * x;
	float q0q3 = w * z;
	float q0q2 = w * y;
	float q2q3 = y * z;

	c[0] = q0q0 + q1q1 - q2q2 - q3q3;
	c[1] = 2.f * (q1q2 - q0q3);
	c[2] = 2.f * (q1q3 + q0q2);

	c[3] = 2.f * (q1q2 + q0q3);
	c[4] = q0q0 - q1q1 + q2q2 - q3q3;
	c[5] = 2.f * (q2q3 - q0q1);

	c[6] = 2.f * (q1q3 - q0q2);
	c[7] = 2.f * (q2q3 + q0q1);
	c[8] = q0q0 - q1q1 - q2q2 + q3q3;

	//c[0] = 1.0f - 2.0f * (q2q2 + q3q3);
	//c[1] = 2.0f * (q1q2 + q0q3);
	//c[2] = 2.0f * (q1q3 - q0q2);
	//c[3] = 2.0f * (q1q2 - q0q3);
	//c[4] = 1.0f - 2.0f * (q1q1 + q3q3);
	//c[5] = 2.0f * (q2q3 + q0q1);
	//c[6] = 2.0f * (q1q3 + q0q2);
	//c[7] = 2.0f * (q2q3 - q0q1);
	//c[8] = 1.0f - 2.0f * (q1q1 + q2q2);
	//c.Transpose();
	return c;
}

const mat4 quat::ToMat4() const
{
	//mat4 c;
	//
	//float q0q0 = w * w;
	//float q1q1 = x * x;
	//float q2q2 = y * y;
	//float q3q3 = z * z;
	//
	//float q1q3 = x * z;
	//float q1q2 = x * y;
	//float q0q1 = w * x;
	//float q0q3 = w * z;
	//float q0q2 = w * y;
	//float q2q3 = y * z;
	//
	//c[0] = q0q0 + q1q1 - q2q2 - q3q3;
	//c[1] = 2.f * (q1q2 - q0q3);
	//c[2] = 2.f * (q1q3 + q0q2);
	//
	//c[4] = 2.f * (q1q2 + q0q3);
	//c[5] = q0q0 - q1q1 + q2q2 - q3q3;
	//c[6] = 2.f * (q2q3 - q0q1);
	//
	//c[8] = 2.f * (q1q3 - q0q2);
	//c[9] = 2.f * (q2q3 + q0q1);
	//c[10] = q0q0 - q1q1 - q2q2 + q3q3;
	//return c;

	mat4 c;
	c = ToMat3();
	return c;
}

vec3 quat::GetVector() const
{
	return vec3(x, y, z);
}

vec3 quat::GetRotationVector() const
{
	vec3 worldRotation = vec3();
	float radian = 0;
	GetAxisAngle(worldRotation, radian);
	auto old = worldRotation;
	worldRotation *= (radian*TODEGREE);
	if (std::isnan(worldRotation.x_))
		worldRotation.x_ = 0;
	if (std::isnan(worldRotation.y_))
		worldRotation.y_ = 0;
	if (std::isnan(worldRotation.z_))
		worldRotation.z_ = 0;

	return worldRotation;
}


void QuaternionInterpolate(mat4& out, const vec4& start, const vec4& end, double fac)
{
	// calc cosine theta
	float cosom = start.x_ * end.x_ + start.y_ * end.y_ + start.z_ * end.z_ + start.w_ * end.w_;

	// adjust signs (if necessary)
	vec4 tmpEnd = end, tmpOut;
	if (cosom < 0.0) {
		cosom = -cosom;
		tmpEnd.x_ = -end.x_;   // Reverse all signs
		tmpEnd.y_ = -end.y_;
		tmpEnd.z_ = -end.z_;
		tmpEnd.w_ = -end.w_;
	}

	// Calculate coefficients
	double sclp, sclq;
	if ((1.f - cosom) > 0.0001f) // 0.0001 -> some epsillon
	{
		// Standard case (slerp)
		double omega, sinom;
		omega = std::acos(cosom); // extract theta from dot product's cos theta
		sinom = std::sin(omega);
		sclp = std::sin((1.0 - fac) * omega) / sinom;
		sclq = std::sin(fac * omega) / sinom;
	}
	else
	{
		// Very close, do linear interp (because it's faster)
		sclp = 1.0 - fac;
		sclq = fac;
	}

	quat q, qout;
	q.x = (float)sclp * start.x_ + (float)sclq * tmpEnd.x_;
	q.y = (float)sclp * start.y_ + (float)sclq * tmpEnd.y_;
	q.z = (float)sclp * start.z_ + (float)sclq * tmpEnd.z_;
	q.w = (float)sclp * start.w_ + (float)sclq * tmpEnd.w_;

	// normalize

	NormalizeQuat(qout, q);
	out = qout.ToMat4();
}

quat  QuaternionInterpolate(const vec4 & start, const vec4 & end, double fac)
{
	// calc cosine theta
	float cosom = start.x_ * end.x_ + start.y_ * end.y_ + start.z_ * end.z_ + start.w_ * end.w_;

	// adjust signs (if necessary)
	vec4 tmpEnd = end, tmpOut;
	if (cosom < 0.0) {
		cosom = -cosom;
		tmpEnd.x_ = -end.x_;   // Reverse all signs
		tmpEnd.y_ = -end.y_;
		tmpEnd.z_ = -end.z_;
		tmpEnd.w_ = -end.w_;
	}

	// Calculate coefficients
	double sclp, sclq;
	if ((1.f - cosom) > 0.0001f) // 0.0001 -> some epsillon
	{
		// Standard case (slerp)
		double omega, sinom;
		omega = std::acos(cosom); // extract theta from dot product's cos theta
		sinom = std::sin(omega);
		sclp = std::sin((1.0 - fac) * omega) / sinom;
		sclq = std::sin(fac * omega) / sinom;
	}
	else
	{
		// Very close, do linear interp (because it's faster)
		sclp = 1.0 - fac;
		sclq = fac;
	}

	quat q, qout;
	q.x = (float)sclp * start.x_ + (float)sclq * tmpEnd.x_;
	q.y = (float)sclp * start.y_ + (float)sclq * tmpEnd.y_;
	q.z = (float)sclp * start.z_ + (float)sclq * tmpEnd.z_;
	q.w = (float)sclp * start.w_ + (float)sclq * tmpEnd.w_;

	// normalize

	NormalizeQuat(qout, q);
	return qout;
}

void  QuaternionNlerp(mat4 & out, const quat & start, const quat & end, double fac)
{
	float dp = QuaternionDotProduct(start, end);
	quat result;
	quat tmpEnd = (dp < 0.0f) ? end.InverseAll() : end;
	tmpEnd.x = fac * ((double)tmpEnd.x - (double)start.x) + start.x;
	tmpEnd.y = fac * ((double)tmpEnd.y - (double)start.y) + start.y;
	tmpEnd.z = fac * ((double)tmpEnd.z - (double)start.z) + start.z;
	tmpEnd.w = fac * ((double)tmpEnd.w - (double)start.w) + start.w;
	NormalizeQuat(result, tmpEnd);
	out = result.ToMat4();
}

quat  QuaternionNlerp(const quat & start, const quat & end, double fac)
{
	float dp = QuaternionDotProduct(start, end);
	quat result;
	quat tmpEnd = (dp < 0.0f) ? end.InverseAll() : end;
	tmpEnd.x = fac * ((double)tmpEnd.x - (double)start.x) + start.x;
	tmpEnd.y = fac * ((double)tmpEnd.y - (double)start.y) + start.y;
	tmpEnd.z = fac * ((double)tmpEnd.z - (double)start.z) + start.z;
	tmpEnd.w = fac * ((double)tmpEnd.w - (double)start.w) + start.w;
	NormalizeQuat(result, tmpEnd);
	return result;
}


//void ConvertQuarternionMtx(mat4& out_mtx, const vec4& st)
//{
//  out_mtx[0] = 1.0f - (2.0f*st.y_*st.y_) - (2.0f * st.z_*st.z_);
//  out_mtx[1] = (2.0f*st.x_*st.y_) - (2.0f*st.z_*st.w_);
//  out_mtx[2] = (2.0f*st.x_*st.z_) + (2.0f*st.y_*st.w_);
//
//  out_mtx[4] = (2.0f*st.x_*st.y_) + (2.0f*st.z_*st.w_);
//  out_mtx[5] = 1.0f - (2.0f*st.x_*st.x_) - (2.0f*st.z_*st.z_);
//  out_mtx[6] = (2.0f*st.y_*st.z_) - (2.0f*st.x_*st.w_);
//
//  out_mtx[8] = (2.0f*st.x_*st.z_) - (2.0f*st.y_*st.w_);
//  out_mtx[9] = (2.0f*st.y_*st.z_) + (2.0f*st.x_*st.w_);
//  out_mtx[10] = 1.0f - (2.0f*st.x_*st.x_) - (2.0f*st.y_*st.y_);
//
//  out_mtx[3] = out_mtx[7] = out_mtx[11] = out_mtx[12] = out_mtx[13] = out_mtx[14] = 0.0f;
//  out_mtx[15] = 1.0f;
//}

float  QuaternionDotProduct(const quat & start, const quat & end)
{
	return start.x * end.x + start.y * end.y + start.z * end.z + start.w * end.w;
}

void NormalizeQuat(quat & out, const quat & in)
{
	float newx = in.x;
	float newy = in.y;
	float newz = in.z;
	float neww = in.w;

	float d = neww * neww + newx * newx + newy * newy + newz * newz;

	if (d < FLT_EPSILON)
		neww = 1.0f;

	d = FastInvSquare(d);// 1.0f / std::sqrt(d);

	if (d > FLT_EPSILON)
	{
		newx *= d;
		newy *= d;
		newz *= d;
		neww *= d;
	}

	out = quat(newx, newy, newz, neww);
}


quat YPRToQuat(const float& yaw, const float& pitch, const float& roll)	// z, y, x
{
	quat q;
	// Abbreviations for the various angular functions
	double cy = cos(yaw * 0.5);
	double sy = sin(yaw * 0.5);
	double cr = cos(roll * 0.5);
	double sr = sin(roll * 0.5);
	double cp = cos(pitch * 0.5);
	double sp = sin(pitch * 0.5);

	q.w = cy * cr * cp + sy * sr * sp;
	q.x = cy * sr * cp - sy * cr * sp;
	q.y = cy * cr * sp + sy * sr * cp;
	q.z = sy * cr * cp - cy * sr * sp;

	// z, y, x
	//q = quat(yaw, vec3(0.0f, 0.0f, 1.0f)) *
	//	quat(pitch, vec3(0.0f, 1.0f, 0.0f)) *
	//	quat(roll, vec3(1.0f, 0.0f, 0.0f))
	//	;

	return q;
}

quat YPRToQuat(const vec3 & yawPitchRoll)
{
	return YPRToQuat(yawPitchRoll.z_, yawPitchRoll.y_, yawPitchRoll.x_);
}

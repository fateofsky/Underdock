/* Start Header
*****************************************************************/
/*!
\file      MathMatrix4D.cpp
\author    
\par       
\date      
\brief

Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the
prior written consent of DigiPen Institute of Technology is prohibited.
*/
/* End Header
*******************************************************************/
#include "stdafx.h"

#pragma warning(suppress: 26495) //unitiaizlied member, but its a union so...
mat4::mat4()
	: m_44{ 1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f }
{}

mat4::mat4(const float* pArr)
{
	std::copy(pArr, pArr + 16, m_44);
}

mat4::mat4(float _00, float _01, float _02, float _03,
		     float _10, float _11, float _12, float _13,
		     float _20, float _21, float _22, float _23,
		     float _30, float _31, float _32, float _33)
  : m00{ _00 }, m10{ _10 }, m20{ _20 }, m30{ _30 },
    m01{ _01 }, m11{ _11 }, m21{ _21 }, m31{ _31 },
    m02{ _02 }, m12{ _12 }, m22{ _22 }, m32{ _32 },
    m03{ _03 }, m13{ _13 }, m23{ _23 }, m33{ _33 }
{}

mat4::mat4(const mat3& mat3)
{
	m00 = mat3.m00;
	m01 = mat3.m01;
	m02 = mat3.m02;
	m03 = 0.0f;
	m10 = mat3.m10;
	m11 = mat3.m11;
	m12 = mat3.m12;
	m13 = 0.0f;
	m20 = mat3.m20;
	m21 = mat3.m21;
	m22 = mat3.m22;
	m23 = 0.0f;
	m30 = 0.0f;
	m31 = 0.0f;
	m32 = 0.0f;
	m33 = 1.0f;
}
#pragma warning(suppress: 26495) //unitiaizlied member, but its a union so...
mat4::mat4(const std::string& str)
{
	size_t comma = 0;
	for (float& elem : m_44)
	{
		if (comma != std::string::npos)
		{
			elem = std::atof(&str.front() + comma);
			comma = str.find(',', comma + 1) + 1;
		}
		else elem = 0;
	}
}

mat4::operator std::string() const
{
	std::string out;
	for (const float& elem : m_44)
	{
		out += std::to_string(elem) + ", ";
	}
	out.pop_back();
	out.pop_back();
	return out;
}

mat4& mat4::operator=(const mat3& rhs)
{
	m00 = rhs.m00;
	m01 = rhs.m01;
	m02 = rhs.m02;
	m03 = 0.0f;
	m10 = rhs.m10;
	m11 = rhs.m11;
	m12 = rhs.m12;
	m13 = 0.0f;
	m20 = rhs.m20;
	m21 = rhs.m21;
	m22 = rhs.m22;
	m23 = 0.0f;
	m30 = 0.0f;
	m31 = 0.0f;
	m32 = 0.0f;
	m33 = 1.0f;
	return *this;
}

mat4& mat4::operator=(const mat4& rhs)
{
	std::copy(rhs.m_44, rhs.m_44 + mat4_LEN, this->m_44);
	return *this;
}

mat4& mat4::operator*=(const mat4& rhs)
{
	*this = *this * rhs;
	return *this;
}

mat4& mat4::operator+=(const mat4& rhs)
{
	*this = *this + rhs;
	return *this;
}

mat4& mat4::operator-=(const mat4& rhs)
{
	*this = *this - rhs;
	return *this;
}

mat4 & mat4::operator=(const float * arr44)
{
	for (int i = 0; i < mat4_LEN; i++)
		m_44[i] = arr44[i];
	return *this;
}

float mat4::operator[](int i) const
{
	return m_44[i];
}

float& mat4::operator[](int i)
{
	return m_44[i];
}

mat4 mat4::inversed() const
{
	mat4 inv;
	mat4Inverse(inv, *this, nullptr);
	return inv;
}

mat4 mat4::transposed() const
{
	mat4 trans;
	mat4Transpose(trans, *this);
	return trans;
}

const float * mat4::get() const
{
	return m_44;
}

void mat4::Identity()
{
	*this = mat4();
}

void mat4::Zero()
{
	*this = mat4Zero();
}

void mat4::Inverse()
{
	float det;
	mat4Inverse(*this, *this, &det);

	if (det <= 0.0f)
		*this = mat4();
}

void mat4::Transpose()
{
	mat4Transpose(*this, *this);
}

mat4 operator*(const mat4 & lhs, const float & rhs)
{
	mat4 tmp(lhs);
	tmp[0] *= rhs;
	tmp[1] *= rhs;
	tmp[2] *= rhs;
	tmp[3] *= rhs;
	tmp[4] *= rhs;
	tmp[5] *= rhs;
	tmp[6] *= rhs;
	tmp[7] *= rhs;
	tmp[8] *= rhs;
	tmp[9] *= rhs;
	tmp[10] *= rhs;
	tmp[11] *= rhs;
	tmp[12] *= rhs;
	tmp[13] *= rhs;
	tmp[14] *= rhs;
	tmp[15] *= rhs;
	return tmp;
}

mat4 operator*(const mat4 &lhs, const mat4 &rhs)
{
	mat4 mt;

	const float * inA = (const float *)&lhs;
	const float * inB = (const float *)&rhs;
	float * out = (float *)&mt;

	__m128 ymm0, ymm1, ymm2, ymm3,	// inA
	ymm4, ymm5, ymm6, ymm7;	// inB

	// Read in the rows of inB
	ymm4 = _mm_load_ps(inB);
	ymm5 = _mm_load_ps(inB + 4);
	ymm6 = _mm_load_ps(inB + 8);
	ymm7 = _mm_load_ps(inB + 12);

	// Results Row 0
	ymm0 = _mm_broadcast_ss(inA);
	ymm1 = _mm_broadcast_ss(inA + 1);
	ymm2 = _mm_broadcast_ss(inA + 2);
	ymm3 = _mm_broadcast_ss(inA + 3);

	ymm0 = _mm_mul_ps(ymm0, ymm4);
	ymm1 = _mm_mul_ps(ymm1, ymm5);
	ymm0 = _mm_add_ps(ymm0, ymm1);

	ymm2 = _mm_mul_ps(ymm2, ymm6);
	ymm3 = _mm_mul_ps(ymm3, ymm7);
	ymm2 = _mm_add_ps(ymm2, ymm3);

	ymm0 = _mm_add_ps(ymm0, ymm2);

	_mm_store_ps(out, ymm0);

	// Results Row 1
	ymm0 = _mm_broadcast_ss(inA + 4);
	ymm1 = _mm_broadcast_ss(inA + 5);
	ymm2 = _mm_broadcast_ss(inA + 6);
	ymm3 = _mm_broadcast_ss(inA + 7);

	ymm0 = _mm_mul_ps(ymm0, ymm4);
	ymm1 = _mm_mul_ps(ymm1, ymm5);
	ymm0 = _mm_add_ps(ymm0, ymm1);

	ymm2 = _mm_mul_ps(ymm2, ymm6);
	ymm3 = _mm_mul_ps(ymm3, ymm7);
	ymm2 = _mm_add_ps(ymm2, ymm3);

	ymm0 = _mm_add_ps(ymm0, ymm2);

	_mm_store_ps(out + 4, ymm0);

	// Results Row 2
	ymm0 = _mm_broadcast_ss(inA + 8);
	ymm1 = _mm_broadcast_ss(inA + 9);
	ymm2 = _mm_broadcast_ss(inA + 10);
	ymm3 = _mm_broadcast_ss(inA + 11);

	ymm0 = _mm_mul_ps(ymm0, ymm4);
	ymm1 = _mm_mul_ps(ymm1, ymm5);
	ymm0 = _mm_add_ps(ymm0, ymm1);

	ymm2 = _mm_mul_ps(ymm2, ymm6);
	ymm3 = _mm_mul_ps(ymm3, ymm7);
	ymm2 = _mm_add_ps(ymm2, ymm3);

	ymm0 = _mm_add_ps(ymm0, ymm2);

	_mm_store_ps(out + 8, ymm0);

	// Results Row 3
	ymm0 = _mm_broadcast_ss(inA + 12);
	ymm1 = _mm_broadcast_ss(inA + 13);
	ymm2 = _mm_broadcast_ss(inA + 14);
	ymm3 = _mm_broadcast_ss(inA + 15);

	ymm0 = _mm_mul_ps(ymm0, ymm4);
	ymm1 = _mm_mul_ps(ymm1, ymm5);
	ymm0 = _mm_add_ps(ymm0, ymm1);

	ymm2 = _mm_mul_ps(ymm2, ymm6);
	ymm3 = _mm_mul_ps(ymm3, ymm7);
	ymm2 = _mm_add_ps(ymm2, ymm3);

	ymm0 = _mm_add_ps(ymm0, ymm2);

	_mm_store_ps(out + 12, ymm0);

	return mt;
	
}

mat4 operator+(const mat4 &lhs, const mat4 &rhs)
{
	mat4 sum;
	
	const float * inA = (const float *)&lhs;
	const float * inB = (const float *)&rhs;
	float * out = (float *)&sum;
	
	for (int i = 0; i < 4; i++)
	{
		__m128 Ymm_A = _mm_load_ps(inA);
		__m128 Ymm_B = _mm_load_ps(inB);
		
		__m128 Ymm_Results = _mm_add_ps(Ymm_A, Ymm_B);
		
		_mm_store_ps(out, Ymm_Results);
		
		inA += 4;
		inB += 4;
		out += 4;
	}

	return sum;
}



mat4 operator-(const mat4 &lhs, const mat4 &rhs)
{
	mat4 diff;

	const float * inA = (const float *)&lhs;
	const float * inB = (const float *)&rhs;
	float * out = (float *)&diff;
	
	for (int i = 0; i < 4; i++)
	{
		__m128 Ymm_A = _mm_load_ps(inA);
		__m128 Ymm_B = _mm_load_ps(inB);
		
		__m128 Ymm_Results = _mm_sub_ps(Ymm_A, Ymm_B);
		
		_mm_store_ps(out, Ymm_Results);
		
		inA += 4;
		inB += 4;
		out += 4;
	}

	return diff;
}

vec3 operator*(const mat4 &pMtx, const vec3& inVec3)
{
	vec4 tmp(inVec3.x_, inVec3.y_, inVec3.z_, 1.0f);
	tmp = pMtx * tmp;
	return vec3(tmp.x_, tmp.y_, tmp.z_);
}

vec4 operator*(const mat4 &pMtx, const vec4& inVec4)
{
	vec4 Temp;
	Temp.x_ = pMtx.m_44[0] * inVec4.x_ + pMtx.m_44[1] * inVec4.y_ + pMtx.m_44[2] * inVec4.z_ + pMtx.m_44[3] * inVec4.w_;
	Temp.y_ = pMtx.m_44[4] * inVec4.x_ + pMtx.m_44[5] * inVec4.y_ + pMtx.m_44[6] * inVec4.z_ + pMtx.m_44[7] * inVec4.w_;
	Temp.z_ = pMtx.m_44[8] * inVec4.x_ + pMtx.m_44[9] * inVec4.y_ + pMtx.m_44[10] * inVec4.z_ + pMtx.m_44[11] * inVec4.w_;
	Temp.w_ = pMtx.m_44[12] * inVec4.x_ + pMtx.m_44[13] * inVec4.y_ + pMtx.m_44[14] * inVec4.z_ + pMtx.m_44[15] * inVec4.w_;
	return Temp;
}

bool operator==(const mat4 &lhs, const mat4 &rhs)
{
	for (int i = 0; i < mat4_LEN; i++)
	{
		if (lhs[i] != rhs[i])
			return false;
	}

	return true;
}

bool operator!=(const mat4 &lhs, const mat4 &rhs)
{
	return !(lhs == rhs);
}

std::ostream& operator<<(std::ostream& os, const mat4& mat4)
{
	os << mat4.m_44[0] << " " << mat4.m_44[1] << " " << mat4.m_44[2] << " " << mat4.m_44[3] << std::endl
		<< mat4.m_44[4] << " " << mat4.m_44[5] << " " << mat4.m_44[6] << " " << mat4.m_44[7] << std::endl
		<< mat4.m_44[8] << " " << mat4.m_44[9] << " " << mat4.m_44[10] << " " << mat4.m_44[11] << std::endl
		<< mat4.m_44[12] << " " << mat4.m_44[13] << " " << mat4.m_44[14] << " " << mat4.m_44[15];

	return os;
}

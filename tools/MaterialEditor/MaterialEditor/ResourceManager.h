#pragma once
#include "Model.h"
#include "Shader.h"
#include "VertexBuffer.h"
#include "Texture.h"

class ResourceManager
{
public:
  static void Initialize();
  static void Destroy();
  static ResourceManager* GetInstance();

  void ChangeCameraPosition(const glm::vec3& pos);
  void ChangeFOV(float degree);
  void ChangeAspect(float asp);
  void ChangeNear(float nearPlane);
  void ChangeFar(float farPlane);

  void UpdateMatrix();

  const std::vector<vPack>* GetVertexBufferData(const std::string& name);
  const std::vector<VertexBuffer>* GetVertexBuffer(const std::string& name, const unsigned& index = 0);
  Texture*      GetTexture(const std::string& name);
  Shader*       GetShader(const std::string& name);
  Model*        GetModel(const std::string& name);
  float*        GetViewProjMtx();
  float*        GetViewMtx();
  float*        GetProjMtx();
  float*        GetObjTransformMtx();

  glm::mat4&    GetViewMtx_Full();
  glm::mat4&    GetProjMtx_Full();
  glm::mat4&    GetVP_Full();
  glm::mat4&    GetObjTransformMtx_Full();

  std::vector<unsigned> CreateVertexBuffer(const std::string& modelName, const std::string& bufferName, const vBufferLayout& bufferLayout);
  std::vector<unsigned> ComputeVertexBufferIndexList(const std::string& modelName, const std::string& bufferName);
  bool                  LoadModel(const std::string& file, bool normalizeVertex = false);
  bool                  LoadTexture(const std::string& file);
  bool                  LoadTexture(const std::string& file, std::string& textureName);
  bool                  LoadShader(const std::string& file, const std::string& sdrName);
  bool                  CreateEmptyShader(const std::string& sdrName);
  bool                  ReplaceShaderContent(const std::string& sdrName, const Shader& newSdr);
  bool                  CreateShaderProgram(const std::string& sdrName);

  // IMGUI USAGE
  std::map<std::string, Texture>& GetAllTextures();
  std::map<std::string, Model>&   GetAllModel();

private:
  ResourceManager();
  static ResourceManager*             m_instance;

  // camera properties
  glm::mat4                           m_camViewProj;
  glm::mat4                           m_camView;
  glm::mat4                           m_camProj;
  glm::vec3                           m_camPosition;
  float                               m_fov;
  float                               m_aspect;
  float                               m_near;
  float                               m_far;

  // Object transformation
  glm::mat4                           m_objectTransformation;

  // opengl vertex buffer resource
  std::map<std::string, std::vector<VertexBuffer>>  m_vBufferMap;     // opengl vertex buffer (vao, vbo, ebo)
  std::map<std::string, std::vector<vPack>>         m_BufferPackMap;  // data containing actual vertex buffer
  std::map<std::string, Texture>                    m_textureMap;     // opengl texture
  std::map<std::string, Shader>                     m_vShaderMap;     // opengl shader
  std::map<std::string, Model>                      m_modelMap;       // model object

  // utilities

  // internal
  bool                                      m_scheduledRefresh = false;
};
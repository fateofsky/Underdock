#include "../pch.h"
#include "Engine.h"
#include "VulkanDebugger.h"

#if defined _DEBUG
const std::vector<const char*> vkDebugger::validationLayer = { "VK_LAYER_KHRONOS_validation" };
VkDebugUtilsMessengerEXT      vkDebugger::vDebugMsgr;

std::string vkDebugger::GetPhysicalDeviceTypeString(const VkPhysicalDeviceType & dType)
{
  switch (dType)
  {
  case VK_PHYSICAL_DEVICE_TYPE_OTHER:
    return "Other";

  case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:
    return "Integrated GPU";

  case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
    return "Dedicated GPU";

  case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:
    return "Virtual GPU";

  case VK_PHYSICAL_DEVICE_TYPE_CPU:
    return "CPU";

  default: return "";
  }
}

bool vkDebugger::InitValidationLayer()
{
  /*
      Create / initialize validation layers
  */
  // get layer properties
  uint32_t layerCount;
  vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
  std::vector<VkLayerProperties> availableLayers(layerCount);
  vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

  // check supports
  for (auto& layerName : validationLayer)
  {
    bool found = false;
    for (auto& layerProp : availableLayers)
    {
      if (std::strcmp(layerName, layerProp.layerName) == 0)
      {
        found = true;
        break;
      }

      if (found) break;
    }
  }

  return true;
}


bool vkDebugger::InitDebugUtils()
{
  /*
    Create vulkan debug utils msg (DEBUG MODE ONLY)
  */
  VkDebugUtilsMessengerCreateInfoEXT dbInfo;

  dbInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
  dbInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
    VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
    VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
  dbInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
    VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
    VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
  dbInfo.pfnUserCallback = vulkanDebugCallback;
  dbInfo.pUserData = NULL;
  dbInfo.flags = 0;
  dbInfo.pNext = NULL;

  auto funcDebug = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(Engine::core::vInst, "vkCreateDebugUtilsMessengerEXT");
  assert(funcDebug != nullptr);
  VkResult vkRes = funcDebug(Engine::core::vInst, &dbInfo, NULL, &vDebugMsgr);
  assert(vkRes == VK_SUCCESS);
  std::cout << "Vulkan debug utility messenger initialized...\n";
  return vkRes == VK_SUCCESS;
}

void vkDebugger::DestroyDebugUtils()
{
  // remove vulkan debug utils
  auto desFunc = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(Engine::core::vInst, "vkDestroyDebugUtilsMessengerEXT");
  assert(desFunc != nullptr);
  desFunc(Engine::core::vInst, vDebugMsgr, NULL);
}


VKAPI_ATTR VkBool32 VKAPI_CALL vkDebugger::vulkanDebugCallback(
  VkDebugUtilsMessageSeverityFlagBitsEXT       messageSeverity, 
  VkDebugUtilsMessageTypeFlagsEXT              messageType, 
  const VkDebugUtilsMessengerCallbackDataEXT * pCallbackData,
  void *                                       )
{
  std::cout << "\n[VULKAN DEBUG] :\n -> type: ";

  switch (messageType)
  {
  case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT:
    std::cout << "General";
    break;
  case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT:
    std::cout << "Validation";
    break;
  case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT:
    std::cout << "Performance";
    break;
  }

  std::cout << "    Severity: ";
  switch (messageSeverity)
  {
  case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
    std::cout << "Verbose\n";
    break;
  case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
    std::cout << "Info\n";
    break;
  case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
    std::cout << "WARNING\n";
    break;
  case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
    std::cout << "ERROR\n";
    break;
  }

  std::cout << "{\n  " << pCallbackData->pMessage << "\n}\n";

  return VK_FALSE;
}
  
void vkDebugger::ShowGPUSpecs()
{
  // Extract GPU properties
  VkPhysicalDeviceProperties gpuProp;
  vkGetPhysicalDeviceProperties(Engine::core::vPdVec[0], &gpuProp);
  std::string vulkanAppVersion(std::to_string(VK_APP_MAJOR) + "." + std::to_string(VK_APP_MID) + "." + std::to_string(VK_APP_MINOR));

  std::cout << "\n --- Vulkan Renderer v" << vulkanAppVersion << " ---\n";
  std::cout << " -> GPU:         " << gpuProp.deviceName << std::endl;
  std::cout << " -> Device type: " << vkDebugger::GetPhysicalDeviceTypeString(gpuProp.deviceType).data() << std::endl;
  std::cout << " -> Device ID:   " << gpuProp.deviceID << std::endl;
  std::cout << " -> Driver:      " << gpuProp.driverVersion << std::endl;
  std::cout << " -> Vendor ID    " << gpuProp.vendorID << std::endl;
  std::cout << " -> API version: " << gpuProp.apiVersion << std::endl << std::endl;
}
#endif
/* Start Header
*****************************************************************/
/*!
\file      MathQuaternion.h
\author    Loo Yan Wen, yanwen.loo, 390005715
\par       yanwen.loo@digipen.edu
\date      Dec 08 2017
\brief

Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the
prior written consent of DigiPen Institute of Technology is prohibited.
*/
/* End Header
*******************************************************************/
#pragma once
#include "MathLib.h"

union mat4;
union mat3;
union vec4;
union vec3;

class quat
{
public:
	quat(float x_ = 0.0f, float y_ = 0.0f, float z_ = 0.0f, float w_ = 1.0f);
	quat(const float& radians, const vec3 & axis);
	quat(const vec4& rhs);
	quat(const std::string& str);

	void Set(const vec3& axis, float radians);
	void GetAxisAngle(vec3 & axis, float & radians) const;
	vec3 GetEulerAngles(vec3::Format format = vec3::XYZ) const;
	void Inversed();
	quat InverseAll() const;
	quat Inverse() const;
	void Integrate(const vec3& dv, float dt);
	operator std::string() const;
	const quat operator*(const quat& rhs) const;
	quat operator*=(const quat& rhs);

	const mat3 ToMat3() const;
	const mat4 ToMat4() const;
	vec3 GetVector() const;
	vec3 GetRotationVector() const;

	float x, y, z, w;
};

//void ConvertQuarternionMtx(mat4& out_mtx, const vec4& stuff);
void  QuaternionInterpolate(mat4& out, const vec4& start, const vec4& end, double fac);
quat  QuaternionInterpolate(const vec4& start, const vec4& end, double fac);
void  QuaternionNlerp(mat4& out, const quat& start, const quat& end, double fac);
quat  QuaternionNlerp(const quat& start, const quat& end, double fac);
float  QuaternionDotProduct(const quat& start, const quat& end);
void  NormalizeQuat(quat& out, const quat& in);
quat  YPRToQuat(const float& yaw, const float& pitch, const float& roll);
quat  YPRToQuat(const vec3& yawPitchRoll);
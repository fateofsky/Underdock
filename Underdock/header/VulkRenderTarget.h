#pragma once
#include <vulkan/vulkan.h>
#include <vector>
#include "VulkTexImage.h"

struct VulkRenderTarget
{
  struct _outputAttachment
  {
    VkFramebuffer             m_vFrameBuffer;
    std::vector<VulkTexImage> m_vOutputAttachment;
  };
  
  void BeginRenderTargetPass(bool primaryBuffer = true);
  void EndRenderTargetPass();
  void LoadRenderTarget_1(const std::string& file);
  void AddAttachment(const unsigned& width, const unsigned& height,
                        VkFormat fmt = VK_FORMAT_R8G8B8A8_UNORM, VkImageUsageFlags usage = VkImageUsageFlagBits::VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
                        VkImageAspectFlags flags = VK_IMAGE_ASPECT_COLOR_BIT);
  std::vector<VkImageView> GetAttachmentImageView(const unsigned& frameIndex);
  VkFramebuffer& GetFramebuffer(const unsigned& frameIndex);
  VkSampler& GetAttachmentSampler();

  std::vector<_outputAttachment>  m_outputAttachmentPerFrame;
  std::vector<VkClearValue>       m_clearValues;
  VkRenderPass                    m_renderPass;
  VkSampler                       m_renderPassSampler;
};
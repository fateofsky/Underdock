#pragma once
#include <vector>


struct Renderables
{
  struct _TextureRefIBPair
  {
    unsigned _renderTargetID;
    unsigned _textureID;
    unsigned _interfaceBlockID;
  };

  struct _renderablesInterfaceBlock
  {
    // access through Engine::swpch::vCurFrame
    std::vector<uint8_t*> _uniformDataPtrList; // number of data based on swapchain images
    unsigned              _size = 0;
    unsigned              _textureRef;
    unsigned              _renderTargetRef;
    bool                  _isAttachmentSampler = false;
  };

  enum _ResourceType
  {
    GLOBAL,
    SHARED,
    INSTANCE,
  };
  Renderables();

  // descriptor infomation
  int      m_descInstanceNumber;
  unsigned m_descSetNumber;
  unsigned m_matPipelineIndex;

  // model information
  unsigned m_modelIndex;

  // resource type
  _ResourceType m_rType;

  // number of interface block in the set this renderable is currently referencing to (binding number)
  std::vector<_renderablesInterfaceBlock> m_interfaceBlockAccess;

};
#include "pch.h"
#include "ImGuiModelViewer.h"
#include "AppManager.h"
#include "WindowSystem.h"
#include "OGLStringElement.h"

std::string selectedModelName = "";
std::string selectedBufferName = "";
int         selectedForPackOrChange = 0;
bool        viewPackingWindowBool = false;

std::vector<char> listOfVBChecks;

void RunViewPackingWindow(MaterialLayout* layout, bool& open)
{
  const int imguiWinFlag =
    ImGuiWindowFlags_::ImGuiWindowFlags_NoResize |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoSavedSettings |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
  ImGui::SetNextWindowPos(ImVec2(80, 50));
  ImGui::SetNextWindowSize(ImVec2(250, 500));

  if (listOfVBChecks.size() < 1)
    listOfVBChecks.resize(oglStrElem::bufferVertexInputItems.size(), 0);

  if (ImGui::Begin("             Packing", &open, imguiWinFlag))
  {
    // State all checkboxes
    for (size_t i = 0; i < listOfVBChecks.size(); ++i)
    {
      bool* data = reinterpret_cast<bool*>(&listOfVBChecks[i]);
      std::string checkBoxName{ oglStrElem::bufferVertexInputItems[i].name };
      ImGui::Checkbox(("##vbInputChoose" + checkBoxName).c_str(), data); ImGui::SameLine();
      ImGui::Text(checkBoxName.c_str());
    }

    ImGui::Spacing();
    ImGui::Separator();
    ImGui::Spacing();
    ImGui::Spacing();

    // Import from current buffer layout
    if (ImGui::Button("Import From layout##importFromLayout"))
    {
      memset(listOfVBChecks.data(), 0, listOfVBChecks.size());
      for (auto& layoutType : layout->m_vBufferLayout.layout)
        listOfVBChecks[layoutType] = true;
    }

    ImGui::Spacing();
    ImGui::Spacing();

    if (ImGui::Button("Pack##startPackButton"))
    {
      // Invoke windows save dialog
      std::string fileLoc = WinSystem::SaveFileAs("Vertex Buffer Binary (.vbin)\0*.vbin\0All Files\0*.*\0", ".vbin");
      if (fileLoc.size())
      {
        // check if selected layout is the same as current layout
        bool isEqualCurrentLayout = true;
        std::vector<char> tmpListVBCheck(listOfVBChecks.size(), 0);
        for (auto& layoutType : layout->m_vBufferLayout.layout)
          tmpListVBCheck[layoutType] = true;

        // final check
        vBufferLayout tmpLayout;
        for (size_t i = 0; i < tmpListVBCheck.size(); ++i)
        {
          if (tmpListVBCheck[i] != listOfVBChecks[i])
            isEqualCurrentLayout = false;
          else
          {
            if (listOfVBChecks[i] == 1)
              tmpLayout.layout.push_back(oglStrElem::bufferVertexInputItems[i].item);
          }
        }

        std::vector<vPack> bufData;
        if (isEqualCurrentLayout)
        {
          auto tmpPtr = ResourceManager::GetInstance()->GetVertexBufferData(selectedBufferName);
          assert(tmpPtr != nullptr);
          bufData = tmpPtr ? *tmpPtr : bufData;
        }
        else
        {
          // create a new tmp vertex buffer for exporting
          const Model* model = ResourceManager::GetInstance()->GetModel(selectedModelName);
          if (model)
            bufData = ModelUtils::PackData(*model, tmpLayout);
        }

        MemoryStream outFile;
        outFile.BeginWrite();

        // write number of mesh
        outFile.Write(bufData.size());
        for (size_t i = 0; i < bufData.size(); ++i)
        {
          // vertex size
          outFile.Write(bufData[i]._VertexSize);

          // vertex and indices info
          outFile.Write(bufData[i]._Data.size());
          outFile.Write(&bufData[i]._Data[0], bufData[i]._Data.size());
          outFile.Write(bufData[i]._Indices.size());
          outFile.Write(&bufData[i]._Indices[0], bufData[i]._Indices.size());
        }

        outFile.Flush(fileLoc);
      }

    }

    ImGui::SameLine();
    if (ImGui::Button("Cancel##cancelPack"))
    {
      memset(listOfVBChecks.data(), 0, listOfVBChecks.size());
      selectedModelName = selectedBufferName = "";
      selectedForPackOrChange = 0;
      open = false;
    }

    ImGui::End();
  }
}


void ImguiWin32::RunModelViewer(bool& open, MaterialLayout* layout)
{
  const int imguiWinFlag =
    ImGuiWindowFlags_::ImGuiWindowFlags_NoResize |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoSavedSettings |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
  ImGui::SetNextWindowPos(ImVec2(330, 50));
  ImGui::SetNextWindowSize(ImVec2(250, 200));

  if (ImGui::Begin("           Model Library", &open, imguiWinFlag))
  {
    ImGui::Text("Models");
    ImGui::SameLine();
    if (ImGui::Button("+ Add##ImportModelToLibrary"))
      AppManager::GetInstance()->ImportModel();
    ImGui::Separator();
    ImGui::Spacing();
    ImGui::Spacing();

    ResourceManager* rm = ResourceManager::GetInstance();
    std::map<std::string, Model>& modelMap = rm->GetAllModel();
    for (auto& model : modelMap)
    {
      if (ImGui::Button(model.first.c_str()))
      {
        ImGui::OpenPopup("PackOrChange");
        selectedModelName = model.first;
        selectedBufferName = selectedModelName + "#" + layout->m_layoutName;
      }
      ImGui::Spacing();
    }

    // Add texture to stage
    if (ImGui::BeginPopup("PackOrChange"))
    {
      if (ImGui::Selectable("Pack##packingModel"))
      {
        selectedForPackOrChange = 1;
        viewPackingWindowBool = true;
      }

      if (ImGui::Selectable("Change##changingModel"))
      {
        selectedForPackOrChange = 2;
        viewPackingWindowBool = false;
      }

      ImGui::EndPopup();
    }

    // Pack model
    if (selectedForPackOrChange == 1)
    {
      RunViewPackingWindow(layout, viewPackingWindowBool);
    }


    // Change model
    if (selectedForPackOrChange == 2)
    {
      if (selectedModelName.size() && layout->m_modelName != selectedModelName)
      {
        // attach new model
        layout->AttachModel(selectedModelName);

        // recreate vertex buffer
        std::vector<unsigned> indexList = rm->ComputeVertexBufferIndexList(selectedModelName, selectedBufferName);
        layout->AttachIndices(indexList);
        layout->AttachVertexBuffer(selectedBufferName);
      }
      selectedModelName = selectedBufferName = "";
      selectedForPackOrChange = 0;
    }

    ImGui::End();
  }
}

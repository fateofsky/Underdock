#pragma once
#include "imgui.h"
#include "imgui_impl_opengl3.h"
#include "imgui_impl_win32.h"

namespace ImguiWin32
{
  extern void InitIMGUI(HWND hwnd);
  extern void NewFrame();
  extern void Render();
  extern void CleanupIMGUI();
}
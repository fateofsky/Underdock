#include "pch.h"
#include "ImGuiPipelineViewer.h"
#include "ResourceManager.h"

const ImVec4                    inputColor               = { 0, 1, 0, 1 };
const ImVec4                    outputColor              = { 1, 0, 0, 1 };
const ImVec4                    uniformColor             = { 1, 1, 0, 1 };
const ImVec4                    uniformGroupColor        = { 1, .5f, .7f, 1 };
const ImVec4                    uniformGroupItemColor    = { 0, 1, .3f, 1 };
const ImVec4                    textureColor             = { 0, 1, 1, 1 };
const ImVec4                    pipelineHeaderColor      = { .7f, .25f, 1, 1 };

int                             currAddShiftValue        = -1;
__int64                         currStateOffset          = -1;
VkDynamicState                  selectedDynamicState     = VkDynamicState::VK_DYNAMIC_STATE_MAX_ENUM;
VkColorComponentFlagBits        ColorCompFlagBitsList[4]
{ VkColorComponentFlagBits::VK_COLOR_COMPONENT_R_BIT,
  VkColorComponentFlagBits::VK_COLOR_COMPONENT_G_BIT,
  VkColorComponentFlagBits::VK_COLOR_COMPONENT_B_BIT,
  VkColorComponentFlagBits::VK_COLOR_COMPONENT_A_BIT
};
Shader::ShaderType              selectedStage            = Shader::ShaderType::VERTEXSDR;
int                             selectedOutputIndex      = -1;
int                             addOrRemove              = -1;
OGL_BASETYPE                    selectedBaseType         = OGL_BASETYPE::INT_TYPE;
bool                            beginChange              = false;

bool                            viewAddInputFromVB       = false;
bool                            viewAddInputFromVBFirst  = false;

bool                            viewModifyStages         = false;
bool                            viewModifyStagesFirst    = false;

bool                            viewShaderEditor         = false;
bool                            viewShaderEditorFirst    = false;

bool                            viewGrouping             = false;
bool                            viewGroupingFirst        = false;

bool                            viewPipelineInfo     = false;
bool                            viewRenderPassInfo     = false;

bool                            HasSelectedDynamicState  = false;
bool                            viewAddCodeShiftPopup    = false;

bool                            HasSelectedOutput        = false;
bool                            shaderBuildError         = false;
std::vector<char>               listOfVBDataChecks;
std::vector<char>               listOfStagesChecks;
std::vector<char>               listOfUniformChecks;
std::string                     groupNameInput;
std::string                     singleSelectedGroupName;
std::string                     singleSelectedUniformName;
std::string                     selectedAddCodeShiftPopupIden = "";
std::string                     renameShaderStringBuffer = "";
std::string                     stageIn = "_in";
std::string                     stageOut = "_out";
std::string                     stageVarName[5] = { "v", "tc", "te", "g", "f" };


void RunDataTypePopupSelectables()
{
  if (ImGui::BeginPopup("SelectOutputPopup"))
  {
    for (size_t n = 0; n < oglStrElem::dataTypeItems.size(); ++n)
    {
      if (ImGui::Selectable(oglStrElem::dataTypeItems[n].name))
      {
        selectedBaseType = oglStrElem::dataTypeItems[n].item;
        addOrRemove = 0;
        beginChange = true;
      }
    }
    ImGui::EndPopup();
  }
}


template <typename T>
void RunUniformGroupingsCombo(T& uniform)
{
  ImGui::PushItemWidth(40);
  unsigned currentGroup = uniform.second->GetGroup();
  const char* setSelection = oglStrElem::integerItems[currentGroup].name;
  if (ImGui::BeginCombo(("##Groupings" + uniform.first).c_str(), setSelection))
  {
    for (size_t n = 0; n < oglStrElem::integerItems.size(); ++n)
    {
      if (ImGui::Selectable(oglStrElem::integerItems[n].name))
        uniform.second->ChangeGroup(unsigned(n));
    }
    ImGui::EndCombo();
  }
  ImGui::PopItemWidth();
  //ImGui::SameLine();
}


template <typename T>
void RunUniformGroupingsComboNonPtr(T& uniform, MaterialLayout* layout, int maxSet = -1)
{
  ImGui::PushItemWidth(40);
  size_t maximumSetNum = maxSet < 0 ? oglStrElem::integerItems.size() : size_t(maxSet);
  unsigned currentGroup = uniform.second.GetGroup();
  const char* setSelection = oglStrElem::integerItems[currentGroup].name;
  if (ImGui::BeginCombo(("##GroupingsNonPtr" + uniform.first).c_str(), setSelection))
  {
    for (size_t n = 0; n < maximumSetNum; ++n)
    {
      if (ImGui::Selectable(oglStrElem::integerItems[n].name))
      {
        unsigned newGroup = unsigned(n);
        uniform.second.ChangeGroup(newGroup);
        auto& groupList = uniform.second.GetGroupList();
        for (auto& gName : groupList)
        {
          UniformResourceBase* rb = layout->GetUniform(gName);
          if (rb) rb->ChangeGroup(newGroup);
        }
      }
    }
    ImGui::EndCombo();
  }
  ImGui::PopItemWidth();
  ImGui::SameLine();
}


template <typename T>
void RunUniformMemoryLayoutCombo(T& group)
{
  ImGui::PushItemWidth(70);
  UniformGroup::_MemoryLayout mLayout = group.second.GetMemoryLayout();
  const char* setSelection = oglStrElem::uniMemLayoutItems[mLayout].name;
  if (ImGui::BeginCombo(("##GroupingMemoryLayout" + group.first).c_str(), setSelection))
  {
    for (size_t n = 0; n < oglStrElem::uniMemLayoutItems.size(); ++n)
    {
      if (ImGui::Selectable(oglStrElem::uniMemLayoutItems[n].name))
        group.second.ChangeMemoryLayout(UniformGroup::_MemoryLayout(n));
    }
    ImGui::EndCombo();
  }
  ImGui::PopItemWidth();
}


template <typename StrElem, typename oglStrType>
void RunGenericCombo(const StrElem& oglStrE, oglStrType& strType, const std::string& identifier,
                     const std::string& labels, const float& widthSize = 70.f)
{
  ImGui::Text(labels.c_str());
  ImGui::SameLine();

  ImGui::PushItemWidth(widthSize);
  const char* setSelection = oglStrE[strType].name;
  if (ImGui::BeginCombo(("##" + identifier).c_str(), setSelection))
  {
    for (size_t n = 0; n < oglStrE.size(); ++n)
    {
      if (ImGui::Selectable(oglStrE[n].name))
        strType = oglStrType(n);
    }
    ImGui::EndCombo();
  }
  ImGui::PopItemWidth();
}

template <typename StrElem, typename oglStrType>
void RunGenericMapCombo(const StrElem& oglStrE, oglStrType& strType, const std::string& identifier,
                        const std::string& labels, const float& widthSize = 70.f)
{
  ImGui::Text(labels.c_str());
  ImGui::SameLine();

  ImGui::PushItemWidth(widthSize);
  const char* setSelection = oglStrE.at(strType);
  if (ImGui::BeginCombo(("##" + identifier).c_str(), setSelection))
  {
    for (const auto& item : oglStrE)
    {
      if (ImGui::Selectable(item.second))
        strType = item.first;
    }
    ImGui::EndCombo();
  }
  ImGui::PopItemWidth();
}

template <typename StrElem, typename oglStrType>
void RunGenericPopup(const std::string& popUpLabel, const StrElem& oglStrE, oglStrType& strType)
{
  if (ImGui::BeginPopup(popUpLabel.c_str()))
  {
    for (size_t n = 0; n < oglStrE.size(); ++n)
    {
      if (ImGui::Selectable(oglStrE[n].name))
        strType = oglStrE[n].item;
    }
    ImGui::EndPopup();
  }
}

template <typename StrElem, typename oglStrType>
void RunGenericPopup2(const std::string& popUpLabel, const StrElem& oglStrE, oglStrType& strType, bool& selectedBool)
{
  if (ImGui::BeginPopup(popUpLabel.c_str()))
  {
    for (size_t n = 0; n < oglStrE.size(); ++n)
    {
      if (ImGui::Selectable(oglStrE[n].name))
      {
        strType = oglStrE[n].item;
        selectedBool = true;
      }
    }
    ImGui::EndPopup();
  }
}


template <typename StrElem, typename oglStrType>
void RunGenericPopupMap(const std::string& popUpLabel, const StrElem& oglStrE, oglStrType& strType)
{
  if (ImGui::BeginPopup(popUpLabel.c_str()))
  {
    for (size_t n = 0; n < oglStrE.size(); ++n)
    {
      if (ImGui::Selectable(oglStrE.at(n)))
        strType = oglStrE.at(n);
    }
    ImGui::EndPopup();
  }
}



void RunVKBoolCheckbox(const std::string& label, const std::string& identifier, VkBool32& boolVal)
{
  bool tmpBool = boolVal;
  ImGui::Text(label.c_str()); ImGui::SameLine();
  if (ImGui::Checkbox(identifier.c_str(), &tmpBool))
    boolVal = tmpBool;
}

void RunDecode32bitMask(uint32_t& code, const std::string& identifier)
{
  std::string codeString = "00000000000000000000000000000000";
  for (uint32_t i = 0; i < 32; ++i)
  {
    uint32_t mask = 1 << i;
    codeString[31 - i] = (mask & code) ? '1' : '0';
  }

  ImGui::Text(codeString.c_str()); ImGui::SameLine();
  if (ImGui::Button(("+" + identifier).c_str()))
  {
    ImGui::OpenPopup("AddNewCodeMaskValToShift");
    viewAddCodeShiftPopup = true;
    currAddShiftValue = -1;
    selectedAddCodeShiftPopupIden = identifier;
  }

  if (viewAddCodeShiftPopup && identifier == selectedAddCodeShiftPopupIden)
  {
    RunGenericPopup("AddNewCodeMaskValToShift", oglStrElem::integerItems, currAddShiftValue);
    if (currAddShiftValue > -1)
    {
      code ^= 1 << currAddShiftValue;
      viewAddCodeShiftPopup = false;
    }
  }

}


void RunUniformGroupings(MaterialLayout* layout, Shader::ShaderType stage, bool& open)
{
  const int imguiWinFlag =
    ImGuiWindowFlags_::ImGuiWindowFlags_NoResize |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoSavedSettings |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
  ImGui::SetNextWindowPos(ImVec2(125, 50));
  ImGui::SetNextWindowSize(ImVec2(320, 600));
  if (ImGui::Begin("             Set Interface Blocks", &open, imguiWinFlag))
  {
    PipelineLayout::pipelineStageMap& stageMap = layout->m_resourceLayout.GetPipelineStageMap();
    const std::string trailer(":: ");
    auto& pStage = stageMap[stage];
    ImGui::Text("             Uniforms Selection");
    ImGui::Separator();
    ImGui::Spacing();
    ImGui::Spacing();

    // Populate uniform selection checkboxes
    if (viewGroupingFirst || listOfUniformChecks.size() != pStage.inputUniforms.size())
    {
      if (pStage.inputUniforms.size())
      {
        listOfUniformChecks.resize(pStage.inputUniforms.size());
        memset(listOfUniformChecks.data(), 0, pStage.inputUniforms.size());
      }
      groupNameInput = "";
      viewGroupingFirst = false;
    }

    int uniformCheckCount = 0;
    for (auto& uniform : pStage.inputUniforms)
    {
      bool* data = reinterpret_cast<bool*>(&listOfUniformChecks[uniformCheckCount++]);
      ImGui::Checkbox(("##uniformSelection" + uniform.first).c_str(), data); ImGui::SameLine();
      ImGui::TextColored(uniformColor, (trailer + uniform.first).c_str());
    }

    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Separator();
    ImGui::Spacing();
    ImGui::Spacing();

    // View uniform group
    ImGui::Text("              Interface Blocks");

    size_t gCount = pStage.uniformGroups.size();
    if (gCount > 0)
    {
      ImGui::Spacing();
      ImGui::Spacing();

      for (auto& group : pStage.uniformGroups)
      {
        ImGui::TextColored(uniformGroupColor, group.first.c_str());
        ImGui::SameLine();

        // Remove group entirely
        if (ImGui::Button(("x##RemoveGroup" + group.first).c_str(), ImVec2(15, 20)))
          singleSelectedGroupName = group.first;

        // Change group set
        ImGui::Text("Set:"); ImGui::SameLine();
        RunUniformGroupingsComboNonPtr(group, layout, 4);
        ImGui::SameLine();

        // Memory layout
        ImGui::Text("Memory Layout:"); ImGui::SameLine();
        RunUniformMemoryLayoutCombo(group);

        // DONT NEED RENAME (JUST RECREATE)

        auto& grpList = group.second.GetGroupList();
        for (auto& uniformName : grpList)
        {
          // Remove one from the group
          if (ImGui::Button(("x##RemoveFromBlock" + uniformName).c_str(), ImVec2(15, 20)))
          {
            singleSelectedGroupName = group.first;
            singleSelectedUniformName = uniformName;
          }
          ImGui::SameLine();
          ImGui::TextColored(uniformGroupItemColor, (" -> " + uniformName).c_str() );
        }

        ImGui::Spacing();
        ImGui::Spacing();
        ImGui::Spacing();
        ImGui::Spacing();
        ImGui::Spacing();
        ImGui::Spacing();
      }
    }
    else ImGui::Text(" - No Block - ");

    ImGui::Spacing();
    ImGui::Spacing();

    ImGui::InputText("##newBlock", &groupNameInput); ImGui::SameLine();
    if (ImGui::Button("New Block##addblk", ImVec2(80, 20)))
    {
      uniformCheckCount = 0;
      if (groupNameInput.size() > 0)
      {
        for (auto& uniform : pStage.inputUniforms)
        {
          bool* data = reinterpret_cast<bool*>(&listOfUniformChecks[uniformCheckCount]);
          (*data) ?
            layout->m_resourceLayout.AddToGroup(stage, groupNameInput, uniform.first, 0, UniformGroup::_MemoryLayout::NONE) :
            layout->m_resourceLayout.RemoveFromGroup(stage, groupNameInput, uniform.first);
          ++uniformCheckCount;
        }
      }
    }

    // delete one from the group
    if (singleSelectedGroupName.size() && singleSelectedUniformName.size())
    {
      layout->m_resourceLayout.RemoveFromGroup(stage, singleSelectedGroupName, singleSelectedUniformName);
      singleSelectedGroupName = singleSelectedUniformName = "";
    }
    // delete whole group
    else if (singleSelectedGroupName.size())
    {
      layout->m_resourceLayout.RemoveGroup(stage, singleSelectedGroupName);
      singleSelectedGroupName = singleSelectedUniformName = "";
    }

    if (ImGui::Button("Close##addgrp", ImVec2(80, 20)))
      open = false;

    ImGui::End();
  }
}


void RunAddInputFromVertexBuffer(MaterialLayout* layout, bool& open)
{
  const int imguiWinFlag =
    ImGuiWindowFlags_::ImGuiWindowFlags_NoResize |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoSavedSettings |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
  ImGui::SetNextWindowPos(ImVec2(145, 50));
  ImGui::SetNextWindowSize(ImVec2(300, 320));
  if (ImGui::Begin("    Add Attributes From Vertex Buffer", &open, imguiWinFlag))
  {
    // initialize checkbox bool list when first launch
    if (listOfVBDataChecks.size() == 0)
      listOfVBDataChecks.resize(oglStrElem::bufferVertexInputItems.size(), 0);
    
    // list the check box based on the buffer layout from the layout the first time
    if (viewAddInputFromVBFirst)
    {
      for (auto& layoutType : layout->m_vBufferLayout.layout)
        listOfVBDataChecks[layoutType] = 1;
      viewAddInputFromVBFirst = false;
    }

    // Checkboxes
    for (size_t i = 0; i < listOfVBDataChecks.size(); ++i)
    {
      bool* data = reinterpret_cast<bool*>(&listOfVBDataChecks[i]);
      std::string checkBoxName{ oglStrElem::bufferVertexInputItems[i].name };
      ImGui::Checkbox(("##vbInput" + checkBoxName).c_str(), data); ImGui::SameLine();
      ImGui::Text(checkBoxName.c_str());
    }

    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();

    // Button actions
    if (ImGui::Button("Ok##InputFromVB", ImVec2(70, 30)))
    {
      layout->m_vBufferLayout.layout.clear();
      for (size_t i = 0; i < listOfVBDataChecks.size(); ++i)
      {
        if (listOfVBDataChecks[i] == 1)
          layout->InsertVBufferLayout(oglStrElem::bufferVertexInputItems[i].item);
      }
      layout->m_resourceLayout.AddVertexInputToVertexStage(layout->m_vBufferLayout);
      open = false;
    }
    ImGui::SameLine();
    if (ImGui::Button("Cancel##InputFromVB", ImVec2(70, 30)))
      open = false;

    ImGui::End();
  }
}


void RunViewModifyStages(MaterialLayout* layout, bool& open)
{
  const int imguiWinFlag =
    ImGuiWindowFlags_::ImGuiWindowFlags_NoResize |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoSavedSettings |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
  ImGui::SetNextWindowPos(ImVec2(145, 50));
  ImGui::SetNextWindowSize(ImVec2(300, 210));

  if (ImGui::Begin("              Modify Stages", &open, imguiWinFlag))
  {
    // initialize checkbox bool list when first launch
    if (listOfStagesChecks.size() == 0)
      listOfStagesChecks.resize(oglStrElem::uniformStageItems.size(), 0);

    // list the check box based on the buffer layout from the layout the first time
    PipelineLayout::pipelineStageMap& stageMap = layout->m_resourceLayout.GetPipelineStageMap();
    if (viewModifyStagesFirst)
    {
      for (auto& layoutType : stageMap)
        listOfStagesChecks[layoutType.first] = 1;
      viewModifyStagesFirst = false;
    }

    // Checkboxes
    for (size_t i = 0; i < listOfStagesChecks.size(); ++i)
    {
      bool* data = reinterpret_cast<bool*>(&listOfStagesChecks[i]);
      std::string checkBoxName{ oglStrElem::uniformStageItems[i].name };
      ImGui::Checkbox(("##StagesInput" + checkBoxName).c_str(), data); ImGui::SameLine();
      ImGui::Text(checkBoxName.c_str());
    }

    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();

    // Button actions
    if (ImGui::Button("Ok##StagesInputOk", ImVec2(70, 30)))
    {
      int count = 0;
      for (auto& stagesChecked : listOfStagesChecks)
      {
        if (stagesChecked)
        {
          Shader::ShaderType addedStage = oglStrElem::uniformStageItems[count].item;
          if (layout->m_resourceLayout.AddStages(addedStage) == 0)
          {
            // Realign input/output
            // output of previous stage is input of the added stage (if any)
            Shader::ShaderType previousStage, nextStage;
            if (layout->m_resourceLayout.FindPreviousStage(addedStage, previousStage))
              stageMap[addedStage].baseInput = stageMap[previousStage].baseOutput;
            
            // clear input of the next stage first (if any)
            if (layout->m_resourceLayout.FindNextStage(addedStage, nextStage))
              stageMap[nextStage].baseInput.clear();
          }
        }
        else
        {
          // quick skip for only remove
          if (stageMap.find(oglStrElem::uniformStageItems[count].item) == stageMap.end())
          {
            ++count;
            continue;
          }

          Shader::ShaderType removeStage = oglStrElem::uniformStageItems[count].item;

          // Realign input/output
          // clear input of the next stage first (if any)
          std::vector<OGL_BASETYPE> oldNextBaseInput;           // Exception safety
          Shader::ShaderType previousStage, nextStage;
          bool hasNextStage = false;
          if (layout->m_resourceLayout.FindNextStage(removeStage, nextStage))
          {
            oldNextBaseInput = stageMap[nextStage].baseInput;
            stageMap[nextStage].baseInput.clear();

            // output of the previous stage is the input of the next stage
            if (layout->m_resourceLayout.FindPreviousStage(removeStage, previousStage))
              stageMap[nextStage].baseInput = stageMap[previousStage].baseOutput;
            
            hasNextStage = true;
          }

          // reinstate back the old base input if remove failed
          if (layout->m_resourceLayout.RemoveStages(oglStrElem::uniformStageItems[count].item) && hasNextStage)
            stageMap[nextStage].baseInput = oldNextBaseInput;
        }
        ++count;
      }
      open = false;
    }
    ImGui::SameLine();
    if (ImGui::Button("Cancel##StagesInputCancel", ImVec2(70, 30)))
      open = false;

    ImGui::End();
  }
}


template <typename StrElem, typename oglStrType>
void RunFlagBitDecoder(const StrElem& oglStrE, oglStrType& strType, const std::string& identifier,
                       const std::string& labels, const float& widthSize = 70.f)
{
  ImGui::Text(labels.c_str());
  ImGui::SameLine();

  std::string popupIdentifier = "FlagBitPopup_" + identifier;
  if (ImGui::Button(("+##FlagBit" + identifier).c_str()))
    ImGui::OpenPopup(popupIdentifier.c_str());

  bool isSelected = false;
  oglStrType newStrType;
  RunGenericPopup2(popupIdentifier.c_str(), oglStrE, newStrType, isSelected);

  // toggle it
  strType = isSelected ? strType ^ newStrType : strType;

  std::string tmpStrIdent = " > ";
  std::string tmpStrIdent2 = " <";
  for (auto& bitFlag : oglStrE)
  {
    if (strType & bitFlag.item)
      ImGui::Text((tmpStrIdent + bitFlag.name + tmpStrIdent2).c_str());
  }

  ImGui::Spacing();
}

void RunViewPipelineInfoViewer(MaterialLayout* layout, bool& open)
{
  const int imguiWinFlag =
    ImGuiWindowFlags_::ImGuiWindowFlags_NoResize |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoSavedSettings |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
  ImGui::SetNextWindowPos(ImVec2(45, 50));
  ImGui::SetNextWindowSize(ImVec2(400, 600));

  if (ImGui::Begin("                   Vulkan Pipeline Info", &open, imguiWinFlag))
  {
    /*
      -- INPUT ASSEMBLY STATE
    */
    ImGui::TextColored(pipelineHeaderColor, "Input Assembly State");
    ImGui::Separator();
    VkPipelineInputAssemblyStateCreateInfo& inputAssInfo =  layout->m_resourceLayout.GetInputAssemblyInfo();
    RunGenericCombo(oglStrElem::primitiveTopItems, inputAssInfo.topology, "##topology", "Topology      ", 230);

    /*
      -- DYNAMIC STATES
    */
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::TextColored(pipelineHeaderColor, "Dynamic State");
    ImGui::Separator();
    std::vector<VkDynamicState>& dynamicStateList = layout->m_resourceLayout.GetDynamicStateList();
    ImGui::Text("Dynamic States"); ImGui::SameLine();
    if (ImGui::Button("+ New State"))
    {
      ImGui::OpenPopup("AddNewDynamicState");
      HasSelectedDynamicState = true;
      selectedDynamicState = VkDynamicState::VK_DYNAMIC_STATE_MAX_ENUM;
    }
    for (auto& state : dynamicStateList)
    {
      if (ImGui::Button((std::string("x##RemoveState") + oglStrElem::dynamicStateItems[state].name).c_str(), ImVec2(15, 20)))
        currStateOffset = (&state) - (&dynamicStateList.front());
      
      ImGui::SameLine();
      const std::string arrowStr("-> ");
      ImGui::Text((arrowStr + oglStrElem::dynamicStateItems[state].name).c_str());
    }
    if (dynamicStateList.size() == 0)
      ImGui::Text("  No Dynamic State");
    if (HasSelectedDynamicState) 
    {
      RunGenericPopup("AddNewDynamicState", oglStrElem::dynamicStateItems, selectedDynamicState);
      if (selectedDynamicState != VkDynamicState::VK_DYNAMIC_STATE_MAX_ENUM)
      {
        dynamicStateList.push_back(selectedDynamicState);
        HasSelectedDynamicState = false;
      }
    }
    if (currStateOffset > -1)
    {
      // Remove Dynamic state
      dynamicStateList.erase(dynamicStateList.begin() + currStateOffset);
      currStateOffset = -1;
    }


    /*
      -- RASTERIZATION STATE
    */
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::TextColored(pipelineHeaderColor, "Rasterization State");
    ImGui::Separator();
    VkPipelineRasterizationStateCreateInfo& rasterInfo = layout->m_resourceLayout.GetRasterizationStateInfo();
    RunGenericCombo(oglStrElem::polyModeItems,     rasterInfo.polygonMode, "##polyMode",  "Polygon       ", 230);
    RunGenericCombo(oglStrElem::cullModeFlagItems, rasterInfo.cullMode,    "##cullMode",  "Culling       ", 230);
    RunGenericCombo(oglStrElem::frontFaceItems,    rasterInfo.frontFace,   "##frontFace", "Faces         ", 230);
    RunVKBoolCheckbox("Depth Clamp   ", "##depthClampEnable", rasterInfo.depthClampEnable);
    RunVKBoolCheckbox("Raster Discard", "##rasterizerDiscardEnable", rasterInfo.rasterizerDiscardEnable);
    RunVKBoolCheckbox("Depth Bias    ", "##depthBiasEnable", rasterInfo.depthBiasEnable);
    
    ImGui::PushItemWidth(100.0f);
    ImGui::Text("Const Factor  "); ImGui::SameLine();
    ImGui::SliderFloat("##ConstFactor", &rasterInfo.depthBiasConstantFactor, -1.f, 1.f, "%.4f");

    ImGui::Text("Clamp         "); ImGui::SameLine();
    ImGui::SliderFloat("##Clamp", &rasterInfo.depthBiasClamp, -1.f, 1.f, "%.4f");
    
    ImGui::Text("Slope Factor  "); ImGui::SameLine();
    ImGui::SliderFloat("##SlopeFactor", &rasterInfo.depthBiasSlopeFactor, -1.f, 1.f, "%.4f");
    
    ImGui::Text("Line Width    "); ImGui::SameLine();
    ImGui::SliderFloat("##LineWidth", &rasterInfo.lineWidth, -1.f, 1.f, "%.4f");
    ImGui::PopItemWidth();


    // TODO: color blend
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::TextColored(pipelineHeaderColor, "Color Blend State");
    ImGui::Separator();

    VkPipelineColorBlendStateCreateInfo& colorBlendStateInfo = layout->m_resourceLayout.GetColorBlendAttStateInfo();
    std::vector<VkPipelineColorBlendAttachmentState>& colorBlendAttStateList = layout->m_resourceLayout.GetColorBlendAttStateList();
    RunVKBoolCheckbox("Logic Op Set  ", "##cbsLogicOpEnable", colorBlendStateInfo.logicOpEnable);
    RunGenericCombo(oglStrElem::logicOpsItems, colorBlendStateInfo.logicOp, "##cbsLogicOp", "Logic Op      ", 230);

    ImGui::PushItemWidth(100.0f);
    ImGui::Text("Blend Const R "); ImGui::SameLine();
    ImGui::SliderFloat("##blendConstR", &colorBlendStateInfo.blendConstants[0], 0, 1.f, "%.4f");

    ImGui::Text("Blend Const G "); ImGui::SameLine();
    ImGui::SliderFloat("##blendConstG", &colorBlendStateInfo.blendConstants[1], 0, 1.f, "%.4f");

    ImGui::Text("Blend Const B "); ImGui::SameLine();
    ImGui::SliderFloat("##blendConstB", &colorBlendStateInfo.blendConstants[2], 0, 1.f, "%.4f");

    ImGui::Text("Blend Const A "); ImGui::SameLine();
    ImGui::SliderFloat("##blendConstA", &colorBlendStateInfo.blendConstants[3], 0, 1.f, "%.4f");
    ImGui::PopItemWidth();

    // Attachment state
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Separator();
    ImGui::Text("Attachment(s) State: ");
    ImGui::Separator();

    auto& stageMap = layout->m_resourceLayout.GetPipelineStageMap();
    if (stageMap.find(Shader::ShaderType::FRAGSDR) != stageMap.end())
    {
      auto& pStage = stageMap[Shader::ShaderType::FRAGSDR];
      if (pStage.baseOutput.size() != colorBlendAttStateList.size())
      {
        std::vector<VkPipelineColorBlendAttachmentState> tmpCBAttStateList = colorBlendAttStateList;
        VkPipelineColorBlendAttachmentState tmpState = PipelineLayout::CreateDefaultColorBlendAttachmentState();
        colorBlendAttStateList.resize(pStage.baseOutput.size(), tmpState);

        for (size_t cbCount = 0; cbCount < tmpCBAttStateList.size() && cbCount < colorBlendAttStateList.size(); ++cbCount)
          colorBlendAttStateList[cbCount] = tmpCBAttStateList[cbCount];
      }

      for (size_t cbCount = 0; cbCount < colorBlendAttStateList.size(); ++cbCount)
      {
        VkPipelineColorBlendAttachmentState& attState = colorBlendAttStateList[cbCount];
        VkBool32 colorWriteState[4];

        // Read from
        for (int i = 0; i < 4; ++i)
          colorWriteState[i] = (attState.colorWriteMask & ColorCompFlagBitsList[i]) > 0 ? VK_TRUE : VK_FALSE;

        std::string cbCountStr = std::to_string(cbCount);
        ImGui::Spacing();
        ImGui::Text(("--> Write Attachment Mask " + cbCountStr).c_str());
        ImGui::Spacing();
        RunVKBoolCheckbox("Red Mask      ", "##attachmentR_" + cbCountStr, colorWriteState[0]);
        RunVKBoolCheckbox("Green Mask    ", "##attachmentG_" + cbCountStr, colorWriteState[1]);
        RunVKBoolCheckbox("Blue Mask     ", "##attachmentB_" + cbCountStr, colorWriteState[2]);
        RunVKBoolCheckbox("Alpha Mask    ", "##attachmentA_" + cbCountStr, colorWriteState[3]);

        RunVKBoolCheckbox("Blending      ", "##cbBlendEnabled_" + cbCountStr, attState.blendEnable);
        RunGenericCombo(oglStrElem::blendOpsItems, attState.alphaBlendOp, "##cbAlphaBlendOp_" + cbCountStr, "Alpha Blend Op", 230);
        RunGenericCombo(oglStrElem::blendOpsItems, attState.colorBlendOp, "##cbColorBlendOp_" + cbCountStr, "Color Blend Op", 230);
        RunGenericCombo(oglStrElem::blendFactorItems, attState.srcColorBlendFactor, "##cbSrcColorBlendFactor_" + cbCountStr, "Src Col Blend ", 230);
        RunGenericCombo(oglStrElem::blendFactorItems, attState.dstColorBlendFactor, "##cbDstColorBlendFactor_" + cbCountStr, "Dst Col Blend ", 230);
        RunGenericCombo(oglStrElem::blendFactorItems, attState.srcAlphaBlendFactor, "##cbSrcAlphaBlendFactor_" + cbCountStr, "Src Alp Blend ", 230);
        RunGenericCombo(oglStrElem::blendFactorItems, attState.dstAlphaBlendFactor, "##cbDstAlphaBlendFactor_" + cbCountStr, "Dst Alp Blend ", 230);

        ImGui::Spacing();
        ImGui::Spacing();
        ImGui::Spacing();

        // write back
        attState.colorWriteMask = 0;
        for (int i = 0; i < 4; ++i)
          attState.colorWriteMask |= colorWriteState[i] ? ColorCompFlagBitsList[i] : 0;
      }

      if (colorBlendAttStateList.size() == 0)
      {
        ImGui::Spacing();
        ImGui::Text(" -- No Output Attachment --");
        ImGui::Spacing();
      }
    }
    else
    {
      ImGui::Spacing();
      ImGui::Text(" -- No Fragment Shader Stage --");
      ImGui::Spacing();
    }

    /*
      -- DEPTH STENCIL STATE
    */
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::TextColored(pipelineHeaderColor, "Depth Stencil State");
    ImGui::Separator();
    VkPipelineDepthStencilStateCreateInfo& depStenInfo = layout->m_resourceLayout.GetDepthStencilStateInfo();
    RunVKBoolCheckbox("Depth Test    ", "##depthTestEnable", depStenInfo.depthTestEnable);
    RunVKBoolCheckbox("Depth Write   ", "##depthWriteEnable", depStenInfo.depthWriteEnable);
    RunVKBoolCheckbox("Depth Bound   ", "##depthBoundsTestEnable", depStenInfo.depthBoundsTestEnable);
    
    ImGui::PushItemWidth(100.0f);
    ImGui::Text("Min Bound     "); ImGui::SameLine();
    ImGui::SliderFloat("##depthMinBound", &depStenInfo.minDepthBounds, -1.f, 1.f, "%.4f");
    ImGui::Text("Max Bound     "); ImGui::SameLine();
    ImGui::SliderFloat("##depthMaxBound", &depStenInfo.maxDepthBounds, -1.f, 1.f, "%.4f");
    ImGui::PopItemWidth();

    RunGenericCombo(oglStrElem::compareOpsItems, depStenInfo.depthCompareOp, "##depthCompareOp", "Dp Compare Op ", 230);
    RunVKBoolCheckbox("Stencil Test  ", "##stencilTestEnable", depStenInfo.stencilTestEnable);

    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Separator();
    ImGui::Text("Front Stencil Op State: ");
    ImGui::Separator();
    RunGenericCombo(oglStrElem::stencilOpsItems, depStenInfo.front.passOp,      "##front.passOp",      "Pass Op       ", 230);
    RunGenericCombo(oglStrElem::stencilOpsItems, depStenInfo.front.failOp,      "##front.failOp",      "Fail Op       ", 230);
    RunGenericCombo(oglStrElem::stencilOpsItems, depStenInfo.front.depthFailOp, "##front.depthFailOp", "Depth Fail Op ", 230);
    RunGenericCombo(oglStrElem::compareOpsItems, depStenInfo.front.compareOp,   "##front.compareOp",   "St Compare Op ", 230);
    ImGui::Text("Compare Mask  "); ImGui::SameLine();
    RunDecode32bitMask(depStenInfo.front.compareMask, "##frontCompareMask");
    ImGui::Text("Write Mask    "); ImGui::SameLine();
    RunDecode32bitMask(depStenInfo.front.writeMask, "##frontWriteMask");
    ImGui::Text("Reference     "); ImGui::SameLine();
    RunDecode32bitMask(depStenInfo.front.reference, "##frontReference");


    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Separator();
    ImGui::Text("Back Stencil Op State: ");
    ImGui::Separator();
    RunGenericCombo(oglStrElem::stencilOpsItems, depStenInfo.back.passOp,      "##back.passOp",      "Pass Op       ", 230);
    RunGenericCombo(oglStrElem::stencilOpsItems, depStenInfo.back.failOp,      "##back.failOp",      "Fail Op       ", 230);
    RunGenericCombo(oglStrElem::stencilOpsItems, depStenInfo.back.depthFailOp, "##back.depthFailOp", "Depth Fail Op ", 230);
    RunGenericCombo(oglStrElem::compareOpsItems, depStenInfo.back.compareOp,   "##back.compareOp",   "St Compare Op ", 230);
    ImGui::Text("Compare Mask  "); ImGui::SameLine();
    RunDecode32bitMask(depStenInfo.back.compareMask, "##backCompareMask");
    ImGui::Text("Write Mask    "); ImGui::SameLine();
    RunDecode32bitMask(depStenInfo.back.writeMask, "##backWriteMask");
    ImGui::Text("Reference     "); ImGui::SameLine();
    RunDecode32bitMask(depStenInfo.back.reference, "##backReference");

    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::End();
  }
}


void RunViewRenderPassInfo(MaterialLayout* layout, bool& open)
{
  const int imguiWinFlag =
    ImGuiWindowFlags_::ImGuiWindowFlags_NoResize |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoSavedSettings |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
  ImGui::SetNextWindowPos(ImVec2(45, 50));
  ImGui::SetNextWindowSize(ImVec2(400, 600));

  if (ImGui::Begin("                Vulkan Render Pass Info", &open, imguiWinFlag))
  {
    ImGui::TextColored(pipelineHeaderColor, "Render Pass State");
    ImGui::Separator();
    ImGui::Text("Depth Attach  "); ImGui::SameLine();
    bool hasDepth = layout->m_resourceLayout.GetHasDepth();
    if (ImGui::Checkbox("##HasRenderpass", &hasDepth))
      layout->m_resourceLayout.ChangeHasDepth(hasDepth);

    auto& stageMap = layout->m_resourceLayout.GetPipelineStageMap();
    const VkPipelineDepthStencilStateCreateInfo& tmpDepStenInfo = layout->m_resourceLayout.GetDepthStencilStateInfo();
    bool hasDepthAttachment = (bool)(tmpDepStenInfo.depthTestEnable & tmpDepStenInfo.depthWriteEnable);
    if (stageMap.find(Shader::ShaderType::FRAGSDR) != stageMap.end())
    {
      ImGui::Text("Dimensions    "); ImGui::SameLine();
      ImGui::DragInt2("##RenderTargetSize", layout->m_resourceLayout.GetRenderTargetDimension(), 1, 0, 2048);

      // output format
      auto& mrtOutputList = layout->m_resourceLayout.GetMRTOutputList_Writable();
      ImGui::Text("Format(s):");
      ImGui::Separator();
      int formatButtonIndenCount = 0;
      for (auto& format : mrtOutputList)
      {
        std::string formatButtonIndexCountStr = std::to_string(formatButtonIndenCount++);
        std::string formatButtonIden = "f_out" + formatButtonIndexCountStr + ":       ";
        RunGenericMapCombo(oglStrElem::commonImgFormatItems, format, "##FormatChangeCombo" + formatButtonIndexCountStr, formatButtonIden, 230);
      }
      ImGui::Spacing();
      ImGui::Spacing();
      ImGui::Spacing();

      // output usage
      auto& mrtOutputUsageList = layout->m_resourceLayout.GetMRTOutputUsageList_Writable();
      ImGui::Text("Usage(s):");
      ImGui::Separator();
      int usageCounter = 0;
      for (auto& usage : mrtOutputUsageList)
      {
        std::string usageStrCounter = std::to_string(usageCounter++);
        std::string usageButtonIden = "f_out" + usageStrCounter + ":       ";
        RunFlagBitDecoder(oglStrElem::imageUsageFlagItems, usage, "##ImageUsageFlag" + usageStrCounter, usageButtonIden, 230);
      }
      ImGui::Spacing();
      ImGui::Spacing();
      ImGui::Spacing();

      // Depth attachment information
      ImGui::Text("Depth Texture "); ImGui::SameLine();
      bool enableRenderToTexture = layout->m_resourceLayout.GetEnableDepthToTexture();
      ImGui::Checkbox("##depthToTexture", &enableRenderToTexture);
      layout->m_resourceLayout.SetEnableDepthToTexture(enableRenderToTexture);

      // create dependancies
      if (ImGui::Button("+##AddDependencies"))
        layout->m_resourceLayout.AddDependencies();
      ImGui::SameLine();
      ImGui::Text("Dependencies  ");
      ImGui::Separator();
      ImGui::Spacing();

      int depenCounter = 0;
      int deleteDependency = -1;
      auto& depenList = layout->m_resourceLayout.GetDependenciesList_Writable();
      for (auto& dependency : depenList)
      {
        std::string depenStrCounter = std::to_string(depenCounter);
        if (ImGui::Button(("x##RemoveDependency" + depenStrCounter).c_str(), ImVec2(15, 20)))
          deleteDependency = depenCounter;
        ImGui::SameLine();
        ImGui::Text(("--> Dependency " + depenStrCounter).c_str());

        //RunGenericCombo(oglStrElem::blendOpsItems, attState.alphaBlendOp, "##cbAlphaBlendOp_" + cbCountStr, "Alpha Blend Op", 230);
        RunGenericMapCombo(oglStrElem::subpassIndexItems, dependency.srcSubpass, "##SourceSubpass" + depenStrCounter, "Src Subpass   ", 230);
        RunGenericMapCombo(oglStrElem::subpassIndexItems, dependency.dstSubpass, "##DestinationSubpass" + depenStrCounter, "Dst Subpass   ", 230);

        RunFlagBitDecoder(oglStrElem::pipelineStageFlagItems, dependency.srcStageMask, "##SourceStageMask" + depenStrCounter, "Src Stage Mask", 230);
        RunFlagBitDecoder(oglStrElem::pipelineStageFlagItems, dependency.dstStageMask, "##DestinationStageMask" + depenStrCounter, "Dst Stage Mask", 230);
        RunFlagBitDecoder(oglStrElem::accessFlagItems, dependency.srcAccessMask, "##SourcAccessMask" + depenStrCounter, "Src Access Mask", 230);
        RunFlagBitDecoder(oglStrElem::accessFlagItems, dependency.dstAccessMask, "##DestinationAccessMask" + depenStrCounter, "Dst Access Mask", 230);
        RunFlagBitDecoder(oglStrElem::dependencyFlagItems, dependency.dependencyFlags, "##DependencyFlag" + depenStrCounter, "Dependency Flag", 230);

        ++depenCounter;
        ImGui::Spacing();
      }

      // delete dependencies
      if (deleteDependency >= 0)
        layout->m_resourceLayout.RemoveDependencies(deleteDependency);
    }

    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::End();
  }
}


void ImguiWin32::RunPipelineViewer(bool& open, MaterialLayout* layout)
{
  const int imguiWinFlag =
    ImGuiWindowFlags_::ImGuiWindowFlags_NoResize |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoSavedSettings |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
  ImGui::SetNextWindowPos(ImVec2(450, 50));
  ImGui::SetNextWindowSize(ImVec2(300, 600));
  int removeLabelCounter = 0;

  if (ImGui::Begin("             Pipeline Viewer", &open, imguiWinFlag))
  {
    PipelineLayout::pipelineStageMap& stageMap = layout->m_resourceLayout.GetPipelineStageMap();
    for (auto& stage : stageMap)
    {
      // show stage
      ImGui::Separator();
      ImGui::Text(oglStrElem::uniformStageItems[stage.first].name);
      ImGui::SameLine();

      if (stage.first == Shader::ShaderType::VERTEXSDR)
      {
        ImGui::SameLine();
        if (ImGui::Button((std::string("+ vb##1") + oglStrElem::uniformStageItems[stage.first].name).c_str()))
        {
          viewAddInputFromVBFirst = viewAddInputFromVB = true;
          viewModifyStages = false;
          viewGroupingFirst = viewGrouping = false;
        }
      }

      ImGui::SameLine();
      if (ImGui::Button((std::string("+ Output##1") + oglStrElem::uniformStageItems[stage.first].name).c_str()))
      {
        HasSelectedOutput = true;
        selectedStage = stage.first;
        ImGui::OpenPopup("SelectOutputPopup");
        viewGroupingFirst = viewGrouping = false;
      }

      ImGui::SameLine();
      if (ImGui::Button((std::string("Edit##2") + oglStrElem::uniformStageItems[stage.first].name).c_str()))
      {
        // TODO: Open shader code editor
        viewShaderEditorFirst = viewShaderEditor = true;
        selectedStage = stage.first;
        viewGroupingFirst = viewGrouping = false;
      }
      ImGui::Separator();

      // show input
      int inputOutputCount = 0;
      for (auto& input : stage.second.baseInput)
      {
        if (stage.first == Shader::ShaderType::VERTEXSDR)
        {
          std::string constructInputVarName = " (" + stageVarName[stage.first] + stageIn + std::to_string(inputOutputCount) + ")";
          ImGui::TextColored(inputColor, (std::string(">> ") + oglStrElem::dataTypeItems[input].name + constructInputVarName).c_str());
        }
        else
        {
          Shader::ShaderType previousStage;
          layout->m_resourceLayout.FindPreviousStage(stage.first, previousStage);
          std::string constructInputVarName = " (" + stageVarName[previousStage] + stageOut + std::to_string(inputOutputCount) + ")";
          ImGui::TextColored(inputColor, (std::string(">> ") + oglStrElem::dataTypeItems[input].name + constructInputVarName).c_str());
        }
        ++inputOutputCount;
      }
      ImGui::Spacing();
      ImGui::Spacing();
      ImGui::Spacing();



      // show uniforms
      const std::string trailer(":: ");
      if (stage.second.inputTextures.size() || stage.second.inputUniforms.size())
      {
        if (ImGui::Button( ("* Interface Block##group" + std::string(oglStrElem::uniformStageItems[stage.first].name)).c_str() ))
        {
          viewGroupingFirst = viewGrouping = true;
          selectedStage = stage.first;
        }
      }
      for (auto& uniform : stage.second.inputUniforms)
        ImGui::TextColored(uniformColor, (trailer + uniform.first).c_str());

      ImGui::Spacing();
      ImGui::Spacing();
      ImGui::Spacing();

      for (auto& texture : stage.second.inputTextures)
      {
        ImGui::TextColored(textureColor, (trailer + texture.first).c_str());
        ImGui::SameLine();
        ImGui::Text("Set:");
        ImGui::SameLine();
        RunUniformGroupingsCombo(texture);
      }

      ImGui::Spacing();
      ImGui::Spacing();
      ImGui::Spacing();


      // show output
      inputOutputCount = 0;
      for (auto& output : stage.second.baseOutput)
      {
        std::string removeLabelStr = std::to_string(removeLabelCounter++);
        std::string constructInputVarName = " (" + stageVarName[stage.first] + stageOut + std::to_string(inputOutputCount) + ")";
        ImGui::TextColored(outputColor, (std::string("<< ") + oglStrElem::dataTypeItems[output].name + constructInputVarName).c_str()); ImGui::SameLine();
        if (ImGui::Button((std::string("Remove##") + removeLabelStr + oglStrElem::dataTypeItems[output].name).c_str()))
        {
          selectedStage = stage.first;
          selectedBaseType = output;
          addOrRemove = 1;
          beginChange = true;
          selectedOutputIndex = inputOutputCount;
          viewGroupingFirst = viewGrouping = false;
        }
        ++inputOutputCount;
      }

      ImGui::Spacing();
      ImGui::Spacing();
      ImGui::Spacing();
      ImGui::Spacing();
      ImGui::Spacing();
      ImGui::Spacing();
    }


    /*
      Pipeline information (Rasterization, color blending, depth info)
    */
    ImGui::Separator();
    if (ImGui::Button("Vulkan Pipeline Info##VulkPipeInfo", ImVec2(175, 20)))
    {
      viewGroupingFirst = viewGrouping = viewShaderEditorFirst = viewShaderEditor = viewAddInputFromVB = false;
      viewPipelineInfo = true;
    }

    if (ImGui::Button("Vulkan Render Pass Info##VulkRenderpassInfo", ImVec2(175, 20)))
    {
      viewGroupingFirst = viewGrouping = viewShaderEditorFirst = viewShaderEditor = viewPipelineInfo = viewAddInputFromVB = false;
      viewRenderPassInfo = true;
    }


    ImGui::Separator();
    ImGui::Spacing();
    ImGui::Spacing();

    // draw normal window with type selection
    if (HasSelectedOutput)
      RunDataTypePopupSelectables();

    // Confirmation, cancelation and additional buttons
    if (ImGui::Button("Compile##pipelineViewer", ImVec2(70, 30))) 
    {
      if (layout->m_resourceLayout.TestMessageFlag(LAYOUTMSG::msg::MODIFYVERTEXBUFFER))
      {
        // recreate vertex buffer
        std::vector<unsigned> indexList = ResourceManager::GetInstance()->CreateVertexBuffer(layout->m_modelName, 
                                                                                             layout->m_modelName + "#" + layout->m_layoutName, 
                                                                                             layout->m_vBufferLayout);
        layout->AttachIndices(indexList);
        layout->AttachVertexBuffer(layout->m_modelName + "#" + layout->m_layoutName);
      }

      if (layout->m_resourceLayout.TestMessageFlag(LAYOUTMSG::msg::MODIFYSHADER))
      {
        // shader compile link error
        if (!ShaderBuilder::GetInstance()->BuildShader_OGL(layout))
          shaderBuildError = true;
        else shaderBuildError = false;
        
      }
    }
    ImGui::SameLine();
    if (ImGui::Button("Close##pipelineViewer", ImVec2(70, 30)))
      viewGroupingFirst = viewGrouping = viewShaderEditorFirst = viewShaderEditor =
      viewPipelineInfo = shaderBuildError = viewAddInputFromVB = viewModifyStages = open = false;
    ImGui::SameLine();
    if (ImGui::Button("Modify Stages##pipelineViewer", ImVec2(110, 30)))
    {
      viewModifyStagesFirst = viewModifyStages = true;
      viewGroupingFirst = viewGrouping = viewShaderEditorFirst = viewShaderEditor = viewAddInputFromVB = false;
    }

    if (shaderBuildError)
    {
      ImGui::Text("Shader compile / build error");
    }

    ImGui::End();
  }


  if (beginChange)
  {
    // ADD
    if (addOrRemove == 0)
    {
      layout->m_resourceLayout.AddOutputToStage(selectedStage, selectedBaseType);
      
      // add input to the next stage
      Shader::ShaderType nextStage;
      if (layout->m_resourceLayout.FindNextStage(selectedStage, nextStage))
        layout->m_resourceLayout.AddInputToStage(nextStage, selectedBaseType);
    }

    // REMOVE
    else if (addOrRemove == 1)
    {
      layout->m_resourceLayout.RemoveOutputFromStage(selectedStage, selectedOutputIndex);

      // remove input from the next stage
      Shader::ShaderType nextStage;
      if (layout->m_resourceLayout.FindNextStage(selectedStage, nextStage))
        layout->m_resourceLayout.RemoveInputFromStage(nextStage, selectedOutputIndex);
    }

    
    HasSelectedOutput = beginChange = false;
    selectedOutputIndex = addOrRemove = -1;
  }

  if (viewAddInputFromVB)
  {
    RunAddInputFromVertexBuffer(layout, viewAddInputFromVB);
  }
  if (viewModifyStages)
  {
    RunViewModifyStages(layout, viewModifyStages);
  }
  if (viewShaderEditor)
  {
    ImguiWin32::RunShaderEditor(viewShaderEditor, layout, selectedStage, viewShaderEditorFirst);
  }
  if (viewGrouping)
  {
    RunUniformGroupings(layout, selectedStage, viewGrouping);
  }
  if (viewPipelineInfo)
  {
    RunViewPipelineInfoViewer(layout, viewPipelineInfo);
  }
  if (viewRenderPassInfo)
  {
    RunViewRenderPassInfo(layout, viewRenderPassInfo);
  }

  if (open == false) 
    renameShaderStringBuffer = "";
}

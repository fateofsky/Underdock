#pragma once

namespace LAYOUTMSG
{
  /*
    -- Message flags
  */
  namespace msg
  {
    extern const unsigned MODIFYSHADER;
    extern const unsigned SHADERBUILDFAILED;
    extern const unsigned MODIFYVERTEXBUFFER;
  }

  /*
    -- Utilities function
  */
  void SetMessageFlag(unsigned& input, const unsigned& flag);
  void ResetMessageFlag(unsigned& input, const unsigned& flag);
  bool TestMessageFlag(const unsigned& input, const unsigned flag);
  void ClearMessageFlag(unsigned& input);
}
#include "pch.h"
#include "ModelLoader.h"

/*
  -- GLOBAL VARIABLES
*/
std::map<std::string, int>      nodeMap; // name to index reference for node heir
unsigned                        nodeIndex = 0;

// Process Meshes
Mesh processMesh(aiMesh* mesh, const aiScene* scene)
{
  Mesh resMesh;
  resMesh.numberOfUvs = mesh->GetNumUVChannels();

  // process vertices
  for (unsigned i = 0; i < mesh->mNumVertices; ++i)
  {
    resMesh.vertices.push_back(glm::vec4(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z, 1.f));
    resMesh.vNormals.push_back((mesh->mNormals) ? glm::vec4(mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z, 0) : glm::vec4(.0f, 1.0f, .0f, 0.f));
    bool tanBiTan = mesh->HasTangentsAndBitangents();
    resMesh.vTangent.push_back(tanBiTan ? glm::vec4(mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z, 0) : glm::vec4(0));
    resMesh.vBiTangent.push_back(tanBiTan ? glm::vec4(mesh->mBitangents[i].x, mesh->mBitangents[i].y, mesh->mBitangents[i].z, 0) : glm::vec4(0));
    for (unsigned j = 0; j < resMesh.numberOfUvs; ++j)
      resMesh.uvCoordSets[j].push_back((mesh->mTextureCoords[0]) ? glm::vec2(mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y) : glm::vec2(0));
  }

  // process indices
  for (unsigned i = 0; i < mesh->mNumFaces; ++i)
  {
    if (mesh->mFaces[i].mNumIndices < 3) continue;

    // compute face index and face normal
    Faces faces;
    for (unsigned j = 0; j < mesh->mFaces[i].mNumIndices; ++j)
      faces.index[j] = mesh->mFaces[i].mIndices[j];

    glm::vec3 bma = resMesh.vertices[faces.index[1]] - resMesh.vertices[faces.index[0]];
    glm::vec3 cma = resMesh.vertices[faces.index[2]] - resMesh.vertices[faces.index[0]];
    faces.fNormal = glm::normalize(glm::cross(bma, cma));
    resMesh.faces.push_back(faces);
  }

  return resMesh;
}

// Process Node
void processNode(aiNode* node, const aiScene * scene, NodeHierarchy* hier)
{
  aiMesh* mesh;
  nodeMap[node->mName.C_Str()] = nodeIndex++;
  hier->m_nodeIndex = nodeMap[node->mName.C_Str()];
  memcpy_s(&hier->m_transform[0], sizeof(glm::mat4), &node->mTransformation.a1, sizeof(glm::mat4));

  // process mesh
  hier->m_meshes.reserve(node->mNumMeshes);
  for (unsigned i = 0; i < node->mNumMeshes; ++i) {
    mesh = scene->mMeshes[node->mMeshes[i]];
    hier->m_meshes.push_back(processMesh(mesh, scene));
  }

  // process children node
  for (unsigned i = 0; i < node->mNumChildren; ++i) {
    hier->m_children.push_back(NodeHierarchy());
    processNode(node->mChildren[i], scene, &hier->m_children.back());
  }
}

void FlattenMeshNonConst(NodeHierarchy& node, std::vector<Mesh*>& result)
{
  for (auto& elem : node.m_meshes)
    result.push_back(&elem);

  for (auto& child : node.m_children)
    FlattenMeshNonConst(child, result);
}

bool ModelUtils::ImportModel(const std::string& file, Model* model, bool normalizeVertex)
{
  if (model == nullptr)
  {
    std::cout << "Error: model is nullptr\n";
    return false;
  }
  Assimp::Importer importer;
  const aiScene *scene = importer.ReadFile(file, aiProcessPreset_TargetRealtime_Quality | 
                                                 aiProcess_FlipUVs | 
                                                 aiProcess_OptimizeGraph | 
                                                 aiProcess_OptimizeMeshes);

  if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
    std::cout << "Error: Model import fail: " << importer.GetErrorString() << "\n";
    return false;
  }

  processNode(scene->mRootNode, scene, &model->m_root);

  if (normalizeVertex)
  {
    std::vector<Mesh*> resMesh;
    FlattenMeshNonConst(model->m_root, resMesh);

    // find centroid
    glm::vec4 centroid(0);
    size_t vertNum = 0;
    for (auto& mesh : resMesh)
    {
      vertNum += mesh->vertices.size();
      for (auto& vertex : mesh->vertices)
        centroid += vertex;
    }
    centroid /= (float)vertNum;

    // shift all vertex to centroid & find max distance from centroid
    float maxDis = std::numeric_limits<float>::min();
    for (auto& mesh : resMesh)
    {
      for (auto& vertex : mesh->vertices)
      {
        float dist = glm::distance(vertex, centroid);
        maxDis = dist > maxDis ? dist : maxDis;
      }
    }

    // scale vertex to -1 to 1
    for (auto& mesh : resMesh)
    {
      for (auto& vertex : mesh->vertices)
      {
        glm::vec4 vertVector = vertex - centroid;
        float newLen = 1.0f / maxDis;
        vertex = vertVector * newLen;
        vertex.w = 1.f; // reaffirm
      }
    }
  }

  // if bone process bone
  return true;
}

std::vector<vPack> ModelUtils::PackData(const Model& mod, const vBufferLayout& bLayout)
{
  std::vector<const Mesh*> fltMesh;
  std::vector<vPack> vPackList;
  unsigned vPackCount = ModelUtils::ComputePerVertexPackSize(bLayout);
  ModelUtils::FlattenMesh(mod.m_root, fltMesh);
  vPackList.resize(fltMesh.size());

  // init vpack memory
  for (unsigned i = 0; i < fltMesh.size(); ++i)
  {
    size_t vertCount = fltMesh[i]->vertices.size();
    vPackList[i]._VertexSize = (unsigned)vertCount;
    vPackList[i]._Data.resize(vPackCount * vertCount);
    vPackList[i]._Indices.resize(fltMesh[i]->faces.size() * 3 * sizeof(unsigned));

    // packing vertex buffer
    for (unsigned j = 0; j < vertCount; ++j)
    {
      unsigned vPackIndex = j * vPackCount;

      for (auto& type : bLayout.layout)
      {
        const void* dataPtr = nullptr;
        unsigned sizeIncrement = 0;
        switch (type)
        {
        case vBufferLayout::VERTEX_POS:
          dataPtr = &fltMesh[i]->vertices[j];
          sizeIncrement = 4 * sizeof(float);
          break;

        case vBufferLayout::VERTEX_NORMAL:
          dataPtr = &fltMesh[i]->vNormals[j];
          sizeIncrement = 4 * sizeof(float);
          break;

        case vBufferLayout::VERTEX_TANGENT:
          dataPtr = &fltMesh[i]->vTangent[j];
          sizeIncrement = 4 * sizeof(float);
          break;

        case vBufferLayout::VERTEX_BITANGENT:
          dataPtr = &fltMesh[i]->vBiTangent[j];
          sizeIncrement = 4 * sizeof(float);
          break;

        case vBufferLayout::VERTEX_BONE_ID:
          dataPtr = &fltMesh[i]->boneIndex[j];
          sizeIncrement = 4 * sizeof(unsigned);
          break;

        case vBufferLayout::VERTEX_BONE_WEIGHT:
          dataPtr = &fltMesh[i]->boneWeight[j];
          sizeIncrement = 4 * sizeof(float);
          break;

        case vBufferLayout::TEX_COORDUV0:
          dataPtr = &(fltMesh[i]->uvCoordSets.find(0)->second[j]);
          sizeIncrement = 2 * sizeof(float);
          break;

        case vBufferLayout::TEX_COORDUV1:
          dataPtr = &(fltMesh[i]->uvCoordSets.find(1)->second[j]);
          sizeIncrement = 2 * sizeof(float);
          break;

        case vBufferLayout::TEX_COORDUV2:
          dataPtr = &(fltMesh[i]->uvCoordSets.find(2)->second[j]);
          sizeIncrement = 2 * sizeof(float);
          break;

        case vBufferLayout::TEX_COORDUV3:
          dataPtr = &(fltMesh[i]->uvCoordSets.find(3)->second[j]);
          sizeIncrement = 2 * sizeof(float);
          break;
        }

        memcpy_s(&vPackList[i]._Data[vPackIndex], sizeIncrement, dataPtr, sizeIncrement);
        vPackIndex += sizeIncrement;
      }

    }

    // pack indices
    size_t facesCount = fltMesh[i]->faces.size();
    unsigned indicesIndex = 0;
    unsigned sizeofUnsigned = sizeof(unsigned);
    unsigned indexStride = 3 * sizeof(unsigned);
    for (unsigned j = 0; j < facesCount; ++j)
    {
      // unroll do it 3 times
      memcpy_s(&vPackList[i]._Indices[indicesIndex],                         sizeofUnsigned, &fltMesh[i]->faces[j].index[0], sizeofUnsigned);
      memcpy_s(&vPackList[i]._Indices[indicesIndex + sizeofUnsigned],        sizeofUnsigned, &fltMesh[i]->faces[j].index[1], sizeofUnsigned);
      memcpy_s(&vPackList[i]._Indices[indicesIndex + (sizeofUnsigned << 1)], sizeofUnsigned, &fltMesh[i]->faces[j].index[2], sizeofUnsigned);
      indicesIndex += indexStride;
    }

  }

  return vPackList;
}


void ModelUtils::FlattenMesh(const NodeHierarchy& node, std::vector<const Mesh*>& result)
{
  for (auto& elem : node.m_meshes)
    result.push_back(&elem);

  for (auto& child : node.m_children)
    FlattenMesh(child, result);
}

unsigned ModelUtils::ComputePerVertexPackSize(const vBufferLayout& layout)
{
  unsigned packSize = 0;
  for (auto& elem : layout.layout)
  {
    switch (elem)
    {
    case vBufferLayout::VERTEX_POS:
    case vBufferLayout::VERTEX_NORMAL:
    case vBufferLayout::VERTEX_TANGENT:
    case vBufferLayout::VERTEX_BITANGENT:
      packSize += 4 * sizeof(float);
      break;

    case vBufferLayout::VERTEX_BONE_ID:
      packSize += 4 * sizeof(unsigned);
      break;

    case vBufferLayout::VERTEX_BONE_WEIGHT:
      packSize += 4 * sizeof(float);
      break;

    case vBufferLayout::TEX_COORDUV0:
    case vBufferLayout::TEX_COORDUV1:
    case vBufferLayout::TEX_COORDUV2:
    case vBufferLayout::TEX_COORDUV3:
      packSize += 2 * sizeof(float);
      break;
    }
  }

  return packSize;
}

unsigned ModelUtils::ComputeElementOffset(OGL_BASETYPE type)
{
  switch (type)
  {
  case INT_TYPE:
    return sizeof(int);

  case INT4_TYPE:
    return sizeof(int) << 2;

  case FLOAT_TYPE:
    return sizeof(float);

  case BOOL_TYPE:
    return sizeof(bool);

  case VEC2_TYPE:
    return sizeof(float) << 1;

  case VEC3_TYPE:
    return sizeof(float) * 3;
    break;

  case VEC4_TYPE:
    return sizeof(float) << 2;
    break;

  case MAT3_TYPE:
    return sizeof(float) * 9;
    break;

  case MAT4_TYPE:
    return sizeof(float) << 4;
    break;

  default:
    throw -1;
  }
}

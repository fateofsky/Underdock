#pragma once
#include "imgui.h"
#include "MaterialLayout.h"

namespace ImguiWin32
{
  extern void RunMaterialViewer(bool& open);
}
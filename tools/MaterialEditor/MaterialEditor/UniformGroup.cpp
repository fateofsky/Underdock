#include "pch.h"
#include "UniformGroup.h"

UniformGroup::UniformGroup()
  : m_groups(), m_groupSet(0), m_memoryLayout(_MemoryLayout::NONE)
{ }

int UniformGroup::InsertUniform(const std::string& uniformName)
{
  if (std::find(m_groups.begin(), m_groups.end(), uniformName) == m_groups.end())
  {
    m_groups.emplace_back(uniformName);
    return 0;
  }
  return 1;
}

int UniformGroup::RemoveUniform(const std::string & uniformName)
{
  auto grpIter = std::find(m_groups.begin(), m_groups.end(), uniformName);
  if (grpIter != m_groups.end())
  {
    m_groups.erase(grpIter);
    return 0;
  }
  return 1;
}

size_t UniformGroup::GetGroupCount() const
{
  return m_groups.size();
}

std::vector<std::string>& UniformGroup::GetGroupList()
{
  return m_groups;
}

void UniformGroup::ChangeGroup(unsigned group)
{
  m_groupSet = group;
}

unsigned UniformGroup::GetGroup() const
{
  return m_groupSet;
}

void UniformGroup::ChangeMemoryLayout(UniformGroup::_MemoryLayout mLayout)
{
  m_memoryLayout = mLayout;
}

UniformGroup::_MemoryLayout UniformGroup::GetMemoryLayout() const
{
  return m_memoryLayout;
}

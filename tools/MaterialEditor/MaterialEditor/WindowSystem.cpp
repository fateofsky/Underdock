#include "pch.h"
#include "WindowSystem.h"
#include "OGL.h"
#include "AppManager.h"
#include "ImguiWin32.h"

HWND WinSystem::hwnd;
HDC WinSystem::hdc;
HGLRC WinSystem::hrc;
bool _APPCLOSE = false;
std::string WinSystem::glslangExe;
std::string WinSystem::curDirectory;
DWORD nBufferLength = 1024;
CHAR lpBuffer[1024]{'\0'};

LRESULT CALLBACK WinSystem::WinProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  if (AppManager::GetInstance() == nullptr || ImGui::GetCurrentContext() == NULL)
    return DefWindowProc(hwnd, msg, wParam, lParam);
  AppManager* appInst = AppManager::GetInstance();
  ImGuiIO& io = ImGui::GetIO();

  switch (msg)
  {
  case WM_CREATE:

    break;

  case WM_LBUTTONDOWN: case WM_LBUTTONDBLCLK:
  case WM_RBUTTONDOWN: case WM_RBUTTONDBLCLK:
  case WM_MBUTTONDOWN: case WM_MBUTTONDBLCLK:
  case WM_XBUTTONDOWN: case WM_XBUTTONDBLCLK:
  {
    int button = 0;
    if (msg == WM_LBUTTONDOWN || msg == WM_LBUTTONDBLCLK) { button = 0; }
    if (msg == WM_RBUTTONDOWN || msg == WM_RBUTTONDBLCLK) { button = 1; }
    if (msg == WM_MBUTTONDOWN || msg == WM_MBUTTONDBLCLK) { button = 2; }
    if (msg == WM_XBUTTONDOWN || msg == WM_XBUTTONDBLCLK) { button = (GET_XBUTTON_WPARAM(wParam) == XBUTTON1) ? 3 : 4; }
    if (!ImGui::IsAnyMouseDown() && ::GetCapture() == NULL)
      ::SetCapture(hwnd);
    io.MouseDown[button] = true;
  } break;

  case WM_LBUTTONUP:
  case WM_RBUTTONUP:
  case WM_MBUTTONUP:
  case WM_XBUTTONUP:
  {
    int button = 0;
    if (msg == WM_LBUTTONUP) { button = 0; }
    if (msg == WM_RBUTTONUP) { button = 1; }
    if (msg == WM_MBUTTONUP) { button = 2; }
    if (msg == WM_XBUTTONUP) { button = (GET_XBUTTON_WPARAM(wParam) == XBUTTON1) ? 3 : 4; }
    io.MouseDown[button] = false;
    if (!ImGui::IsAnyMouseDown() && ::GetCapture() == hwnd)
      ::ReleaseCapture();
  } break;

  case WM_MOUSEWHEEL:
  {
    short zDelta = GET_WHEEL_DELTA_WPARAM(wParam);
    appInst->ScrollCam(zDelta > 0 ? 1 : -1);
    io.MouseWheel += (float)GET_WHEEL_DELTA_WPARAM(wParam) / (float)WHEEL_DELTA;
  } break;

  case WM_MOUSEHWHEEL:
    io.MouseWheelH += (float)GET_WHEEL_DELTA_WPARAM(wParam) / (float)WHEEL_DELTA;
    break;

  case WM_KEYDOWN:
  case WM_SYSKEYDOWN:
    if (wParam < 256)
      io.KeysDown[wParam] = 1;
    break;

  case WM_KEYUP:
  case WM_SYSKEYUP:
    if (wParam < 256)
      io.KeysDown[wParam] = 0;
    break;

  case WM_CHAR:
    // You can also use ToAscii()+GetKeyboardState() to retrieve characters.
    io.AddInputCharacter((unsigned int)wParam);
    break;

  case WM_DESTROY:
  case WM_CLOSE:
    _APPCLOSE = true;
    break;
  default:
    break;
  }
  return DefWindowProc(hwnd, msg, wParam, lParam);
}

void WinSystem::InitWindow(HINSTANCE instanceH)
{
  AllocConsole();
  freopen("CONOUT$", "w", stdout);
  std::cout << "Console Initialized..." << std::endl;
  _APPCLOSE = false;

  //setting up window
  WNDCLASSEX wcex;
  wcex.cbSize = sizeof(WNDCLASSEXA);
  wcex.style = CS_HREDRAW | CS_VREDRAW;
  wcex.lpfnWndProc = WinProc;
  wcex.cbClsExtra = 0;
  wcex.cbWndExtra = 0;
  wcex.hInstance = instanceH;
  wcex.hIcon = (HICON)LoadImage(NULL, "material.ico",
    IMAGE_ICON, 16, 16, LR_LOADFROMFILE | LR_LOADTRANSPARENT | LR_SHARED);
  wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
  wcex.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
  wcex.lpszMenuName = APP_TITLE;
  wcex.lpszClassName = APP_TITLE;
  wcex.hIconSm = (HICON)LoadImage(NULL, "material.ico",
    IMAGE_ICON, 16, 16, LR_LOADFROMFILE | LR_LOADTRANSPARENT | LR_SHARED);

  if (!RegisterClassEx(&wcex))
  {
    MessageBox(NULL, "Register class fail!", "Error", NULL);
    assert(false);
    exit(-1);
  }


  int nStyle = WS_POPUP | WS_OVERLAPPED | WS_SYSMENU | WS_VISIBLE | WS_CAPTION;
  hwnd = CreateWindow(wcex.lpszClassName, wcex.lpszMenuName,
    nStyle, 0, 0, DEFAULT_SCREEN_WIDTH, DEFAULT_SCREEN_HEIGHT, NULL, NULL, instanceH, NULL);

  // Pixel format
  hdc = GetDC(hwnd);
  PIXELFORMATDESCRIPTOR pfd;
  pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
  pfd.nVersion = 1;
  pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
  pfd.iLayerType = PFD_MAIN_PLANE;
  pfd.iPixelType = LPD_TYPE_RGBA;
  pfd.cDepthBits = 16;
  pfd.cColorBits = 32;

  if (!SetPixelFormat(hdc, ChoosePixelFormat(hdc, &pfd), &pfd))
    MessageBox(NULL, "Set pixel fail!", "Error", NULL);

  ShowWindow(hwnd, SW_SHOW);

  // Initialize window file name
  curDirectory = WinSystem::GetWorkingDirectory();
  std::ifstream ifs("_glslangValidator_loc", std::ios::in | std::ios::ate);
  if (!ifs.is_open()) throw 1;

  std::streampos fileSize = ifs.tellg();
  glslangExe.resize(fileSize);
  ifs.seekg(ifs.beg);
  ifs.read(&glslangExe[0], fileSize);
  ifs.close();
}



void WinSystem::Run()
{
  // Init all managers
  ResourceManager::Initialize();
  ShaderBuilder::Initialize();
  AppManager::Initialize();
  AppManager* appInst = AppManager::GetInstance();

  // Init Mouse
  POINT mousePointWin;
  GetCursorPos(&mousePointWin);
  appInst->InitializeMouse(mousePointWin.x, mousePointWin.y);
  if (!appInst->LoadProjectLocation())
    appInst->LoadDefault();
  appInst->InitializeSettings();

  while (1)
  {
    /*
      -- WINDOWS MESSAGES
    */
    MSG msg;
    while (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
    {
#pragma warning(suppress: 6387) //param 2 being 0 does not adhere to spec, but its optional
      if (!TranslateAccelerator(msg.hwnd, NULL, &msg))
      {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    }
    if (_APPCLOSE) return;


    /*
      -- MAIN LOOP
    */
    // Input update
    POINT mousePointWin;
    GetCursorPos(&mousePointWin);
    appInst->SetNewMousePosition(mousePointWin.x, mousePointWin.y);

    appInst->Update();
    appInst->EditorUpdate();
    appInst->Render();
    appInst->EditorRender();
    appInst->LastUpdate();

    SwapBuffers(hdc);
  }
}

void WinSystem::Cleanup()
{
  /*
    Cleanup
  */
  DestroyWindow(hwnd);
  wglMakeCurrent(NULL, NULL);
  wglDeleteContext(hrc);

  ShaderBuilder::Destroy();
  ResourceManager::Destroy();
  AppManager::Destroy();
}

void WinSystem::InvokeProcess(const char * lpApplicationName, char * cmdLine)
{
  // additional information
  STARTUPINFO si;
  PROCESS_INFORMATION pi;

  // set the size of the structures
  ZeroMemory(&si, sizeof(si));
  si.cb = sizeof(si);
  ZeroMemory(&pi, sizeof(pi));

  // start the program up
  BOOL procCreated = 
    CreateProcess(
    lpApplicationName,               // the path
    cmdLine,                         // Command line
    NULL,                            // Process handle not inheritable
    NULL,                            // Thread handle not inheritable
    FALSE,                           // Set handle inheritance to FALSE
    0,                               // No creation flags
    NULL,                            // Use parent's environment block
    NULL,                            // Use parent's starting directory 
    &si,                             // Pointer to STARTUPINFO structure
    &pi                              // Pointer to PROCESS_INFORMATION structure (removed extra parentheses)
  );

  if (!procCreated)
  {
    std::cout << "Error : Cannot create process module " << lpApplicationName << std::endl;
    std::cout << GetLastError() << std::endl;
  }

  // Close process and thread handles. 
  WaitForSingleObject(pi.hProcess, INFINITE);
  CloseHandle(pi.hProcess);
  CloseHandle(pi.hThread);
}

void WinSystem::ClearTempFile(const char * filePath)
{
  BOOL tryDelete = DeleteFileA(filePath);
  if (!tryDelete)
    std::cout << "Error: Failed to delete file " << filePath << std::endl;
}

std::string WinSystem::SaveFileAs(LPCSTR filter, LPCSTR extToAppend)
{
  OPENFILENAME ofn;
  ZeroMemory(&ofn, sizeof(ofn));
  CHAR szFile[255]{ '\0' };
  std::string defaultSaveDir = GetWorkingDirectory() + "\\saves";

  ofn.lStructSize = sizeof(ofn);
  ofn.hwndOwner = hwnd;
  ofn.lpstrFile = szFile;
  ofn.nMaxFile = sizeof(szFile);
  ofn.lpstrFilter = filter;
  ofn.nFilterIndex = 1;
  ofn.lpstrFileTitle = szFile;
  ofn.nMaxFileTitle = sizeof(szFile);
  ofn.lpstrInitialDir = (CHAR*)defaultSaveDir.c_str();
  ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR | OFN_OVERWRITEPROMPT;

  // Display the Open dialog box. 
  if (GetSaveFileName(&ofn) == TRUE)
  {
    std::string resultFile(ofn.lpstrFileTitle);
    if (resultFile.find(extToAppend) == std::string::npos)
      resultFile += extToAppend;
    return resultFile;
  }
  return "";
}

std::string WinSystem::OpenFile(LPCSTR filter)
{
  OPENFILENAME ofn;
  ZeroMemory(&ofn, sizeof(ofn));
  CHAR szFile[255]{ '\0' };
  std::string defaultSaveDir = GetWorkingDirectory() + "\\saves";

  ofn.lStructSize = sizeof(ofn);
  ofn.hwndOwner = hwnd;
  ofn.lpstrFile = szFile;
  ofn.nMaxFile = sizeof(szFile);
  ofn.lpstrFilter = filter;
  ofn.nFilterIndex = 1;
  ofn.lpstrFileTitle = szFile;
  ofn.nMaxFileTitle = sizeof(szFile);
  ofn.lpstrInitialDir = (CHAR*)defaultSaveDir.c_str();
  ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR | OFN_OVERWRITEPROMPT;

  // Display the Open dialog box. 
  if (GetOpenFileName(&ofn) == TRUE)
    return std::string(ofn.lpstrFileTitle);
  return "";
}

std::string WinSystem::GetWorkingDirectory()
{
  char* cwd = _getcwd(0, 0);
  std::string working_directory(cwd);
  std::free(cwd);
  return working_directory;
}

std::string WinSystem::GetTempFilePath()
{
  DWORD len = GetTempPathA(nBufferLength, lpBuffer);
  if (len > 0)
  {
    std::string result;
    result.resize(len);
    memcpy_s(&result[0], len, lpBuffer, len);
    return result;
  }
  return std::string("");
}

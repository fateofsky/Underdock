#pragma once
#include "imgui.h"
#include "MaterialLayout.h"

namespace ImguiWin32
{
  extern void RunTextureViewer(ImTextureID id, bool& open);
  extern void RunTextureLibraryViewer(bool& open, MaterialLayout* layout);
}
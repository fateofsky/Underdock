#pragma once
#include <vector>

struct vPack
{
  unsigned          _VertexSize;
  std::vector<char> _Data;
  std::vector<char> _Indices;
};

struct vBufferLayout
{
  enum vBufferLayoutType
  {
    VERTEX_POS,               // vec4
    VERTEX_NORMAL,            // ~
    VERTEX_TANGENT,           // ~
    VERTEX_BITANGENT,           // ~
    VERTEX_BONE_ID,           // (4 x unsigned)
    VERTEX_BONE_WEIGHT,       // vec4 (4 x float)
    TEX_COORDUV0,             // vec2
    TEX_COORDUV1,             // ~
    TEX_COORDUV2,             // ~
    TEX_COORDUV3,             // ~
  };

  std::vector<vBufferLayoutType> layout;
};
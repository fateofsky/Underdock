#pragma once
#include "EntityTracker.h"
#include <limits>

#ifndef COMPONENT_VALID
#define COMPONENT_VALID 0x1
#endif // !COMPONENT_VALID

#ifndef COMPONENT_INVALID
#define COMPONENT_INVALID 0x0
#endif // !COMPONENT_INVALID

#ifndef MAX_UNSIGNED
#define MAX_UNSIGNED (std::numeric_limits<unsigned int>::max)()
#endif// !MAX_UNSIGNED

class Gameobject;

class ComponentHandleBase {};

template <typename T>
class ComponentHandle : public ComponentHandleBase
{
  friend class Gameobject;

  public:
    ComponentHandle() : m_reference(MAX_UNSIGNED), m_flag(0), m_gameobj(0), m_gameobjRef(0)
    { }

    ~ComponentHandle() 
    {
      DestroyInfo();
    }

    ComponentHandle(const unsigned& ref, char* flag, Gameobject* obj, const unsigned& objRef)
      : m_reference(ref), m_flag(flag), m_gameobj(obj), m_gameobjRef(objRef)
    { }

    ComponentHandle(const ComponentHandle<T>&& rhs)
    {
      m_reference = rhs.m_reference;
      m_flag = rhs.m_flag;
      m_gameobj = rhs.m_gameobj;
      m_gameobjRef = rhs.m_gameobjRef;
    }

    inline void MoveComponentHandleOver(ComponentHandle<T>&& rRef)
    {
      DestroyInfo();
      m_reference = rRef.m_reference;
      m_flag = rRef.m_flag;
      m_gameobj = rRef.m_gameobj;
      m_gameobjRef = rRef.m_gameobjRef;

      rRef.m_reference = MAX_UNSIGNED;
      rRef.m_flag = 0;
      rRef.m_gameobj = 0;
      rRef.m_gameobjRef = 0;
    }

    inline T* GetComponent()
    {
      return m_flag && (*m_flag & COMPONENT_VALID) ? m_gameobj->GetComponentData<T>(m_reference) : nullptr;
    }

    inline bool IsValid() const
    {
      return m_flag && (*m_flag & COMPONENT_VALID);
    }

    const unsigned& GetComponentReference() const
    {
      return m_reference;
    }

    //ComponentEnum GetType() const;

    void DestroyInfo()
    {
      EntityTracker::GetInstance()->RemoveComponentReference(m_gameobjRef, rtti::DeriveTypeID<T>(), m_reference);
      m_reference = MAX_UNSIGNED;
      m_flag = 0;
      m_gameobj = 0;
      m_gameobjRef = 0;
    }

  private:
    ComponentHandle(const ComponentHandle&) = delete;    // non construction-copyable
    ComponentHandle& operator=(const ComponentHandle&) = delete;

    inline void _setData(const unsigned& ref, char* flag, Gameobject* obj, const unsigned& objRef)
    {
      m_reference = ref;
      m_flag = flag;
      m_gameobj = obj;
      m_gameobjRef = objRef;
    }

    unsigned      m_reference;
    char*         m_flag;
    Gameobject*   m_gameobj;
    unsigned      m_gameobjRef;
};


#include "pch.h"
#include "UniformResource.h"

UniformResourceBase::UniformResourceBase(OGL_BASETYPE type)
  : m_type(type), m_usage(UNIFORM_USAGE::USER_INPUT)
{ }

OGL_BASETYPE UniformResourceBase::GetType() const
{
  return m_type;
}

UNIFORM_USAGE UniformResourceBase::GetUsage() const
{
  return m_usage;
}


void UniformResourceBase::ChangeUsage(UNIFORM_USAGE newUsage)
{
  m_usage = newUsage;
}

void UniformResourceBase::ChangeGroup(const unsigned & set)
{
  m_groupSet = set;
}

unsigned UniformResourceBase::GetGroup() const
{
  return m_groupSet;
}


UniformResourceBase* UniformResourceBase::CreateUniform(OGL_BASETYPE baseType, UNIFORM_USAGE usage, unsigned group)
{
  UniformResourceBase* _base = nullptr;
  switch (baseType)
  {
  case INT_TYPE:
    _base = new UniformInt();
    break;

  case INT4_TYPE:
    _base = new UniformInt4();
    break;

  case FLOAT_TYPE:
    _base = new UniformFloat();
    break;

  case BOOL_TYPE:
    _base = new UniformBool();
    break;

  case VEC2_TYPE:
    _base = new UniformVec2();
    break;

  case VEC3_TYPE:
    _base = new UniformVec3();
    break;

  case VEC4_TYPE:
    _base = new UniformVec4();
    break;

  case MAT3_TYPE:
    _base = new UniformMat3();
    break;

  case MAT4_TYPE:
    _base = new UniformMat4();
    break;
  }

  if (_base)
  {
    _base->ChangeUsage(usage);
    _base->ChangeGroup(group);
  }

  return _base;
}


/*
  -- INT
*/
UniformInt::UniformInt()
  : UniformResourceBase(OGL_BASETYPE::INT_TYPE)
    , m_data(0)
{ }

void* UniformInt::GetUniformData()
{
  return &m_data;
}

void UniformInt::BindUniform(GLint location, bool transpose)
{
  glUniform1i(location, m_data);
}

void UniformInt::Serialize(MemoryStream& ofs)
{
  ofs.Write(reinterpret_cast<char*>(&m_data), sizeof(int));
}

void UniformInt::DeSerialize(MemoryStream& ifs)
{
  ifs.Read(reinterpret_cast<char*>(&m_data), sizeof(int));
}



/*
  -- FLOAT
*/
UniformFloat::UniformFloat()
  : UniformResourceBase(OGL_BASETYPE::FLOAT_TYPE)
  , m_data(0)
{ }

void* UniformFloat::GetUniformData()
{
  return &m_data;
}

void UniformFloat::BindUniform(GLint location, bool transpose)
{
  glUniform1f(location, m_data);
}

void UniformFloat::Serialize(MemoryStream& ofs)
{
  ofs.Write(reinterpret_cast<char*>(&m_data), sizeof(float));
}

void UniformFloat::DeSerialize(MemoryStream& ifs)
{
  ifs.Read(reinterpret_cast<char*>(&m_data), sizeof(float));
}



/*
  -- BOOLEAN
*/
UniformBool::UniformBool()
  : UniformResourceBase(OGL_BASETYPE::BOOL_TYPE)
  , m_data(0)
{ }

void* UniformBool::GetUniformData()
{
  return &m_data;
}

void UniformBool::BindUniform(GLint location, bool transpose)
{
  int promoteData = m_data;
  glUniform1i(location, promoteData);
}

void UniformBool::Serialize(MemoryStream& ofs)
{
  ofs.Write(reinterpret_cast<char*>(&m_data), sizeof(bool));
}

void UniformBool::DeSerialize(MemoryStream& ifs)
{
  ifs.Read(reinterpret_cast<char*>(&m_data), sizeof(bool));
}



/*
  -- VECTOR 2
*/
UniformVec2::UniformVec2()
  : UniformResourceBase(OGL_BASETYPE::VEC2_TYPE)
  , m_data(0)
{ }

void* UniformVec2::GetUniformData()
{
  return &m_data.x;
}

void UniformVec2::BindUniform(GLint location, bool)
{
  glUniform2fv(location, 1, &m_data.x);
}

void UniformVec2::Serialize(MemoryStream& ofs)
{
  ofs.Write(reinterpret_cast<char*>(&m_data), sizeof(glm::vec2));
}

void UniformVec2::DeSerialize(MemoryStream& ifs)
{
  ifs.Read(reinterpret_cast<char*>(&m_data), sizeof(glm::vec2));
}



/*
  -- VECTOR 3
*/
UniformVec3::UniformVec3()
  : UniformResourceBase(OGL_BASETYPE::VEC3_TYPE)
  , m_data(0)
{ }


void* UniformVec3::GetUniformData()
{
  return &m_data.x;
}

void UniformVec3::BindUniform(GLint location, bool)
{
  glUniform3fv(location, 1, &m_data.x);
}

void UniformVec3::Serialize(MemoryStream& ofs)
{
  ofs.Write(reinterpret_cast<char*>(&m_data), sizeof(glm::vec3));
}

void UniformVec3::DeSerialize(MemoryStream& ifs)
{
  ifs.Read(reinterpret_cast<char*>(&m_data), sizeof(glm::vec3));
}



/*
  -- VECTOR 4
*/
UniformVec4::UniformVec4()
  : UniformResourceBase(OGL_BASETYPE::VEC4_TYPE)
  , m_data(0)
{ }


void* UniformVec4::GetUniformData()
{
  return &m_data.x;
}

void UniformVec4::BindUniform(GLint location, bool)
{
  glUniform4fv(location, 1, &m_data.x);
}

void UniformVec4::Serialize(MemoryStream & ofs)
{
  ofs.Write(reinterpret_cast<char*>(&m_data), sizeof(glm::vec4));
}

void UniformVec4::DeSerialize(MemoryStream& ifs)
{
  ifs.Read(reinterpret_cast<char*>(&m_data), sizeof(glm::vec4));
}



/*
  -- MATRIX 3
*/
UniformMat3::UniformMat3()
  : UniformResourceBase(OGL_BASETYPE::MAT3_TYPE)
  , m_data(0)
{ }


void* UniformMat3::GetUniformData()
{
  return &m_data[0][0];
}

void UniformMat3::BindUniform(GLint location, bool transpose)
{
  glUniformMatrix3fv(location, 1, transpose ? GL_TRUE : GL_FALSE, &m_data[0][0]);
}

void UniformMat3::Serialize(MemoryStream & ofs)
{
  ofs.Write(reinterpret_cast<char*>(&m_data), sizeof(glm::mat3));
}

void UniformMat3::DeSerialize(MemoryStream& ifs)
{
  ifs.Read(reinterpret_cast<char*>(&m_data), sizeof(glm::mat3));
}



/*
  -- MATRIX 4
*/
UniformMat4::UniformMat4()
  : UniformResourceBase(OGL_BASETYPE::MAT4_TYPE)
  , m_data(0)
{ }


void* UniformMat4::GetUniformData()
{
  return &m_data[0][0];
}

void UniformMat4::BindUniform(GLint location, bool transpose)
{
  glUniformMatrix4fv(location, 1, transpose ? GL_TRUE : GL_FALSE, &m_data[0][0]);
}

void UniformMat4::Serialize(MemoryStream & ofs)
{
  ofs.Write(reinterpret_cast<char*>(&m_data), sizeof(glm::mat4));
}

void UniformMat4::DeSerialize(MemoryStream& ifs)
{
  ifs.Read(reinterpret_cast<char*>(&m_data), sizeof(glm::mat4));
}


UniformInt4::UniformInt4()
  : UniformResourceBase(OGL_BASETYPE::INT4_TYPE)
  , m_data{ 0 }
{ }

void* UniformInt4::GetUniformData()
{
  return m_data;
}

void UniformInt4::BindUniform(GLint location, bool transpose)
{
  glUniform4i(location, m_data[0], m_data[2], m_data[2], m_data[3]);
}

void UniformInt4::Serialize(MemoryStream& ofs)
{
  ofs.Write(reinterpret_cast<char*>(m_data), sizeof(int) << 2);
}

void UniformInt4::DeSerialize(MemoryStream& ifs)
{
  ifs.Read(m_data, sizeof(int) << 2);
}

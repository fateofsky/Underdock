#include "pch.h"
#include "DemoLevel.h"

// TEST
GameobjectHandle CameraObj;
GameobjectHandle textureObj;
GameobjectHandle textureObj2;
GameobjectHandle treeObjs[9];

GameobjectHandle globalWaterObjSet0;
GameobjectHandle sharedWaterObjSet1;
GameobjectHandle waterObj;

AppManager* app = nullptr;
GraphicsManager* gm = nullptr;

VulkUtils::VulkRenderingTree DemoLevel::_levelRenderingTree;

void DemoLevel::LoadLevel()
{
  GameobjectManager* mgr = GameobjectManager::GetInstance();
  app = AppManager::GetInstance();
  gm = GraphicsManager::GetInstance();

  CameraObj.MoveGameobjectHandleOver(mgr->GetGameobjectHandle(mgr->CreateGameobject()));
  if (CameraObj.IsValid())
  {
    auto transform = CameraObj.GetData()->AddComponent_hc<Transform>();
    if (transform.IsValid())
    {
      auto transformPtr = transform.GetComponent();
      transformPtr->SetPosition(vec3(3.f, 5.f, 3.f));
      transformPtr->SetScale(vec3(1, 1, 1));
      transformPtr->SetRotation(vec3(0.f, 0.f, 0.f));

      app->RegisterEntityForInit(transformPtr);
    }

    auto camComp = CameraObj.GetData()->AddComponent_hc<CameraComponent>();
    if (camComp.IsValid())
    {
      auto camCompPtr = camComp.GetComponent();
      camCompPtr->SetNearPlane(0.1f);
      camCompPtr->SetFarPlane(100.f);
      camCompPtr->SetFOV(TORADIAN * 60.f);
      camCompPtr->SetAspectRatio((float)Engine::core::windowsWidth / (float)Engine::core::windowsHeight);
      camCompPtr->SetLookAt(vec3(-3.f, -5.f, -3.f));
      camCompPtr->SetOrthogonal(false);

      app->RegisterEntityForInit(camComp.GetComponent());
    }

    auto matComp = CameraObj.GetData()->AddComponent_hc<MaterialComponent>();
    if (matComp.IsValid())
    {
      auto matCompPtr = matComp.GetComponent();
      matCompPtr->SetMaterialPipeline(0);
      matCompPtr->SetDescriptorSetNumber(0);
      matCompPtr->SetResourceAccessibilityType(Renderables::_ResourceType::GLOBAL);

      gm->RegisterMaterial(matCompPtr, _levelRenderingTree);
      //RegisterPreRenderEntity(matCompPtr);
      //RegisterEntity(matComp.GetComponent());
    }

    app->RegisterMainCamera(CameraObj);
  }

  // Test Try to populate shared resources
  textureObj.MoveGameobjectHandleOver(mgr->GetGameobjectHandle(mgr->CreateGameobject()));
  if (textureObj.IsValid())
  {
    auto matComp = textureObj.GetData()->AddComponent_hc<MaterialComponent>();
    if (matComp.IsValid())
    {
      auto matCompPtr = matComp.GetComponent();
      matCompPtr->SetMaterialPipeline(0);
      matCompPtr->SetDescriptorSetNumber(1);
      matCompPtr->SetResourceAccessibilityType(Renderables::_ResourceType::SHARED);
      matCompPtr->SetGlobalResourceTag(0);  // SHARED RESOURCE TYPE REQUIRES BOTH GLOBAL RESOURCE TAG

      matCompPtr->SetTextureListSize(1);
      matCompPtr->PushTextureList(0, 0, 0);
      gm->RegisterMaterial(matCompPtr, _levelRenderingTree);
    }
  }

  textureObj2.MoveGameobjectHandleOver(mgr->GetGameobjectHandle(mgr->CreateGameobject()));
  if (textureObj2.IsValid())
  {
    auto matComp = textureObj2.GetData()->AddComponent_hc<MaterialComponent>();
    if (matComp.IsValid())
    {
      auto matCompPtr = matComp.GetComponent();
      matCompPtr->SetMaterialPipeline(0);
      matCompPtr->SetDescriptorSetNumber(1);
      matCompPtr->SetResourceAccessibilityType(Renderables::_ResourceType::SHARED);
      matCompPtr->SetGlobalResourceTag(0);  // SHARED RESOURCE TYPE REQUIRES BOTH GLOBAL RESOURCE TAG
      matCompPtr->SetTextureListSize(1);
      matCompPtr->PushTextureList(0, 1, 0);
      gm->RegisterMaterial(matCompPtr, _levelRenderingTree);
    }
  }

  // Test: Try to populate tree branches
  const float originX = -2.f;
  const float originZ = -2.f;
  //const float originX = 0.f;
  //const float originZ = 0.f;
  for (int height = 0; height < 3; ++height)
  {
    for (int width = 0; width < 3; ++width)
    {
      float currX = width * 2;
      float currZ = height * 2;
      int whIndex = width + height * 3;
      treeObjs[whIndex].MoveGameobjectHandleOver(mgr->GetGameobjectHandle(mgr->CreateGameobject()));
      auto transform = treeObjs[whIndex].GetData()->AddComponent_hc<Transform>();
      if (transform.IsValid())
      {
        auto transformPtr = transform.GetComponent();
        transformPtr->SetPosition(vec3(originX + currX, 0.f, originZ + currZ));
        transformPtr->SetScale(vec3(1, 1, 1));
        transformPtr->SetRotation(vec3(0, 90, 0));

        app->RegisterEntityForInit(transformPtr);
      }

      auto matComp = treeObjs[whIndex].GetData()->AddComponent_hc<MaterialComponent>();
      if (matComp.IsValid())
      {
        auto matCompPtr = matComp.GetComponent();
        matCompPtr->SetMaterialPipeline(0);
        matCompPtr->SetDescriptorSetNumber(2); // set number 2
        matCompPtr->SetResourceAccessibilityType(Renderables::_ResourceType::INSTANCE);
        matCompPtr->SetGlobalResourceTag(0);  // INSTANCE RESOURCE TYPE REQUIRES BOTH GLOBAL RESOURCE TAG & SHARED RESOURCE TAG
        matCompPtr->SetSharedResourceTag((whIndex > 4) ? 1 : 0); // ~

        gm->RegisterMaterial(matCompPtr, _levelRenderingTree);
      }

      auto modelComponent = treeObjs[whIndex].GetData()->AddComponent_hc<ModelComponent>();
      if (modelComponent.IsValid())
      {
        modelComponent.GetComponent()->SetModelIndex(0);
        gm->RegisterModel(modelComponent.GetComponent());
        //RegisterEntity(modelComponent.GetComponent());
      }
    }
  }


  // water plane
  globalWaterObjSet0.MoveGameobjectHandleOver(mgr->GetGameobjectHandle(mgr->CreateGameobject()));
  if (globalWaterObjSet0.IsValid())
  {
    auto matComp = globalWaterObjSet0.GetData()->AddComponent_hc<MaterialComponent>();
    if (matComp.IsValid())
    {
      auto matCompPtr = matComp.GetComponent();
      matCompPtr->SetMaterialPipeline(1);
      matCompPtr->SetDescriptorSetNumber(0); // set number 0
      matCompPtr->SetResourceAccessibilityType(Renderables::_ResourceType::GLOBAL);

      gm->RegisterMaterial(matCompPtr, _levelRenderingTree);
    }
  }

  sharedWaterObjSet1.MoveGameobjectHandleOver(mgr->GetGameobjectHandle(mgr->CreateGameobject()));
  if (sharedWaterObjSet1.IsValid())
  {
    auto matComp = sharedWaterObjSet1.GetData()->AddComponent_hc<MaterialComponent>();
    if (matComp.IsValid())
    {
      auto matCompPtr = matComp.GetComponent();
      matCompPtr->SetMaterialPipeline(1);
      matCompPtr->SetDescriptorSetNumber(1); // set number 1
      matCompPtr->SetResourceAccessibilityType(Renderables::_ResourceType::SHARED);
      matCompPtr->SetGlobalResourceTag(1);  // INSTANCE RESOURCE TYPE REQUIRES BOTH GLOBAL RESOURCE TAG & SHARED RESOURCE TAG
      matCompPtr->SetTextureListSize(1);
      matCompPtr->PushTextureList(0, 2, 0);

      gm->RegisterMaterial(matCompPtr, _levelRenderingTree);
    }
  }


  waterObj.MoveGameobjectHandleOver(mgr->GetGameobjectHandle(mgr->CreateGameobject()));
  if (waterObj.IsValid())
  {
    auto transform = waterObj.GetData()->AddComponent_hc<Transform>();
    if (transform.IsValid())
    {
      auto transformPtr = transform.GetComponent();
      transformPtr->SetPosition(vec3(.0f, -0.5f, 0.f));
      transformPtr->SetScale(vec3(10, 1, 10));
      transformPtr->SetRotation(vec3(0, 0, 0));

      app->RegisterEntityForInit(transformPtr);
    }

    auto matComp = waterObj.GetData()->AddComponent_hc<MaterialComponent>();
    if (matComp.IsValid())
    {
      auto matCompPtr = matComp.GetComponent();
      matCompPtr->SetMaterialPipeline(1);
      matCompPtr->SetDescriptorSetNumber(2); // set number 2
      matCompPtr->SetResourceAccessibilityType(Renderables::_ResourceType::INSTANCE);
      matCompPtr->SetGlobalResourceTag(1);  // INSTANCE RESOURCE TYPE REQUIRES BOTH GLOBAL RESOURCE TAG & SHARED RESOURCE TAG
      matCompPtr->SetSharedResourceTag(0); // ~

      gm->RegisterMaterial(matCompPtr, _levelRenderingTree);
    }

    auto modelComponent = waterObj.GetData()->AddComponent_hc<ModelComponent>();
    if (modelComponent.IsValid())
    {
      modelComponent.GetComponent()->SetModelIndex(2);
      gm->RegisterModel(modelComponent.GetComponent());
    }
  }
}

void DemoLevel::InitLevel()
{
}

void DemoLevel::UpdateLevel()
{
  mat4 vpMtx = CameraObj.GetData()->GetFirstOfComponentHandle<CameraComponent>().GetComponent()->GetViewProjMatrix_Transpose();
  CameraObj.GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&vpMtx, sizeof(mat4), 0, 0, Engine::swpch::vCurFrame);
  for (int i = 0; i < 9; ++i)
  {
    const mat4 trans = treeObjs[i].GetData()->GetFirstOfComponentHandle<Transform>().GetComponent()->GetTransformMtx_Transpose();
    const vec3 colorBump(1.1f, 1.f, 1.f);
    treeObjs[i].GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&trans, sizeof(mat4), 0, 0, Engine::swpch::vCurFrame);
    treeObjs[i].GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&colorBump, sizeof(vec3), 1, 0, Engine::swpch::vCurFrame);
  }

  // set 0
  float warmedTime = app->GetAppTime() + 10.f;
  globalWaterObjSet0.GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&warmedTime, sizeof(float), 0, 0, Engine::swpch::vCurFrame);
  globalWaterObjSet0.GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&vpMtx, sizeof(mat4), 0, sizeof(vec4), Engine::swpch::vCurFrame);

  // set2
  const mat4 trans = waterObj.GetData()->GetFirstOfComponentHandle<Transform>().GetComponent()->GetTransformMtx_Transpose();
  const vec2 waveSpeedClamp(1.f, .1f);
  const vec4 colorBumpDamping(1.f, 1.1f, 1.05f, .25f);
  waterObj.GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&trans, sizeof(mat4), 0, 0, Engine::swpch::vCurFrame);
  waterObj.GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&waveSpeedClamp, sizeof(vec2), 0, sizeof(mat4), Engine::swpch::vCurFrame);
  waterObj.GetData()->GetFirstOfComponentHandle<MaterialComponent>().GetComponent()->SetData(&colorBumpDamping, sizeof(vec4), 1, 0, Engine::swpch::vCurFrame);
}

void DemoLevel::RenderLevel()
{
  const vec4& clearColor = gm->GetClearColor();
  const VulkBuffer& vBuffer = gm->GetVulkVertexBuffer();
  const VulkBuffer& iBuffer = gm->GetVulkIndexBuffer();


  Engine::core::SwapChainImageAcquire();
  Engine::core::StartRecord();

  // main render pass
  Engine::core::BeginMainRenderPass(Engine::swpch::vRenderpass, Engine::swpch::vFBVec[Engine::swpch::vCurFrame], &clearColor.x_);

  // glviewport
  Engine::core::SetViewportScissor(Engine::swpch::vCurFrame);

  const static VkDeviceSize v_Offsets[1] = { 0 };
  vkCmdBindVertexBuffers(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame], 0, 1, vBuffer.GetBufferPtr(), v_Offsets);
  vkCmdBindIndexBuffer(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame], iBuffer.GetBuffer(), 0, VK_INDEX_TYPE_UINT32);

  // for each pipeline (set 0)
  for (auto& global : _levelRenderingTree._globalNodes)
  {
    VulkPipeline& pipeline = gm->GetPipeline(global._globalRenderable->m_matPipelineIndex);
    pipeline.BindPipeline();
    pipeline.BindDescriptorSet(global._globalRenderable->m_descSetNumber, global._globalRenderable->m_descInstanceNumber);

    // for each shared resources (set 1)
    for (auto& shrResource : global._sharedNodes)
    {
      pipeline.BindDescriptorSet(shrResource._sharedRenderable->m_descSetNumber, shrResource._sharedRenderable->m_descInstanceNumber);

      // for each instances
      for (auto& instance : shrResource._instanceNodes)
      {
        // bind and draw if model exist
        pipeline.BindDescriptorSet(instance._renderable->m_descSetNumber, instance._renderable->m_descInstanceNumber);
        if (instance._renderable->m_modelIndex != (std::numeric_limits<unsigned>::max)())
        {
          auto& modelInfo = gm->GetModelInfo(instance._renderable->m_modelIndex);
          for (auto& mesh : modelInfo._meshInfoList)
            gm->DrawMesh(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame], mesh._indexSize, mesh._indicesOffset, mesh._meshOffset);
        }
      }
    }
  }

  Engine::core::EndMainRenderPass();


  Engine::core::EndRecord();
  Engine::core::SwapChainImageSubmitAndPresent();
}

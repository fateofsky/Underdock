#include "../pch.h"
#include "Engine.h"
#include "AppManager.h"

/*
  extern definition
*/
bool Engine::_quit = false;

HDC                           Engine::core::hdc;
HWND                          Engine::core::hwnd;
VkQueue                       Engine::core::vGraphicsQue;
VkQueue                       Engine::core::vPresentQue;
std::vector<VkPhysicalDevice> Engine::core::vPdVec;
VkInstance                    Engine::core::vInst;
VkSurfaceKHR                  Engine::core::vSurfKHR;
VkDevice                      Engine::core::vDevice;
VkViewport                    Engine::core::vViewport;
VkRect2D                      Engine::core::vScissor;
unsigned                      Engine::core::vGraphicQueueFamIndex = UINT32_MAX;
unsigned                      Engine::core::vPresentQueueFamIndex = UINT32_MAX;
unsigned                      Engine::core::windowsWidth = 0;
unsigned                      Engine::core::windowsHeight = 0;

VkSwapchainKHR                Engine::swpch::vSwapchain;
VkImage                       Engine::swpch::vDepthImg;
VkImageView                   Engine::swpch::vDepthImgView;
VkRenderPass                  Engine::swpch::vRenderpass;
std::vector<VkImageView>      Engine::swpch::vScImgViewVec;
std::vector<VkFramebuffer>    Engine::swpch::vFBVec;
std::vector<VkSemaphore>      Engine::swpch::vImgAcSemaVec;
std::vector<VkSemaphore>      Engine::swpch::vImgDoneSemaVec;
std::vector<VkFence>          Engine::swpch::vRenderFenceVec;
std::vector<VkCommandBuffer>  Engine::swpch::vCmdBuffVec;
VkCommandPool                 Engine::swpch::vCmdPool;
VkFormat                      Engine::swpch::vFmt;
VkFormat                      Engine::swpch::vDepthFmt;
unsigned                      Engine::swpch::vFBIndex = 0;
unsigned                      Engine::swpch::vCurFrame = 0;
unsigned                      Engine::swpch::vNumSCImgs = 0;
bool                          Engine::swpch::vHasDepth = false;


LRESULT CALLBACK Engine::core::WinProc(HWND Hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  InputManager* inputMgr = InputManager::GetInstance();

  switch (msg)
  {
  case WM_RBUTTONDOWN:
  case WM_LBUTTONDOWN:
  case WM_KEYDOWN:
    inputMgr->InputKeyValue__(wParam, _KEY_PRESSED);
    break;

  case WM_LBUTTONUP:
  case WM_RBUTTONUP:
  case WM_KEYUP:
    inputMgr->InputKeyValue__(wParam, _KEY_RELEASED);
    break;

  case WM_MBUTTONDOWN:
    inputMgr->InputKeyValue__(KEY_MBUTTON, _KEY_PRESSED);
    break;

  case WM_MBUTTONUP:
    inputMgr->InputKeyValue__(KEY_MBUTTON, _KEY_RELEASED);
    break;

  case WM_MOUSEMOVE:
    inputMgr->InputMouseMove__();
    break;

  case WM_MOUSEWHEEL:
    inputMgr->InputMouseWheel__(GET_WHEEL_DELTA_WPARAM(wParam) < 0 ? KEY_MOUSEWHEELUP : KEY_MOUSEWHEELDOWN);
    break;

  case WM_DESTROY:
  case WM_CLOSE:
    vkDeviceWaitIdle(Engine::core::vDevice);
    _quit = true;
    break;
  default:
    break;
  }
  return DefWindowProc(Hwnd, msg, wParam, lParam);
}


bool Engine::core::memory_type_from_properties(uint32_t typeBits, VkFlags requirements_mask, uint32_t * typeIndex)
{
  VkPhysicalDeviceMemoryProperties gpuMemProp;
  vkGetPhysicalDeviceMemoryProperties(Engine::core::vPdVec[0], &gpuMemProp);

  // Search memtypes to find first index with those properties
  for (uint32_t i = 0; i < gpuMemProp.memoryTypeCount; i++) {
    if ((typeBits & 1) == 1) {
      // Type is available, does it match user properties?
      if ((gpuMemProp.memoryTypes[i].propertyFlags & requirements_mask) == requirements_mask) {
        *typeIndex = i;
        return true;
      }
    }
    typeBits >>= 1;
  }
  // No memory types matched, return failure
  return false;
}


void Engine::core::SwapChainImageAcquire()
{
  // swap buffer with syncronization
#ifdef _DEBUG
  VkResult vkRes = vkWaitForFences(Engine::core::vDevice, 1, &Engine::swpch::vRenderFenceVec[Engine::swpch::vCurFrame], VK_TRUE, UINT64_MAX);
  assert(vkRes == VK_SUCCESS);
  vkRes = vkResetFences(Engine::core::vDevice, 1, &Engine::swpch::vRenderFenceVec[Engine::swpch::vCurFrame]);
  assert(vkRes == VK_SUCCESS);
  vkRes = vkAcquireNextImageKHR(Engine::core::vDevice, Engine::swpch::vSwapchain,
    UINT64_MAX,
    Engine::swpch::vImgAcSemaVec[Engine::swpch::vCurFrame],
    VK_NULL_HANDLE,
    &Engine::swpch::vFBIndex);
  assert(vkRes == VK_SUCCESS);
#else
  vkWaitForFences(Engine::core::vDevice, 1, &Engine::swpch::vRenderFenceVec[Engine::swpch::vCurFrame], VK_TRUE, UINT64_MAX);
  vkResetFences(Engine::core::vDevice, 1, &Engine::swpch::vRenderFenceVec[Engine::swpch::vCurFrame]);
  vkAcquireNextImageKHR(Engine::core::vDevice, Engine::swpch::vSwapchain,
    UINT64_MAX,
    Engine::swpch::vImgAcSemaVec[Engine::swpch::vCurFrame],
    VK_NULL_HANDLE,
    &Engine::swpch::vFBIndex);
#endif // _DEBUG
}


void Engine::core::SwapChainImageSubmitAndPresent()
{
  // Queue submission for execution
  VkPipelineStageFlags pipeStageFlag = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
  //const VkCommandBuffer tmpCmdBuf[] = { Engine::vCmdBuff };
  VkSubmitInfo submitInfo;
  submitInfo.pNext = NULL;
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.waitSemaphoreCount = 1;
  submitInfo.pWaitSemaphores = &Engine::swpch::vImgAcSemaVec[Engine::swpch::vCurFrame];
  submitInfo.pWaitDstStageMask = &pipeStageFlag;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame];
  submitInfo.signalSemaphoreCount = 1;
  submitInfo.pSignalSemaphores = &Engine::swpch::vImgDoneSemaVec[Engine::swpch::vCurFrame];


#ifdef _DEBUG
  VkResult vkRes = vkQueueSubmit(Engine::core::vGraphicsQue, 1, &submitInfo, Engine::swpch::vRenderFenceVec[Engine::swpch::vCurFrame]);
  assert(vkRes == VK_SUCCESS);
#else
  vkQueueSubmit(Engine::core::vGraphicsQue, 1, &submitInfo, Engine::swpch::vRenderFenceVec[Engine::swpch::vCurFrame]);
#endif // _DEBUG

  /*
    PRESENT
  */
  VkPresentInfoKHR presentInfo;
  presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
  presentInfo.pNext = NULL;
  presentInfo.swapchainCount = 1;
  presentInfo.pSwapchains = &Engine::swpch::vSwapchain;
  presentInfo.pImageIndices = &Engine::swpch::vFBIndex;
  presentInfo.waitSemaphoreCount = 1;
  presentInfo.pWaitSemaphores = &Engine::swpch::vImgDoneSemaVec[Engine::swpch::vCurFrame];
  presentInfo.pResults = NULL;

#ifdef _DEBUG
  vkRes = vkQueuePresentKHR(Engine::core::vPresentQue, &presentInfo);
  assert(vkRes == VK_SUCCESS);
#else
  vkQueuePresentKHR(Engine::core::vPresentQue, &presentInfo);
#endif

  // increment frame
  Engine::swpch::vCurFrame = (Engine::swpch::vCurFrame + 1) % Engine::swpch::vNumSCImgs;
}

void Engine::core::StartRecord()
{
  // reset command first
  vkResetCommandBuffer(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame], 0);

  VkCommandBufferBeginInfo cmdBufBegInfo = {};
  cmdBufBegInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  cmdBufBegInfo.pNext = NULL;
  cmdBufBegInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
  cmdBufBegInfo.pInheritanceInfo = NULL;
#ifdef _DEBUG
  VkResult vkRes = vkBeginCommandBuffer(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame], &cmdBufBegInfo);
  assert(vkRes == VK_SUCCESS);
#else
  vkBeginCommandBuffer(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame], &cmdBufBegInfo);
#endif // _DEBUG
}

void Engine::core::BeginMainRenderPass(VkRenderPass rp, VkFramebuffer fb, const float* clearColor,
  int primaryBuffer, const float& depthVal, const uint32_t& stencilVal)
{
  VkRenderPassBeginInfo rpBeg;
  VkClearValue clear_values[2];
  clear_values[0].color.float32[0] = clearColor[0];
  clear_values[0].color.float32[1] = clearColor[1];
  clear_values[0].color.float32[2] = clearColor[2];
  clear_values[0].color.float32[3] = clearColor[3];
  clear_values[1].depthStencil.depth = depthVal;
  clear_values[1].depthStencil.stencil = stencilVal;
  rpBeg.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
  rpBeg.pNext = NULL;
  rpBeg.renderPass = rp;
  rpBeg.framebuffer = fb;
  rpBeg.renderArea.offset.x = rpBeg.renderArea.offset.y = 0;  // can be draw time
  rpBeg.renderArea.extent.width = Engine::core::windowsWidth;       // can be draw time
  rpBeg.renderArea.extent.height = Engine::core::windowsHeight;     // can be draw time
  rpBeg.clearValueCount = 2;
  rpBeg.pClearValues = clear_values;
  vkCmdBeginRenderPass(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame], &rpBeg, 
                       primaryBuffer == 1 ? VK_SUBPASS_CONTENTS_INLINE : VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);
}

void Engine::core::EndMainRenderPass()
{
  vkCmdEndRenderPass(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame]);
}


void Engine::core::EndRecord()
{
#ifdef _DEBUG
  VkResult vkRes = vkEndCommandBuffer(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame]);
  assert(vkRes == VK_SUCCESS);
#else
  vkEndCommandBuffer(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame]);
#endif // _DEBUG
}

void Engine::core::SetViewportScissor(const unsigned& bufferIndex)
{
  vViewport.width = (float)windowsWidth;
  vViewport.height = (float)windowsHeight;
  vViewport.minDepth = (float)0.0f;
  vViewport.maxDepth = (float)1.0f;
  vViewport.x = 0;
  vViewport.y = 0;
  vkCmdSetViewport(Engine::swpch::vCmdBuffVec[bufferIndex], 0, 1, &vViewport);

  vScissor.extent.width = windowsWidth;
  vScissor.extent.height = windowsHeight;
  vScissor.offset.x = 0;
  vScissor.offset.y = 0;
  vkCmdSetScissor(Engine::swpch::vCmdBuffVec[bufferIndex], 0, 1, &vScissor);
}


bool Engine::Init(HINSTANCE instanceH)
{
  /*
    Windows Initialize
  */
  WNDCLASSEX wcex;
  wcex.cbSize = sizeof(WNDCLASSEXA);
  wcex.style = CS_HREDRAW | CS_VREDRAW;
  wcex.lpfnWndProc = Engine::core::WinProc;
  wcex.cbClsExtra = 0;
  wcex.cbWndExtra = 0;
  wcex.hInstance = instanceH;
  wcex.hIcon = (HICON)LoadImage(NULL, "data/Engine/ud.ico",
    IMAGE_ICON, 16, 16, LR_LOADFROMFILE | LR_LOADTRANSPARENT | LR_SHARED);
  wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
  wcex.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
  wcex.lpszMenuName = APP_TITLE;
  wcex.lpszClassName = APP_TITLE;
  wcex.hIconSm = (HICON)LoadImage(NULL, "data/Engine/c.ico",
    IMAGE_ICON, 16, 16, LR_LOADFROMFILE | LR_LOADTRANSPARENT | LR_SHARED);

  if (!RegisterClassEx(&wcex))
  {
    MessageBox(NULL, "Register class fail!", "Error", NULL);
    return false;
  }

  //int nStyle = WS_POPUP | WS_OVERLAPPED | WS_VISIBLE;
  int nStyle = WS_OVERLAPPED | WS_SYSMENU | WS_VISIBLE | WS_CAPTION | WS_MINIMIZEBOX | WS_SIZEBOX | WS_MAXIMIZEBOX;
  Engine::core::hwnd = CreateWindow(wcex.lpszClassName, wcex.lpszMenuName,
    nStyle, 0, 0, DEFAULT_SCREEN_WIDTH, DEFAULT_SCREEN_HEIGHT, NULL, NULL, instanceH, NULL);

  // Pixel format
  Engine::core::hdc = GetDC(Engine::core::hwnd);
  PIXELFORMATDESCRIPTOR pfd;
  pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
  pfd.nVersion = 1;
  pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
  pfd.iLayerType = PFD_MAIN_PLANE;
  pfd.iPixelType = LPD_TYPE_RGBA;
  pfd.cDepthBits = 16;
  pfd.cColorBits = 32;

  if (!SetPixelFormat(Engine::core::hdc, ChoosePixelFormat(Engine::core::hdc, &pfd), &pfd))
    MessageBox(NULL, "Set pixel fail!", "Error", NULL);

  ShowWindow(Engine::core::hwnd, SW_SHOW);

  /*
    Vulkan Initialize
  */
#ifdef _DEBUG
  vkDebugger::InitValidationLayer();
#endif // _DEBUG


  /*
      Create / initialize vulkan app and instance
  */
  VkApplicationInfo    appInfo;   // application info
  VkInstanceCreateInfo instInfo;  // instance info
#if defined _DEBUG
  const char* extenName[3]{ VK_KHR_SURFACE_EXTENSION_NAME, VK_KHR_WIN32_SURFACE_EXTENSION_NAME, VK_EXT_DEBUG_UTILS_EXTENSION_NAME };
#else
  const char* extenName[2]{ VK_KHR_SURFACE_EXTENSION_NAME, VK_KHR_WIN32_SURFACE_EXTENSION_NAME };
#endif
  appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
  appInfo.pNext = nullptr;
  appInfo.pApplicationName = APP_TITLE;
  appInfo.applicationVersion = APP_VERSION;
  appInfo.pEngineName = APP_TITLE;
  appInfo.engineVersion = APP_VERSION;
  appInfo.apiVersion = VK_MAKE_VERSION(VK_APP_MAJOR, VK_APP_MID, VK_APP_MINOR);
  instInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
  instInfo.pNext = nullptr;
  instInfo.flags = 0;
  instInfo.pApplicationInfo = &appInfo;
  instInfo.ppEnabledExtensionNames = extenName;

#if defined _DEBUG
  instInfo.enabledExtensionCount = 3;
  instInfo.enabledLayerCount = (unsigned)vkDebugger::validationLayer.size();
  instInfo.ppEnabledLayerNames = vkDebugger::validationLayer.data();
#else
  instInfo.enabledExtensionCount = 2;
  instInfo.enabledLayerCount = 0;
  instInfo.ppEnabledLayerNames = NULL;
#endif

  VkResult vkRes = vkCreateInstance(&instInfo, NULL, &Engine::core::vInst);
  assert(vkRes == VK_SUCCESS);
  std::cout << "Vulkan instance initialized...\n";

#if defined _DEBUG
  vkDebugger::InitDebugUtils();
#endif


  /*
      Get physical device
  */
  unsigned gpuCnt = 0;
  vkRes = vkEnumeratePhysicalDevices(Engine::core::vInst, &gpuCnt, NULL);
  assert(gpuCnt != 0);
  Engine::core::vPdVec.resize(gpuCnt);
  vkRes = vkEnumeratePhysicalDevices(Engine::core::vInst, &gpuCnt, Engine::core::vPdVec.data());
  assert(vkRes == VK_SUCCESS);
  std::cout << "Vulkan physical device initialized...\n";

  /*
      Get physical device queue
  */
  std::vector<VkQueueFamilyProperties> qFamVec;
  unsigned familyQCount = 0;
  vkGetPhysicalDeviceQueueFamilyProperties(Engine::core::vPdVec[0], &familyQCount, NULL);
  if (familyQCount == 0) {
    std::cout << "No device queue family found...\n";
    return false;
  }
  qFamVec.resize(familyQCount);
  vkGetPhysicalDeviceQueueFamilyProperties(Engine::core::vPdVec[0], &familyQCount, qFamVec.data());


  /*
      Create win32 surface extension for swapchains
  */
  VkWin32SurfaceCreateInfoKHR sfInfo;
  sfInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
  sfInfo.pNext = NULL;
  sfInfo.hinstance = instanceH;
  sfInfo.hwnd = Engine::core::hwnd;
  sfInfo.flags = 0;
  vkRes = vkCreateWin32SurfaceKHR(Engine::core::vInst, &sfInfo, NULL, &Engine::core::vSurfKHR);
  assert(vkRes == VK_SUCCESS);
  std::cout << "Vulkan win32 surface initialized...\n";


  /*
      Search and assign graphic / present queue
  */
  // Iterate over each queue to learn whether it supports presenting:
  std::vector<VkBool32> pPresentVec(familyQCount, VK_FALSE);
  for (unsigned int i = 0; i < familyQCount; ++i)
    vkGetPhysicalDeviceSurfaceSupportKHR(Engine::core::vPdVec[0], i, Engine::core::vSurfKHR, &pPresentVec[i]);

  for (unsigned int i = 0; i < familyQCount; ++i) {
    if (qFamVec[i].queueCount > 0 && (qFamVec[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) == 0) continue;
    Engine::core::vGraphicQueueFamIndex = Engine::core::vGraphicQueueFamIndex == UINT32_MAX ? i : Engine::core::vGraphicQueueFamIndex;

    // basically it an escape condition when present queue is found
    if (pPresentVec[i] == VK_TRUE) {
      Engine::core::vGraphicQueueFamIndex = Engine::core::vPresentQueueFamIndex = i;
      break;
    }
  }

  // If didn't find a queue that supports both graphics and present, then
  // find a separate present queue. (Hopefully this will not get called)
  if (Engine::core::vPresentQueueFamIndex == UINT32_MAX) {
    for (unsigned i = 0; i < familyQCount; ++i) {
      if (pPresentVec[i] == VK_TRUE) {
        Engine::core::vPresentQueueFamIndex = i;
        break;
      }
    }
  }
  assert(Engine::core::vGraphicQueueFamIndex != UINT32_MAX);
  assert(Engine::core::vPresentQueueFamIndex != UINT32_MAX);
  pPresentVec.clear();
  qFamVec.clear();


  /*
      Initialize / create logical device
  */
  // Enable device features
  VkPhysicalDeviceFeatures devFeat{ VK_FALSE };
  vkGetPhysicalDeviceFeatures(Engine::core::vPdVec[0], &devFeat);
  devFeat.samplerAnisotropy = VK_TRUE;

  std::cout << "Vulkan enabled sampler anisotropy...\n";
  VkDeviceQueueCreateInfo qInfo;
  float qPriorities = 0.0f;
  qInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
  qInfo.pNext = NULL;
  qInfo.queueCount = 1;
  qInfo.pQueuePriorities = &qPriorities;
  qInfo.queueFamilyIndex = Engine::core::vGraphicQueueFamIndex;
  qInfo.flags = 0;

  VkDeviceCreateInfo devInfo;
  const char* extentionNames[2] = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };
  devInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
  devInfo.pNext = NULL;
  devInfo.enabledExtensionCount = 1;

#ifdef _DEBUG
  devInfo.enabledLayerCount = (unsigned)vkDebugger::validationLayer.size();
  devInfo.ppEnabledLayerNames = vkDebugger::validationLayer.data();
#else
  devInfo.enabledLayerCount = 0;
  devInfo.ppEnabledLayerNames = NULL;
#endif // _DEBUG

  devInfo.queueCreateInfoCount = 1;
  devInfo.pQueueCreateInfos = &qInfo;
  devInfo.ppEnabledExtensionNames = extentionNames;
  devInfo.pEnabledFeatures = &devFeat;
  devInfo.flags = 0;
  vkRes = vkCreateDevice(Engine::core::vPdVec[0], &devInfo, NULL, &Engine::core::vDevice);
  assert(vkRes == VK_SUCCESS);
  std::cout << "Vulkan device created...\n";

  // Get device queue
  vkGetDeviceQueue(Engine::core::vDevice, Engine::core::vGraphicQueueFamIndex, 0, &Engine::core::vGraphicsQue);
  if (Engine::core::vGraphicQueueFamIndex == Engine::core::vPresentQueueFamIndex)
    Engine::core::vPresentQue = Engine::core::vGraphicsQue;
  else vkGetDeviceQueue(Engine::core::vDevice, Engine::core::vGraphicQueueFamIndex, 0, &Engine::core::vPresentQue);

  assert(vkRes == VK_SUCCESS);
  std::cout << "Vulkan logical device initialized...\n";

  if (!Engine::swpch::InitSwapchains(false))
    return false;

  return true;
}


void Engine::Run()
{
  AppManager* appMgr = AppManager::GetInstance();
  while (true)
  {
    MSG msg;
    while (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
    {
#pragma warning(suppress: 6387) //param 2 being 0 does not adhere to spec, but its optional
      if (!TranslateAccelerator(msg.hwnd, NULL, &msg))
      {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    }
    if (_quit) break;

    // Main update loop
    appMgr->Run();

    InputManager::GetInstance()->InputRefresh__();
  }

  appMgr->Destroy();
}


void Engine::DestroyEngine()
{
  vkDeviceWaitIdle(Engine::core::vDevice);

#ifdef _DEBUG
  vkDebugger::DestroyDebugUtils();
#endif // _DEBUG

  Engine::swpch::DestroySwapchains();

  // remove engine
  vkDestroyDevice(Engine::core::vDevice, NULL);
  vkDestroyInstance(Engine::core::vInst, NULL);
}

bool Engine::swpch::InitSwapchains(bool initDepth)
{
  vHasDepth = initDepth;
  /*
      Create / initialize swapchain
  */
  // Get the list of VkFormats that are supported:
  uint32_t formatCount;
  VkResult vkRes = vkGetPhysicalDeviceSurfaceFormatsKHR(Engine::core::vPdVec[0], Engine::core::vSurfKHR, &formatCount, NULL);
  assert(vkRes == VK_SUCCESS);
  std::vector<VkSurfaceFormatKHR> surfFormatsVec(formatCount);
  vkRes = vkGetPhysicalDeviceSurfaceFormatsKHR(Engine::core::vPdVec[0], Engine::core::vSurfKHR, &formatCount, &surfFormatsVec[0]);
  assert(vkRes == VK_SUCCESS);

  // If the format list includes just one entry of VK_FORMAT_UNDEFINED,
  // the surface has no preferred format.  Otherwise, at least one
  // supported format will be returned.

  if (formatCount == 1 && surfFormatsVec[0].format == VK_FORMAT_UNDEFINED) {
    vFmt = VK_FORMAT_B8G8R8A8_UNORM;
  }
  else {
    assert(formatCount >= 1);
    vFmt = surfFormatsVec[0].format;
  }

  // Enquire surface capabilities
  VkSurfaceCapabilitiesKHR surfCap;
  vkRes = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(Engine::core::vPdVec[0], Engine::core::vSurfKHR, &surfCap);
  assert(vkRes == VK_SUCCESS);
  vNumSCImgs = surfCap.minImageCount + 1;

  // Enquire swapchain info and extend
  VkExtent2D swapchainExtent;
  VkPresentModeKHR swapchainPresMode = VK_PRESENT_MODE_FIFO_KHR;
  //VkPresentModeKHR swapchainPresMode = VK_PRESENT_MODE_MAILBOX_KHR;


  if (surfCap.currentExtent.width != 0xFFFFFFFF)
    swapchainExtent = surfCap.currentExtent;
  else {
    // adjustment if go overbound
    swapchainExtent.width = (unsigned)DEFAULT_SCREEN_WIDTH < surfCap.minImageExtent.width ? surfCap.minImageExtent.width :
      (unsigned)DEFAULT_SCREEN_WIDTH > surfCap.maxImageExtent.width ? surfCap.maxImageExtent.width : (unsigned)DEFAULT_SCREEN_WIDTH;

    swapchainExtent.height = (unsigned)DEFAULT_SCREEN_HEIGHT < surfCap.minImageExtent.height ? surfCap.minImageExtent.height :
      (unsigned)DEFAULT_SCREEN_HEIGHT > surfCap.maxImageExtent.height ? surfCap.maxImageExtent.height : (unsigned)DEFAULT_SCREEN_HEIGHT;
  }
  Engine::core::windowsWidth = swapchainExtent.width;
  Engine::core::windowsHeight = swapchainExtent.height;

  VkSurfaceTransformFlagBitsKHR preTrans = surfCap.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR ?
    VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR : surfCap.currentTransform;

  // find supported composite alpha mode
  VkCompositeAlphaFlagBitsKHR compAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
  VkCompositeAlphaFlagBitsKHR compAlphaFlags[4]{ VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
                                                 VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR,
                                                 VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR,
                                                 VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR };
  for (unsigned i = 0; i < 4; ++i) {
    if (surfCap.supportedCompositeAlpha & compAlphaFlags[i]) {
      compAlpha = compAlphaFlags[i];
      break;
    }
  }

  // swapchain info
  VkSwapchainCreateInfoKHR scinfo;
  scinfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
  scinfo.pNext = NULL;
  scinfo.flags = 0;
  scinfo.surface = Engine::core::vSurfKHR;
  scinfo.minImageCount = vNumSCImgs;
  scinfo.imageFormat = vFmt;
  scinfo.imageExtent = swapchainExtent;
  scinfo.preTransform = preTrans;
  scinfo.compositeAlpha = compAlpha;
  scinfo.imageArrayLayers = 1;
  scinfo.presentMode = swapchainPresMode;
  scinfo.oldSwapchain = NULL;
  scinfo.clipped = true;
  scinfo.imageColorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
  scinfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
  if (Engine::core::vGraphicQueueFamIndex != Engine::core::vPresentQueueFamIndex) {
    unsigned int qFamInd[2]{ Engine::core::vGraphicQueueFamIndex, Engine::core::vPresentQueueFamIndex };
    scinfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
    scinfo.queueFamilyIndexCount = 2;
    scinfo.pQueueFamilyIndices = qFamInd;
  }
  else {
    scinfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    scinfo.queueFamilyIndexCount = 0;
    scinfo.pQueueFamilyIndices = NULL;
  }
  vkRes = vkCreateSwapchainKHR(Engine::core::vDevice, &scinfo, NULL, &vSwapchain);
  assert(vkRes == VK_SUCCESS);
  std::cout << "Vulkan swapchain initialized...\n";

  // Create imageviews from the instantiated swapchain
  unsigned swapchainImgCount = 0;
  vkRes = vkGetSwapchainImagesKHR(Engine::core::vDevice, vSwapchain, &swapchainImgCount, NULL);
  assert(vkRes == VK_SUCCESS);
  std::vector<VkImage> ScImgVec(swapchainImgCount);      // list of vulkan color images, OWNER IS SWAPCHAIN
  vScImgViewVec.resize(swapchainImgCount);
  vkRes = vkGetSwapchainImagesKHR(Engine::core::vDevice, vSwapchain, &swapchainImgCount, ScImgVec.data());
  assert(vkRes == VK_SUCCESS);

  // Assign vkimage into image view
  for (unsigned i = 0; i < swapchainImgCount; ++i)
  {
    VkImageViewCreateInfo colImgViewInfo;
    colImgViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    colImgViewInfo.pNext = NULL;
    colImgViewInfo.flags = 0;
    colImgViewInfo.image = ScImgVec[i];
    colImgViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    colImgViewInfo.format = vFmt;
    colImgViewInfo.components.r = VK_COMPONENT_SWIZZLE_R;
    colImgViewInfo.components.g = VK_COMPONENT_SWIZZLE_G;
    colImgViewInfo.components.b = VK_COMPONENT_SWIZZLE_B;
    colImgViewInfo.components.a = VK_COMPONENT_SWIZZLE_A;
    colImgViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    colImgViewInfo.subresourceRange.baseMipLevel = 0;
    colImgViewInfo.subresourceRange.levelCount = 1;
    colImgViewInfo.subresourceRange.baseArrayLayer = 0;
    colImgViewInfo.subresourceRange.layerCount = 1;

    vkRes = vkCreateImageView(Engine::core::vDevice, &colImgViewInfo, NULL, &vScImgViewVec[i]);
    assert(vkRes == VK_SUCCESS);
  }
  std::cout << "Vulkan swapchain image view initialized...\n";
  ScImgVec.clear();


  /*
      Command buffer pool creation & allocation
  */
  VkCommandPoolCreateFlags cmdPoolCreateFlag = VkCommandPoolCreateFlagBits::VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
  VkCommandPoolCreateInfo cmdPoolInfo;
  cmdPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
  cmdPoolInfo.pNext = NULL;
  cmdPoolInfo.queueFamilyIndex = Engine::core::vGraphicQueueFamIndex;
  cmdPoolInfo.flags = cmdPoolCreateFlag;
  vkRes = vkCreateCommandPool(Engine::core::vDevice, &cmdPoolInfo, NULL, &vCmdPool);
  assert(vkRes == VK_SUCCESS);
  std::cout << "Vulkan command pool initialized...\n";

  VkCommandBufferAllocateInfo cmdAInfo;
  cmdAInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  cmdAInfo.commandPool = vCmdPool;
  cmdAInfo.pNext = NULL;
  cmdAInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  cmdAInfo.commandBufferCount = 1;
  vCmdBuffVec.resize(vNumSCImgs);
  for (unsigned i = 0; i < vNumSCImgs; ++i)
  {
    vkRes = vkAllocateCommandBuffers(Engine::core::vDevice, &cmdAInfo, &vCmdBuffVec[i]);
    assert(vkRes == VK_SUCCESS);
  }
  std::cout << "Vulkan command buffer initialized...\n";


  /*
    - DEPTH
  */
  // Create depth buffer create info
  vDepthFmt = VK_FORMAT_D16_UNORM;
  if (vHasDepth)
  {
    VkImageCreateInfo depthImgInfo;
    VkFormatProperties props;
    vkGetPhysicalDeviceFormatProperties(Engine::core::vPdVec[0], vDepthFmt, &props);
    if (props.linearTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT)
      depthImgInfo.tiling = VK_IMAGE_TILING_LINEAR;
    else if (props.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT)
      depthImgInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    else {
      /* Try other depth formats? */
      std::cout << "VK_FORMAT_D16_UNORM Unsupported.\n";
      exit(-1);
    }
    depthImgInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    depthImgInfo.pNext = NULL;
    depthImgInfo.imageType = VK_IMAGE_TYPE_2D;
    depthImgInfo.format = vDepthFmt;
    depthImgInfo.extent.width = Engine::core::windowsWidth;
    depthImgInfo.extent.height = Engine::core::windowsHeight;
    depthImgInfo.extent.depth = 1;
    depthImgInfo.mipLevels = 1;
    depthImgInfo.arrayLayers = 1;
    depthImgInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    depthImgInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depthImgInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
    depthImgInfo.queueFamilyIndexCount = 0;
    depthImgInfo.pQueueFamilyIndices = NULL;
    depthImgInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    depthImgInfo.flags = 0;

    // depth buffer allocation info
    VkMemoryAllocateInfo mem_allocInfo = {};
    mem_allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    mem_allocInfo.pNext = NULL;
    mem_allocInfo.allocationSize = 0;
    mem_allocInfo.memoryTypeIndex = 0;

    // depth buffer image create info
    VkImageViewCreateInfo DepthView_info = {};
    DepthView_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    DepthView_info.pNext = NULL;
    DepthView_info.image = VK_NULL_HANDLE;
    DepthView_info.format = vDepthFmt;
    DepthView_info.components.r = VK_COMPONENT_SWIZZLE_R;
    DepthView_info.components.g = VK_COMPONENT_SWIZZLE_G;
    DepthView_info.components.b = VK_COMPONENT_SWIZZLE_B;
    DepthView_info.components.a = VK_COMPONENT_SWIZZLE_A;
    DepthView_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
    DepthView_info.subresourceRange.baseMipLevel = 0;
    DepthView_info.subresourceRange.levelCount = 1;
    DepthView_info.subresourceRange.baseArrayLayer = 0;
    DepthView_info.subresourceRange.layerCount = 1;
    DepthView_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
    DepthView_info.flags = 0;

    // get depth buffer memory requirement
    VkMemoryRequirements Depth_mem_reqs;
    VkDeviceMemory depDevMem;

    // Create image
    vkRes = vkCreateImage(Engine::core::vDevice, &depthImgInfo, NULL, &vDepthImg);
    assert(vkRes == VK_SUCCESS);

    // create depth buffer memory info and size
    vkGetImageMemoryRequirements(Engine::core::vDevice, vDepthImg, &Depth_mem_reqs);
    mem_allocInfo.allocationSize = Depth_mem_reqs.size;

    // Search memtypes to find first index with those properties
#ifdef _DEBUG
    bool memTypeFound =
      Engine::core::memory_type_from_properties(Depth_mem_reqs.memoryTypeBits,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        &mem_allocInfo.memoryTypeIndex);
    assert(memTypeFound);
#else
    Engine::core::memory_type_from_properties(Depth_mem_reqs.memoryTypeBits,
      VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
      &mem_allocInfo.memoryTypeIndex);
#endif // _DEBUG


    // allocate and bind depth buffer memory
    vkRes = vkAllocateMemory(Engine::core::vDevice, &mem_allocInfo, NULL, &depDevMem);
    assert(vkRes == VK_SUCCESS);
    vkRes = vkBindImageMemory(Engine::core::vDevice, vDepthImg, depDevMem, 0);
    assert(vkRes == VK_SUCCESS);
    DepthView_info.image = vDepthImg;
    vkRes = vkCreateImageView(Engine::core::vDevice, &DepthView_info, NULL, &vDepthImgView);
    assert(vkRes == VK_SUCCESS);
    std::cout << "Vulkan depth buffer initialized...\n";
  }

  /*
    - RENDER PASS
  */
  // Color attachment
  VkAttachmentDescription attDesc[2];
  attDesc[0].format = vFmt;
  attDesc[0].samples = VK_SAMPLE_COUNT_1_BIT;
  attDesc[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
  attDesc[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
  attDesc[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
  attDesc[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
  attDesc[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  attDesc[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
  attDesc[0].flags = 0;

  // Depth attachment
  if (vHasDepth)
  {
    attDesc[1].format = vDepthFmt;
    attDesc[1].samples = VK_SAMPLE_COUNT_1_BIT;
    attDesc[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attDesc[1].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attDesc[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attDesc[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attDesc[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    attDesc[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    attDesc[1].flags = 0;
  }

  // create subpass
  VkAttachmentReference colRef;
  colRef.attachment = 0;
  colRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
  VkAttachmentReference depRef;
  depRef.attachment = 1;
  depRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

  VkSubpassDescription subpassDesc;
  subpassDesc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
  subpassDesc.flags = 0;
  subpassDesc.inputAttachmentCount = 0;
  subpassDesc.pInputAttachments = NULL;
  subpassDesc.colorAttachmentCount = 1;
  subpassDesc.pColorAttachments = &colRef;
  subpassDesc.pResolveAttachments = NULL;
  subpassDesc.pDepthStencilAttachment = vHasDepth ? &depRef : NULL;
  subpassDesc.preserveAttachmentCount = 0;
  subpassDesc.pPreserveAttachments = NULL;

  // render pass create info and creation
  VkRenderPassCreateInfo rpInfo;
  rpInfo.flags = 0;
  rpInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
  rpInfo.pNext = NULL;
  rpInfo.attachmentCount = vHasDepth ? 2 : 1;
  rpInfo.pAttachments = attDesc;
  rpInfo.subpassCount = 1;
  rpInfo.pSubpasses = &subpassDesc;
  rpInfo.dependencyCount = 0;
  rpInfo.pDependencies = NULL;
  vkRes = vkCreateRenderPass(Engine::core::vDevice, &rpInfo, NULL, &vRenderpass);
  assert(vkRes == VK_SUCCESS);
  std::cout << "Vulkan renderpass created...\n";


  /*
    - FRAMEBUFFERS
  */
  // number of framebuffers tied to number of swap chain images
  vFBVec.resize(vNumSCImgs);
  for (unsigned i = 0; i < vNumSCImgs; ++i)
  {
    VkImageView fbAttachments[2]{ vScImgViewVec[i], vDepthImgView };
    VkFramebufferCreateInfo fbInfo;
    fbInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    fbInfo.pNext = NULL;
    fbInfo.renderPass = vRenderpass;
    fbInfo.attachmentCount = vHasDepth ? 2 : 1;
    fbInfo.pAttachments = fbAttachments;
    fbInfo.width = Engine::core::windowsWidth;
    fbInfo.height = Engine::core::windowsHeight;
    fbInfo.layers = 1;
    fbInfo.flags = 0;
    vkRes = vkCreateFramebuffer(Engine::core::vDevice, &fbInfo, NULL, &vFBVec[i]);
    assert(vkRes == VK_SUCCESS);
  }
  std::cout << "Vulkan Framebuffer created...\n";


  /*
    - FENCE AND SEMAPHORE FOR LOOPS
  */
  VkFenceCreateInfo fenceInfo;
  fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  fenceInfo.pNext = NULL;
  fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
  VkSemaphoreCreateInfo iasInfo;
  iasInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
  iasInfo.pNext = NULL;
  iasInfo.flags = 0;
  vImgAcSemaVec.resize(vNumSCImgs);
  vImgDoneSemaVec.resize(vNumSCImgs);
  vRenderFenceVec.resize(vNumSCImgs);

  for (unsigned i = 0; i < vNumSCImgs; ++i)
  {
    vkRes = vkCreateSemaphore(Engine::core::vDevice, &iasInfo, NULL, &vImgAcSemaVec[i]);
    assert(vkRes == VK_SUCCESS);
    vkRes = vkCreateSemaphore(Engine::core::vDevice, &iasInfo, NULL, &vImgDoneSemaVec[i]);
    assert(vkRes == VK_SUCCESS);
    vkCreateFence(Engine::core::vDevice, &fenceInfo, NULL, &vRenderFenceVec[i]);
    assert(vkRes == VK_SUCCESS);
  }

  return true;
}


void Engine::swpch::DestroySwapchains()
{
  // remove renderpass
  vkDestroyRenderPass(Engine::core::vDevice, vRenderpass, NULL);

  // remove framebuffers
  for (unsigned i = 0; i < vFBVec.size(); ++i)
    vkDestroyFramebuffer(Engine::core::vDevice, vFBVec[i], NULL);

  // remove command buffer and pool
  for (unsigned i = 0; i < vNumSCImgs; ++i)
    vkFreeCommandBuffers(Engine::core::vDevice, vCmdPool, 1, &vCmdBuffVec[i]);
  vkDestroyCommandPool(Engine::core::vDevice, vCmdPool, NULL);

  // remove swap chain images / depth and swap chain object itself
  if (vHasDepth)
  {
    vkDestroyImageView(Engine::core::vDevice, vDepthImgView, NULL);
    vkDestroyImage(Engine::core::vDevice, vDepthImg, NULL);
  }
  for (unsigned i = 0; i < vScImgViewVec.size(); i++)
    vkDestroyImageView(Engine::core::vDevice, vScImgViewVec[i], NULL);
  vkDestroySwapchainKHR(Engine::core::vDevice, vSwapchain, NULL);

  // destroy fence and semaphore
  for (unsigned i = 0; i < vNumSCImgs; ++i)
  {
    vkDestroyFence(Engine::core::vDevice, vRenderFenceVec[i], NULL);
    vkDestroySemaphore(Engine::core::vDevice, vImgAcSemaVec[i], NULL);
    vkDestroySemaphore(Engine::core::vDevice, vImgDoneSemaVec[i], NULL);
  }
}


VkCommandBuffer Engine::utils::beginSingleTimeCommands()
{
  VkCommandBufferAllocateInfo allocInfo;
  allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  allocInfo.commandPool = Engine::swpch::vCmdPool;
  allocInfo.commandBufferCount = 1;
  allocInfo.pNext = NULL;

  VkCommandBuffer commandBuffer;
#ifdef _DEBUG
  VkResult vkRes = vkAllocateCommandBuffers(Engine::core::vDevice, &allocInfo, &commandBuffer);
  assert(vkRes == VK_SUCCESS);
#else
  vkAllocateCommandBuffers(Engine::core::vDevice, &allocInfo, &commandBuffer);
#endif // _DEBUG


  VkCommandBufferBeginInfo beginInfo = {};
  beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
  beginInfo.pNext = NULL;
  beginInfo.pInheritanceInfo = NULL;

#ifdef _DEBUG
  vkRes = vkBeginCommandBuffer(commandBuffer, &beginInfo);
  assert(vkRes == VK_SUCCESS);
#else
  vkBeginCommandBuffer(commandBuffer, &beginInfo);
#endif // _DEBUG

  return commandBuffer;
}

void Engine::utils::endSingleTimeCommands(const VkCommandBuffer & commandBuffer)
{
  vkEndCommandBuffer(commandBuffer);

  VkSubmitInfo submitInfo;
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &commandBuffer;
  submitInfo.pNext = NULL;
  submitInfo.waitSemaphoreCount = submitInfo.signalSemaphoreCount = 0;
  submitInfo.pWaitSemaphores = NULL;
  submitInfo.pWaitDstStageMask = 0;
  submitInfo.pSignalSemaphores = NULL;

  vkQueueSubmit(Engine::core::vGraphicsQue, 1, &submitInfo, VK_NULL_HANDLE);
  vkQueueWaitIdle(Engine::core::vGraphicsQue);

  vkFreeCommandBuffers(Engine::core::vDevice, Engine::swpch::vCmdPool, 1, &commandBuffer);
}

void Engine::utils::copyVkBuffers(VkCommandBuffer cmdBuf, VkBuffer srcBuf, VkBuffer desBuf, VkDeviceSize bufSize)
{
  VkBufferCopy copyRegion = {};
  copyRegion.size = bufSize;
  vkCmdCopyBuffer(cmdBuf, srcBuf, desBuf, 1, &copyRegion);
}

void Engine::utils::copyVkBufferToImage(VkCommandBuffer cmdBuf, VkBuffer srcBuf, VkImage desImg, VkImageAspectFlags flg, unsigned width, unsigned height)
{
  // set image copy info
  VkBufferImageCopy imgCpyReg;
  imgCpyReg.bufferOffset = 0;
  imgCpyReg.bufferRowLength = 0;
  imgCpyReg.bufferImageHeight = 0;
  imgCpyReg.imageSubresource.aspectMask = flg;
  imgCpyReg.imageSubresource.mipLevel = 0;
  imgCpyReg.imageSubresource.baseArrayLayer = 0;
  imgCpyReg.imageSubresource.layerCount = 1;
  imgCpyReg.imageOffset = { 0, 0, 0 };
  imgCpyReg.imageExtent.width = width;
  imgCpyReg.imageExtent.height = height;
  imgCpyReg.imageExtent.depth = 1;

  // copy command
  vkCmdCopyBufferToImage(cmdBuf, srcBuf, desImg, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &imgCpyReg);
}

void Engine::utils::ImageTransition_Undef_Transfer(VkCommandBuffer cmdBuf, VkImage image, VkImageAspectFlags flgs)
{
  // Image information
  VkImageMemoryBarrier imgBar;
  imgBar.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
  imgBar.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  imgBar.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
  imgBar.srcQueueFamilyIndex = imgBar.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  imgBar.image = image;
  imgBar.subresourceRange.aspectMask = flgs;
  imgBar.subresourceRange.baseMipLevel = 0;
  imgBar.subresourceRange.levelCount = 1;
  imgBar.subresourceRange.baseArrayLayer = 0;
  imgBar.subresourceRange.layerCount = 1;
  imgBar.pNext = NULL;
  imgBar.srcAccessMask = 0;
  imgBar.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

  // pipeline stage
  VkPipelineStageFlags srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
  VkPipelineStageFlags desStage = VK_PIPELINE_STAGE_TRANSFER_BIT;

  vkCmdPipelineBarrier(cmdBuf, srcStage, desStage, 0, 0, NULL, 0, NULL, 1, &imgBar);
}

void Engine::utils::ImageTransition_Transfer_FragRead(VkCommandBuffer cmdBuf, VkImage image, VkImageAspectFlags flgs)
{
  // Image information
  VkImageMemoryBarrier imgBar;
  imgBar.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
  imgBar.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
  imgBar.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
  imgBar.srcQueueFamilyIndex = imgBar.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  imgBar.image = image;
  imgBar.subresourceRange.aspectMask = flgs;
  imgBar.subresourceRange.baseMipLevel = 0;
  imgBar.subresourceRange.levelCount = 1;
  imgBar.subresourceRange.baseArrayLayer = 0;
  imgBar.subresourceRange.layerCount = 1;
  imgBar.pNext = NULL;
  imgBar.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
  imgBar.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

  // pipeline stage
  VkPipelineStageFlags srcStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
  VkPipelineStageFlags desStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;

  vkCmdPipelineBarrier(cmdBuf, srcStage, desStage, 0, 0, NULL, 0, NULL, 1, &imgBar);
}

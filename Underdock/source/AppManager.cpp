#include "pch.h"
#include "AppManager.h"
#include "DemoLevel.h"
#include "DemoLevel2.h"

AppManager* AppManager::m_instance = nullptr;
AppManager::AppManager()
  : m_state(AppManager::GAMELOOP_STATE::LOAD), m_appTime(0.0f)
{ }

AppManager* AppManager::GetInstance()
{
  return m_instance == nullptr ? m_instance = new AppManager() : m_instance;
}

void AppManager::Destroy()
{
  GraphicsManager::Destroy();
  delete m_instance;
}

void AppManager::RegisterEntityForInit(ComponentBase * compEntity)
{
  m_entitiesInitVec.push_back(compEntity);
}

void AppManager::RegisterEntityForUpdate(ComponentBase* compEntity)
{
  m_entitiesUpdateVec.push_back(compEntity);
}

//void AppManager::RegisterPreRenderEntity(ComponentBase* compEntity)
//{
//  m_entitiesPreRenderVec.push_back(compEntity);
//}

void AppManager::RegisterMainCamera(GameobjectHandle& cameraObj)
{
  // MOVE SEMANTICS
  m_mainCameraHandle.MoveComponentHandleOver(cameraObj.GetData()->AddComponent_hc<CameraComponent>());
}


void AppManager::Run()
{
  GraphicsManager* gm = GraphicsManager::GetInstance();

  if (m_state == GAMELOOP_STATE::LOAD)
  {
    gm->LoadResources();
    DemoLevel2::LoadLevel();
    m_state = GAMELOOP_STATE::INIT;
  }
  else if (m_state == GAMELOOP_STATE::INIT)
  {
    // Game instance init / post init
    for (auto& entity : m_entitiesInitVec)
      entity->Init();

    DemoLevel2::InitLevel();

    // Graphics Manager init
    gm->Init();

    for (auto& entity : m_entitiesInitVec)
      entity->PostInit();

    m_state = GAMELOOP_STATE::UPDATE;
  }
  else
  {
    // object update
    for (auto& entity : m_entitiesUpdateVec)
      entity->Update();

    // level update
    DemoLevel2::UpdateLevel();

    // Render
    DemoLevel2::RenderLevel();

    // post update
    for (auto& entity : m_entitiesUpdateVec)
      entity->PostUpdate();
  }

  // TODO: FIX APP TIME
  m_appTime += 0.016666f;
}

const float& AppManager::GetAppTime()
{
  return m_appTime;
}

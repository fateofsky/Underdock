#pragma once
#include "OGLBaseType.h"
#include "UniformResource.h"
#include "UniformGroup.h"
#include "CommonVBLayout.h"
#include <vulkan/vulkan.h>
#include <vector>
#include <map>


namespace oglStrElem
{
  template <typename T>
  struct itemPair
  {
    const char* name;
    T           item;
  };

  extern const std::vector<itemPair<OGL_BASETYPE>>                     dataTypeItems;
  extern const std::vector<itemPair<UNIFORM_USAGE>>                    uniformUsageItems;
  extern const std::vector<itemPair<Shader::ShaderType>>               uniformStageItems;
  extern const std::vector<itemPair<Shader::ShaderType>>               uniformStageExtItems;
  extern const std::vector<itemPair<vBufferLayout::vBufferLayoutType>> bufferVertexInputItems;
  extern const std::vector<itemPair<int>>                              integerItems;
  extern const std::vector<itemPair<UniformGroup::_MemoryLayout>>      uniMemLayoutItems;
  extern const std::vector<itemPair<VkDynamicState>>                   dynamicStateItems;
  extern const std::vector<itemPair<VkPrimitiveTopology>>              primitiveTopItems;
  extern const std::vector<itemPair<VkPolygonMode>>                    polyModeItems;
  extern const std::vector<itemPair<VkCullModeFlagBits>>               cullModeFlagItems;
  extern const std::vector<itemPair<VkFrontFace>>                      frontFaceItems;
  extern const std::vector<itemPair<VkBlendOp>>                        blendOpsItems;
  extern const std::vector<itemPair<VkBlendFactor>>                    blendFactorItems;
  extern const std::vector<itemPair<VkLogicOp>>                        logicOpsItems;
  extern const std::vector<itemPair<VkCompareOp>>                      compareOpsItems;
  extern const std::vector<itemPair<VkStencilOp>>                      stencilOpsItems;
  extern const std::vector<itemPair<VkPipelineStageFlagBits>>          pipelineStageFlagItems;
  extern const std::vector<itemPair<VkAccessFlagBits>>                 accessFlagItems;
  extern const std::vector<itemPair<VkDependencyFlagBits>>             dependencyFlagItems;
  extern const std::vector<itemPair<VkImageUsageFlagBits>>             imageUsageFlagItems;

  extern const std::map<uint32_t, const char*>                         subpassIndexItems;
  extern const std::map<VkFormat, const char*>                         commonImgFormatItems;


  extern OGL_BASETYPE                     Find_OGL_BASETYPE(const std::string& name);
  extern UNIFORM_USAGE                    Find_UNIFORM_USAGE(const std::string& name);
  extern Shader::ShaderType               Find_ShaderType(const std::string& name);
  extern vBufferLayout::vBufferLayoutType Find_vBufferLayoutType(const std::string& name);
}
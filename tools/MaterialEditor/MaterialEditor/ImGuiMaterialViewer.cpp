#include "pch.h"
#include "ImGuiMaterialViewer.h"
#include "AppManager.h"

void ImguiWin32::RunMaterialViewer(bool & open)
{
  const int imguiWinFlag =
    ImGuiWindowFlags_::ImGuiWindowFlags_NoResize |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoSavedSettings |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
  ImGui::SetNextWindowPos(ImVec2(80, 50));
  ImGui::SetNextWindowSize(ImVec2(250, 200));

  if (ImGui::Begin("         Material Library", &open, imguiWinFlag))
  {
    ImGui::Text("Layouts");
    ImGui::SameLine();
    if (ImGui::Button("+ Add##ImportMaterialToLibrary"))
      AppManager::GetInstance()->OpenProject();
    ImGui::Separator();
    ImGui::Spacing();
    ImGui::Spacing();

    AppManager* appMgr = AppManager::GetInstance();
    MaterialLayoutMap& matLayout = appMgr->GetAllMaterialLayouts();
    for (auto& layout : matLayout)
    {
      if (ImGui::Button(layout.first.c_str()))
      {
        appMgr->SetSaveDataLocationFromMaterial(layout.second.m_saveDataLocation);
        appMgr->SetMaterialFromLayoutMap(&layout.second);
      }
    }

    ImGui::End();
  }
}

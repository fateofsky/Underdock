#include "pch.h"
#include "ImGuiTextureViewer.h"
#include "ResourceManager.h"
#include "OGLStringElement.h"
#include "WindowSystem.h"

const ImVec4                    textureColor            = { 0, 1, 1, 1 };
std::string                     selectedTextureToAdd    = "";
std::string                     selectedTextureToDelete = "";
Shader::ShaderType              selectedStageForTexture = Shader::ShaderType::VERTEXSDR;

void ImguiWin32::RunTextureViewer(ImTextureID id, bool& open)
{
  {
    const int imguiWinFlag =
      ImGuiWindowFlags_::ImGuiWindowFlags_NoResize |
      ImGuiWindowFlags_::ImGuiWindowFlags_NoSavedSettings |
      ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
    ImGui::SetNextWindowPos(ImVec2(330, 50));
    ImGui::SetNextWindowSize(ImVec2(616, 637));

    if (ImGui::Begin("                                      Texture Viewer", &open, imguiWinFlag))
    {
      ImGui::Image(id, ImVec2(600, 600));
      ImGui::End();
    }
  }
}

void ImguiWin32::RunTextureLibraryViewer(bool& open, MaterialLayout* layout)
{
  const int imguiWinFlag =
    ImGuiWindowFlags_::ImGuiWindowFlags_NoResize |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoSavedSettings |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
  ImGui::SetNextWindowPos(ImVec2(580, 50));
  ImGui::SetNextWindowSize(ImVec2(375, 600));

  if (ImGui::Begin("                   Texture Library", &open, imguiWinFlag))
  {
    ResourceManager* rm = ResourceManager::GetInstance();
    std::map<std::string, Texture>& texMap = rm->GetAllTextures();
    std::vector<std::string> texNameList;
    std::vector<std::string> texNameListFull;
    std::vector<ImTextureID> texIdList;

    // populate texture list
    for (auto& texture : texMap)
    {
      ImTextureID texID = (ImTextureID)(long long)texture.second.GetTexture();
      texIdList.push_back(texID);
      texNameListFull.push_back(texture.first);
      if (texture.first.size() >= 9)
      {
        std::string shortenName = texture.first.substr(0, 9) + "...";
        texNameList.push_back(shortenName);
      }
      else texNameList.push_back(texture.first);
    }

    ImGui::Text("Texture in Stages");
    ImGui::Separator();
    ImGui::Spacing();
    ImGui::Spacing();

    auto& stageMap = layout->m_resourceLayout.GetPipelineStageMap();
    for (auto& stage : stageMap)
    {
      ImGui::Text((std::string("-> ") + oglStrElem::uniformStageItems[stage.first].name).c_str());
      ImGui::SameLine();

      ImGui::Spacing();
      if (stage.second.inputTextures.size() < 1)
      {
        ImGui::Text("             - No Texture -");
        ImGui::Spacing();
        ImGui::Spacing();
      }

      for (auto& texture : stage.second.inputTextures)
      {
        ImTextureID texID = (ImTextureID)(long long)texture.second->GetTexture();
        if (ImGui::Button(("X##deleteTexture" + texture.first).c_str()))
        {
          selectedTextureToDelete = texture.first;
          selectedStageForTexture = stage.first;
        }
        ImGui::SameLine();
        ImGui::Image(texID, ImVec2(20, 20));
        ImGui::SameLine();
        ImGui::TextColored(textureColor, texture.first.c_str());
      }
    }


    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Text("Library");
    ImGui::SameLine();
    if (ImGui::Button("+ Add##TextureLibraryAddTexture"))
    {
      std::string fileLoc = WinSystem::OpenFile("PNG\0*.png\0JPG\0*.jpg\0JPEG\0*.jpeg\0All Files\0*.*\0");
      if (fileLoc.size() > 0)
      {
        std::string texName;
        ResourceManager::GetInstance()->LoadTexture(fileLoc);
      }
    }
    ImGui::Separator();
    ImGui::Spacing();
    ImGui::Spacing();

    // arrange texMap size into
    if (texIdList.size() > 0)
    {
      size_t heightCount = texIdList.size() >= 4 ? (texIdList.size() >> 2) : 1;
      int texIdListCount = 0;
      int texNameListCount = 0;
      bool continuousHeight = true;

      while (continuousHeight)
      {
        // layout image
        for (int widthCount = 0; widthCount < 4; ++widthCount)
        {
          if (texIdListCount < texIdList.size())
          {
            // assign to a stage
            if (ImGui::ImageButton(texIdList[texIdListCount], ImVec2(75, 75)))
            {
              ImGui::OpenPopup("AddTextureToStagePopup");
              selectedTextureToAdd = texNameListFull[texIdListCount];
            }
            if ((texIdListCount + 1) % 4 != 0 && texIdListCount < texIdList.size() - 1)
              ImGui::SameLine();
            ++texIdListCount;
          }
          else break;
        }

        // layout text
        for (int widthCount = 0; widthCount < 4; ++widthCount)
        {
          if (texNameListCount < texNameList.size())
          {
            ImGui::Text(texNameList[texNameListCount].c_str());
            if ((texNameListCount + 1) % 4 != 0)
              ImGui::SameLine();
            ++texNameListCount;
          }
          else break;
        }

        ImGui::Spacing();
        ImGui::Spacing();
        ImGui::Spacing();
        ImGui::Spacing();

        continuousHeight = texIdListCount >= texIdList.size() ? false : continuousHeight;
      }
    }
    
    // Add texture to stage
    if (ImGui::BeginPopup("AddTextureToStagePopup"))
    {
      for (auto& stage : stageMap)
      {
        if (ImGui::Selectable(oglStrElem::uniformStageItems[stage.first].name))
          stage.second.inputTextures[selectedTextureToAdd] = ResourceManager::GetInstance()->GetTexture(selectedTextureToAdd);
      }
      ImGui::EndPopup();
    }

    // Delete texture from stage
    if (selectedTextureToDelete.size())
    {
      stageMap[selectedStageForTexture].inputTextures.erase(selectedTextureToDelete);
      selectedTextureToDelete = "";
    }

    ImGui::End();
  }
}

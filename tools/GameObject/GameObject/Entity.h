#pragma once

class Entity
{
  public:
    Entity(const unsigned& _id);
  protected:
    unsigned m_id;
};
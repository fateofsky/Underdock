#version 420
#extension GL_ARB_separate_shader_objects : enable

layout (binding = 0) uniform commonMatrix
{
    mat4 viewMat;
    mat4 ProjMat;
}commonMatrix;

layout (binding = 1) uniform mvpMatrix
{
    mat4 mvp;
}mvpMatrix;

layout (location = 0) in vec4 pos;
layout (location = 1) in vec2 uvPos;

layout (location = 0) out vec2 outUV;

void main() 
{
	outUV = uvPos;
	gl_Position = mvpMatrix.mvp * pos;
}

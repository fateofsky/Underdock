#include "pch.h"
#include "CameraComponent.h"
#include "Transform.h"

const mat4 clipMtx
{
  1.f,  0.f, 0.f,  0.f,
  0.f, -1.f, 0.f,  0.f,
  0.f,  0.f, 0.5f, 0.5f,
  0.f,  0.f, 0.f,  1.0f
};

const vec3 globalUp(0, 1, 0);

CameraComponent::CameraComponent(Gameobject* owner, const unsigned& _id)
  : ComponentBase(owner, _id) 
{
  ComponentBase::InsertParam(0, &m_near);
  ComponentBase::InsertParam(1, &m_far);
  ComponentBase::InsertParam(2, &m_fov);
  ComponentBase::InsertParam(3, &m_aspect);
  ComponentBase::InsertParam(4, &m_lookAt);
  ComponentBase::InsertParam(5, &m_orthorgonal);
}

CameraComponent::~CameraComponent()
{ }

void CameraComponent::Init()
{

}

void CameraComponent::PostInit()
{
  m_transformHandle.MoveComponentHandleOver(GetOwner()->GetFirstOfComponentHandle<Transform>());
  m_matHandle.MoveComponentHandleOver(GetOwner()->GetFirstOfComponentHandle<MaterialComponent>());
  _ComputeMatrices();
}

void CameraComponent::Update()
{
  if (_update || m_transformHandle.GetComponent()->GetRequiredUpdate())
  {
    _ComputeMatrices();
    _update = false;
  }

}

void CameraComponent::PreRender()
{

}

void CameraComponent::PostUpdate()
{
}

void CameraComponent::SetOrthogonal(bool state)
{
  _update = MATH_OPS_FLOATCOMPARE(m_orthorgonal.GetData(), state);
  m_orthorgonal.GetData() = state;
}

bool CameraComponent::IsOrthogonal() const
{
  return m_orthorgonal.GetData();
}

void CameraComponent::SetFOV(const float& FOV)
{
  _update = MATH_OPS_FLOATCOMPARE(m_fov.GetData(), FOV);
  m_fov.GetData() = FOV;
}

const float& CameraComponent::GetFOV() const
{
  return m_fov.GetData();
}

void CameraComponent::SetNearPlane(const float& nearPlane)
{
  _update = MATH_OPS_FLOATCOMPARE(m_near.GetData(), nearPlane);
  m_near.GetData() = nearPlane;
}

const float& CameraComponent::GetNearPlane() const
{
  return m_near.GetData();
}

void CameraComponent::SetFarPlane(const float& farPlane)
{
  _update = MATH_OPS_FLOATCOMPARE(m_far.GetData(), farPlane);
  m_far.GetData() = farPlane;
}

const float& CameraComponent::GetFarPlane() const
{
  return m_far.GetData();
}

void CameraComponent::SetAspectRatio(const float& aspectR)
{
  _update = MATH_OPS_FLOATCOMPARE(m_aspect.GetData(), aspectR);
  m_aspect.GetData() = aspectR;
}

const float& CameraComponent::GetAspectRatio() const
{
  return m_aspect.GetData();
}

void CameraComponent::SetLookAt(const vec3& direction)
{
  _update = vec3Compare(m_lookAt.GetData(), direction);
  m_lookAt.GetData() = direction;
}

const vec3& CameraComponent::GetLookAt() const
{
  return m_lookAt.GetData();
}

const mat4& CameraComponent::GetViewMatrix() const
{
  return m_viewMat;
}

const mat4& CameraComponent::GetProjMatrix() const
{
  return m_projMat;
}

const mat4& CameraComponent::GetViewProjMatrix() const
{
  return m_viewProjMat;
}

mat4 CameraComponent::GetViewMatrix_Transpose() const
{
  return m_viewMat.transposed();
}

mat4 CameraComponent::GetProjMatrix_Transpose() const
{
  return m_projMat.transposed();
}

mat4 CameraComponent::GetViewProjMatrix_Transpose() const
{
  return m_viewProjMat.transposed();
}

bool CameraComponent::GetRequiredUpdate() const
{
  return _update;
}

void CameraComponent::_ComputeMatrices()
{
  if (m_transformHandle.IsValid())
  {
    Transform* trans = m_transformHandle.GetComponent();
    vec3 pos = trans->GetPosition();
    vec3 rotation = trans->GetRotation();

    m_projMat = m_orthorgonal.GetData() ?
      Math::Orthogonal(-m_aspect.GetData(), m_aspect.GetData(), -1.f, 1.f, m_near.GetData(), m_far.GetData()) :
      Math::Perspective(m_fov.GetData(), m_aspect.GetData(), m_near.GetData(), m_far.GetData());

    // compute view
    vec3 camRight = vec3CrossProduct(m_lookAt.GetData(), globalUp).normalized();
    vec3 camBack = -m_lookAt.GetData().normalized();
    vec3 camUp = vec3CrossProduct(camBack, camRight).normalized();
    m_viewMat[0] = camRight.x_;   m_viewMat[1] = camRight.y_;   m_viewMat[2] = camRight.z_;
    m_viewMat[4] = camUp.x_;      m_viewMat[5] = camUp.y_;      m_viewMat[6] = camUp.z_;
    m_viewMat[8] = camBack.x_;    m_viewMat[9] = camBack.y_;    m_viewMat[10] = camBack.z_;
    m_viewMat[3] = -vec3DotProduct(pos, camRight);
    m_viewMat[7] = -vec3DotProduct(pos, camUp);
    m_viewMat[11] = -vec3DotProduct(pos, camBack);

    m_viewProjMat = clipMtx * m_projMat * m_viewMat;
  }
  else
  {
    std::cout << "Error: CameraComponent::Init() has invalid transform component\n";
    assert(false);
  }
}

#include "pch.h"
#include "WindowSystem.h"
#include "OGL.h"
#include "ImguiWin32.h"

int WINAPI WinMain(HINSTANCE instanceH, HINSTANCE, LPSTR, int)
{
  WinSystem::InitWindow(instanceH);
  ogl::InitContext(WinSystem::hdc, WinSystem::hrc);
  ImguiWin32::InitIMGUI(WinSystem::hwnd);
  WinSystem::Run();
  ImguiWin32::CleanupIMGUI();
  WinSystem::Cleanup();

  return 0;
}

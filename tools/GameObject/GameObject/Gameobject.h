#pragma once
#include "ComponentBase.h"
#include "EntityTracker.h"
#include <map>
#include <list>

class Gameobject : public Entity
{
  friend class EntityTracker;
  friend class GameobjectManager;
  template<typename> friend class ComponentHandle;

  public:
    ~Gameobject();
    Gameobject(const Gameobject&)            = delete;  // non construction-copyable
    Gameobject& operator=(const Gameobject&) = delete;  // non copyable

    /*
        ADD COMPONENT AND RETURNS A REFERENCE INDEX
    */
    template <typename T, bool R = std::is_base_of<_SINGLE, T>()>
    unsigned AddComponent_ref()
    {
      ComponentEnum id = rtti::DeriveTypeID<T>();
      if constexpr (R)
      {
        if (m_multiComponentMap.find(id) != m_multiComponentMap.end())
        {
          std::cout << "Error: Cannot add more than one of this component\n";
          return MAX_UNSIGNED;
        }
      }

      // Generate component ID
      unsigned indCount = EntityTracker::GetInstance()->GenerateID();
      m_multiComponentMap[id][indCount] = new T(this, indCount);

      EntityTracker::GetInstance()->AddComponentHash(m_id, id, indCount);
      return indCount;
    }

    /*
        ADD COMPONENT AND RETURNS A HANDLE
    */
    template <typename T, bool R = std::is_base_of<_SINGLE, T>()>
    ComponentHandle<T> AddComponent_hc()
    {
      ComponentEnum id = rtti::DeriveTypeID<T>();

      if constexpr (R)
      {
        if (m_multiComponentMap.find(id) != m_multiComponentMap.end())
        {
          std::cout << "Error: Cannot add more than one of this component\n";
          return ComponentHandle<T>(MAX_UNSIGNED, nullptr, this, MAX_UNSIGNED);
        }
      }

      // Generate component ID
      unsigned indCount = EntityTracker::GetInstance()->GenerateID();
      m_multiComponentMap[id][indCount] = new T(this, indCount);

      EntityTracker::GetInstance()->AddComponentHash(m_id, id, indCount);
      char* tmpFlag = EntityTracker::GetInstance()->GetComponentFlag(m_id, id, indCount);
      return ComponentHandle<T>(indCount, tmpFlag, this, m_id);
    }


    /*
        DELETE COMPONENT USING HANDLE
    */
    template <typename T>
    void DeleteComponent(ComponentHandle<T>& ch)
    {
      ComponentEnum id = rtti::DeriveTypeID<T>();
      unsigned compRef = ch.GetComponentReference();
      if (m_multiComponentMap.find(id) != m_multiComponentMap.end() &&
        m_multiComponentMap[id].find(compRef) != m_multiComponentMap[id].end())
      {
        m_multiComponentMap[id].erase(compRef);

        // Clear out if the component is single
        if(m_multiComponentMap[id].size() < 1)
          m_multiComponentMap.erase(id);
      }

      EntityTracker::GetInstance()->SetComponentInvalid(m_id, id, compRef);
    }

    /*
        DELETE COMPONENT USING REFERENCE
    */
    void DeleteComponent(const unsigned& compRef)
    {
      for (auto& compType : m_multiComponentMap) {
        for (auto& comp : compType.second) {
          if (comp.first == compRef)
          {
            ComponentEnum id = compType.first;
            m_multiComponentMap[id].erase(compRef);

            // Clear out if the component is single
            if (m_multiComponentMap[id].size() < 1)
              m_multiComponentMap.erase(id);

            EntityTracker::GetInstance()->SetComponentInvalid(m_id, id, compRef);
            return;
          }
        }
      }
    }

    void ClearAllComponent();

    /*
        GET COMPONENT HANDLE WITH A REFERENCE INDEX
    */
    template <typename T>
    ComponentHandle<T> GetComponentHandle(const unsigned& index)
    {
      ComponentEnum id = rtti::DeriveTypeID<T>();

      // safety checks
      auto tmpBaseMap = m_multiComponentMap.find(id) != m_multiComponentMap.end() ? m_multiComponentMap.at(id) : std::map<unsigned, ComponentBase*>();
      if (tmpBaseMap.size() < 1 || (tmpBaseMap.find(index) == tmpBaseMap.end())) return ComponentHandle<T>(MAX_UNSIGNED, nullptr, this, MAX_UNSIGNED);

      char* tmpFlag = EntityTracker::GetInstance()->GetComponentFlag(m_id, id, index);
      return ComponentHandle<T>(index, tmpFlag, this, m_id);
    }

    /*
        GET COMPONENT HANDLE FIRST FIND
    */
    template <typename T>
    ComponentHandle<T> GetFirstOfComponentHandle()
    {
      ComponentEnum id = rtti::DeriveTypeID<T>();

      // safety checks
      auto tmpBaseMap = m_multiComponentMap.find(id) != m_multiComponentMap.end() ? m_multiComponentMap.at(id) : std::map<unsigned, ComponentBase*>();
      if (tmpBaseMap.size() < 1) return ComponentHandle<T>(MAX_UNSIGNED, nullptr, this, MAX_UNSIGNED);

      unsigned index = tmpBaseMap.begin()->first;
      char* tmpFlag = EntityTracker::GetInstance()->GetComponentFlag(m_id, id, index);
      return ComponentHandle<T>(index, tmpFlag, this, m_id);
    }

    /*
        REINITIALIZE OR RESET THE HANDLE
    */
    template <typename T>
    void ResetHandle(ComponentHandle<T>& ch, const unsigned& compRef)
    {
      ComponentEnum id = rtti::DeriveTypeID<T>();
      if (EntityTracker::GetInstance()->HasComponentReference(ch.m_gameobjRef, id, ch.m_reference) && compRef == ch.m_reference) return;

      // safety checks
      auto tmpBaseMap = m_multiComponentMap.find(id) != m_multiComponentMap.end() ? m_multiComponentMap.at(id) : std::map<unsigned, ComponentBase*>();
      if (tmpBaseMap.size() < 1 || (tmpBaseMap.find(compRef) == tmpBaseMap.end()))
      {
        ch._setData(0, nullptr, this, m_id);
        return;
      }

      EntityTracker::GetInstance()->RemoveComponentReference(ch.m_gameobjRef, id, ch.m_reference);
      char* tmpFlag = EntityTracker::GetInstance()->GetComponentFlag(m_id, id, compRef);
      ch._setData(compRef, tmpFlag, this, m_id);
    }

    const unsigned& GetGameobjectID() const;

    ComponentBase* GetComponentBase(ComponentEnum cEnum, unsigned refPos = 0);

  private:
    Gameobject(const unsigned& id);
    Gameobject();                         // ONLY FOR DESERIALIZATION
    void SetHashId(const unsigned& id);   // ~

    /*
        PRIVATE FUNCTION
    */
    template<typename T>
    T* GetComponentData(const unsigned& ref)
    {
      ComponentEnum id = rtti::DeriveTypeID<T>();
      return reinterpret_cast<T*>(m_multiComponentMap[id][ref]);
    }


    /*
        **ADD COMPONENT USING PREDEFINED ID (ONLY DESERIALIZATION USE THIS)
    */
    public:
    template <typename T, bool R = std::is_base_of<_SINGLE, T>()>
    void AddComponent_ID(unsigned compID)
    {
      ComponentEnum id = rtti::DeriveTypeID<T>();
      if constexpr (R)
      {
        if (m_multiComponentMap.find(id) != m_multiComponentMap.end())
        {
          std::cout << "Error: Cannot add more than one of this component\n";
          return;
        }
      }

#ifdef _DEBUG
      // Check with current ID
      unsigned currID = EntityTracker::GetInstance()->GetCurrentID();
      bool idCheck = compID < currID || !EntityTracker::GetInstance()->IsUnuseID(compID);
      if (idCheck)
      {
        std::cout << "Error: Unable to create " << rtti::GetComponentEnumString(id) << " , possible duplicate of ID\n";
        assert(false);
      }
      else
      {
#endif // _DEBUG
        m_multiComponentMap[id][compID] = new T(this, compID);
        EntityTracker::GetInstance()->AddComponentHash(m_id, id, compID);
#ifdef _DEBUG
      }
#endif // _DEBUG
    }


    std::map<ComponentEnum, std::map<unsigned, ComponentBase*>> m_multiComponentMap;
};
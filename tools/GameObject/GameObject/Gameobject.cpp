#include "pch.h"
#include "Gameobject.h"

Gameobject::Gameobject(const unsigned& id)
  : Entity(id),
    m_multiComponentMap()
{
  EntityTracker::GetInstance()->AddGameobjectHash(m_id);
}

// ONLY FOR DESERIALIZATION
Gameobject::Gameobject()
  : Entity(0),
    m_multiComponentMap()
{ }

// ONLY FOR DESERIALIZATION
void Gameobject::SetHashId(const unsigned & id)
{
  m_id = id;
  EntityTracker::GetInstance()->AddGameobjectHash(m_id);
}


Gameobject::~Gameobject()
{
  ClearAllComponent();
}

void Gameobject::ClearAllComponent()
{
  // invalidate all existing flag
  for (auto& compType : m_multiComponentMap) {
    for (auto& comp : compType.second) {
      EntityTracker::GetInstance()->SetComponentInvalid(m_id, compType.first, comp.first);
    }
  }

  m_multiComponentMap.clear();
}

const unsigned & Gameobject::GetGameobjectID() const
{
  return m_id;
}

ComponentBase* Gameobject::GetComponentBase(ComponentEnum cEnum, unsigned refPos)
{
#ifdef _DEBUG
  if (m_multiComponentMap.find(cEnum) == m_multiComponentMap.end())
  {
    std::cout << "Error: Unable to find component of type " << rtti::GetComponentEnumString(cEnum) << std::endl;
    return nullptr;
  }
  if (m_multiComponentMap[cEnum].find(refPos) == m_multiComponentMap[cEnum].end())
  {
    std::cout << "Error: Unable to find index " << refPos << " of component type " << rtti::GetComponentEnumString(cEnum) << std::endl;
    return nullptr;
  }
#endif // _DEBUG

  return m_multiComponentMap[cEnum][refPos];
}

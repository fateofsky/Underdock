#include "pch.h"
#include "ImGuiUniformViewer.h"
#include <vector>

const char*              uniformTypeSelection     = "";
const char*              uniformUsageSelection    = "";
const char*              uniformPipelineSelection = "";
OGL_BASETYPE             uniformTypeENUM          = OGL_BASETYPE::INT_TYPE;
UNIFORM_USAGE            uniformUsageENUM         = UNIFORM_USAGE::CAM_VIEW;
Shader::ShaderType       uniformPipelineENUM      = Shader::ShaderType::VERTEXSDR;
int                      ModificationError        = 0;
char                     resourceNameBuffer[1024] = { '\0' };

void ImguiWin32::RunUniformViewer(bool& open, MaterialLayout* layout)
{
  const int imguiWinFlag =
    ImGuiWindowFlags_::ImGuiWindowFlags_NoResize |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoSavedSettings |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
  ImGui::SetNextWindowPos(ImVec2(500, 50));
  ImGui::SetNextWindowSize(ImVec2(317, 250));

  if (ImGui::Begin("                Add Uniform", &open, imguiWinFlag))
  {
    /*
      -- UNIFORM NAME
    */
    //
    ImGuiInputTextFlags_ unformNameFlag = ImGuiInputTextFlags_CharsNoBlank;
    ImGui::Text("Name:     "); ImGui::SameLine();
    ImGui::InputText("##UniformName", resourceNameBuffer, 20, unformNameFlag);
    ImGui::Spacing();
    ImGui::Spacing();


    /*
      - UNIFORM TYPE
    */
    ImGui::Text("Type:     "); ImGui::SameLine();
    if (ImGui::BeginCombo("##Uniform Type", uniformTypeSelection))
    {
      for (size_t n = 0; n < oglStrElem::dataTypeItems.size(); ++n)
      {
        if (ImGui::Selectable(oglStrElem::dataTypeItems[n].name))
        {
          uniformTypeSelection = oglStrElem::dataTypeItems[n].name;
          uniformTypeENUM = oglStrElem::dataTypeItems[n].item;
        }
      }
      ImGui::EndCombo();
    }

    ImGui::Spacing();
    ImGui::Spacing();

    /*
      - UNIFORM USAGE
    */
    ImGui::Text("Usage:    "); ImGui::SameLine();
    if (ImGui::BeginCombo("##Uniform Usage", uniformUsageSelection))
    {
      for (size_t n = 0; n < oglStrElem::uniformUsageItems.size(); ++n)
      {
        if (ImGui::Selectable(oglStrElem::uniformUsageItems[n].name))
        {
          uniformUsageSelection = oglStrElem::uniformUsageItems[n].name;
          uniformUsageENUM = oglStrElem::uniformUsageItems[n].item;
        }
      }
      ImGui::EndCombo();
    }

    ImGui::Spacing();
    ImGui::Spacing();

    /*
      - UNIFORM Pipeline
    */
    ImGui::Text("Pipeline: "); ImGui::SameLine();
    if (ImGui::BeginCombo("##Uniform Pipeline", uniformPipelineSelection))
    {
      for (size_t n = 0; n < oglStrElem::uniformStageItems.size(); ++n)
      {
        if (ImGui::Selectable(oglStrElem::uniformStageItems[n].name))
        {
          uniformPipelineSelection = oglStrElem::uniformStageItems[n].name;
          uniformPipelineENUM = oglStrElem::uniformStageItems[n].item;
        }
      }
      ImGui::EndCombo();
    }

    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();

    if (ImGui::Button("OK", ImVec2(70, 30)))
    {
      // append result
      ModificationError = 0;
      bool strlenTest = strlen(resourceNameBuffer) > 0;
      switch (uniformTypeENUM)
      {
      case INT_TYPE:
      case INT4_TYPE:
      case BOOL_TYPE:
      case VEC2_TYPE:
      case VEC3_TYPE:
      case VEC4_TYPE:
      case MAT3_TYPE:
        if (uniformUsageENUM == UNIFORM_USAGE::USER_INPUT && strlenTest)
          ModificationError = layout->AddUniform(resourceNameBuffer, uniformTypeENUM, uniformUsageENUM, uniformPipelineENUM, 0);
        else ModificationError = 1;
        break;

      case FLOAT_TYPE:
        if ((uniformUsageENUM == UNIFORM_USAGE::USER_INPUT || uniformUsageENUM  == UNIFORM_USAGE::DRIVER_TIME) && strlenTest)
          ModificationError = layout->AddUniform(resourceNameBuffer, uniformTypeENUM, uniformUsageENUM, uniformPipelineENUM, 0);
        else ModificationError = 1;
        break;

      case MAT4_TYPE:
        if (strlenTest)
          ModificationError = layout->AddUniform(resourceNameBuffer, uniformTypeENUM, uniformUsageENUM, uniformPipelineENUM, 0);
        else ModificationError = 1;
        break;

      default:
        assert(false);
        break;
      }

      open = !(ModificationError == 0);
    }
    ImGui::SameLine();
    if (ImGui::Button("Cancel", ImVec2(70, 30)))
      open = false;

    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Text(ModificationError != 0 ? "Error: Wrong usage or empty/duplicate name" : "");

    ImGui::End();
  }

}

void ImguiWin32::ShowUniformEditor(const std::string& name, UniformResourceBase* data)
{
  OGL_BASETYPE type = data->GetType();
  UNIFORM_USAGE usage = data->GetUsage();
  const static std::string driverSpec("Driver: ");

  switch (usage)
  {
  case UNIFORM_USAGE::CAM_VIEW:
  case UNIFORM_USAGE::CAM_PROJ:
  case UNIFORM_USAGE::CAM_VIEWPROJ:
  case UNIFORM_USAGE::TRANSFORM:
  case UNIFORM_USAGE::DRIVER_TIME:
    ImGui::Text((driverSpec + oglStrElem::uniformUsageItems[(int)usage].name).c_str());
    ImGui::TextColored(ImVec4(0.f, 1.f, 0.f, 1.f), name.c_str());
    break;

  case UNIFORM_USAGE::USER_INPUT:
    switch (type)
    {
    case INT_TYPE:
    {
      int* value = reinterpret_cast<int*>(data->GetUniformData());
      ImGui::TextColored(ImVec4(0.f, 1.f, 0.f, 1.f), ("Int: " + name).c_str());
      ImGui::SliderInt(("##" + name).c_str(), value, -99999, 99999);
    } break;

    case INT4_TYPE:
    {
      int* value = reinterpret_cast<int*>(data->GetUniformData());
      ImGui::TextColored(ImVec4(0.f, 1.f, 0.f, 1.f), ("Int: " + name).c_str());
      ImGui::SliderInt4(("##" + name).c_str(), value, -99999, 99999);
    } break;

    case FLOAT_TYPE:
    {
      float* value = reinterpret_cast<float*>(data->GetUniformData());
      ImGui::TextColored(ImVec4(0.f, 1.f, 0.f, 1.f), ("Float: " + name).c_str());
      ImGui::SliderFloat(("##" + name).c_str(), value, -99999.f, 99999.f, "%.4f");
    } break;

    case BOOL_TYPE:
    {
      bool* value = reinterpret_cast<bool*>(data->GetUniformData());
      ImGui::TextColored(ImVec4(0.f, 1.f, 0.f, 1.f), ("Bool: " + name).c_str());
      ImGui::Checkbox(("##" + name).c_str(), value);
    } break;

    case VEC2_TYPE:
    {
      float* value = reinterpret_cast<float*>(data->GetUniformData());
      ImGui::TextColored(ImVec4(0.f, 1.f, 0.f, 1.f), ("Vec2: " + name).c_str());
      ImGui::SliderFloat(("##" + name + "0").c_str(),     value,     -99999.f, 99999.f, "%.4f");
      ImGui::SliderFloat(("##" + name + "1").c_str(), value + 1, -99999.f, 99999.f, "%.4f");
    } break;

    case VEC3_TYPE:
    {
      float* value = reinterpret_cast<float*>(data->GetUniformData());
      ImGui::TextColored(ImVec4(0.f, 1.f, 0.f, 1.f), ("Vec3: " + name).c_str());
      ImGui::SliderFloat(("##" + name + "0").c_str(),     value,     -99999.f, 99999.f, "%.4f");
      ImGui::SliderFloat(("##" + name + "1").c_str(), value + 1, -99999.f, 99999.f, "%.4f");
      ImGui::SliderFloat(("##" + name + "2").c_str(), value + 2, -99999.f, 99999.f, "%.4f");
    } break;

    case VEC4_TYPE:
    {
      float* value = reinterpret_cast<float*>(data->GetUniformData());
      ImGui::TextColored(ImVec4(0.f, 1.f, 0.f, 1.f), ("Vec4: " + name).c_str());
      ImGui::DragFloat(("##" + name + "0").c_str(),     value,     1, -99999.f, 99999.f, "%.4f");
      ImGui::DragFloat(("##" + name + "1").c_str(), value + 1, 1, -99999.f, 99999.f, "%.4f");
      ImGui::DragFloat(("##" + name + "2").c_str(), value + 2, 1, -99999.f, 99999.f, "%.4f");
      ImGui::DragFloat(("##" + name + "3").c_str(), value + 3, 1, -99999.f, 99999.f, "%.4f");
    } break;

    case MAT3_TYPE:
    {
      float* value = reinterpret_cast<float*>(data->GetUniformData());
      ImGui::TextColored(ImVec4(0.f, 1.f, 0.f, 1.f), ("Mat3: " + name).c_str());
      ImGui::DragFloat3(("##" + name + "0").c_str(),     value,     1, -99999.f, 99999.f, "%.4f");
      ImGui::DragFloat3(("##" + name + "1").c_str(), value + 3, 1, -99999.f, 99999.f, "%.4f");
      ImGui::DragFloat3(("##" + name + "2").c_str(), value + 6, 1, -99999.f, 99999.f, "%.4f");
    } break;

    case MAT4_TYPE:
    {
      float* value = reinterpret_cast<float*>(data->GetUniformData());
      ImGui::TextColored(ImVec4(0.f, 1.f, 0.f, 1.f), ("Mat4: " + name).c_str());
      ImGui::DragFloat4(("##" + name + "0").c_str(),     value,      1, -99999.f, 99999.f, "%.4f");
      ImGui::DragFloat4(("##" + name + "1").c_str(), value + 4,  1, -99999.f, 99999.f, "%.4f");
      ImGui::DragFloat4(("##" + name + "2").c_str(), value + 8,  1, -99999.f, 99999.f, "%.4f");
      ImGui::DragFloat4(("##" + name + "3").c_str(), value + 12, 1, -99999.f, 99999.f, "%.4f");
    } break;
    }
    break;
  }
}

void ImguiWin32::RenameUniformData(bool& open, const std::string* resourceName, MaterialLayout* layout)
{
  const int imguiWinFlag =
    ImGuiWindowFlags_::ImGuiWindowFlags_NoResize |
    ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
  ImGui::SetNextWindowPos(ImVec2(550, 150));
  ImGui::SetNextWindowSize(ImVec2(300, 125));
  ImGuiInputTextFlags_ unformNameFlag = ImGuiInputTextFlags_CharsNoBlank;
  std::string title = "              Rename Uniform";
  if (ImGui::Begin(title.c_str(), &open, imguiWinFlag))
  {
    ImGui::Text("New Name:");
    ImGui::SameLine();
    ImGui::InputText("", resourceNameBuffer, 20, unformNameFlag);

    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Spacing();

    if (ImGui::Button("OK", ImVec2(70, 20)))
    {
      ModificationError = false;
      if (strlen(resourceNameBuffer) > 0 && !layout->m_resourceLayout.UniformExist(resourceNameBuffer))
      {
        layout->m_resourceLayout.RenameUniform(*resourceName, resourceNameBuffer);
        open = false;
      }
      else ModificationError = true;
    }
    ImGui::SameLine();
    if (ImGui::Button("Cancel", ImVec2(70, 20)))
      ModificationError = open = false;

    ImGui::Spacing();
    ImGui::Spacing();

    // Show error
    ImGui::Text(ModificationError ? "      Error: Empty / duplicate name!" : "");
    ImGui::End();
  }
}

void ImguiWin32::ResetResourceNameBuffer()
{
  memset(resourceNameBuffer, 0, 1024);
}

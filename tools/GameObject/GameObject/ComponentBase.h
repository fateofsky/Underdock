#pragma once
#include "ComponentRTTI.h"
#include "ComponentHandle.h"
#include "Entity.h"
#include "Parameters.h"

struct _SINGLE {};
class Gameobject;
class Entity;

class ComponentBase : public Entity
{
  friend class Gameobject;
  friend class GameobjectManager;

  public:
    virtual ~ComponentBase();
    virtual void Init() = 0;
    virtual void PostInit() = 0;
    virtual void Update() = 0;
    virtual void PreRender() = 0;
    virtual void PostUpdate() = 0;
    Gameobject* GetOwner();
    ParameterBase* GetParamBase(unsigned char uniLoc);

  protected:
    ComponentBase(Gameobject* owner, const unsigned& _id);
    ComponentBase(const ComponentBase&) = delete;             // non construction-copyable
    ComponentBase& operator=(const ComponentBase&) = delete;  // non copyable
    void InsertParam(unsigned char locId, ParameterBase* paramB);

  private:
    Gameobject* m_owner;
    std::map<unsigned char, ParameterBase*> m_paramCollection;
};

#pragma once
#include "KeyMacros.h"
#include <Windows.h>

class InputManager
{
public:
  static InputManager* GetInstance();
  void   Shutdown();

  /************************ BASIC INPUT CONTROLS ***********************/
  bool KeyPressed(int _key);                        // Check if a key is triggered
  bool KeyHold(int _key);                           // Check if a key is hold down
  bool KeyReleased(int _key);                       // check if a key has recently been released
  int  GetMouse_X();                                // Get mouse position x
  int  GetMouse_Y();                                // Get mouse position y
  void InputKeyValue__(int _key, char val);         // update the key array for each input (FOR WINPROC MESSAGES ONLY)
  void InputMouseWheel__(int _key);                 // enable mouse scroll
  POINT& InputGetDelta();
  /*********************************************************************/


  /*********************** SYSTEM & CONTROL LOOPS **********************/
  void InputRefresh__();                            // update keyboard input
  void InputMouseMove__();                          // update mouse input
  void BlockKeyInput(bool state);
  /*********************************************************************/


private:
  InputManager();
  static InputManager* m_instance;                             // Input manager instance
  char                 m_keys[_NUM_OF_KEYS];                   // all possible keys from the input
  POINT                m_mouse_abs;                            // the absolute mouse position
  POINT                m_lastPos;                              // the last mouse position
  POINT                m_delta;                                // diff in last and current mouse position
  bool                 m_unbounded;                            // mouse look is unbounded
};

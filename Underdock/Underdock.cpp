#include "pch.h"
#include "Engine.h"

#pragma warning(suppress: 28251) //no annotation for WinMain, annotations are optional
int WINAPI WinMain(HINSTANCE instanceH, HINSTANCE, LPSTR, int)
{
#if defined _DEBUG
  AllocConsole();
#pragma warning(suppress: 6031) //ignored return value, used for side effects
  FILE* pCout = 0;
  freopen_s(&pCout, "CONOUT$", "w", stdout);
  std::cout << "Console initialized..." << std::endl;
#endif // _DEBUG

  if (!Engine::Init(instanceH))
    return -1;

  Engine::Run();

  Engine::DestroyEngine();
  return 0;
}

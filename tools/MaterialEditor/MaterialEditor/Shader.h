#pragma once
#include <fstream>
#include <vector>
#include <string>
#include "GL/glew.h"
#include "GL/wglew.h"

#define MAX_SDR_OBJ_VAL 0xFFFFFFFF

class Shader
{
public:
  enum ShaderType
  {
    VERTEXSDR,
    TESSCTRLSDR,
    TESSEVALSDR,
    GEOMSDR,
    FRAGSDR,
    COMPUTESDR
  };

  Shader();
  void   Destroy();
  void   CopyFrom(const Shader& sdr);
  bool   LoadShaderFile(const std::string& filePath);
  bool   CreateShader(const char* shaderSrc, ShaderType type);
  void   DeleteShader(ShaderType type);
  bool   CreateProgram();
  void   DeleteProgram();
  void   UseProgram();
  GLuint GetProgram();

private:

  struct SDROBJ_TYPE_PAIR
  {
    GLuint     sdrObj;
    ShaderType sdrType;
  };

  std::vector<SDROBJ_TYPE_PAIR> m_sdrObjVec;
  GLuint                        m_sdrPrg;
};
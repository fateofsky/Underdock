#pragma once
#include "OGLHeader.h"
#include <string>

class Texture
{
public:
  Texture();
  bool          CreateTexture(const char* file);
  bool          CreateTexture(const std::string& file);
  void          DestroyTexture();
  void          BindTexture(GLint location);
  const GLuint& GetTexture() const;
  void          ChangeGroup(const int& newGroup);
  int           GetGroup() const;

private:
  GLuint m_texture;
  int    m_texWidth;
  int    m_texHeight;
  int    m_texChannel;
  int    m_groupSet;
};
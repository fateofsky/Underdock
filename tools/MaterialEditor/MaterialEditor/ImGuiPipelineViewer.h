#pragma once
#include "ImGuiShaderEditor.h"

namespace ImguiWin32
{
  extern void RunPipelineViewer(bool& open, MaterialLayout* layout);
}
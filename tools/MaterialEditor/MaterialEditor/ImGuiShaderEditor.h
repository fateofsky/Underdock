#pragma once
#include "imgui.h"
#include "imgui_stdlib.h"
#include "MaterialLayout.h"
#include "ShaderBuilder.h"
#include "OGLStringElement.h"

namespace ImguiWin32
{
  extern void RunShaderEditor(bool& open, MaterialLayout* layout, Shader::ShaderType stage, bool& firstTime);
}
#pragma once
#include "MemoryStream.h"
#include "VulkUtils.h"
#include "VulkBuffer.h"
#include "VulkRenderTarget.h"
#include <vulkan\vulkan.h>
#include <string>


using LayoutMap    = std::map<unsigned, VulkUtils::VulkDescSetLayout>;
using ShaderMap    = std::map<VulkUtils::ShaderStage, VulkUtils::VulkShaderInfo>;
using VulkTypeList = std::vector<VulkUtils::VK_BASE_TYPE>;

class VulkPipeline
{
public:
  VulkPipeline();

  void LoadPipeline_1(const std::string& file, VulkRenderTarget* vrt = nullptr);
  void Destroy();
  void BindPipeline();

  void BindDescriptorSet(const unsigned& set, const unsigned& instanceID);

  /*
    Object usage
  */
  int RegisterDescriptor(const unsigned& setNumber); // return value = index
  //VulkUtils::DescriptorSetInst* GetDescriptorSetInst(const unsigned& setNumber, const int& index);
  void CompileRegisteredDescriptor();
  VulkUtils::VulkDescSetLayout* GetLayout(const unsigned& setNumber);
  void* GetBufferMemory();
  const VulkBuffer& GetVulkBufferObj() const;
  VulkRenderTarget* GetRenderTarget();

private:
  LayoutMap                          m_descSetMap;
  ShaderMap                          m_shaderMap;
  VulkTypeList                       m_inputBaseTypeList;
  VulkBuffer                         m_vUniformBuffer;
  bool                               m_hasRenderTarget;

  // vulkan API
  VkDescriptorPool                   m_setPool; // maybe u can use just 1 pool
  VkPipelineLayout                   m_pipelineLayout;
  VkPipeline                         m_pipeline;
  VulkRenderTarget*                  m_renderTargetPtr;
};
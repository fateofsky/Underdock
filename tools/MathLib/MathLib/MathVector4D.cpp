/* Start Header
*****************************************************************/
/*!
\file      MathVector4D.cpp
\author    
\par       
\date      
\brief

Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the
prior written consent of DigiPen Institute of Technology is prohibited.
*/
/* End Header
*******************************************************************/
#include "stdafx.h"

///////////
// vec4 //--------------------------------------------------------------
///////////
/* -----------------------------*/
/* ----- Member Functions ----- */
/* -----------------------------*/
vec4::vec4(int x_, int y_, int z_, int w_)
	: x_{ (float)x_ }, y_{ (float)y_ }, z_{ (float)z_ }, w_{ (float)w_ } {}

vec4::vec4(float x_, float y_, float z_, float w_)
	: x_{ x_ }, y_{ y_ }, z_{ z_ }, w_{ w_ } {}


vec4::vec4(const vec3& v, float w_)
	: x_{ v.x_ }, y_{ v.y_ }, z_{ v.z_ }, w_{ w_ } {}

vec4::vec4(const vec2& v, float z_, float w_)
	: x_{ v.x_ }, y_{ v.y_ }, z_{ z_ }, w_{ w_ } {}

#pragma warning(suppress: 26495) //unitialized member, but its a union so...
vec4::vec4(const std::string& str)
{
	size_t comma = 0;
	for (float& elem : ar)
	{
		if (comma != std::string::npos)
		{
			elem = std::atof(&str.front() + comma);
			comma = str.find(',', comma + 1) + 1;
		}
		else elem = 0;
	}
}

vec4::vec4(const quat & qua)
{
	x_ = qua.x;
	y_ = qua.y;
	z_ = qua.z;
	w_ = qua.w;
}

vec4::operator std::string() const
{
	return std::to_string(x_) + ", " + std::to_string(y_) + ", " + std::to_string(z_) + ", " + std::to_string(w_);
}

vec4& vec4::operator+=(const vec4& b)
{
	*this = *this + b;
	return *this;
}

vec4& vec4::operator-=(const vec4& b)
{
	*this = *this - b;
	return *this;
}

vec4& vec4::operator*=(const float& s)
{
	*this = *this * s;
	return *this;
}

vec4& vec4::operator*=(const vec4& b)
{
	*this = *this * b;
	return *this;
}

vec4& vec4::operator/=(const float& s)
{
	assert(s != 0.0f);
	*this = *this / s;
	return *this;
}

vec4& vec4::operator/=(const vec4& b)
{
	assert(b.x_ != 0.0f);
	assert(b.y_ != 0.0f);
	assert(b.z_ != 0.0f);
	assert(b.w_ != 0.0f);
	*this = *this / b;
	return *this;
}

vec4& vec4::operator=(const float rhs[])
{
	x_ = rhs[0];
	y_ = rhs[1];
	z_ = rhs[2];
	w_ = rhs[3];
	return *this;
}

vec4 & vec4::operator=(const vec4 & rhs)
{
  x_ = rhs[0];
  y_ = rhs[1];
  z_ = rhs[2];
  w_ = rhs[3];
  return *this;
}


vec4& vec4::operator++()
{
	++x_;
	++y_;
	++z_;
	++w_;
	return *this;
}

vec4 vec4::operator++(int)
{
	vec4 Temp(x_, y_, z_, w_);
	x_++;
	y_++;
	z_++;
	w_++;
	return Temp;
}

vec4& vec4::operator--()
{
	--x_;
	--y_;
	--z_;
	--w_;
	return *this;
}

vec4 vec4::operator--(int)
{
	vec4 Temp(x_, y_, z_, w_);
	x_--;
	y_--;
	z_--;
	w_--;
	return Temp;
}

float& vec4::operator[](int i)
{
	return ar[i];
}

const float& vec4::operator[](int i) const
{
	return ar[i];
}

float vec4::magnitude() const
{
	return FastSquare(sqrMagnitude());
}

float vec4::sqrMagnitude() const
{
	return (x_*x_ + y_*y_ + z_*z_ + w_*w_);
}

vec4 vec4::normalized() const
{
	vec4 norm;
	vec4Normalize(norm, *this);
	return norm;
}

float vec4::manhattanDistance() const
{
	return abs(x_) + abs(y_) + abs(z_) + abs(w_);
}

void vec4::Normalize()
{
	vec4Normalize(*this, *this);
}

/* ---------------------------------*/
/* ----- Non-Member Functions ----- */
/* ---------------------------------*/
vec4 operator+(const vec4& a, const vec4& b)
{
	return vec4(a.x_ + b.x_, a.y_ + b.y_, a.z_ + b.z_, a.w_ + b.w_);
}

vec4 operator+(const vec4& a)
{
	return a;
}

vec4 operator-(const vec4& a, const vec4& b)
{
	return vec4(a.x_ - b.x_, a.y_ - b.y_, a.z_ - b.z_, a.w_ - b.w_);
}

vec4 operator-(const vec4& a)
{
	return vec4(-a.x_, -a.y_, -a.z_, -a.w_);
}

vec4 operator*(const vec4& a, const float& s)
{
	return vec4(a.x_ * s, a.y_ * s, a.z_ * s, a.w_ * s);
}

vec4 operator*(const float& s, const vec4& a)
{
	return a * s;
}

vec4 operator*(const vec4& a, const vec4& b)
{
	vec4 tmp;
	tmp.x_ = a.x_ * b.x_;
	tmp.y_ = a.y_ * b.y_;
	tmp.z_ = a.z_ * b.z_;
	tmp.w_ = a.w_ * b.w_;
	return tmp;
}

vec4 operator/(const vec4& a, const float& s)
{
	assert(s != 0.0f);
	return vec4(a.x_ / s, a.y_ / s, a.z_ / s, a.w_ / s);
}

vec4 operator/(const vec4& a, const vec4& b)
{
	assert(b.x_ != 0.0f);
	assert(b.y_ != 0.0f);
	assert(b.z_ != 0.0f);
	assert(b.w_ != 0.0f);
	vec4 tmp;
	tmp.x_ = a.x_ / b.x_;
	tmp.y_ = a.y_ / b.y_;
	tmp.z_ = a.z_ / b.z_;
	tmp.w_ = a.w_ / b.w_;
	return tmp;
}

vec4 operator%(const vec4& a, const float& s)
{
	return vec4(fmodf(a.x_, s), fmodf(a.y_, s), fmodf(a.z_, s), fmodf(a.w_, s));
}

//bool operator==(const vec4& a, const vec4& b)
//{
//	return (a.x_ == b.x_ && a.y_ == b.y_ && a.z_ == b.z_ && a.w_ == b.w_);
//}
//
//bool operator!=(const vec4& a, const vec4& b)
//{
//	return !(a == b);
//}

std::ostream & operator<<(std::ostream & os, const vec4 & rhs)
{
	std::cout << "(" << rhs.x_ << "," << rhs.y_ << "," << rhs.z_ << "," << rhs.w_ << ")";
	return os;
}

 vec4 VecNotation(const vec3 & rhs)
{
	return vec4(rhs.x_, rhs.y_, rhs.z_, 0.0f);
}

 vec4 PntNotation(const vec3 & rhs)
{
	return vec4(rhs.x_, rhs.y_, rhs.z_, 1.0f);
}

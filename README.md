# **Underdock Renderer (Work in Progress)**

Underdock Renderer is a simple 3D rendering engine using Vulkan API. 
This engine works hand in hand with an external application called Material Editor, implemented in OpenGL + ImGui. 
The Material Editor enables you to visualize and modify Vulkan resources such as Graphics Pipeline information, Descriptor Sets / Layout and GLSL shader. 
There are other functionality as well, such as packing vertex attribute data in accordance to the vertex shaders' attribute input.

#### **Setup**
Before the application can be compiled, the application will need [**GLM**](https://glm.g-truc.net/0.9.9/index.html) math library and [**Vulkan SDK**](https://vulkan.lunarg.com/sdk/home)
installed in C:/ drive.

\*The main content of the GLM math library should be in a directory like this **C:\glm\glm\\** 

\*The Vulkan SDK version for this project is **1.2.131.1**



#### **Material Editor**
The Material Editor is located at "tools/MaterialEditor/"" folder.

Before building / running Material Editor
*  Open the file "_ProjectLocation" located at "tools/MaterialEditor/
*  Change the directory into your own save directory
*  Open the file "_glslangValidator_loc"
*  Change the directory to the location of "glslangValidator.exe" from Vulkan SDK
*  Save and build / run the application
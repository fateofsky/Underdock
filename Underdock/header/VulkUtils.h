#pragma once
#include <map>
#include <vector>
#include "Renderables.h"

namespace VulkUtils
{
  enum ShaderStage
  {
    VERTEX,
    TESSELLATION_CTRL,
    TESSELLATION_EVAL,
    GEOMETRY,
    FRAGMENT,
    COMPUTE,

    // NVIDIA SPECIFICS
    RAYGEN,
    ANY_HIT,
    CLOSEST_HIT,
    MISS,
    INTERSECTION,
    CALLABLE,
    TASK,
    MESH
  };

  enum VK_BASE_TYPE
  {
    NONE_TYPE = -1,
    INT_TYPE,
    INT4_TYPE,
    FLOAT_TYPE,
    BOOL_TYPE,
    VEC2_TYPE,
    VEC3_TYPE,
    VEC4_TYPE,
    MAT3_TYPE,
    MAT4_TYPE
  };

  struct VulkInterfaceBlockInfo
  {
    unsigned                  _vkGroupSet;         // Read Outside
    unsigned                  _vkGroupBinding;
    unsigned                  _vkBlockSize;
    unsigned                  _vkBlockStride;      // allocation size for this interface block
    unsigned                  _vkTrueBlockStride;  // actual size for this interface block based on num of swapchain imgs
    ShaderStage               _vkStage;
    std::vector<VK_BASE_TYPE> _vkBaseTypeLayout;
  };

  // like a registration object
  struct DescriptorSetInst
  {
    DescriptorSetInst();
    void BindDescriptorSet_Graphics(const unsigned& curFrame, const unsigned& setNumber, VkPipelineLayout setLayout);
    std::vector<VkDescriptorSet>        _set;
  };

  struct VulkDescSetLayout
  {
    VulkDescSetLayout();
    uint8_t* ComputeMemoryLocation(uint8_t* bufMem, unsigned& offsetCompute, const unsigned& instIndex,
                                   const unsigned& blockID, const unsigned& frameIndex);
    VkWriteDescriptorSet ComputeWriteSet_UniformBuffer(VkDescriptorSet vSet, VkBuffer vBuf, VkDeviceSize offset, 
                                                       VkDeviceSize range, uint32_t binding);
    VkWriteDescriptorSet ComputeWriteSet_CombineTextureSampler(VkDescriptorSet vSet, VkImageView vImgView, VkSampler vImgSampler, uint32_t binding);

    DescriptorSetInst& GetSetInstance(const unsigned& instanceID);

    unsigned                            _refCount;              // how many object register with this set
    unsigned                            _setStartingOffset;
    unsigned                            _setStride;
    unsigned                            _trueSetStride;
    std::vector<DescriptorSetInst>      _setInstance;           // number of object instances refer to this particular layout
    std::vector<VulkInterfaceBlockInfo> _setInterfaceBlocks;    // number of different interface block from the same set number
    VkDescriptorSetLayout               _vkDSLayout;            // descriptor layout itself
  };

  struct VulkShaderInfo
  {
    VkShaderModuleCreateInfo _vkSdrInfo;
    VkShaderModule           _vkSdrModule;
  };

  struct _VulkModelInfo
  {
    struct _VulkMeshInfo
    {
      unsigned          _meshOffset;
      unsigned          _indicesOffset;

      unsigned          _indexSize;
      unsigned          _vertexSize;

      //unsigned          _computedAllocMeshSize;
      //unsigned          _computedAllocIndicesSize;

      std::vector<char> _tmpMeshData;
      std::vector<char> _tmpIndicesData;
    };

    void LoadModelBuffer_1(const std::string& file);
    void CreateQuad();

    std::vector<_VulkMeshInfo> _meshInfoList;
  };

  struct VulkVBStorage
  {
    std::vector<char> _fullMeshData;
    std::vector<char> _fullIndicesData;
    void CompileModel(std::vector<_VulkModelInfo>& modelList);
  };


  struct VulkRenderingTree
  {
    struct _GlobalNode
    {
      struct _SharedNode
      {
        struct _InstanceNode
        {
          Renderables* _renderable;
        };

        Renderables* _sharedRenderable;
        std::vector<_InstanceNode> _instanceNodes;
      };

      Renderables* _globalRenderable;
      std::vector<_SharedNode> _sharedNodes;
    };

    void CreateGlobalNode(Renderables* rend);
    void CreateSharedNode(Renderables* rend, const unsigned& globalIndex);
    void CreateInstanceNode(Renderables* rend, const unsigned& globalIndex, const unsigned& sharedIndex);

    std::vector<_GlobalNode> _globalNodes;
  };

  VkShaderStageFlagBits DetermineShaderStage(ShaderStage stage);
  unsigned ComputeSizeOfVkBaseType(VK_BASE_TYPE baseType);
}
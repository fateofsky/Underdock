#include "pch.h"
#include "VertexBuffer.h"
#include "ModelLoader.h"

void VertexBuffer::Create(GLsizeiptr dataSize, const void* data,
                          GLsizeiptr indicesSize, const void* indicesData,
                          const vBufferLayout& bufLayout)
{
  // creation
  glGenVertexArrays(1, &m_vao);
  glGenBuffers(2, &m_vbo);

  // bindings
  glBindVertexArray(m_vao);
  glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
  glBufferData(GL_ARRAY_BUFFER, dataSize, data, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesSize, indicesData, GL_STATIC_DRAW);

  unsigned vPackCount = ModelUtils::ComputePerVertexPackSize(bufLayout);

  // vertex array pointer
  GLuint layoutSize = (GLuint)bufLayout.layout.size();
  long long ptrOffset = 0;
  for (GLuint i = 0; i < layoutSize; ++i)
  {
    vBufferLayout::vBufferLayoutType type = bufLayout.layout[i];
    glEnableVertexAttribArray(i);

    switch (type)
    {
    case vBufferLayout::VERTEX_POS:
    case vBufferLayout::VERTEX_NORMAL:
    case vBufferLayout::VERTEX_TANGENT:
    case vBufferLayout::VERTEX_BITANGENT:
      glVertexAttribPointer(i, 4, GL_FLOAT, GL_FALSE, vPackCount, (GLvoid*)ptrOffset);
      ptrOffset += 4 * sizeof(float);
      break;

    case vBufferLayout::VERTEX_BONE_ID:
      glVertexAttribPointer(i, 4, GL_UNSIGNED_INT, GL_FALSE, vPackCount, (GLvoid*)ptrOffset);
      ptrOffset += 4 * sizeof(unsigned);
      break;

    case vBufferLayout::VERTEX_BONE_WEIGHT:
      glVertexAttribPointer(i, 4, GL_FLOAT, GL_FALSE, vPackCount, (GLvoid*)ptrOffset);
      ptrOffset += 4 * sizeof(float);
      break;

    case vBufferLayout::TEX_COORDUV0:
    case vBufferLayout::TEX_COORDUV1:
    case vBufferLayout::TEX_COORDUV2:
    case vBufferLayout::TEX_COORDUV3:
      glVertexAttribPointer(i, 2, GL_FLOAT, GL_FALSE, vPackCount, (GLvoid*)ptrOffset);
      ptrOffset += 2 * sizeof(float);
      break;
    }
  }

  glBindVertexArray(0);

  // cleanup
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}

//void VertexBuffer::ResetAttribPointerLayout(const vBufferLayout & bufLayout) const
//{
//  glBindVertexArray(m_vao);
//  glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
//  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
//
//  unsigned vPackCount = ModelUtils::ComputePerVertexPackSize(bufLayout);
//
//  // vertex array pointer
//  GLuint layoutSize = (GLuint)bufLayout.layout.size();
//  long long ptrOffset = 0;
//  for (GLuint i = 0; i < layoutSize; ++i)
//  {
//    vBufferLayout::vBufferLayoutType type = bufLayout.layout[i];
//    glEnableVertexAttribArray(i);
//
//    switch (type)
//    {
//    case vBufferLayout::VERTEX_POS:
//    case vBufferLayout::VERTEX_NORMAL:
//    case vBufferLayout::VERTEX_TANGENT:
//    case vBufferLayout::VERTEX_BITANGENT:
//      glVertexAttribPointer(i, 4, GL_FLOAT, GL_FALSE, vPackCount, (GLvoid*)ptrOffset);
//      ptrOffset += 4 * sizeof(float);
//      break;
//
//    case vBufferLayout::VERTEX_BONE_ID:
//      glVertexAttribPointer(i, 4, GL_UNSIGNED_INT, GL_FALSE, vPackCount, (GLvoid*)ptrOffset);
//      ptrOffset += 4 * sizeof(unsigned);
//      break;
//
//    case vBufferLayout::VERTEX_BONE_WEIGHT:
//      glVertexAttribPointer(i, 4, GL_FLOAT, GL_FALSE, vPackCount, (GLvoid*)ptrOffset);
//      ptrOffset += 4 * sizeof(float);
//      break;
//
//    case vBufferLayout::TEX_COORDUV0:
//    case vBufferLayout::TEX_COORDUV1:
//    case vBufferLayout::TEX_COORDUV2:
//    case vBufferLayout::TEX_COORDUV3:
//      glVertexAttribPointer(i, 2, GL_FLOAT, GL_FALSE, vPackCount, (GLvoid*)ptrOffset);
//      ptrOffset += 2 * sizeof(float);
//      break;
//    }
//  }
//
//  glBindVertexArray(0);
//
//  // cleanup
//  glBindBuffer(GL_ARRAY_BUFFER, 0);
//  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
//}

void VertexBuffer::Destroy()
{
  glDeleteVertexArrays(1, &m_vao);
  glDeleteBuffers(2, &m_vbo);
}

void VertexBuffer::DrawBuffer(unsigned size)
{
  glBindVertexArray(m_vao);
  glDrawElements(GL_TRIANGLES, size, GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);
}

void VertexBuffer::DrawBuffer(unsigned size) const
{
  glBindVertexArray(m_vao);
  glDrawElements(GL_TRIANGLES, size, GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);
}

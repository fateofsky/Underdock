#include "pch.h"
#include "Parameters.h"

ParameterBase::ParameterBase(_PARAM_TYPE type)
  : m_type(type)
{ }

_PARAM_TYPE ParameterBase::GetType() const
{
  return m_type;
}

GameObjectParam::GameObjectParam()
  : ParameterBase(_PARAM_TYPE::GAMEOBJECT)
{ }

void GameObjectParam::Serialize(MemoryStream& ofs)
{
  ofs.Write(m_id);
}

void GameObjectParam::Deserialize(MemoryStream & ifs)
{
  ifs.Read(&m_id, sizeof(unsigned));
}

unsigned & GameObjectParam::GetID()
{
  return m_id;
}

ComponentParam::ComponentParam()
  : ParameterBase(_PARAM_TYPE::COMPONENT)
{ }

void ComponentParam::Serialize(MemoryStream& ofs)
{
  ofs.Write(m_owner);
  ofs.Write(m_id);
}

void ComponentParam::Deserialize(MemoryStream & ifs)
{
  ifs.Read(&m_owner, sizeof(unsigned) << 1);
}

unsigned& ComponentParam::GetOwner()
{
  return m_owner;
}

unsigned& ComponentParam::GetID()
{
  return m_id;
}

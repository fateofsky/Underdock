/* Start Header
*****************************************************************/
/*!
\file      MathVector4D.h
\author
\par
\date
\brief

Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the
prior written consent of DigiPen Institute of Technology is prohibited.
*/
/* End Header
*******************************************************************/
#pragma once
#include "MathLib.h"

union vec2;
union vec3;
class quat;

///////////
// vec4 //--------------------------------------------------------------
///////////
union vec4
{
  struct
  {
    float x_;
    float y_;
    float z_;
    float w_;
  };


  float ar[4];

  vec4(int x, int y, int z, int a);
  vec4(float x = 0.0f, float y = 0.0f, float z = 0.0f, float a = 0.0f);
  vec4(const vec3& v, float w_ = 0.0f);
  vec4(const vec2& v, float z_ = 0.0f, float w_ = 0.0f);
  vec4(const std::string& str);
  vec4(const quat& qua);

  operator std::string() const;
  vec4& operator+=(const vec4& b);
  vec4& operator-=(const vec4& b);
  vec4& operator*=(const float& s);
  vec4& operator*=(const vec4& b);
  vec4& operator/=(const float& s);
  vec4& operator/=(const vec4& b);
  vec4& operator=(const float rhs[]);
  vec4& operator=(const vec4& rhs);
  vec4& operator++();
  vec4 operator++(int);
  vec4& operator--();
  vec4 operator--(int);
  float& operator[](int i);
  const float& operator[](int i) const;

  // Properties
  float magnitude() const;
  float sqrMagnitude() const;
  vec4 normalized() const;
  float manhattanDistance() const;

  // Method
  void Normalize();
};

vec4 operator+(const vec4& a, const vec4& b);
vec4 operator+(const vec4& a);
vec4 operator-(const vec4& a, const vec4& b);
vec4 operator-(const vec4& a);
vec4 operator*(const vec4& a, const float& s);
vec4 operator*(const float& s, const vec4& a);
vec4 operator*(const vec4& a, const vec4& b);
vec4 operator/(const vec4& a, const float& s);
vec4 operator/(const vec4& a, const vec4& b);
vec4 operator%(const vec4& a, const float& s);
std::ostream & operator<<(std::ostream & os, const vec4 & rhs);
vec4 VecNotation(const vec3& rhs);
vec4 PntNotation(const vec3& rhs);
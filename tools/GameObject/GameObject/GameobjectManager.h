#pragma once
#include <map>
#include "Gameobject.h"
#include "GameobjectHandle.h"
#include "ComponentRTTIFunc.h"

class Gameobject;

class GameobjectManager
{
  friend class GameobjectHandle;

  public:
    static GameobjectManager* GetInstance();

    GameobjectHandle GetGameobjectHandle(const unsigned& objID);
    unsigned CreateGameobject();
    void DestroyGameobject(const unsigned& objID);
    void DestroyGameobject(const GameobjectHandle& objHC);
    void DestroyAllGameobject();

    // serialization
    void Serialize(const std::string& file);
    void Deserialize(const std::string& file);

  private:
    GameobjectManager();
    Gameobject* GetGameobject(const unsigned& objID);
    void        CreateGameobject(unsigned ObjID); // * Deserialization only

    static GameobjectManager* m_instance;

    std::map<unsigned, Gameobject*> m_gameObjectMap;
};
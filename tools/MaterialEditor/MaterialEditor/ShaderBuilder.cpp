#include "pch.h"
#include "ShaderBuilder.h"
#include "OGLStringElement.h"
#include "AppManager.h"
#include "WindowSystem.h"

/*
  -- COMMON SHADER TEXT
*/
const std::string mainSemiColon{ ";" };
const std::string mainHeader{ "#version 450 core\n\n" };
const std::string mainOpening{ "\nvoid main()\n{\n" };
const std::string mainClosing{ "\n}\n" };
const std::string mainSamplingTex{ "uniform sampler2D " };
const std::string mainTexture{ "texture" };            // texture0, texture1...
const std::string mainUniform{ "uniform " };
const std::string mainInput{ "in " };
const std::string mainOutput{ "out " };
const std::string startingBlk{ "\n{\n" };
const std::string endingBlk{ "\n}\n" };
const std::string tmpOutName{ "tmp" };
char commandLineBuffer[1024]{ '\0 ' };
/*
  -- LOCAL HELPER FUNCTIONS
*/
std::string SetLayoutLocationStr_OGL(unsigned index)
{
  return std::string("layout (location = ") + std::to_string(index) + ") ";
}

std::string SetLayoutBindingStr(unsigned index)
{
  return std::string("layout (binding = ") + std::to_string(index) + ") ";
}

std::string SetLayoutBindingStr_Vulkan(UniformGroup::_MemoryLayout mLayout, unsigned index, unsigned setGroup)
{
  std::string memLayoutStr = "";
  switch (mLayout)
  {
  case UniformGroup::STD140:
  case UniformGroup::STD430:
    memLayoutStr = oglStrElem::uniMemLayoutItems[mLayout].name;
    memLayoutStr += ", ";
    break;
  }
  return std::string("layout (") + memLayoutStr + std::string("set = ") + std::to_string(setGroup) + std::string(", binding = ") + std::to_string(index) + ") ";
}

std::string SetTextureStr_OGL(unsigned index)
{
  return mainSamplingTex + std::to_string(index);
}

void ReplaceString(std::string& str, const std::string& newStr, const std::string& oldStr)
{
  std::string::size_type pos = 0u;
  while ((pos = str.find(oldStr, pos)) != std::string::npos)
  {
    str.replace(pos, oldStr.length(), newStr);
    pos += newStr.length();
  }
}

std::string ConstructInputNameBasedOnStage(Shader::ShaderType currentStage)
{
  switch (currentStage)
  {
  case Shader::VERTEXSDR:
    return "v_in";

  case Shader::TESSCTRLSDR:
    return "tc_in";

  case Shader::TESSEVALSDR:
    return "te_in";

  case Shader::GEOMSDR:
    return "g_in";

    // Cannot have input have based on fragment shader
    // because fragment shader is the last stage in the pipeline
  case Shader::FRAGSDR:
    throw - 1;
    return "";
  }

  return " error ";
}

std::string ConstructOutputNameBasedOnStage(Shader::ShaderType currentStage)
{
  switch (currentStage)
  {
  case Shader::VERTEXSDR:
    return "v_out";

  case Shader::TESSCTRLSDR:
    return "tc_out";

  case Shader::TESSEVALSDR:
    return "te_out";

  case Shader::GEOMSDR:
    return "g_out";

  case Shader::FRAGSDR:
    return "f_out";
  }

  return " error ";
}

std::string ConstructInputName(PipelineLayout& pipeline, Shader::ShaderType currentStage)
{
  Shader::ShaderType previousStage;
  if (pipeline.FindPreviousStage(currentStage, previousStage))
    return ConstructOutputNameBasedOnStage(previousStage);
  else return ConstructInputNameBasedOnStage(currentStage);
}

// setNumber is the key
unsigned FindBindingNumber(std::map<unsigned, unsigned>& bindCounter, const unsigned& setNumber)
{
  if (bindCounter.find(setNumber) == bindCounter.end())
  {
    bindCounter[setNumber] = 1;
    return 0;
  }
  else
  {
    unsigned retVal = bindCounter[setNumber]++;
    return retVal;
  }
}

unsigned RoundAlignmentToVulkanCompatible(const unsigned& currentSize)
{
  const unsigned alignmentReference[3]{ sizeof(float), sizeof(float) << 1, sizeof(float) << 2 };
  for (int i = 0; i < 3; ++i)
  {
    if (currentSize <= alignmentReference[i]) return alignmentReference[i];
  }
  return alignmentReference[2];
}


/*
  - LOCAL STRUCT
*/

struct VulkanDescriptorInfo
{
  enum _DescriptorType
  {
    UNIFORM_BUFFER,
    COMBINE_TEXTURE_SAMPLER
  };

  unsigned                  _vkGroupSet;
  unsigned                  _vkGroupBinding;
  unsigned                  _vkBlockSize;
  Shader::ShaderType        _vkStage;
  _DescriptorType           _vkDescType;
  std::vector<OGL_BASETYPE> _vkBaseTypeLayout;

  void Serialize(MemoryStream& out)
  {
    //out.Write(_vkGroupSet); // For sorting only
    out.Write(_vkGroupBinding);
    out.Write(_vkBlockSize);
    out.Write((int)_vkStage);
    out.Write(_vkBaseTypeLayout.size());
    for (auto& elem : _vkBaseTypeLayout)
      out.Write((int)elem);
  }
};

const std::vector<unsigned> vulkanGroupBlockSizeList
{
  sizeof(int),           // int
  sizeof(int) << 2,      // int4
  sizeof(float),         // float
  sizeof(bool),          // bool
  sizeof(float) << 1,    // vec2
  sizeof(float) * 3,     // vec3
  sizeof(float) << 2,    // vec4
  sizeof(float) * 9,     // mat3
  sizeof(float) << 4,    // mat4
};

ShaderBuilder* ShaderBuilder::m_instance = nullptr;
ShaderBuilder::ShaderBuilder()
{ }

ShaderBuilder* ShaderBuilder::GetInstance()
{
  return m_instance;
}

void ShaderBuilder::Initialize()
{
  m_instance = new ShaderBuilder();
}

void ShaderBuilder::Destroy()
{
  delete m_instance;
}

bool ShaderBuilder::BuildShader_OGL(MaterialLayout* layout, bool debugPrint)
{
  ResourceManager* rm = ResourceManager::GetInstance();
  Shader* prevSdr = rm->GetShader(layout->m_shaderName);
  MaterialLayout::BindMap uniformLocMap;

  // build shader based on pipeline
  PipelineLayout::pipelineStageMap& stageMap = layout->m_resourceLayout.GetPipelineStageMap();
  Shader newShader;
  std::string _inputName, _outputName;
  int uniformCount = 0;

  for (auto& stage : stageMap)
  {
    std::string fullShaderCode = mainHeader;

    // determine input / output prefix
    _inputName = ConstructInputName(layout->m_resourceLayout, stage.first);

    // construct inputs
    unsigned locaCount = 0;
    if (stage.first == Shader::ShaderType::VERTEXSDR || stage.first == Shader::ShaderType::FRAGSDR)
    {
      for (auto& input : stage.second.baseInput)
      {
        fullShaderCode += SetLayoutLocationStr_OGL(locaCount);       // layout (location = x) 
        fullShaderCode += mainInput;                                 // in 
        fullShaderCode += oglStrElem::dataTypeItems[input].name;     // type
        fullShaderCode += " ";                                       // space
        fullShaderCode += ConstructInputName(layout->m_resourceLayout, stage.first) + std::to_string(locaCount++); // inputName
        fullShaderCode += mainSemiColon + "\n";
      }
    }
    else if (stage.first == Shader::ShaderType::GEOMSDR)
    {
      // TODO: NOT SUPPORTED YET
    }
    else if (stage.first == Shader::ShaderType::TESSCTRLSDR)
    {
      // TODO: NOT SUPPORTED YET
    }
    else if (stage.first == Shader::ShaderType::TESSEVALSDR)
    {
      // TODO: NOT SUPPORTED YET
    }

    fullShaderCode += "\n";

    // construct uniform
    for (auto& uniform : stage.second.inputUniforms)
    {
      OGL_BASETYPE type = uniform.second->GetType();
      fullShaderCode += SetLayoutLocationStr_OGL(uniformCount);    // layout (location = x) 
      fullShaderCode += mainUniform;                               // uniform
      fullShaderCode += oglStrElem::dataTypeItems[type].name;      // type
      fullShaderCode += " ";                                       // space
      fullShaderCode += uniform.first;                             // uniform name
      fullShaderCode += mainSemiColon + "\n";
      uniformLocMap[uniform.first] = uniformCount++;               // assign the location to map
    }

    fullShaderCode += '\n';

    // construct textures
    locaCount = 0;
    std::string _textureName;
    for (auto& texture : stage.second.inputTextures)
    {
      _textureName = mainTexture + std::to_string(locaCount++);  // construct texture name + count

      fullShaderCode += SetLayoutBindingStr(uniformCount);       // layout (binding = x)
      fullShaderCode += mainSamplingTex;                         // uniform sampler2D 
      fullShaderCode += _textureName;                            // texture0, texture1...
      fullShaderCode += mainSemiColon + "\n";
      uniformLocMap[texture.first] = uniformCount++;             // assign the location to map
    }

    fullShaderCode += '\n';

    // construct outputs
    locaCount = 0;
    for (auto& output : stage.second.baseOutput)
    {
      fullShaderCode += SetLayoutLocationStr_OGL(locaCount);       // layout (location = x) 
      fullShaderCode += mainOutput;                                // out
      fullShaderCode += oglStrElem::dataTypeItems[output].name;    // type
      fullShaderCode += " ";                                       // space
      fullShaderCode += ConstructOutputNameBasedOnStage(stage.first) + std::to_string(locaCount++); // outputName
      fullShaderCode += mainSemiColon + "\n";
    }

    // main shader implementation
    fullShaderCode += mainOpening;
    fullShaderCode += stage.second.shaderMainCode;
    fullShaderCode += mainClosing;

    if (newShader.CreateShader(fullShaderCode.c_str(), stage.first) == false)
    {
      layout->m_resourceLayout.SetMessageFlag(LAYOUTMSG::msg::SHADERBUILDFAILED);

      // Debug cout
      if (debugPrint) std::cout << fullShaderCode;

      return false;
    }
  }

  if (newShader.CreateProgram())
  {
    if (prevSdr == nullptr)
    {
      rm->CreateEmptyShader(layout->m_shaderName);
      layout->m_shader = rm->GetShader(layout->m_shaderName);
    }
    else
    {
      layout->m_shader->Destroy();
    }

    layout->m_shader->CopyFrom(newShader);
    layout->m_bindUniformMap = uniformLocMap;
    return true;
  }
  return false;
}


bool ShaderBuilder::ExportPipelineBinary_Vulkan(MaterialLayout* layout, const std::string& saveOutput, bool RawShader)
{
  PipelineLayout::pipelineStageMap& stageMap = layout->m_resourceLayout.GetPipelineStageMap();
  std::map<std::string, unsigned>            uniformLocMap;
  std::map<std::string, unsigned>            groupLocMap;
  std::map<Shader::ShaderType, SPIR_V_OBJ>   shaderCodeMap;
  std::map<unsigned, unsigned>               bindCounter;
  std::string                                exportFullShader = "";
  const UniformGroup::_MemoryLayout          noMemLayout = UniformGroup::_MemoryLayout::NONE;

  // build shader based on pipeline
  for (auto& stage : stageMap)
  {
    std::string fullShaderCode = "";
    if (RawShader)
    {
      fullShaderCode += oglStrElem::uniformStageItems[stage.first].name;
      fullShaderCode += "\n############################################################\n\n";
    }
    fullShaderCode += mainHeader;

    // construct inputs
    unsigned locaCount = 0;
    if (stage.first == Shader::ShaderType::VERTEXSDR || stage.first == Shader::ShaderType::FRAGSDR)
    {
      for (auto& input : stage.second.baseInput)
      {
        fullShaderCode += SetLayoutLocationStr_OGL(locaCount);       // layout (location = x) 
        fullShaderCode += mainInput;                                 // in 
        fullShaderCode += oglStrElem::dataTypeItems[input].name;     // type
        fullShaderCode += " ";                                       // space
        fullShaderCode += ConstructInputName(layout->m_resourceLayout, stage.first) + std::to_string(locaCount++); // inputName
        fullShaderCode += mainSemiColon + "\n";
      }
    }
    else if (stage.first == Shader::ShaderType::GEOMSDR)
    {
      // TODO: NOT SUPPORTED YET
    }
    else if (stage.first == Shader::ShaderType::TESSCTRLSDR)
    {
      // TODO: NOT SUPPORTED YET
    }
    else if (stage.first == Shader::ShaderType::TESSEVALSDR)
    {
      // TODO: NOT SUPPORTED YET
    }

    fullShaderCode += "\n";

    // construct struct for group uniforms
    ResourcesMap nonGroupUniformMap = stage.second.inputUniforms;
    for (auto& group : stage.second.uniformGroups)
    {
      unsigned setNumber = group.second.GetGroup();
      UniformGroup::_MemoryLayout mLayout = group.second.GetMemoryLayout();
      unsigned bindNumber = FindBindingNumber(bindCounter, setNumber);
      groupLocMap[group.first] = bindNumber;
      fullShaderCode += SetLayoutBindingStr_Vulkan(mLayout, bindNumber, setNumber);   // layout (set = x, binding = y)
      fullShaderCode += mainUniform;                                                  // uniform
      fullShaderCode += group.first + "_";                                            // group/struct/class name appended with _ (underscore)
      fullShaderCode += startingBlk;                                                  // {

      const std::vector<std::string>& glist = group.second.GetGroupList();
      for (auto& uName : glist)
      {
        UniformResourceBase* urb = layout->GetUniform(uName);
        if (urb)
        {
          fullShaderCode += oglStrElem::dataTypeItems[urb->GetType()].name;           // type
          fullShaderCode += " ";                                                      // space
          fullShaderCode += uName;                                                    // uniform name
          fullShaderCode += mainSemiColon + "\n";
          nonGroupUniformMap.erase(uName);
        }
      }

      fullShaderCode += "} ";
      fullShaderCode += group.first + ";\n\n";
    }

    fullShaderCode += '\n';

    // DETECT NON GROUPED UNIFORMS (CANNOT USE UNIFORM WITHOUT INTERFACE BLOCK)
    for (auto& nonGUniform : nonGroupUniformMap)
    {
      std::cout << "Error: Non grouped uniform " << nonGUniform.first << std::endl;
      return false;
    }

    fullShaderCode += '\n';

    // construct textures
    locaCount = 0;
    std::string _textureName;
    for (auto& texture : stage.second.inputTextures)
    {
      unsigned setNumber = texture.second->GetGroup();
      unsigned bindNumber = FindBindingNumber(bindCounter, setNumber);
      _textureName = mainTexture + std::to_string(locaCount++);                         // construct texture name + count
      fullShaderCode += SetLayoutBindingStr_Vulkan(noMemLayout, bindNumber, setNumber); // layout (binding = x)
      fullShaderCode += mainSamplingTex;                                                // uniform sampler2D 
      fullShaderCode += _textureName;                                                   // texture0, texture1...
      fullShaderCode += mainSemiColon + "\n";
      uniformLocMap[texture.first] = bindNumber;                                        // assign the location to map (binding)
    }

    fullShaderCode += '\n';

    // construct outputs
    locaCount = 0;
    for (auto& output : stage.second.baseOutput)
    {
      fullShaderCode += SetLayoutLocationStr_OGL(locaCount);       // layout (location = x) 
      fullShaderCode += mainOutput;                                // out
      fullShaderCode += oglStrElem::dataTypeItems[output].name;    // type
      fullShaderCode += " ";                                       // space
      fullShaderCode += ConstructOutputNameBasedOnStage(stage.first) + std::to_string(locaCount++); // outputName
      fullShaderCode += mainSemiColon + "\n";
    }

    std::string code = stage.second.shaderMainCode;
    for (auto& group : stage.second.uniformGroups)
    {
      const std::vector<std::string>& glist = group.second.GetGroupList();
      for (auto& uName : glist)
      {
        // find and replace uName with groupName.uName
        std::string newString = group.first + "." + uName;
        ReplaceString(code, newString, uName);
      }
    }

    // main shader implementation
    fullShaderCode += mainOpening;
    fullShaderCode += code;
    fullShaderCode += mainClosing;

    if (RawShader)
    {
      fullShaderCode += "\n############################################################\n\n\n";
      exportFullShader += fullShaderCode;
    }
    else
    {
      // save shader code raw
      std::string tmpPath = WinSystem::GetTempFilePath();
      ReplaceString(tmpPath, "/", "\\");
      std::string outPutFileName = tmpOutName + oglStrElem::uniformStageExtItems[stage.first].name;
      std::string tmpOutputFileNameFull = tmpPath + outPutFileName;
      {
        MemoryStream tmpOut;
        tmpOut.BeginWrite();
        size_t _strSize = strlen(fullShaderCode.c_str()) + 1;
        tmpOut.Write(fullShaderCode.c_str(), _strSize);
        tmpOut.Flush(tmpOutputFileNameFull);
      }

      // Build command line to invoke process to generate SPIR-V
      std::string tmpOutputFileNameBinFull = tmpPath + outPutFileName + "bin";
      std::string fullCmdLine = WinSystem::glslangExe + " -V " + tmpOutputFileNameFull + " -o " + tmpOutputFileNameBinFull;
      memset(commandLineBuffer, '\0', 1024);
      memcpy_s(commandLineBuffer, fullCmdLine.size(), fullCmdLine.c_str(), fullCmdLine.size());
      WinSystem::InvokeProcess(WinSystem::glslangExe.c_str(), commandLineBuffer);

      // Read back binary
      std::streampos shaderBinaryLen = 0;
      {
        MemoryStream tmpIn;
        if (tmpIn.FileRead(tmpOutputFileNameBinFull))
        {
          SPIR_V_OBJ spObj;
          char* data = tmpIn.GetStreamedData(spObj._dataSize);
          spObj._data = new char[spObj._dataSize];
          memcpy_s(spObj._data, spObj._dataSize, data, spObj._dataSize);
          shaderCodeMap[stage.first] = spObj;
        }
        else
        {
          // clear previous memory before returning
          for (auto& code : shaderCodeMap)
            delete[] code.second._data;
          return false;
        }
      }

      WinSystem::ClearTempFile(tmpOutputFileNameFull.c_str());
      WinSystem::ClearTempFile(tmpOutputFileNameBinFull.c_str());
    }
  }

  MemoryStream outfile;
  outfile.BeginWrite();

  // Save raw shader file
  if (RawShader)
  {
    outfile.Write(exportFullShader.c_str(), exportFullShader.size());
    return outfile.Flush(saveOutput);
  }
  // Save pipeline layout
  // ENUM TYPE USES INTEGER (FOR PORTABILITY)
  else
  {
    // save stage size
    outfile.Write(stageMap.size());
    std::vector<VulkanDescriptorInfo> descriptorInfoList;
    for (auto& stage : stageMap)
    {
      outfile.Write((int)stage.first);                      // stage type

      // WRITE SPIR-V SHADER
      outfile.Write(shaderCodeMap[stage.first]._dataSize);                                   // size of the SPIR-V binary data
      outfile.Write(shaderCodeMap[stage.first]._data, shaderCodeMap[stage.first]._dataSize); // SPIR-V binary data


      // SEPARATE GROUPED UNIFORMS
      for (auto& group : stage.second.uniformGroups)
      {
        VulkanDescriptorInfo info;
        info._vkStage = stage.first;
        info._vkGroupSet = group.second.GetGroup();
        info._vkGroupBinding = groupLocMap[group.first];
        info._vkDescType = VulkanDescriptorInfo::_DescriptorType::UNIFORM_BUFFER;
        info._vkBaseTypeLayout.resize(group.second.GetGroupCount());

        std::vector<std::string>& groupList = group.second.GetGroupList();
        unsigned _blockSize = 0;

        // find nearest alignment up to 16
        unsigned alignment = 0;
        for (size_t listCount = 0; listCount < groupList.size(); ++listCount)
        {
          UniformResourceBase* urb = layout->GetUniform(groupList[listCount]);
          if (urb == nullptr) continue;

          // round to nearest 4, 8, or 16
          if (vulkanGroupBlockSizeList[urb->GetType()] > alignment)
            alignment = RoundAlignmentToVulkanCompatible(vulkanGroupBlockSizeList[urb->GetType()]);
        }

        for (size_t listCount = 0; listCount < groupList.size(); ++listCount)
        {
          UniformResourceBase* urb = layout->GetUniform(groupList[listCount]);
          if (urb == nullptr) continue;
          info._vkBaseTypeLayout[listCount] = urb->GetType();
          unsigned memberDataSize = vulkanGroupBlockSizeList[urb->GetType()];
          _blockSize += memberDataSize < alignment ? alignment : memberDataSize;
        }
        info._vkBlockSize = _blockSize;
        descriptorInfoList.push_back(info);
      }

      // SEPARATE TEXTURE UNIFORMS
      for (auto& texture : stage.second.inputTextures)
      {
        VulkanDescriptorInfo info;
        info._vkStage = stage.first;
        info._vkGroupSet = texture.second->GetGroup();
        info._vkGroupBinding = uniformLocMap[texture.first];
        info._vkDescType = VulkanDescriptorInfo::_DescriptorType::COMBINE_TEXTURE_SAMPLER;
        info._vkBlockSize = 0;
        descriptorInfoList.push_back(info);
      }
    }

    // group descriptorInfoList by sets
    std::map<unsigned, std::vector<VulkanDescriptorInfo*>> descSetMaps;
    for (auto& descInfo : descriptorInfoList)
      descSetMaps[descInfo._vkGroupSet].push_back(&descInfo);

    // Save descriptor set based on set number
    outfile.Write(descSetMaps.size());  // Total number of sets
    for (auto& set : descSetMaps)
    {
      outfile.Write(set.first);         // set number
      outfile.Write(set.second.size()); // number of unique descriptor in the set
      for (auto& desc : set.second)
        desc->Serialize(outfile);       // infomation
    }

    /*
      Vertex input state
    */
    std::vector<OGL_BASETYPE> VertexInputType;
    if (stageMap.find(Shader::ShaderType::VERTEXSDR) != stageMap.end())
    {
      VertexInputType = stageMap[Shader::ShaderType::VERTEXSDR].baseInput;
      outfile.Write(stageMap[Shader::ShaderType::VERTEXSDR].baseInput.size());
      for (auto& bInput : stageMap[Shader::ShaderType::VERTEXSDR].baseInput)
        outfile.Write((int)bInput);
    }
    else throw - 1;


    /*
      Has render target
    */
    //outfile.Write(layout->m_resourceLayout.GetHasRenderTarget());
    //if (layout->m_resourceLayout.GetHasRenderTarget())
    //{
    //  // write dimensions
    //  outfile.Write(layout->m_resourceLayout.GetRenderTargetDimension(), sizeof(int) << 1);

    //  // write enable depth to texture
    //  outfile.Write(layout->m_resourceLayout.GetEnableDepthToTexture());

    //  // write dependencies
    //  const auto& depenList = layout->m_resourceLayout.GetDependenciesList();
    //  outfile.Write(depenList.size());
    //  for (auto& dependency : depenList)
    //    outfile.Write(dependency);

    //  // write attachment output format
    //  const auto& mrtOutputList = layout->m_resourceLayout.GetMRTOutputList();
    //  outfile.Write(mrtOutputList.size());
    //  for (auto& outputFormat : mrtOutputList)
    //    outfile.Write(outputFormat);


    //  // write image attachment usage
    //  const auto& mrtOutputUsageList = layout->m_resourceLayout.GetMRTOutputUsageList();
    //  outfile.Write(mrtOutputUsageList.size());
    //  for (auto& outputUsage : mrtOutputUsageList)
    //    outfile.Write(outputUsage);
    //}


    /*
      Export Vulkan graphics pipeline information
    */
    VkPipelineInputAssemblyStateCreateInfo& inputAssemblyState = layout->m_resourceLayout.GetInputAssemblyInfo();
    VkPipelineRasterizationStateCreateInfo& rasterState = layout->m_resourceLayout.GetRasterizationStateInfo();
    VkPipelineColorBlendStateCreateInfo& colorBlendState = layout->m_resourceLayout.GetColorBlendAttStateInfo();
    VkPipelineDepthStencilStateCreateInfo& depthStencilState = layout->m_resourceLayout.GetDepthStencilStateInfo();
    std::vector<VkDynamicState>& dynamicStateList = layout->m_resourceLayout.GetDynamicStateList();
    std::vector<VkPipelineColorBlendAttachmentState>& cbAttStateList = layout->m_resourceLayout.GetColorBlendAttStateList();


    // write VkDynamicState
    outfile.Write(dynamicStateList.size());
    for (auto& dState : dynamicStateList)
      outfile.Write(dState);


    // Run-time generated and write VkVertexInputBindingDescription
    VkVertexInputBindingDescription viBinding;
    viBinding.binding = 0;
    viBinding.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    viBinding.stride = ModelUtils::ComputePerVertexPackSize(layout->m_vBufferLayout);
    outfile.Write(viBinding);

    std::vector<VkVertexInputAttributeDescription> attDescList;
    auto vkFormatList = layout->m_resourceLayout.ComputeListOfVkFormats(VertexInputType);
    attDescList.resize(VertexInputType.size());
    unsigned currentOffset = 0;
    for (size_t i = 0; i < VertexInputType.size(); ++i)
    {
      attDescList[i].binding = 0;
      attDescList[i].location = uint32_t(i);
      attDescList[i].format = vkFormatList[i];
      attDescList[i].offset = currentOffset;
      currentOffset += ModelUtils::ComputeElementOffset(VertexInputType[i]);
    }

    // Run-time generated and write VkVertexInputAttributeDescription
    outfile.Write(attDescList.size());
    for (auto& description : attDescList)
      outfile.Write(description);

    // write VkPipelineInputAssemblyStateCreateInfo
    outfile.Write(inputAssemblyState);

    // write VkPipelineRasterizationStateCreateInfo
    outfile.Write(rasterState);

    // write list of VkPipelineColorBlendAttachmentState
    outfile.Write(cbAttStateList.size());
    for (auto& state : cbAttStateList)
      outfile.Write(state);

    // write VkPipelineColorBlendStateCreateInfo (INPUT LIST OF VkPipelineColorBlendAttachmentState UPON READING)
    outfile.Write(colorBlendState);

    // write VkPipelineDepthStencilStateCreateInfo
    outfile.Write(depthStencilState);

    // viewport state (default)
    VkPipelineViewportStateCreateInfo vpInfo;
    vpInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    vpInfo.pNext = NULL;
    vpInfo.flags = 0;
    vpInfo.viewportCount = 1;
    vpInfo.scissorCount = 1;
    vpInfo.pScissors = NULL;
    vpInfo.pViewports = NULL;
    outfile.Write(vpInfo);

    // Multi sample state (default)
    VkPipelineMultisampleStateCreateInfo msInfo;
    msInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    msInfo.pNext = NULL;
    msInfo.flags = 0;
    msInfo.pSampleMask = NULL;
    msInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    msInfo.sampleShadingEnable = VK_FALSE;
    msInfo.alphaToCoverageEnable = VK_FALSE;
    msInfo.alphaToOneEnable = VK_FALSE;
    msInfo.minSampleShading = 0.0;
    outfile.Write(msInfo);

    // clear memory
    for (auto& code : shaderCodeMap)
      delete[] code.second._data;

    return outfile.Flush(saveOutput);
  }
}

bool ShaderBuilder::ExportRenderPass_Vulkan(MaterialLayout* layout, const std::string& saveOutput)
{
  MemoryStream outfile;
  outfile.BeginWrite();

  /*
    Has render target
  */
  // write has depth
  bool hasDepth = layout->m_resourceLayout.GetHasDepth();
  outfile.Write(hasDepth);

  // write enable depth to texture
  outfile.Write(layout->m_resourceLayout.GetEnableDepthToTexture());


  // write dimensions
  outfile.Write(layout->m_resourceLayout.GetRenderTargetDimension(), sizeof(int) << 1);


  // write dependencies
  const auto& depenList = layout->m_resourceLayout.GetDependenciesList();
  outfile.Write(depenList.size());
  for (auto& dependency : depenList)
    outfile.Write(dependency);

  // write attachment output format
  const auto& mrtOutputList = layout->m_resourceLayout.GetMRTOutputList();
  outfile.Write(mrtOutputList.size());
  for (auto& outputFormat : mrtOutputList)
    outfile.Write(outputFormat);


  // write image attachment usage
  const auto& mrtOutputUsageList = layout->m_resourceLayout.GetMRTOutputUsageList();
  outfile.Write(mrtOutputUsageList.size());
  for (auto& outputUsage : mrtOutputUsageList)
    outfile.Write(outputUsage);


  return outfile.Flush(saveOutput);
}

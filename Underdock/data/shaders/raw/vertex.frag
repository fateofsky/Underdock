#version 420
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec2 uvPos;
layout (location = 0) out vec4 outColor;

layout (binding = 2) uniform sampler2D texture0;

void main() 
{
	outColor = texture(texture0, uvPos);;
}
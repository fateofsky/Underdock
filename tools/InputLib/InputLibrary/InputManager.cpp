#include "stdafx.h"
#include "InputManager.h"
#include <assert.h>

bool blockInput = false;

InputManager* InputManager::m_instance = nullptr;
InputManager::InputManager()
  : m_keys{ 0 }
{
  m_delta.x = m_delta.y = 0;
}

InputManager* InputManager::GetInstance()
{
  return m_instance == nullptr ? m_instance = new InputManager() : m_instance;
}


void InputManager::Shutdown()
{}


bool InputManager::KeyPressed(int _key)
{
  // Should at least initialize the ioMgr once
  assert(m_instance != nullptr);
  return (m_keys[_key] == _KEY_PRESSED && !blockInput) ? true : false;
}


bool InputManager::KeyHold(int _key)
{
  // Should at least initialize the ioMgr once
  assert(m_instance != nullptr);
  return (GetAsyncKeyState(_key) & _KEY_DOWN && !blockInput) ? true : false;
}


bool InputManager::KeyReleased(int _key)
{
  // Should at least initialize the ioMgr once
  assert(m_instance != nullptr);
  return (m_keys[_key] == _KEY_RELEASED && !blockInput) ? true : false;
}


int InputManager::GetMouse_X()
{
  // Should at least initialize the ioMgr once
  assert(m_instance != nullptr);
  return m_mouse_abs.x;
}


int InputManager::GetMouse_Y()
{
  // Should at least initialize the ioMgr once
  assert(m_instance != nullptr);
  return m_mouse_abs.y;
}


void InputManager::InputKeyValue__(int _key, char val)
{
  // Should at least initialize the ioMgr once
  assert(m_instance != nullptr);
  m_instance->m_keys[_key] = ((m_instance->m_keys[_key] == _KEY_PRESSED || m_instance->m_keys[_key] == _KEY_HOLD) && val == _KEY_PRESSED) ?
    m_instance->m_keys[_key] : val;
}


void InputManager::InputMouseWheel__(int _key)
{
  // Should at least initialize the ioMgr once
  assert(m_instance != nullptr);
  m_instance->m_keys[_key] = (char)_key;
}

POINT& InputManager::InputGetDelta()
{
  // Should at least initialize the ioMgr once
  assert(m_instance != nullptr);

  return m_delta;
}


void InputManager::InputRefresh__()
{
  // Should at least initialize the ioMgr once
  assert(m_instance != nullptr);

  static int i = 0;
  for (i = 0; i < _NUM_OF_KEYS; ++i)
  {
    m_keys[i] = (m_keys[i] == _KEY_PRESSED) ? _KEY_HOLD :
      (m_keys[i] == _KEY_RELEASED) ? _KEY_IDLE : m_keys[i];
  }

  m_keys[KEY_MOUSEWHEELDOWN] = 0;
  m_keys[KEY_MOUSEWHEELUP] = 0;
  m_delta.x = m_delta.y = 0;
}


void InputManager::InputMouseMove__()
{
  // Should at least initialize the ioMgr once
  assert(m_instance != nullptr);

  m_lastPos = m_mouse_abs;
  GetCursorPos(&m_mouse_abs);
  m_delta.x = m_mouse_abs.x - m_lastPos.x;
  m_delta.y = m_mouse_abs.y - m_lastPos.y;
}

void InputManager::BlockKeyInput(bool state)
{
  blockInput = state;
}


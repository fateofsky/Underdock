#include "pch.h"
#include "PipelineLayout.h"
#include "AppManager.h"


PipelineLayout::PipelineLayout()
  : m_flags(0),
    m_renderTargetDimension{ 0, 0 },
    m_hasDepth(false),
    m_enableDepthToTexture(false),
    m_inputAssemblyCreateInfo(),
    m_rasterCreateInfo(),
    m_cBlendStateCreateInfo(),
    m_depthStencilStateCreateInfo(),
    m_pipelineStages(),
    m_dynamicStateList(),
    m_cBlendAttStateList(),
    m_dependencies(),
    m_mrtOutputFormat(),
    m_mrtOutputUsage()
{
  // Default input assembly state
  m_inputAssemblyCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
  m_inputAssemblyCreateInfo.pNext = NULL;
  m_inputAssemblyCreateInfo.flags = 0;
  m_inputAssemblyCreateInfo.primitiveRestartEnable = VK_FALSE;
  m_inputAssemblyCreateInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;

  // Default rasterization state
  m_rasterCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
  m_rasterCreateInfo.pNext = NULL;
  m_rasterCreateInfo.flags = 0;
  m_rasterCreateInfo.polygonMode = VK_POLYGON_MODE_FILL;
  m_rasterCreateInfo.cullMode = VK_CULL_MODE_BACK_BIT;
  m_rasterCreateInfo.frontFace = VK_FRONT_FACE_CLOCKWISE;
  m_rasterCreateInfo.depthClampEnable = VK_FALSE;
  m_rasterCreateInfo.rasterizerDiscardEnable = VK_FALSE;
  m_rasterCreateInfo.depthBiasEnable = VK_FALSE;
  m_rasterCreateInfo.depthBiasConstantFactor = 0;
  m_rasterCreateInfo.depthBiasClamp = 0;
  m_rasterCreateInfo.depthBiasSlopeFactor = 0;
  m_rasterCreateInfo.lineWidth = 1.0f;

  // default depth stencil state
  m_depthStencilStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
  m_depthStencilStateCreateInfo.pNext = NULL;
  m_depthStencilStateCreateInfo.flags = 0;
  m_depthStencilStateCreateInfo.depthTestEnable = VK_TRUE;
  m_depthStencilStateCreateInfo.depthWriteEnable = VK_TRUE;
  m_depthStencilStateCreateInfo.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
  m_depthStencilStateCreateInfo.depthBoundsTestEnable = VK_FALSE;
  m_depthStencilStateCreateInfo.minDepthBounds = 0;
  m_depthStencilStateCreateInfo.maxDepthBounds = 0;
  m_depthStencilStateCreateInfo.stencilTestEnable = VK_FALSE;

  // Front
  m_depthStencilStateCreateInfo.front.failOp = VK_STENCIL_OP_KEEP;
  m_depthStencilStateCreateInfo.front.passOp = VK_STENCIL_OP_KEEP;
  m_depthStencilStateCreateInfo.front.compareOp = VK_COMPARE_OP_ALWAYS;
  m_depthStencilStateCreateInfo.front.compareMask = 0;
  m_depthStencilStateCreateInfo.front.reference = 0;
  m_depthStencilStateCreateInfo.front.depthFailOp = VK_STENCIL_OP_KEEP;
  m_depthStencilStateCreateInfo.front.writeMask = 0;

  // Back
  m_depthStencilStateCreateInfo.back.failOp = VK_STENCIL_OP_KEEP;
  m_depthStencilStateCreateInfo.back.passOp = VK_STENCIL_OP_KEEP;
  m_depthStencilStateCreateInfo.back.compareOp = VK_COMPARE_OP_ALWAYS;
  m_depthStencilStateCreateInfo.back.compareMask = 0;
  m_depthStencilStateCreateInfo.back.reference = 0;
  m_depthStencilStateCreateInfo.back.depthFailOp = VK_STENCIL_OP_KEEP;
  m_depthStencilStateCreateInfo.back.writeMask = 0;

  // default color blend state (from material editor)m_cBlendStateCreateInfo
  m_cBlendStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
  m_cBlendStateCreateInfo.pNext = NULL;
  m_cBlendStateCreateInfo.flags = 0;
  m_cBlendStateCreateInfo.attachmentCount = 0;
  m_cBlendStateCreateInfo.pAttachments = NULL;
  m_cBlendStateCreateInfo.logicOpEnable = VK_FALSE;
  m_cBlendStateCreateInfo.logicOp = VK_LOGIC_OP_NO_OP; // default
  m_cBlendStateCreateInfo.blendConstants[0] = m_cBlendStateCreateInfo.blendConstants[1] =
  m_cBlendStateCreateInfo.blendConstants[2] = m_cBlendStateCreateInfo.blendConstants[3] = 1.0f;


}

void PipelineLayout::Destroy()
{
  for (auto& stage : m_pipelineStages)
    stage.second.DestroyUniformData();
  m_pipelineStages.clear();
}

int PipelineLayout::AddStages(Shader::ShaderType shaderStage)
{
  if (m_pipelineStages.find(shaderStage) != m_pipelineStages.end()) return 1;
  m_pipelineStages.insert(std::pair<Shader::ShaderType, _pipelineStage>(shaderStage, _pipelineStage{}));
  LAYOUTMSG::SetMessageFlag(m_flags, LAYOUTMSG::msg::MODIFYSHADER);
  return 0;
}

int PipelineLayout::AddVertexInputToVertexStage(const vBufferLayout& layout)
{
  if (m_pipelineStages.find(Shader::ShaderType::VERTEXSDR) == m_pipelineStages.end()) return 1;
  m_pipelineStages[Shader::ShaderType::VERTEXSDR].baseInput.clear();
  for (auto& buf : layout.layout)
  {
    OGL_BASETYPE baseType = OGL_BASETYPE::FLOAT_TYPE;
    switch (buf)
    {
    case vBufferLayout::VERTEX_POS:
    case vBufferLayout::VERTEX_NORMAL:
    case vBufferLayout::VERTEX_TANGENT:
    case vBufferLayout::VERTEX_BITANGENT:
      baseType = OGL_BASETYPE::VEC4_TYPE;
      break;

    case vBufferLayout::VERTEX_BONE_ID:
      baseType = OGL_BASETYPE::INT4_TYPE;
      break;

    case vBufferLayout::VERTEX_BONE_WEIGHT:
      baseType = OGL_BASETYPE::VEC4_TYPE;
      break;

    case vBufferLayout::TEX_COORDUV0:
    case vBufferLayout::TEX_COORDUV1:
    case vBufferLayout::TEX_COORDUV2:
    case vBufferLayout::TEX_COORDUV3:
      baseType = OGL_BASETYPE::VEC2_TYPE;
      break;
    }

    m_pipelineStages[Shader::ShaderType::VERTEXSDR].baseInput.push_back(baseType);
  }

  LAYOUTMSG::SetMessageFlag(m_flags, LAYOUTMSG::msg::MODIFYSHADER | LAYOUTMSG::msg::MODIFYVERTEXBUFFER);
  return 0;
}

int PipelineLayout::AddInputToStage(Shader::ShaderType shaderStage, OGL_BASETYPE type)
{
  if (m_pipelineStages.find(shaderStage) == m_pipelineStages.end()) return 1;
  m_pipelineStages[shaderStage].baseInput.push_back(type);
  LAYOUTMSG::SetMessageFlag(m_flags, LAYOUTMSG::msg::MODIFYSHADER);
  return 0;
}

int PipelineLayout::AddOutputToStage(Shader::ShaderType shaderStage, OGL_BASETYPE type)
{
  if (m_pipelineStages.find(shaderStage) == m_pipelineStages.end()) return 1;
  m_pipelineStages[shaderStage].baseOutput.push_back(type);
  if (shaderStage == Shader::ShaderType::FRAGSDR)
  {
    m_mrtOutputFormat.push_back(VkFormat::VK_FORMAT_R8G8B8A8_UNORM);                       // default 
    m_mrtOutputUsage.push_back(VkImageUsageFlagBits::VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT); // ~
  }
  LAYOUTMSG::SetMessageFlag(m_flags, LAYOUTMSG::msg::MODIFYSHADER);
  return 0;
}

int PipelineLayout::AddUniformToStage(const std::string& name, Shader::ShaderType shaderStage, UniformResourceBase* base)
{
  if (m_pipelineStages.find(shaderStage) == m_pipelineStages.end()) return 1;
  if (m_pipelineStages[shaderStage].inputUniforms.find(name) != m_pipelineStages[shaderStage].inputUniforms.end()) return 1;
  m_pipelineStages[shaderStage].inputUniforms[name] = base;
  LAYOUTMSG::SetMessageFlag(m_flags, LAYOUTMSG::msg::MODIFYSHADER);
  return 0;
}

int PipelineLayout::AddTextureToStage(const std::string& name, Shader::ShaderType shaderStage, Texture* tex)
{
  if (m_pipelineStages.find(shaderStage) == m_pipelineStages.end()) return 1;
  if (m_pipelineStages[shaderStage].inputTextures.find(name) != m_pipelineStages[shaderStage].inputTextures.end()) return 1;
  m_pipelineStages[shaderStage].inputTextures[name] = tex;
  LAYOUTMSG::SetMessageFlag(m_flags, LAYOUTMSG::msg::MODIFYSHADER);
  return 0;
}

int PipelineLayout::AddShaderCodeToStage(Shader::ShaderType shaderStage, const std::string & sdrCode)
{
  if (m_pipelineStages.find(shaderStage) == m_pipelineStages.end()) return 1;
  m_pipelineStages[shaderStage].shaderMainCode = sdrCode;
  LAYOUTMSG::SetMessageFlag(m_flags, LAYOUTMSG::msg::MODIFYSHADER);
  return 0;
}

bool PipelineLayout::UniformExist(const std::string name)
{
  for (auto& stage : m_pipelineStages)
  {
    for (auto& uniform : stage.second.inputUniforms)
      if (uniform.first == name) return true;
  }

  return false;
}

bool PipelineLayout::TextureExist(const std::string name)
{
  for (auto& stage : m_pipelineStages)
  {
    for (auto& texture : stage.second.inputTextures)
      if (texture.first == name) return true;
  }

  return false;
}

void PipelineLayout::RenameUniform(const std::string oldName, const std::string newName)
{
  for (auto& stage : m_pipelineStages)
  {
    bool nameFound = false;
    for (auto& uniform : stage.second.inputUniforms)
    {
      if (uniform.first == oldName)
      {
        UniformResourceBase* tmpUniform = uniform.second;
        stage.second.inputUniforms.erase(oldName);
        stage.second.inputUniforms.insert(std::pair<std::string, UniformResourceBase*>(newName, tmpUniform));
        nameFound = true;
        break;
      }
    }

    // rename uniform in uniform group if any
    if (nameFound)
    {
      if (stage.second.uniformGroups.size() < 1) return;
      for (auto& group : stage.second.uniformGroups)
      {
        if (group.second.RemoveUniform(oldName) == 0)
        {
          group.second.InsertUniform(newName);
          return;
        }
      }
    }
  }
}


int PipelineLayout::RemoveStages(Shader::ShaderType shaderStage)
{
  if (m_pipelineStages.find(shaderStage) == m_pipelineStages.end()) return 1;
  m_pipelineStages[shaderStage].DestroyUniformData();
  m_pipelineStages.erase(shaderStage);
  return 0;
}

int PipelineLayout::RemoveInputFromStage(Shader::ShaderType shaderStage, const int& index)
{
  if (m_pipelineStages.find(shaderStage) == m_pipelineStages.end()) return 1;
  if (index >= m_pipelineStages[shaderStage].baseInput.size()) return 1;
  m_pipelineStages[shaderStage].baseInput.erase(m_pipelineStages[shaderStage].baseInput.begin() + index);
  LAYOUTMSG::SetMessageFlag(m_flags, LAYOUTMSG::msg::MODIFYSHADER);
  return 0;
}

int PipelineLayout::RemoveOutputFromStage(Shader::ShaderType shaderStage, const int& index)
{
  if (m_pipelineStages.find(shaderStage) == m_pipelineStages.end()) return 1;
  if (index >= m_pipelineStages[shaderStage].baseOutput.size()) return 1;
  m_pipelineStages[shaderStage].baseOutput.erase(m_pipelineStages[shaderStage].baseOutput.begin() + index);

  if (shaderStage == Shader::ShaderType::FRAGSDR)
  {
    m_mrtOutputFormat.erase(m_mrtOutputFormat.begin() + index);
    m_mrtOutputUsage.erase(m_mrtOutputUsage.begin() + index);
  }

  LAYOUTMSG::SetMessageFlag(m_flags, LAYOUTMSG::msg::MODIFYSHADER);
  return 0;
}

int PipelineLayout::RemoveUniform(const std::string& name)
{
  for (auto& stage : m_pipelineStages)
  {
    for (auto& uniform : stage.second.inputUniforms)
    {
      if (uniform.first == name)
      {
        stage.second.inputUniforms.erase(name);
        LAYOUTMSG::SetMessageFlag(m_flags, LAYOUTMSG::msg::MODIFYSHADER);
        return 0;
      }
    }
  }

  return 1;
}

int PipelineLayout::RemoveTexture(const std::string& name)
{
  for (auto& stage : m_pipelineStages)
  {
    for (auto& uniform : stage.second.inputTextures)
    {
      if (uniform.first == name)
      {
        stage.second.inputTextures.erase(name);
        LAYOUTMSG::SetMessageFlag(m_flags, LAYOUTMSG::msg::MODIFYSHADER);
        return 0;
      }
    }
  }

  return 1;
}

void PipelineLayout::BindPipeline(const std::map<std::string, GLint>& nameMapBind)
{
  // Bind Uniform
  const unsigned sizeMat4 = sizeof(glm::mat4);
  const unsigned sizeFloat = sizeof(float);
  float driverTime;
  for (auto& stage : m_pipelineStages)
  {
    for (auto& uniform : stage.second.inputUniforms)
    {
      // Bind default matrix
      UNIFORM_USAGE _usage = uniform.second->GetUsage();
      void* sourceData = nullptr;
      switch (_usage)
      {
      case UNIFORM_USAGE::CAM_VIEW:
        sourceData = ResourceManager::GetInstance()->GetViewMtx();
        break;

      case UNIFORM_USAGE::CAM_PROJ:
        sourceData = ResourceManager::GetInstance()->GetProjMtx();
        break;

      case UNIFORM_USAGE::CAM_VIEWPROJ:
        sourceData = ResourceManager::GetInstance()->GetViewProjMtx();
        break;

      case UNIFORM_USAGE::TRANSFORM:
        sourceData = ResourceManager::GetInstance()->GetObjTransformMtx();
        break;

      case UNIFORM_USAGE::USER_INPUT:
        break;

      case UNIFORM_USAGE::DRIVER_TIME:
        driverTime = AppManager::GetInstance()->GetDriverTime();
        sourceData = &driverTime;
        break;
      }
      if (_usage != UNIFORM_USAGE::USER_INPUT && _usage != UNIFORM_USAGE::DRIVER_TIME)
        memcpy_s(uniform.second->GetUniformData(), sizeMat4, sourceData, sizeMat4);
      else if(_usage == UNIFORM_USAGE::DRIVER_TIME)
        memcpy_s(uniform.second->GetUniformData(), sizeFloat, sourceData, sizeFloat);

      if (nameMapBind.find(uniform.first) != nameMapBind.end())
        uniform.second->BindUniform(nameMapBind.at(uniform.first));
    }

    // Bind Texture
    for (auto & texture : stage.second.inputTextures)
    {
      if (nameMapBind.find(texture.first) != nameMapBind.end())
      {
        GLint location = nameMapBind.at(texture.first);
        glActiveTexture(GL_TEXTURE0 + location);
        glUniform1i(location, location);
        glBindTexture(GL_TEXTURE_2D, texture.second->GetTexture());
      }
    }
  }
}

void PipelineLayout::UnbindPipeline()
{
  // Unbind all textures
  glBindTexture(GL_TEXTURE_2D, 0);
}

bool PipelineLayout::FindNextStage(Shader::ShaderType currentStage, Shader::ShaderType& result)
{
  if (m_pipelineStages.size() < 1) return false;

  std::vector<Shader::ShaderType> linearizePipeline;
  
  // linearize and get current stage count
  int typeToInt = -1;
  int counter = 0;
  for (auto& stage : m_pipelineStages)
  {
    linearizePipeline.emplace_back(stage.first);
    typeToInt = (stage.first == currentStage) ? counter : typeToInt;
    ++counter;
  }

  int NextTypeToInt = typeToInt + 1;
  if (typeToInt >= linearizePipeline.size() || NextTypeToInt >= linearizePipeline.size())
    return false;
  else
  {
    result = linearizePipeline[NextTypeToInt];
    return true;
  }
}

bool PipelineLayout::FindPreviousStage(Shader::ShaderType currentStage, Shader::ShaderType& result)
{
  if (m_pipelineStages.size() < 1) return false;

  std::vector<Shader::ShaderType> linearizePipeline;
  
  // linearize and get current stage count
  int typeToInt = -1;
  int counter = 0;
  for (auto& stage : m_pipelineStages)
  {
    linearizePipeline.emplace_back(stage.first);
    typeToInt = (stage.first == currentStage) ? counter : typeToInt;
    ++counter;
  }

  int prevTypeToInt = typeToInt - 1;
  if (typeToInt >= linearizePipeline.size() || 
      prevTypeToInt >= linearizePipeline.size() ||
      prevTypeToInt < 0)
    return false;
  else
  {
    result = linearizePipeline[prevTypeToInt];
    return true;
  }
}

int PipelineLayout::AddToGroup(Shader::ShaderType stage, const std::string& groupName, 
                               const std::string& uniformName, unsigned group, UniformGroup::_MemoryLayout mLayout)
{
  if (m_pipelineStages.find(stage) == m_pipelineStages.end()) return 1;
  m_pipelineStages[stage].uniformGroups[groupName].ChangeGroup(group);
  m_pipelineStages[stage].uniformGroups[groupName].ChangeMemoryLayout(mLayout);
  return m_pipelineStages[stage].uniformGroups[groupName].InsertUniform(uniformName);
}

int PipelineLayout::RemoveFromGroup(Shader::ShaderType stage, const std::string & groupName, const std::string & uniformName)
{
  if (m_pipelineStages.find(stage) == m_pipelineStages.end()) return 1;
  return m_pipelineStages[stage].uniformGroups[groupName].RemoveUniform(uniformName);
}

int PipelineLayout::RenameUniformGroup(Shader::ShaderType stage, const std::string& groupName, const std::string & newGroupName)
{
  if (m_pipelineStages.find(stage) == m_pipelineStages.end()) return 1;
  if (m_pipelineStages[stage].uniformGroups.find(groupName) == m_pipelineStages[stage].uniformGroups.end()) return 1;
  UniformGroup tmpGroup = m_pipelineStages[stage].uniformGroups[groupName];
  m_pipelineStages[stage].uniformGroups.erase(groupName);
  m_pipelineStages[stage].uniformGroups[newGroupName] = tmpGroup;
  return 0;
}

int PipelineLayout::RemoveGroup(Shader::ShaderType stage, const std::string & groupName)
{
  if (m_pipelineStages.find(stage) == m_pipelineStages.end()) return 1;
  if (m_pipelineStages[stage].uniformGroups.find(groupName) == m_pipelineStages[stage].uniformGroups.end()) return 1;
  m_pipelineStages[stage].uniformGroups.erase(groupName);
  return 0;
}


void PipelineLayout::ChangeHasDepth(bool target)
{
  m_hasDepth = target;
}

bool PipelineLayout::GetHasDepth() const
{
  return m_hasDepth;
}

PipelineLayout::pipelineStageMap& PipelineLayout::GetPipelineStageMap()
{
  return m_pipelineStages;
}

UniformResourceBase* PipelineLayout::GetUniform(const std::string & name)
{
  for (auto& stage : m_pipelineStages)
  {
    for (auto& uniform : stage.second.inputUniforms)
      if (uniform.first == name) return uniform.second;
  }

  return nullptr;
}

VkPipelineInputAssemblyStateCreateInfo& PipelineLayout::GetInputAssemblyInfo()
{
  return m_inputAssemblyCreateInfo;
}

VkPipelineRasterizationStateCreateInfo& PipelineLayout::GetRasterizationStateInfo()
{
  return m_rasterCreateInfo;
}

VkPipelineColorBlendStateCreateInfo& PipelineLayout::GetColorBlendAttStateInfo()
{
  return m_cBlendStateCreateInfo;
}

VkPipelineDepthStencilStateCreateInfo& PipelineLayout::GetDepthStencilStateInfo()
{
  return m_depthStencilStateCreateInfo;
}

std::vector<VkDynamicState>& PipelineLayout::GetDynamicStateList()
{
  return m_dynamicStateList;
}

std::vector<VkPipelineColorBlendAttachmentState>& PipelineLayout::GetColorBlendAttStateList()
{
  return m_cBlendAttStateList;
}

VkPipelineColorBlendAttachmentState PipelineLayout::CreateDefaultColorBlendAttachmentState()
{
  VkPipelineColorBlendAttachmentState cbAttState;
  cbAttState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | 
                              VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
  cbAttState.blendEnable = VK_FALSE;
  cbAttState.alphaBlendOp = VK_BLEND_OP_ADD;
  cbAttState.colorBlendOp = VK_BLEND_OP_ADD;
  cbAttState.srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
  cbAttState.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
  cbAttState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
  cbAttState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
  return cbAttState;
}

std::vector<VkFormat> PipelineLayout::ComputeListOfVkFormats(const std::vector<OGL_BASETYPE>& oglTypeList)
{
  std::vector<VkFormat> formatList;
  for (const OGL_BASETYPE& oglType : oglTypeList)
  {
    switch (oglType)
    {
    case INT4_TYPE:
    case INT_TYPE:
      formatList.push_back(VkFormat::VK_FORMAT_R32_SINT);
      break;

    case FLOAT_TYPE:
      formatList.push_back(VkFormat::VK_FORMAT_R32_SFLOAT);
      break;

    case BOOL_TYPE:
      formatList.push_back(VkFormat::VK_FORMAT_R8_SNORM);
      break;

    case VEC2_TYPE:
      formatList.push_back(VkFormat::VK_FORMAT_R32G32_SFLOAT);
      break;

    case VEC3_TYPE:
      formatList.push_back(VkFormat::VK_FORMAT_R32G32B32_SFLOAT);
      break;

    case VEC4_TYPE:
      formatList.push_back(VkFormat::VK_FORMAT_R32G32B32A32_SFLOAT);
      break;
    }
  }

  return formatList;
}

void PipelineLayout::AddDependencies()
{
  VkSubpassDependency defaultDependencies;
  defaultDependencies.srcSubpass = VK_SUBPASS_EXTERNAL;
  defaultDependencies.dstSubpass = 0;
  defaultDependencies.srcStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
  defaultDependencies.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
  defaultDependencies.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
  defaultDependencies.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
  defaultDependencies.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;
  m_dependencies.push_back(defaultDependencies);
}

void PipelineLayout::RemoveDependencies(const unsigned& index)
{
  if (index < m_dependencies.size())
    m_dependencies.erase(m_dependencies.begin() + index);
}

const std::vector<VkSubpassDependency>& PipelineLayout::GetDependenciesList() const
{
  return m_dependencies;
}

std::vector<VkSubpassDependency>& PipelineLayout::GetDependenciesList_Writable()
{
  return m_dependencies;
}

bool PipelineLayout::GetEnableDepthToTexture() const
{
  return m_enableDepthToTexture;
}

void PipelineLayout::SetEnableDepthToTexture(bool state)
{
  m_enableDepthToTexture = state;
}

const std::vector<VkFormat>& PipelineLayout::GetMRTOutputList() const
{
  return m_mrtOutputFormat;
}

std::vector<VkFormat>& PipelineLayout::GetMRTOutputList_Writable()
{
  return m_mrtOutputFormat;
}

const std::vector<VkImageUsageFlags>& PipelineLayout::GetMRTOutputUsageList() const
{
  return m_mrtOutputUsage;
}

std::vector<VkImageUsageFlags>& PipelineLayout::GetMRTOutputUsageList_Writable()
{
  return m_mrtOutputUsage;
}



void PipelineLayout::SetMessageFlag(const unsigned& toSet)
{
  LAYOUTMSG::SetMessageFlag(m_flags, toSet);
}

bool PipelineLayout::TestMessageFlag(const unsigned & toTest)
{
  return LAYOUTMSG::TestMessageFlag(m_flags, toTest);
}

int* PipelineLayout::GetRenderTargetDimension()
{
  return m_renderTargetDimension;
}

void PipelineLayout::SetRenderTargetDimension(int width, int height)
{
  m_renderTargetDimension[0] = width;
  m_renderTargetDimension[1] = height;
}

void PipelineLayout::_pipelineStage::DestroyUniformData()
{
  for (auto& uniform : inputUniforms)
    delete uniform.second;
  inputUniforms.clear();
}

/* Start Header
*****************************************************************/
/*!
\file      MathFunctions.h
\author    
\par       
\date      
\brief

Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the
prior written consent of DigiPen Institute of Technology is prohibited.
*/
/* End Header
*******************************************************************/
#pragma once
#include "MathLib.h"

#ifndef PI
#define PI                     3.14159265358979323846f
#endif // !PI


namespace FrustumIntersectionType
{
	enum Type { Outside, Inside, Overlaps, NotImplemented };
	static const char* Names[] = { "Coplanar", "Outside", "Inside", "Overlaps", "NotImplemented" };
}

vec3  ProjectPointOnPlane(const vec3& point, const vec3& normal, float planeDistance);
FrustumIntersectionType::Type  PlaneSphere(const vec4& plane,
	const vec3& sphereCenter, float sphereRadius);
FrustumIntersectionType::Type  PlaneAabb(const vec4& plane,
	const vec3& aabbMin, const vec3& aabbMax);
FrustumIntersectionType::Type  FrustumAabb(const vec4 planes[6],
	const vec3& aabbMin, const vec3& aabbMax);

bool  AabbAabb(const vec3& aabbMin0, const vec3& aabbMax0,
  const vec3& aabbMin1, const vec3& aabbMax1);

namespace Math
{
	mat4  Orthogonal(float left, float right, float bottom, float top, float nearZ, float farZ);
	mat4  Perspective(float fov, float aspect, float nearZ, float farZ);
	vec3  CatmullRom(const vec3& v1, const vec3& v2, const vec3& v3, const vec3& v4, float s);

	template<typename T>
	T sqrDistance(const T& x1, const T& y1, const T& x2, const T& y2)
	{
		return T((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));
	}

	template<typename T>
	T distance(const T& x1, const T& y1, const T& x2, const T& y2)
	{
		return T(FastSquare(sqrDistance<T>(x1, y1, x2, y2)));
	}

  template<typename T1, typename T2>
	T1 approach(const T1& start, const T1& end, const T2& amount)
	{
		if (start < end - amount) return T1(start + amount);
		else if (start > end + amount) return T1(start - amount);
		else return T1(end);
	}

	template<typename T, typename T2>
	T lerp(const T& a, const T& b, const T2& t)
	{
		return a + (b - a) * t;
	}

	template<typename T, typename T2>
	T lerp(const T& a, const T& b, const T2& t, T2(*ease)(T2))
	{
		return a + (b - a) * ease(t);
	}

	template<typename T>
	T clamp(const T& value, const T& min, const T& max)
	{
		return T((value < min) ? min : (value > max ? max : value));
	}

	template<typename T>
	int sign(const T& a)
	{
		return ((a < 0) ? -1 : (a > 0 ? 1 : 0));
	}

	

	template<typename T>
	T angleDiff(const T& a1, const T& a2)
	{
		T diff = a2 - a1;
		while (diff < -180) diff += 360;
		while (diff > 180) diff -= 360;
		return diff;
	}

	template<typename T>
	T angleDiffRad(const T& a1, const T& a2)
	{
		T diff = a2 - a1;
		while (diff < -PI) diff += 2 * PI;
		while (diff > PI) diff -= 2 * PI;
		return diff;
	}

	template <typename T>
	T toRad(const T& deg)
	{
		return deg * T(PI / 180);
	}

	template <typename T>
	T toDeg(const T& rad)
	{
		return rad * T(180 / PI);
	}

	//mat3 tensor(const vec3& a, const vec3& b)
	//{
	//	mat3 mtx;
	//	mtx.Zero();
	//	for (size_t i = 0; i < mat3_ROW; ++i)
	//	{
	//		mtx.m2_33[i][0] = a.x_;
	//		mtx.m2_33[i][1] = a.y_;
	//		mtx.m2_33[i][2] = a.z_;
	//	}
	//	for (size_t i = 0; i < mat3_ROW; ++i)
	//	{
	//		mtx.m2_33[0][i] *= b.x_;
	//		mtx.m2_33[1][i] *= b.y_;
	//		mtx.m2_33[2][i] *= b.z_;
	//	}
	//	return mtx;
	//}
	//
	//mat4 tensor(const vec4& a, const vec4& b)
	//{
	//	mat4 mtx;
	//	mtx.Zero();
	//	for (size_t i = 0; i < mat4_ROW; ++i)
	//	{
	//		mtx.m2_44[i][0] = a.x_;
	//		mtx.m2_44[i][1] = a.y_;
	//		mtx.m2_44[i][2] = a.z_;
	//		mtx.m2_44[i][3] = a.w_;
	//	}
	//	for (size_t i = 0; i < mat4_ROW; ++i)
	//	{
	//		mtx.m2_44[0][i] *= b.x_;
	//		mtx.m2_44[1][i] *= b.y_;
	//		mtx.m2_44[2][i] *= b.z_;
	//		mtx.m2_44[3][i] *= b.w_;
	//	}
	//	return mtx;
	//}

	//returns greatest common divisor
	template<typename T>
	inline typename std::enable_if<std::is_integral<T>::value, T>::type gcd(const T& a, const T& b)
	{
		if (b = 0) return a;
		else return gcd(b, a % b);
	}
	//Overloading resolution fails for std::abs() with unsigned types, so we use
	//function template wrappers to allow use of abs for unsigned types
	//for unsigned types, simply return val, for signed type, return std::abs(val)
	//this is required as rand() fn template uses abs()
	//and we might call rand<unsigned>()
	template<typename T>
	inline typename std::enable_if<std::is_unsigned<T>::value, T>::type abs(const T& val) { return val; };
	template<typename T>
	inline typename std::enable_if<std::is_signed<T>::value, T>::type abs(const T& val) { return std::abs(val); };


	namespace Easing
	{
#define bounceConst1 0.3636363636363636
#define bounceConst2 0.7272727272727273
#define bounceConst3 0.5454545454545455
#define bounceConst4 0.9090909090909091
#define bounceConst5 0.8181818181818182
#define bounceConst6 0.9545454545454545

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			quadIn(T t) { return t * t; }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			quadOut(T t) { return -t * (t - 2); }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			quadInOut(T t) { return t <= 0.5 ? t * t * 2 : 1 - (--t) * t * 2; }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			cubeIn(T t) { return t * t * t; }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			cubeOut(T t) { return 1 + (--t) * t *t; }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			cubeInOut(T t) { return t <= 0.5 ? t * t * t * 4 : 1 + (--t) * t * t * 4; }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			quartIn(T t) { return t * t * t * t; }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			quartOut(T t) { return 1 - (t -= 1) * t * t * t; }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			quartInOut(T t) { return t <= 0.5 ? t * t * t * t * 8 : (1 - (t = t * 2 - 2) * t * t * t) / 2 + 0.5; }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			quintIn(T t) { return t * t * t * t * t; }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			quintOut(T t) { return (t = t - 1) * t * t * t * t + 1; }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			quintInOut(T t) { return ((t *= 2) < 1) ? (t * t * t * t * t) / 2 : ((t -= 2) * t * t * t * t + 2) / 2; }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			sineIn(T t) { return -std::cos(PI * 2 * t) + 1; }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			sineOut(T t) { return std::sin(PI * 2 * t); }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			sineInOut(T t) { return -std::cos(PI * t) / 2 + 0.5; }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			circIn(T t) { return -(FastSquare(1 - t * t) - 1); }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			circOut(T t) { return FastSquare(1 - (t - 1) * (t - 1)); }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			circInOut(T t) { return t <= 0.5 ? (FastSquare(1 - t * t * 4) - 1) / -2 : (FastSquare(1 - (t * 2 - 2) * (t * 2 - 2)) + 1) / 2; }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			expoIn(T t) { return std::pow(2, 10 * (t - 1)); }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			expoOut(T t) { return -std::pow(2, -10 * t) + 1; }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			expoInOut(T t) { return t < 0.5 ? std::pow<T>(2, 10 * (t * 2 - 1)) / 2 : (-std::pow<T>(2, -10 * (t * 2 - 1)) + 2) / 2; }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			backIn(T t) { return t * t * (2.70158 * t - 1.70158); }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			backOut(T t) { return 1 - (--t) * (t)* (-2.70158 * t - 1.70158); }

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			backInOut(T t)
		{
			t *= 2;
			if (t < 1) return t * t * (2.70158 * t - 1.70158) / 2;
			--t;
			return (1 - (--t) * (t)* (-2.70158 * t - 1.70158)) / 2 + 0.5;
		}

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			bounceIn(T t)
		{
			t = 1 - t;
			if (t < bounceConst1) return 1 - 7.5625 * t * t;
			if (t < bounceConst2) return 1 - (7.5625 * (t - bounceConst3) * (t - bounceConst3) + 0.75);
			if (t < bounceConst4) return 1 - (7.5625 * (t - bounceConst5) * (t - bounceConst5) + 0.9375);
			return 1 - (7.5625 * (t - bounceConst6) * (t - bounceConst6) + 0.984375);
		}

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			bounceOut(T t)
		{
			if (t < bounceConst1) return 7.5625 * t * t;
			if (t < bounceConst2) return 7.5625 * (t - bounceConst3) * (t - bounceConst3) + 0.75;
			if (t < bounceConst4) return 7.5625 * (t - bounceConst5) * (t - bounceConst5) + 0.9375;
			return 7.5625 * (t - bounceConst6) * (t - bounceConst6) + 0.984375;
		}

		template <typename T = double>
		typename std::enable_if<std::is_arithmetic<T>::value, T>::type
			bounceInOut(T t)
		{
			if (t < 0.5)
			{
				t = 1 - t * 2;
				if (t < bounceConst1) return (1 - 7.5625 * t * t) / 2;
				if (t < bounceConst2) return (1 - (7.5625 * (t - bounceConst3) * (t - bounceConst3) + 0.75)) / 2;
				if (t < bounceConst4) return (1 - (7.5625 * (t - bounceConst5) * (t - bounceConst5) + 0.9375)) / 2;
				return (1 - (7.5625 * (t - bounceConst6) * (t - bounceConst6) + 0.984375)) / 2;
			}
			t = t * 2 - 1;
			if (t < bounceConst1) return (7.5625 * t * t) / 2 + 0.5;
			if (t < bounceConst2) return (7.5625 * (t - bounceConst3) * (t - bounceConst3) + .75) / 2 + 0.5;
			if (t < bounceConst4) return (7.5625 * (t - bounceConst5) * (t - bounceConst5) + .9375) / 2 + 0.5;
			return (7.5625 * (t - bounceConst6) * (t - bounceConst6) + .984375) / 2 + 0.5;
		}

    enum FunctionType
    {
      QUAD_IN,
      QUAD_OUT,
      QUAD_INOUT,
      CUBE_IN,
      CUBE_OUT,
      CUBE_INOUT,
      QUART_IN,
      QUART_OUT,
      QUART_INOUT,
      QUINT_IN,
      QUINT_OUT,
      QUINT_INOUT,
      SINE_IN,
      SINE_OUT,
      SINE_INOUT,
      CIRC_IN,
      CIRC_OUT,
      CIRC_INOUT,
      EXPO_IN,
      EXPO_OUT,
      EXPO_INOUT,
      BACK_IN,
      BACK_OUT,
      BACK_INOUT,
      NONE
    };

    template <typename T = double, typename = std::enable_if<std::is_arithmetic<T>::value, T>>
    T(*GetEasingFn(FunctionType t))(T)
    {
      switch (t)
      {
      case  QUAD_IN: return Easing::quadIn<T>;
      case  QUAD_OUT: return Easing::quadOut<T>;
      case  QUAD_INOUT: return Easing::quadInOut<T>;

      case  CUBE_IN: return Easing::cubeIn<T>;
      case  CUBE_OUT: return Easing::cubeOut<T>;
      case  CUBE_INOUT: return Easing::cubeInOut<T>;

      case  QUART_IN: return Easing::quartIn<T>;
      case  QUART_OUT: return Easing::quartOut<T>;
      case  QUART_INOUT: return Easing::quartInOut<T>;

      case  QUINT_IN: return Easing::quintIn<T>;
      case  QUINT_OUT: return Easing::quintOut<T>;
      case  QUINT_INOUT: return Easing::quintInOut<T>;

      case  SINE_IN: return Easing::sineIn<T>;
      case  SINE_OUT: return Easing::sineOut<T>;
      case  SINE_INOUT: return Easing::sineInOut<T>;

      case  CIRC_IN: return Easing::circIn<T>;
      case  CIRC_OUT: return Easing::circOut<T>;
      case  CIRC_INOUT: return Easing::circInOut<T>;

      case  EXPO_IN: return Easing::expoIn<T>;
      case  EXPO_OUT: return Easing::expoOut<T>;
      case  EXPO_INOUT: return Easing::expoInOut<T>;

      case  BACK_IN: return Easing::backIn<T>;
      case  BACK_OUT: return Easing::backOut<T>;
      case  BACK_INOUT: return Easing::backInOut<T>;

      default: return nullptr;
      }
	}
  }
}


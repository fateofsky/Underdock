#include "../pch.h"
#define STB_IMAGE_IMPLEMENTATION
#include "Engine.h"

bool VulkTexImage::Load2DTextureImage(const char * file, VkFormat fmt, VkImageAspectFlags flgs)
{
  // Load texture file
  int texW, texH, texC;
  stbi_uc* ImgData = stbi_load(file, &texW, &texH, &texC, STBI_rgb_alpha);
  vTexImageDeviceSize = texW * texH * 4;
  if (!ImgData)
  {
    std::cout << "Error: " << file << " cannot be loaded" << std::endl;
    return false;
  }
  vProp._width = texW;
  vProp._height = texH;
  vProp._depth = 1;
  vProp._channels = texC;
  vTexImageFormat = fmt;

  // initialize vulkan texture
  VkImageCreateInfo texImgInfo;
  texImgInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
  texImgInfo.imageType = VK_IMAGE_TYPE_2D;
  texImgInfo.extent.width = texW;
  texImgInfo.extent.height = texH;
  texImgInfo.extent.depth = 1;
  texImgInfo.mipLevels = 1;
  texImgInfo.arrayLayers = 1;
  texImgInfo.format = vTexImageFormat;
  texImgInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
  texImgInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  texImgInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
  texImgInfo.samples = VK_SAMPLE_COUNT_1_BIT;
  texImgInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
  texImgInfo.pNext = NULL;
  texImgInfo.pQueueFamilyIndices = 0;
  texImgInfo.queueFamilyIndexCount = 0;
  texImgInfo.flags = NULL;
#ifdef _DEBUG
  VkResult vkRes = vkCreateImage(Engine::core::vDevice, &texImgInfo, NULL, &vTexImg);
  assert(vkRes == VK_SUCCESS);
#else
  vkCreateImage(Engine::core::vDevice, &texImgInfo, NULL, &vTexImg);
#endif // _DEBUG

  // texture image allocate memory and bind
  VkMemoryRequirements texImg_memReq;
  VkMemoryAllocateInfo texImg_allocInfo;
  vkGetImageMemoryRequirements(Engine::core::vDevice, vTexImg, &texImg_memReq);
  texImg_allocInfo.pNext = NULL;
  texImg_allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
  texImg_allocInfo.allocationSize = texImg_memReq.size;

#ifdef _DEBUG
  bool texMemGetType = 
    Engine::core::memory_type_from_properties(texImg_memReq.memoryTypeBits,
    VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
    &texImg_allocInfo.memoryTypeIndex);
  vkRes = vkAllocateMemory(Engine::core::vDevice, &texImg_allocInfo, NULL, &vTexImgMem);
  assert(texMemGetType && vkRes == VK_SUCCESS);
  vkRes = vkBindImageMemory(Engine::core::vDevice, vTexImg, vTexImgMem, 0);
  assert(vkRes == VK_SUCCESS);
#else
  Engine::core::memory_type_from_properties(texImg_memReq.memoryTypeBits,
      VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
      &texImg_allocInfo.memoryTypeIndex);
  vkAllocateMemory(Engine::core::vDevice, &texImg_allocInfo, NULL, &vTexImgMem);
  vkBindImageMemory(Engine::core::vDevice, vTexImg, vTexImgMem, 0);
#endif // _DEBUG


  // create image view for texture
  VkImageViewCreateInfo texImgViewInfo;
  texImgViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
  texImgViewInfo.pNext = NULL;
  texImgViewInfo.flags = 0;
  texImgViewInfo.image = vTexImg;
  texImgViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
  texImgViewInfo.format = vTexImageFormat;
  texImgViewInfo.components.r = VK_COMPONENT_SWIZZLE_R;
  texImgViewInfo.components.g = VK_COMPONENT_SWIZZLE_G;
  texImgViewInfo.components.b = VK_COMPONENT_SWIZZLE_B;
  texImgViewInfo.components.a = VK_COMPONENT_SWIZZLE_A;
  texImgViewInfo.subresourceRange.aspectMask = flgs;
  texImgViewInfo.subresourceRange.baseMipLevel = 0;
  texImgViewInfo.subresourceRange.levelCount = 1;
  texImgViewInfo.subresourceRange.baseArrayLayer = 0;
  texImgViewInfo.subresourceRange.layerCount = 1;
#ifdef _DEBUG
  vkRes = vkCreateImageView(Engine::core::vDevice, &texImgViewInfo, NULL, &vTexImgView);
  assert(vkRes == VK_SUCCESS);
#else
  vkCreateImageView(Engine::core::vDevice, &texImgViewInfo, NULL, &vTexImgView);
#endif // _DEBUG

  // create staging buffer 
  vStagingBuffer.CreateBuffer(static_cast<unsigned>(vTexImageDeviceSize), 1, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                                                                             VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                                                                             VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
  vStagingBuffer.MapMemory(ImgData, static_cast<unsigned>(vTexImageDeviceSize));
  stbi_image_free(ImgData);

  return true;
}

void VulkTexImage::CreateAttachment(const unsigned& width, const unsigned& height, VkFormat fmt, VkImageUsageFlags usage, VkImageAspectFlags flags)
{
  vProp._width  = width;
  vProp._height = height;
  vProp._channels = 4;
  vProp._depth = 1;
  vTexImageFormat = fmt;

  // initialize attachment texture VkImage
  VkImageCreateInfo texImgInfo;
  texImgInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
  texImgInfo.imageType = VK_IMAGE_TYPE_2D;
  texImgInfo.extent.width = width;
  texImgInfo.extent.height = height;
  texImgInfo.extent.depth = 1;
  texImgInfo.mipLevels = 1;
  texImgInfo.arrayLayers = 1;
  texImgInfo.format = vTexImageFormat;
  texImgInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
  texImgInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  texImgInfo.usage = usage;// | VK_IMAGE_USAGE_SAMPLED_BIT;
  texImgInfo.samples = VK_SAMPLE_COUNT_1_BIT;
  texImgInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
  texImgInfo.pNext = NULL;
  texImgInfo.pQueueFamilyIndices = 0;
  texImgInfo.queueFamilyIndexCount = 0;
  texImgInfo.flags = NULL;
#ifdef _DEBUG
  VkResult vkRes = vkCreateImage(Engine::core::vDevice, &texImgInfo, NULL, &vTexImg);
  assert(vkRes == VK_SUCCESS);
#else
  vkCreateImage(Engine::core::vDevice, &texImgInfo, NULL, &vTexImg);
#endif // _DEBUG

  // texture image allocate memory and bind
  VkMemoryRequirements texImg_memReq;
  VkMemoryAllocateInfo texImg_allocInfo;
  vkGetImageMemoryRequirements(Engine::core::vDevice, vTexImg, &texImg_memReq);
  texImg_allocInfo.pNext = NULL;
  texImg_allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
  texImg_allocInfo.allocationSize = texImg_memReq.size;

#ifdef _DEBUG
  bool texMemGetType = 
    Engine::core::memory_type_from_properties(texImg_memReq.memoryTypeBits,
                                              VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                                              &texImg_allocInfo.memoryTypeIndex);
  vkRes = vkAllocateMemory(Engine::core::vDevice, &texImg_allocInfo, NULL, &vTexImgMem);
  assert(texMemGetType && vkRes == VK_SUCCESS);
  vkRes = vkBindImageMemory(Engine::core::vDevice, vTexImg, vTexImgMem, 0);
  assert(vkRes == VK_SUCCESS);
#else
  Engine::core::memory_type_from_properties(texImg_memReq.memoryTypeBits,
                                            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                                            &texImg_allocInfo.memoryTypeIndex);
  vkAllocateMemory(Engine::core::vDevice, &texImg_allocInfo, NULL, &vTexImgMem);
  vkBindImageMemory(Engine::core::vDevice, vTexImg, vTexImgMem, 0);
#endif // _DEBUG

  // VkImageView
  VkImageViewCreateInfo texImgViewInfo;
  texImgViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
  texImgViewInfo.pNext = NULL;
  texImgViewInfo.flags = 0;
  texImgViewInfo.image = vTexImg;
  texImgViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
  texImgViewInfo.format = vTexImageFormat;
  texImgViewInfo.components.r = VK_COMPONENT_SWIZZLE_R;
  texImgViewInfo.components.g = VK_COMPONENT_SWIZZLE_G;
  texImgViewInfo.components.b = VK_COMPONENT_SWIZZLE_B;
  texImgViewInfo.components.a = VK_COMPONENT_SWIZZLE_A;
  texImgViewInfo.subresourceRange.aspectMask = flags;
  texImgViewInfo.subresourceRange.baseMipLevel = 0;
  texImgViewInfo.subresourceRange.levelCount = 1;
  texImgViewInfo.subresourceRange.baseArrayLayer = 0;
  texImgViewInfo.subresourceRange.layerCount = 1;
#ifdef _DEBUG
  vkRes = vkCreateImageView(Engine::core::vDevice, &texImgViewInfo, NULL, &vTexImgView);
  assert(vkRes == VK_SUCCESS);
#else
  vkCreateImageView(Engine::core::vDevice, &texImgViewInfo, NULL, &vTexImgView);
#endif // _DEBUG

}

void VulkTexImage::CreateShaderSampler(VkFilter minFilter, VkFilter magFilter, VkSamplerAddressMode uvwMode)
{
  // Create image samplers
  VkSamplerCreateInfo texSamInfo;
  texSamInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
  texSamInfo.minFilter = minFilter;
  texSamInfo.magFilter = magFilter;
  texSamInfo.addressModeU = texSamInfo.addressModeV =
  texSamInfo.addressModeW = uvwMode;
  texSamInfo.anisotropyEnable = VK_TRUE;
  texSamInfo.maxAnisotropy = 16;
  texSamInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK;
  texSamInfo.unnormalizedCoordinates = VK_FALSE;
  texSamInfo.compareEnable = VK_FALSE;
  texSamInfo.compareOp = VK_COMPARE_OP_ALWAYS;
  texSamInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
  texSamInfo.mipLodBias = 0.0f;
  texSamInfo.minLod = 0.0f;
  texSamInfo.maxLod = 0.0f;
  texSamInfo.pNext = NULL;
  texSamInfo.flags = 0;

#ifdef _DEBUG
  VkResult vkRes = vkCreateSampler(Engine::core::vDevice, &texSamInfo, NULL, &vTexSampler);
  assert(vkRes == VK_SUCCESS);
#else
  vkCreateSampler(Engine::core::vDevice, &texSamInfo, NULL, &vTexSampler);
#endif // _DEBUG
}


void VulkTexImage::_InitializeImage(VkCommandBuffer cmdBuf)
{
  vStagingBuffer.BindBufferMemory();
  Engine::utils::ImageTransition_Undef_Transfer(cmdBuf, vTexImg);
  Engine::utils::copyVkBufferToImage(cmdBuf, vStagingBuffer.GetBuffer(), vTexImg, VK_IMAGE_ASPECT_COLOR_BIT, vProp._width, vProp._height);
  Engine::utils::ImageTransition_Transfer_FragRead(cmdBuf, vTexImg);
}

void VulkTexImage::_Cleanup()
{
  vStagingBuffer.DestroyBuffer();
}

void VulkTexImage::DestroyImage()
{
  vkDestroySampler(Engine::core::vDevice, vTexSampler, nullptr);
  vkDestroyImageView(Engine::core::vDevice, vTexImgView, NULL);
  vkDestroyImage(Engine::core::vDevice, vTexImg, NULL);
  vkFreeMemory(Engine::core::vDevice, vTexImgMem, NULL);
}

VkImage VulkTexImage::GetTextureImage() const
{
  return vTexImg;
}

VkImageView VulkTexImage::GetTextureImageView() const
{
  return vTexImgView;
}

VkDeviceMemory VulkTexImage::GetTextureImageDevMem() const
{
  return vTexImgMem;
}

VkFormat VulkTexImage::GetTextureImageFormat() const
{
  return vTexImageFormat;
}

VkSampler VulkTexImage::GetTextureImageSampler() const
{
  return vTexSampler;
}

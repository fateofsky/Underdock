#include "pch.h"
#include "Engine.h"
#include "VulkRenderTarget.h"

void VulkRenderTarget::BeginRenderTargetPass(bool primaryBuffer)
{
  VkRenderPassBeginInfo rpBeg;
  rpBeg.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
  rpBeg.pNext = NULL;
  rpBeg.renderPass = m_renderPass;
  rpBeg.framebuffer = m_outputAttachmentPerFrame[Engine::swpch::vCurFrame].m_vFrameBuffer;
  rpBeg.renderArea.offset.x = rpBeg.renderArea.offset.y = 0;  // can be draw time
  rpBeg.renderArea.extent.width = Engine::core::windowsWidth;       // can be draw time
  rpBeg.renderArea.extent.height = Engine::core::windowsHeight;     // can be draw time
  rpBeg.clearValueCount = (uint32_t)m_clearValues.size();
  rpBeg.pClearValues = m_clearValues.data();
  vkCmdBeginRenderPass(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame], &rpBeg,
    primaryBuffer == 1 ? VK_SUBPASS_CONTENTS_INLINE : VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);
}

void VulkRenderTarget::EndRenderTargetPass()
{
  vkCmdEndRenderPass(Engine::swpch::vCmdBuffVec[Engine::swpch::vCurFrame]);
}

void VulkRenderTarget::LoadRenderTarget_1(const std::string& file)
{
  MemoryStream inFile;
  if (!inFile.FileRead(file)) return;

  std::vector<VkSubpassDependency> spDependencies;
  std::vector<VkFormat>            colorFormatOutput;
  std::vector<VkImageUsageFlags>   imageUsageOutput;
  bool hasDepth = false;
  bool hasRenderToDepthTexture = false;
  int rendertargetDimension[2]{ 0, 0 };

  inFile.Read(&hasDepth);
  inFile.Read(&hasRenderToDepthTexture);
  inFile.Read(rendertargetDimension, sizeof(int) << 1);

  // read dependencies
  size_t subpassDependencySize;
  inFile.Read(&subpassDependencySize);
  for (size_t i = 0; i < subpassDependencySize; ++i)
  {
    VkSubpassDependency tmpDependency;
    inFile.Read(&tmpDependency);
    spDependencies.push_back(tmpDependency);
  }

  // read output format (EXCLUDING depth attachment)
  size_t outputFormatSize;
  inFile.Read(&outputFormatSize);
  for (size_t i = 0; i < outputFormatSize; ++i)
  {
    VkFormat readFormat;
    inFile.Read(&readFormat);
    colorFormatOutput.push_back(readFormat);
  }

  // read usage (EXCLUDING depth attachment)
  size_t outputUsageSize;
  inFile.Read(&outputUsageSize);
  for (size_t i = 0; i < outputUsageSize; ++i)
  {
    VkImageUsageFlags readUsage;
    inFile.Read(&readUsage);
    imageUsageOutput.push_back(readUsage);
  }

  // apply correct dimension
  rendertargetDimension[0] = rendertargetDimension[0] == 0 ? Engine::core::windowsWidth : rendertargetDimension[0];
  rendertargetDimension[1] = rendertargetDimension[1] == 0 ? Engine::core::windowsHeight : rendertargetDimension[1];

  size_t totalAttachment = (uint32_t)(hasDepth ? outputUsageSize + 1 : outputUsageSize);
  std::vector<VkAttachmentDescription> attDescriptionList;
  std::vector<VkAttachmentReference>   colorAttachmentRefList;
  VkAttachmentReference                depthAttachmentRef;
  VkAttachmentReference* depthAttachmentRefPtr = nullptr;

  for (uint32_t i = 0; i < totalAttachment; ++i)
  {
    VkAttachmentDescription tmpAttDesc;

    if (hasDepth && i == outputUsageSize)
    {
      // depth attachment description
      tmpAttDesc.format = Engine::swpch::vDepthFmt;
      tmpAttDesc.samples = VK_SAMPLE_COUNT_1_BIT;
      tmpAttDesc.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
      tmpAttDesc.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
      tmpAttDesc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
      tmpAttDesc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
      tmpAttDesc.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
      tmpAttDesc.finalLayout = hasRenderToDepthTexture ? VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL : VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
      tmpAttDesc.flags = 0;

      // depth attachment reference
      depthAttachmentRef.attachment = i;
      depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
      depthAttachmentRefPtr = &depthAttachmentRef;

      VkImageUsageFlags depthUsage = hasRenderToDepthTexture ?
        VkImageUsageFlagBits::VK_IMAGE_USAGE_SAMPLED_BIT | VkImageUsageFlagBits::VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT :
        VkImageUsageFlagBits::VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
      AddAttachment(rendertargetDimension[0], rendertargetDimension[1],
        Engine::swpch::vDepthFmt,
        depthUsage, VkImageAspectFlagBits::VK_IMAGE_ASPECT_DEPTH_BIT);
    }
    else
    {
      // color attachment description
      tmpAttDesc.format = colorFormatOutput[i];// Engine::swpch::vFmt;
      tmpAttDesc.samples = VK_SAMPLE_COUNT_1_BIT;
      tmpAttDesc.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
      tmpAttDesc.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
      tmpAttDesc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
      tmpAttDesc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
      tmpAttDesc.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
      tmpAttDesc.finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
      tmpAttDesc.flags = 0;

      // color attachment reference
      VkAttachmentReference tmpAttachmentRef;
      tmpAttachmentRef.attachment = i;
      tmpAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
      colorAttachmentRefList.push_back(tmpAttachmentRef);

      AddAttachment(rendertargetDimension[0], rendertargetDimension[1],
        colorFormatOutput[i], imageUsageOutput[i], VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT);
    }

    attDescriptionList.push_back(tmpAttDesc);
  }

  VkSubpassDescription subpassDesc;
  subpassDesc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
  subpassDesc.flags = 0;
  subpassDesc.inputAttachmentCount = 0;
  subpassDesc.pInputAttachments = NULL;
  subpassDesc.colorAttachmentCount = (uint32_t)colorAttachmentRefList.size();
  subpassDesc.pColorAttachments = colorAttachmentRefList.data();
  subpassDesc.pResolveAttachments = NULL;
  subpassDesc.pDepthStencilAttachment = depthAttachmentRefPtr;
  subpassDesc.preserveAttachmentCount = 0;
  subpassDesc.pPreserveAttachments = NULL;


  VkRenderPassCreateInfo rpInfo;
  rpInfo.flags = 0;
  rpInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
  rpInfo.pNext = NULL;
  rpInfo.attachmentCount = (uint32_t)attDescriptionList.size();
  rpInfo.pAttachments = attDescriptionList.data();
  rpInfo.subpassCount = 1;
  rpInfo.pSubpasses = &subpassDesc;
  rpInfo.dependencyCount = (uint32_t)spDependencies.size();
  rpInfo.pDependencies = spDependencies.data();
#ifdef _DEBUG
  VkResult vkRes = vkCreateRenderPass(Engine::core::vDevice, &rpInfo, NULL, &m_renderPass);
  assert(vkRes == VK_SUCCESS);
#else
  vkCreateRenderPass(Engine::core::vDevice, &rpInfo, NULL, &m_renderPass);
#endif // _DEBUG

  // create frame buffers (color attachment created before hand)
  for (unsigned i = 0; i < Engine::swpch::vNumSCImgs; ++i)
  {
    // Grab rendertarget image view attachment
    auto imageViewList = GetAttachmentImageView(i);
    VkFramebufferCreateInfo fbInfo;
    fbInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    fbInfo.pNext = NULL;
    fbInfo.renderPass = m_renderPass;
    fbInfo.attachmentCount = (uint32_t)imageViewList.size();
    fbInfo.pAttachments = imageViewList.data();
    fbInfo.width = rendertargetDimension[0] ? rendertargetDimension[0] : Engine::core::windowsWidth;
    fbInfo.height = rendertargetDimension[1] ? rendertargetDimension[1] : Engine::core::windowsHeight;
    fbInfo.layers = 1;
    fbInfo.flags = 0;
#ifdef _DEBUG
    vkRes = vkCreateFramebuffer(Engine::core::vDevice, &fbInfo, NULL, &GetFramebuffer(i));
    assert(vkRes == VK_SUCCESS);
#else
    vkCreateFramebuffer(Engine::core::vDevice, &fbInfo, NULL, &GetFramebuffer(i));
#endif // _DEBUG
  }

  // Create samplers
  VkSamplerCreateInfo texSamInfo;
  texSamInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
  texSamInfo.minFilter = VkFilter::VK_FILTER_LINEAR;
  texSamInfo.magFilter = VkFilter::VK_FILTER_LINEAR;
  texSamInfo.addressModeU = texSamInfo.addressModeV =
  texSamInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
  texSamInfo.anisotropyEnable = VK_TRUE;
  texSamInfo.maxAnisotropy = 16;
  texSamInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK;
  texSamInfo.unnormalizedCoordinates = VK_FALSE;
  texSamInfo.compareEnable = VK_FALSE;
  texSamInfo.compareOp = VK_COMPARE_OP_ALWAYS;
  texSamInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
  texSamInfo.mipLodBias = 0.0f;
  texSamInfo.minLod = 0.0f;
  texSamInfo.maxLod = 0.0f;
  texSamInfo.pNext = NULL;
  texSamInfo.flags = 0;

#ifdef _DEBUG
  vkRes = vkCreateSampler(Engine::core::vDevice, &texSamInfo, NULL, &m_renderPassSampler);
  assert(vkRes == VK_SUCCESS);
#else
  vkCreateSampler(Engine::core::vDevice, &texSamInfo, NULL, &m_renderPassSampler);
#endif // _DEBUG
}

void VulkRenderTarget::AddAttachment(const unsigned& width, const unsigned& height, VkFormat fmt, VkImageUsageFlags usage, VkImageAspectFlags flags)
{
  if (m_outputAttachmentPerFrame.size() < 1)
    m_outputAttachmentPerFrame.resize(Engine::swpch::vNumSCImgs);
  for (auto& attachment : m_outputAttachmentPerFrame)
  {
    VulkTexImage tmpImg;
    tmpImg.CreateAttachment(width, height, fmt, usage, flags);
    attachment.m_vOutputAttachment.push_back(tmpImg);
  }
  
  VkClearValue tmpCV;
  tmpCV.color.float32[0] = 0.f;
  tmpCV.color.float32[1] = 0.f;
  tmpCV.color.float32[2] = 0.f;
  tmpCV.color.float32[3] = 0.f;
  tmpCV.depthStencil.depth = flags & VkImageAspectFlagBits::VK_IMAGE_ASPECT_DEPTH_BIT ? 1.f : 0.f;
  tmpCV.depthStencil.stencil = 0;
  m_clearValues.push_back(tmpCV);
}

std::vector<VkImageView> VulkRenderTarget::GetAttachmentImageView(const unsigned& frameIndex)
{
  std::vector<VkImageView> returnImageView;
  if (frameIndex >= m_outputAttachmentPerFrame.size()) return returnImageView;
  for (auto& texImage : m_outputAttachmentPerFrame[frameIndex].m_vOutputAttachment)
    returnImageView.push_back(texImage.GetTextureImageView());
  return returnImageView;
}

VkFramebuffer& VulkRenderTarget::GetFramebuffer(const unsigned& frameIndex)
{
  return m_outputAttachmentPerFrame[frameIndex].m_vFrameBuffer;
}

VkSampler& VulkRenderTarget::GetAttachmentSampler()
{
  return m_renderPassSampler;
}

#include "pch.h"
#include "LayoutMessages.h"

const unsigned LAYOUTMSG::msg::MODIFYSHADER       = 1;
const unsigned LAYOUTMSG::msg::SHADERBUILDFAILED  = 2;
const unsigned LAYOUTMSG::msg::MODIFYVERTEXBUFFER = 4;

void LAYOUTMSG::SetMessageFlag(unsigned& input, const unsigned& flag)
{
  input |= flag;
}

void LAYOUTMSG::ResetMessageFlag(unsigned& input, const unsigned & flag)
{
  unsigned invertFlag = ~flag;
  input &= invertFlag;
}

bool LAYOUTMSG::TestMessageFlag(const unsigned& input, const unsigned flag)
{
  return (input & flag) != 0;
}

void LAYOUTMSG::ClearMessageFlag(unsigned & input)
{
  input = 0;
}

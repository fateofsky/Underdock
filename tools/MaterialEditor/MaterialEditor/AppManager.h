#pragma once
#include "ResourceManager.h"
#include "ShaderBuilder.h"
#include "ModelLoader.h"
#include "ShaderBuilder.h"
#include "CommonVBLayout.h"

using MaterialLayoutMap = std::map<std::string, MaterialLayout>;

class AppManager
{
public:
  static void Initialize();
  static void Destroy();
  static AppManager* GetInstance();
  
  // File system
  void LoadDefault();
  bool LoadProjectLocation();
  //bool LoadRecentlyOpen();
  void InitializeSettings();
  bool Serialize(const std::string& fileLoc, const std::string& newFileName = "");
  bool DeSerialize(const std::string& file);

  void Update();
  void Render();
  void LastUpdate();

  void SetNewMousePosition(int x, int y);
  void InitializeMouse(int x, int y);
  int* GetMouseDelta();
  void ScrollCam(int dir);
  const float& GetDriverTime() const;


  /*
    IMGUI Update
  */
  void EditorUpdate();
  void EditorRender();
  
  MaterialLayoutMap& GetAllMaterialLayouts();
  void SetSaveDataLocationFromMaterial(const std::string& filePathLoc);
  void SetMaterialFromLayoutMap(MaterialLayout* layout);

  void OpenProject();
  void ImportModel();

private:
  AppManager();
  static AppManager* m_instance;

  // usage
  MaterialLayoutMap  m_materialLayouts;
  MaterialLayout*    m_curMatLayout;

public:
  const std::string  _DEFAULT_MODEL_LOC   = "assets/models/";
  const std::string  _DEFAULT_SHADER_LOC  = "assets/shaders/";
  const std::string  _DEFAULT_TEXTURE_LOC = "assets/textures/";
  const std::string  _DEFAULT_ASSETS_LOC  = "assets/";
  const std::string  _DEFAULT_SAVES_LOC   = "saves/";

private:              
  // Input           
  int                m_mouseDelta[2];
  int                m_oldPos[2];
                     
  // camera controls 
  glm::vec2          m_eulerAngle;
  float              m_camSpeed;
  float              m_camZPos;
  float              m_camZStrength;
  int                m_scrollDir;

  // driver specific
  float              m_driverTime;

  // internal
  std::string        m_openFilePath;
  bool               m_loadedDefault;
};
#include "pch.h"
#include "EntityTracker.h"

EntityTracker* EntityTracker::m_instance = nullptr;
EntityTracker::EntityTracker() : m_IDTracker(0), m_gameobjectHash() { }

const char* CheckValidity(char check)
{
  return check & COMPONENT_VALID ? "VALID" : "INVALID";
}


EntityTracker* EntityTracker::GetInstance()
{
  m_instance = m_instance == nullptr ? new EntityTracker() : m_instance;
  return m_instance;
}

void EntityTracker::AddGameobjectHash(const unsigned & objRef)
{
  _objComponentHash _obj;
  _obj._valid = COMPONENT_VALID;
  m_gameobjectHash[objRef] = _obj;

#ifdef _DEBUG
  ++m_NumberOfGameObjectsCreated;
#endif // _DEBUG

}

char* EntityTracker::GetGameobjectFlag(const unsigned & objRef)
{
  if ((m_gameobjectHash.find(objRef) != m_gameobjectHash.end()))
  {
    m_gameobjectHash[objRef]._ref++; 

#ifdef _DEBUG
      ++m_NumberOfGameObjectReferenceCreated;
#endif // _DEBUG

    return &m_gameobjectHash[objRef]._valid;
  }
  return nullptr;
}

void EntityTracker::RemoveGameobjectReference(const unsigned& objRef)
{
  if (m_gameobjectHash.find(objRef) != m_gameobjectHash.end())
  {
    _objComponentHash& _obj = m_gameobjectHash[objRef];
    unsigned& tRef = _obj._ref;
    tRef -= tRef > 0 ? 1 : 0;

    // invalidate all component reference of this gameobject
    //unsigned compRefCount = 0;
    //for (auto& comp : _obj._compHash)
    //{
    //  for (auto& compType : comp.second)
    //  {
    //    compType.second._flag = COMPONENT_INVALID;
    //    compRefCount += compType.second._ref;
    //  }
    //}

#ifdef _DEBUG
    ++m_NumberOfGameObjectReferenceDestroyed;
#endif // _DEBUG

    // if no one else reference to any component in this gameobject, clear it from hash
    if (tRef == 0 && (_obj._valid & COMPONENT_VALID) == COMPONENT_INVALID)
      m_gameobjectHash.erase(objRef);
  }
}


void EntityTracker::SetGameobjectInvalid(const unsigned & objRef)
{
  if (m_gameobjectHash.find(objRef) != m_gameobjectHash.end())
  {
    m_gameobjectHash[objRef]._valid = COMPONENT_INVALID;
    
    // invalidate all component reference of this gameobject
    _objComponentHash& _obj = m_gameobjectHash[objRef];
    unsigned compRefCount = 0;
    for (auto& comp : _obj._compHash)
    {
      for (auto& compType : comp.second)
      {
        compType.second._flag = COMPONENT_INVALID;

#ifdef _DEBUG
        ++m_NumberOfComponentsDestroyed;
#endif // _DEBUG

        compRefCount += compType.second._ref;
      }
    }

    // delete hash if no component of this gameobject is being referenced
    if (compRefCount == 0 && _obj._ref == 0)
      m_gameobjectHash.erase(objRef);

#ifdef _DEBUG
    ++m_NumberOfGameObjectsDestroyed;
#endif // _DEBUG
  }
}

void EntityTracker::AddComponentHash(const unsigned& objRef, const ComponentEnum & compId, const unsigned & compRef)
{
  assert(m_gameobjectHash.find(objRef) != m_gameobjectHash.end());
  _objComponentHash& _obj = m_gameobjectHash[objRef];
  if (_obj._valid) _obj._compHash[compId][compRef]._flag = COMPONENT_VALID;
  else assert(false);

#ifdef _DEBUG
  ++m_NumberOfComponentsCreated;
#endif // _DEBUG
}

char* EntityTracker::GetComponentFlag(const unsigned & objRef, const ComponentEnum & compId, const unsigned & compRef)
{
  assert(m_gameobjectHash.find(objRef) != m_gameobjectHash.end());
  _objComponentHash& _obj = m_gameobjectHash[objRef];
  if (_obj._valid)
  {
    assert(_obj._compHash.find(compId) != _obj._compHash.end());
    _obj._compHash[compId][compRef]._ref++;

#ifdef _DEBUG
    ++m_NumberOfComponentReferenceCreated;
#endif // _DEBUG

    return &_obj._compHash[compId][compRef]._flag;
  }
  return nullptr;
}

void EntityTracker::RemoveComponentReference(const unsigned & objRef, const ComponentEnum & compId, const unsigned & compRef)
{
  if (m_gameobjectHash.find(objRef) != m_gameobjectHash.end())
  {
    _objComponentHash& _obj = m_gameobjectHash[objRef];
    if (_obj._compHash.find(compId) != _obj._compHash.end() &&
        _obj._compHash[compId].find(compRef) != _obj._compHash[compId].end())
    {
      unsigned& tRef = _obj._compHash[compId][compRef]._ref;
      char& tFlag = _obj._compHash[compId][compRef]._flag;
      tRef -= tRef > 0 ? 1 : 0;

#ifdef _DEBUG
      ++m_NumberOfComponentReferenceDestroyed;
#endif // _DEBUG

      // delete component if no one referencing
      if (tRef == 0 && (tFlag & COMPONENT_VALID) == COMPONENT_INVALID)
      {
        _obj._compHash[compId].erase(compRef);
        if(_obj._compHash[compId].size() == 0)
          _obj._compHash.erase(compId);

        // help clear game object if no one referencing
        if (_obj._compHash.size() == 0 && _obj._valid == COMPONENT_INVALID && _obj._ref == 0)
          m_gameobjectHash.erase(objRef);
      }
    }
  }
}

void EntityTracker::SetComponentInvalid(const unsigned & objRef, const ComponentEnum & compId, const unsigned& compRef)
{
  if (m_gameobjectHash.find(objRef) != m_gameobjectHash.end())
  {
    _objComponentHash& _obj = m_gameobjectHash[objRef];
    if (_obj._compHash.find(compId) != _obj._compHash.end())
    {
      auto& map = _obj._compHash[compId];
      if (map.find(compRef) != map.end())
      {
        map[compRef]._flag = COMPONENT_INVALID;

        // clean up if no reference into it
        if (map[compRef]._ref == 0)
          map.erase(compRef);

#ifdef _DEBUG
        ++m_NumberOfComponentsDestroyed;
#endif // _DEBUG

      }
    }
  }
}

bool EntityTracker::HasGameObjectReference(const unsigned & objRef)
{
  return (m_gameobjectHash.find(objRef) != m_gameobjectHash.end());
}


bool EntityTracker::HasComponentReference(const unsigned & objRef, const ComponentEnum & compId, const unsigned & compRef)
{
  return (m_gameobjectHash.find(objRef) != m_gameobjectHash.end()
    && m_gameobjectHash[objRef]._compHash.find(compId) != m_gameobjectHash[objRef]._compHash.end()
    && m_gameobjectHash[objRef]._compHash[compId].find(compRef) != m_gameobjectHash[objRef]._compHash[compId].end());
}

void EntityTracker::RunDebugHashTable()
{
  std::cout << " ----------------------------------------------- \n";
  std::cout << " --                                           -- \n";
  std::cout << " --            [ENTITY HASH DEBUG]            -- \n";
  std::cout << " --                                           -- \n";
  std::cout << " ----------------------------------------------- \n";
  std::cout << " Total No. of Gameobject(s) in memory: " << m_gameobjectHash.size() << std::endl;

#ifdef _DEBUG
  std::cout << "\n <DEBUG MODE>\n";
  std::cout << " No. of Gameobject(s) created: " << m_NumberOfGameObjectsCreated << std::endl;
  std::cout << " No. of Gameobject(s) destroyed: " << m_NumberOfGameObjectsDestroyed << std::endl;
  std::cout << " No. of Gameobject handle(s) created: " << m_NumberOfGameObjectReferenceCreated << std::endl;
  std::cout << " No. of Gameobject handle(s) destroyed: " << m_NumberOfGameObjectReferenceDestroyed << std::endl;
  std::cout << " No. of Component(s) created: " << m_NumberOfComponentsCreated << std::endl;
  std::cout << " No. of Component(s) destroyed: " << m_NumberOfComponentsDestroyed << std::endl;
  std::cout << " No. of Component handle(s) created: " << m_NumberOfComponentReferenceCreated << std::endl;
  std::cout << " No. of Component handle(s) destroyed: " << m_NumberOfComponentReferenceDestroyed << std::endl;
  // Add size as well
#endif // _DEBUG

  for (auto& elem : m_gameobjectHash)
  {
    std::cout << "\n Gameobject ID: " << elem.first << std::endl;
    std::cout << "   -> Reference flag: " << CheckValidity(elem.second._valid) << std::endl;
    std::cout << "   -> Reference count: " << elem.second._ref << std::endl;
    if (elem.second._compHash.size() == 0)
      std::cout << "      - No component - \n";
    else
    {
      std::cout << "   -> Number of component type: " << elem.second._compHash.size() << std::endl;
      std::cout << " -----------------------------------------------\n";
    }

    for (auto& elem2 : elem.second._compHash)
    {
      std::cout << "        [Component: " << rtti::GetComponentEnumString(elem2.first) << "]" << std::endl;
      for (auto& elem3 : elem2.second)
      {
        std::cout << "          :ID: " << elem3.first << std::endl;
        std::cout << "              :-> Reference count: " << elem3.second._ref << std::endl;
        std::cout << "              :-> Reference flag: " << CheckValidity(elem3.second._flag) << std::endl;
      }

      if (elem2.second.size() == 0)
        std::cout << "          - No reference - \n";
      std::cout << " -----------------------------------------------\n";
    }
    std::cout << "\n";
  }

  std::cout << "\n";
}

unsigned EntityTracker::GenerateID()
{
  return m_IDTracker++;
}

unsigned EntityTracker::GetCurrentID()
{
  return m_IDTracker;
}

void EntityTracker::SetMaxID(const unsigned & maxID)
{
  m_IDTracker = maxID;
}

bool EntityTracker::IsUnuseID(const unsigned & ID) const
{
  for (auto& gObj : m_gameobjectHash)
  {
    if (ID == gObj.first) return false;

    for (auto& gComp : gObj.second._compHash)
    {
      for (auto& compHash : gComp.second)
        if (ID == compHash.first) return false;
    }
  }
  
  return true;
}

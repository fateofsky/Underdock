/* Start Header
*****************************************************************/
/*!
\file      MathMatrixOperation.cpp
\author
\par
\date
\brief

Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the
prior written consent of DigiPen Institute of Technology is prohibited.
*/
/* End Header
*******************************************************************/
#include "stdafx.h"

////////////////
// MatrixUtil //--------------------------------------------------------------
////////////////
mat3 mat3Zero()
{
	mat3 mtx;
	std::fill(mtx.m_33, mtx.m_33 + mat3_LEN, 0.0f);
	return mtx;
}

mat3 mat3Identity()
{
	return mat3();
}

void mat3Translate(mat3& out, const vec2& trans)
{
	out.m02 = trans.x_;
	out.m12 = trans.y_;
}

void mat3Translate(mat3& out, float x, float y)
{
	out.m02 = x;
	out.m12 = y;
}

void mat3Scale(mat3& out, const vec2& scale)
{
	out.m00 = scale.x_;
	out.m11 = scale.y_;
}

void mat3Scale(mat3& out, float x, float y)
{
	out.m00 = x;
	out.m11 = y;
}

void mat3RotRad(mat3& out, float angle)
{
	out.Identity();
	out.m00 = out.m11 = cos(angle);
	out.m10 = sin(angle);
	out.m01 = -out.m10;
}

void mat3RotDeg(mat3& out, float angle)
{
	mat3RotRad(out, ::Math::toRad<float>(angle));
}

void mat3Transpose(mat3& out, const mat3& mtx)
{
	if (&out != &mtx)
	{
		out.m00 = mtx.m00, out.m01 = mtx.m10, out.m02 = mtx.m20;
		out.m10 = mtx.m01, out.m11 = mtx.m11, out.m12 = mtx.m21;
		out.m20 = mtx.m02, out.m21 = mtx.m12, out.m22 = mtx.m22;
	}

	else
	{
		mat3 tmp;
		tmp.m00 = mtx.m00, tmp.m01 = mtx.m10, tmp.m02 = mtx.m20;
		tmp.m10 = mtx.m01, tmp.m11 = mtx.m11, tmp.m12 = mtx.m21;
		tmp.m20 = mtx.m02, tmp.m21 = mtx.m12, tmp.m22 = mtx.m22;
		out = tmp;
	}

}

void mat3Inverse(mat3& out, const mat3& mtx, float* determinant)
{
	float tmpDet = 0;
	if (determinant == nullptr) determinant = &tmpDet;
	*determinant = mtx.m_33[0] * mtx.m_33[4] * mtx.m_33[8] +
		mtx.m_33[1] * mtx.m_33[5] * mtx.m_33[6] +
		mtx.m_33[2] * mtx.m_33[3] * mtx.m_33[7] -
		mtx.m_33[2] * mtx.m_33[4] * mtx.m_33[6] -
		mtx.m_33[1] * mtx.m_33[3] * mtx.m_33[8] -
		mtx.m_33[0] * mtx.m_33[5] * mtx.m_33[7];
	if (abs(*determinant) <= DBL_EPSILON) return;

	if (&out != &mtx)
	{
		out.m00 = mtx.m11 * mtx.m22 - mtx.m12 * mtx.m21;
		out.m01 = mtx.m02 * mtx.m21 - mtx.m01 * mtx.m22;
		out.m02 = mtx.m01 * mtx.m12 - mtx.m02 * mtx.m11;
		out.m10 = mtx.m12 * mtx.m20 - mtx.m10 * mtx.m22;
		out.m11 = mtx.m00 * mtx.m22 - mtx.m02 * mtx.m20;
		out.m12 = mtx.m02 * mtx.m10 - mtx.m00 * mtx.m12;
		out.m20 = mtx.m10 * mtx.m21 - mtx.m11 * mtx.m20;
		out.m21 = mtx.m01 * mtx.m20 - mtx.m00 * mtx.m21;
		out.m22 = mtx.m00 * mtx.m11 - mtx.m01 * mtx.m10;

		//Multiply by 1 / determinant
		for (size_t i = 0; i < mat3_LEN; ++i) out.m_33[i] /= *determinant;
	}

	else
	{
		mat3 tmp;
		tmp.m00 = mtx.m11 * mtx.m22 - mtx.m12 * mtx.m21;
		tmp.m01 = mtx.m02 * mtx.m21 - mtx.m01 * mtx.m22;
		tmp.m02 = mtx.m01 * mtx.m12 - mtx.m02 * mtx.m11;
		tmp.m10 = mtx.m12 * mtx.m20 - mtx.m10 * mtx.m22;
		tmp.m11 = mtx.m00 * mtx.m22 - mtx.m02 * mtx.m20;
		tmp.m12 = mtx.m02 * mtx.m10 - mtx.m00 * mtx.m12;
		tmp.m20 = mtx.m10 * mtx.m21 - mtx.m11 * mtx.m20;
		tmp.m21 = mtx.m01 * mtx.m20 - mtx.m00 * mtx.m21;
		tmp.m22 = mtx.m00 * mtx.m11 - mtx.m01 * mtx.m10;

		//Multiply by 1 / determinant
		for (size_t i = 0; i < mat3_LEN; ++i) tmp.m_33[i] /= *determinant;

		out = tmp;
	}
}

mat4 mat4Zero()
{
	mat4 mtx;
	float z_ = 0.0f;
	__m128 ymmZero = _mm_broadcast_ss(&z_);
	float * tmp = (float *)&mtx;

	for (int i = 0; i < mat4_LEN; i += mat4_ROW)
		_mm_store_ps(tmp + i, ymmZero);

	return mtx;
}

mat4 mat4Identity()
{
	return mat4();
}

void mat4Translate(mat4& out, const vec3& trans)
{
	out.m03 = trans.x_;
	out.m13 = trans.y_;
	out.m23 = trans.z_;
}

void mat4Translate(mat4& out, float x, float y, float z)
{
	out.m03 = x;
	out.m13 = y;
	out.m23 = z;
}

void mat4Scale(mat4& out, const vec3& scale)
{
	out.m00 = scale.x_;
	out.m11 = scale.y_;
	out.m22 = scale.z_;
}

void mat4Scale(mat4& out, float x, float y, float z)
{
	out.m00 = x;
	out.m11 = y;
	out.m22 = z;
}

// Combined the rotation functions
void mat4RotXRad(mat4& out, float angle)
{
	out.m11 = cos(angle);
	out.m21 = sin(angle);
	out.m12 = -out.m21;
	out.m22 = out.m11;
}

void mat4RotXDeg(mat4& out, float angle)
{
	mat4RotXRad(out, Math::toRad(angle));
}

void mat4RotYRad(mat4& out, float angle)
{
	out.m00 = cos(angle);
	out.m20 = -sin(angle);
	out.m02 = -out.m20;
	out.m22 = out.m00;
}

void mat4RotYDeg(mat4& out, float angle)
{
	mat4RotYRad(out, Math::toRad(angle));
}

void mat4RotZRad(mat4& out, float angle)
{
	out.m00 = out.m11 = cos(angle);
	out.m10 = sin(angle);
	out.m01 = -out.m10;
}

void mat4RotZDeg(mat4& out, float angle)
{
	mat4RotZRad(out, Math::toRad(angle));
}

void mat4RotRad(mat4& out, float xAngle, float yAngle, float zAngle)
{
	//todo, optimize
	mat4 a, b, c;
	mat4RotZRad(a, zAngle), mat4RotYRad(b, yAngle), mat4RotXRad(c, xAngle);
	out = a * b * c;
}

void mat4RotDeg(mat4& out, float xAngle, float yAngle, float zAngle)
{
	return mat4RotRad(out, Math::toRad(xAngle), Math::toRad(yAngle), Math::toRad(zAngle));
}

void mat4Transpose(mat4& out, const mat4& mtx)
{
	__m128 i0, i1, i2, i3, t0, t1, t2, t3;
	const float * in = (const float *)&mtx;
	float * tmp;
	mat4 m;

	if (&out != &mtx)
		tmp = (float *)&out;
	else
		tmp = (float *)&m;

	i0 = _mm_load_ps(in);
	i1 = _mm_load_ps(in + 4);
	i2 = _mm_load_ps(in + 8);
	i3 = _mm_load_ps(in + 12);

	t0 = _mm_unpacklo_ps(i0, i1);
	t1 = _mm_unpacklo_ps(i2, i3);
	t2 = _mm_unpackhi_ps(i0, i1);
	t3 = _mm_unpackhi_ps(i2, i3);

	i0 = _mm_movelh_ps(t0, t1);
	i1 = _mm_movehl_ps(t1, t0);
	i2 = _mm_movelh_ps(t2, t3);
	i3 = _mm_movehl_ps(t3, t2);

	_mm_store_ps(tmp, i0);
	_mm_store_ps(tmp + 4, i1);
	_mm_store_ps(tmp + 8, i2);
	_mm_store_ps(tmp + 12, i3);

	if (&out == &mtx)
		out = m;
}

void mat4Inverse(mat4& out, const mat4&mtx, float* determinant)
{
	float tmpDet = 0;
	if (determinant == nullptr) determinant = &tmpDet;

	float * src = (float *)&mtx;
	float * result;
	mat4 m;

	if (&out != &mtx)
		result = (float *)&out;
	else
		result = (float *)&m;

	__m128 minor0, minor1, minor2, minor3;
	__m128 row0, row1 = __m128(), row2, row3 = __m128();
	__m128 det, tmp1 = __m128();

	tmp1 = _mm_loadh_pi(_mm_loadl_pi(tmp1, (__m64*)(src)), (__m64*)(src + 4));
	row1 = _mm_loadh_pi(_mm_loadl_pi(row1, (__m64*)(src + 8)), (__m64*)(src + 12));
	row0 = _mm_shuffle_ps(tmp1, row1, 0x88);
	row1 = _mm_shuffle_ps(row1, tmp1, 0xDD);
	tmp1 = _mm_loadh_pi(_mm_loadl_pi(tmp1, (__m64*)(src + 2)), (__m64*)(src + 6));
	row3 = _mm_loadh_pi(_mm_loadl_pi(row3, (__m64*)(src + 10)), (__m64*)(src + 14));
	row2 = _mm_shuffle_ps(tmp1, row3, 0x88);
	row3 = _mm_shuffle_ps(row3, tmp1, 0xDD);
	//-----------------------------------------------
	tmp1 = _mm_mul_ps(row2, row3);
	tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0xB1);
	minor0 = _mm_mul_ps(row1, tmp1);
	minor1 = _mm_mul_ps(row0, tmp1);
	tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0x4E);
	minor0 = _mm_sub_ps(_mm_mul_ps(row1, tmp1), minor0);
	minor1 = _mm_sub_ps(_mm_mul_ps(row0, tmp1), minor1);
	minor1 = _mm_shuffle_ps(minor1, minor1, 0x4E);
	//-----------------------------------------------
	tmp1 = _mm_mul_ps(row1, row2);
	tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0xB1);
	minor0 = _mm_add_ps(_mm_mul_ps(row3, tmp1), minor0);
	minor3 = _mm_mul_ps(row0, tmp1);
	tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0x4E);
	minor0 = _mm_sub_ps(minor0, _mm_mul_ps(row3, tmp1));
	minor3 = _mm_sub_ps(_mm_mul_ps(row0, tmp1), minor3);
	minor3 = _mm_shuffle_ps(minor3, minor3, 0x4E);
	//-----------------------------------------------
	tmp1 = _mm_mul_ps(_mm_shuffle_ps(row1, row1, 0x4E), row3);
	tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0xB1);
	row2 = _mm_shuffle_ps(row2, row2, 0x4E);
	minor0 = _mm_add_ps(_mm_mul_ps(row2, tmp1), minor0);
	minor2 = _mm_mul_ps(row0, tmp1);
	tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0x4E);
	minor0 = _mm_sub_ps(minor0, _mm_mul_ps(row2, tmp1));
	minor2 = _mm_sub_ps(_mm_mul_ps(row0, tmp1), minor2);
	minor2 = _mm_shuffle_ps(minor2, minor2, 0x4E);
	//-----------------------------------------------
	tmp1 = _mm_mul_ps(row0, row1);

	tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0xB1);
	minor2 = _mm_add_ps(_mm_mul_ps(row3, tmp1), minor2);
	minor3 = _mm_sub_ps(_mm_mul_ps(row2, tmp1), minor3);
	tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0x4E);
	minor2 = _mm_sub_ps(_mm_mul_ps(row3, tmp1), minor2);
	minor3 = _mm_sub_ps(minor3, _mm_mul_ps(row2, tmp1));
	//-----------------------------------------------
	tmp1 = _mm_mul_ps(row0, row3);
	tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0xB1);
	minor1 = _mm_sub_ps(minor1, _mm_mul_ps(row2, tmp1));
	minor2 = _mm_add_ps(_mm_mul_ps(row1, tmp1), minor2);
	tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0x4E);
	minor1 = _mm_add_ps(_mm_mul_ps(row2, tmp1), minor1);
	minor2 = _mm_sub_ps(minor2, _mm_mul_ps(row1, tmp1));
	//-----------------------------------------------
	tmp1 = _mm_mul_ps(row0, row2);
	tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0xB1);
	minor1 = _mm_add_ps(_mm_mul_ps(row3, tmp1), minor1);
	minor3 = _mm_sub_ps(minor3, _mm_mul_ps(row1, tmp1));
	tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0x4E);
	minor1 = _mm_sub_ps(minor1, _mm_mul_ps(row3, tmp1));
	minor3 = _mm_add_ps(_mm_mul_ps(row1, tmp1), minor3);
	//-----------------------------------------------
	det = _mm_mul_ps(row0, minor0);
	det = _mm_add_ps(_mm_shuffle_ps(det, det, 0x4E), det);
	det = _mm_add_ss(_mm_shuffle_ps(det, det, 0xB1), det);
	tmp1 = _mm_rcp_ss(det);
	det = _mm_sub_ss(_mm_add_ss(tmp1, tmp1), _mm_mul_ss(det, _mm_mul_ss(tmp1, tmp1)));
	det = _mm_shuffle_ps(det, det, 0x00);
	minor0 = _mm_mul_ps(det, minor0);

	_mm_storel_pi((__m64*)(result), minor0);
	_mm_storeh_pi((__m64*)(result + 2), minor0);
	minor1 = _mm_mul_ps(det, minor1);
	_mm_storel_pi((__m64*)(result + 4), minor1);
	_mm_storeh_pi((__m64*)(result + 6), minor1);
	minor2 = _mm_mul_ps(det, minor2);
	_mm_storel_pi((__m64*)(result + 8), minor2);
	_mm_storeh_pi((__m64*)(result + 10), minor2);
	minor3 = _mm_mul_ps(det, minor3);
	_mm_storel_pi((__m64*)(result + 12), minor3);
	_mm_storeh_pi((__m64*)(result + 14), minor3);

	det = _mm_rcp_ss(det);
	_mm_store_ss((determinant), det);

	if (&out == &mtx)
		out = m;
}

void correctAngleDomains(vec3& rotation)
{
	if (rotation.x_ < 0) rotation.x_ = 180.f + (180.f - abs(rotation.x_));
	if (rotation.y_ < 0) rotation.y_ = 180.f + (180.f - abs(rotation.y_));
	if (rotation.z_ < 0) rotation.z_ = 180.f + (180.f - abs(rotation.z_));
	if (rotation.x_ >= 359.9f) rotation.x_ = 0.f;
	if (rotation.z_ >= 359.9f) rotation.z_ = 0.f;
}

void orientRotationToOldRotation(vec3& rotation, const vec3& oldRotation)
{
	float diffX = (rotation.x_ - oldRotation.x_);
	float diffZ = (rotation.z_ - oldRotation.z_);
	if (abs(diffX) >= 135 && abs(diffZ) >= 135 && abs(diffX) <= 225 && abs(diffZ) <= 225)
	{
    if (diffX > 0) rotation.x_ -= 180.f;
    else rotation.x_ += 180.f;
    if (diffZ > 0) rotation.z_ -= 180.f;
    else rotation.z_ += 180.f;
		if (rotation.y_ < 180.f) rotation.y_ = 90.f + (90.f - rotation.y_);
		else rotation.y_ = 270.f - (rotation.y_ - 270.f);
	}
}

void mtxToTransformations(const mat4& in, vec3* rotation, vec3* scale, vec3* translation, vec3* oldRotation)
{
  vec3 oldRot;
  if (oldRotation != nullptr) oldRot = *oldRotation;
	if (translation != nullptr)
	{
		*translation = vec3(in.m03, in.m13, in.m23);
	}
	vec3 col1 = vec3(in.m00, in.m10, in.m20);
	vec3 col2 = vec3(in.m01, in.m11, in.m21);
	vec3 col3 = vec3(in.m02, in.m12, in.m22);
	vec3 thisScale;
	thisScale.x_ = col1.magnitude();
	thisScale.y_ = col2.magnitude();
	thisScale.z_ = col3.magnitude();
	if (scale != nullptr) *scale = thisScale;
	if (rotation != nullptr)
	{
		col1 /= thisScale.x_;
		col2 /= thisScale.y_;
		col3 /= thisScale.z_;
		float sy = sqrt(col1.x_ * col1.x_ + col1.y_ * col1.y_);

		bool singular = sy < 0.000001f;
		if (!singular)
		{
			(*rotation).x_ = atan2(col2.z_, col3.z_);
			(*rotation).y_ = atan2(-col1.z_, sy);
			(*rotation).z_ = atan2(col1.y_, col1.x_);

		}
		else
		{
			(*rotation).x_ = atan2(-col3.y_, col2.y_);
			(*rotation).y_ = atan2(-col1.z_, sy);
			(*rotation).z_ = 0.f;

		}
		(*rotation) *= TODEGREE;
		if (isnan((*rotation).x_)) (*rotation).x_ = -90.f;
		if (isnan((*rotation).y_)) (*rotation).y_ = -90.f;
		if (isnan((*rotation).z_)) (*rotation).z_ = -90.f;
		
		correctAngleDomains(*rotation);
		if (oldRotation != nullptr)
		{
			orientRotationToOldRotation(*rotation, oldRot);
			
		}
	}
}
#pragma once
#include <vulkan\vulkan.h>

class VulkBuffer
{
public:
  void CreateBuffer(unsigned size, unsigned copies, VkBufferUsageFlags usage,
                    VkMemoryPropertyFlags memory_property);
  void DestroyBuffer();

  void BindBufferMemory();
  void MapMemory(void* dataToMap, unsigned size, VkDeviceSize offset = 0);
  void CopyBuffer(VkCommandBuffer cmd, const VulkBuffer& rhsBuffer);

  VkBuffer            GetBuffer() const;
  VkBuffer*           GetBufferPtr();
  const VkBuffer*     GetBufferPtr() const;
  VkDeviceMemory      GetBufferDevMem() const;
  void*               GetMappedMemoryData();
  const VkDeviceSize& GetTotalAllocatedMemory() const;
  const VkDeviceSize& GetBufferAlignmentSize() const;

private:
  VkBuffer           vBuffer;
  VkDeviceMemory     vBufMem;
  void*              vBufMemData;
  VkBufferUsageFlags vBufUsageFlags;
  VkDeviceSize       vTotalAllocated;
  VkDeviceSize       vTotalRequested;
  VkDeviceSize       vBufferAlignment;
};
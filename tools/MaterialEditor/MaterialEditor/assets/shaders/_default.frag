#version 450 core

// pipeline input
layout (location = 0) in vec2 uvOut;

// uniform input (*VULKAN: binding)
layout (binding = 4) uniform sampler2D texture0;

// const input

// Fragment output
layout (location = 0) out vec4 FragColor;

void main()
{
  FragColor = texture(texture0, uvOut);
}
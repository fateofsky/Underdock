#pragma once
#include <vulkan\vulkan.h>
#include <string>
#include <iostream>
#include <vector>

#if defined _DEBUG
namespace vkDebugger
{
  extern bool                          InitValidationLayer();
  extern bool                          InitDebugUtils();
  extern void                          DestroyDebugUtils();
  extern void                          ShowGPUSpecs();
  extern VkDebugUtilsMessengerEXT      vDebugMsgr;       // Vulkan debugging validation layer
  extern const std::vector<const char*> validationLayer;

  extern VKAPI_ATTR VkBool32 VKAPI_CALL vulkanDebugCallback(
                                                    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
                                                    VkDebugUtilsMessageTypeFlagsEXT messageType,
                                                    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
                                                    void* pUserData);

  extern std::string GetPhysicalDeviceTypeString(const VkPhysicalDeviceType& dType);
}
#endif
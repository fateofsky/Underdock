#include "pch.h"
#include "Transform.h"

Transform::Transform(Gameobject* owner, const unsigned& _id)
  : ComponentBase(owner, _id) 
{
  ComponentBase::InsertParam(0, &m_position);
  ComponentBase::InsertParam(1, &m_scale);
  ComponentBase::InsertParam(2, &m_rotation);
}

Transform::~Transform()
{ }

void Transform::Init()
{

}

void Transform::PostInit()
{
  mat4Translate(m_translationMtx, m_position.GetData());
  mat4RotDeg(m_rotationMtx, m_rotation.GetData().x_, m_rotation.GetData().y_, m_rotation.GetData().z_);
  mat4Scale(m_scaleMtx, m_scale.GetData().x_, m_scale.GetData().y_, m_scale.GetData().z_);
  m_transformation = m_translationMtx * m_rotationMtx * m_scaleMtx;

  m_matHandle.MoveComponentHandleOver(GetOwner()->GetFirstOfComponentHandle<MaterialComponent>());
}

void Transform::Update()
{
  if (_update)
  {
    mat4Translate(m_translationMtx, m_position.GetData());
    mat4RotDeg(m_rotationMtx, m_rotation.GetData().x_, m_rotation.GetData().y_, m_rotation.GetData().z_);
    mat4Scale(m_scaleMtx, m_scale.GetData().x_, m_scale.GetData().y_, m_scale.GetData().z_);
    m_transformation = m_translationMtx * m_rotationMtx * m_scaleMtx;
    _update = false;
  }
}

void Transform::PreRender()
{
}

void Transform::PostUpdate()
{
}

const vec3& Transform::GetPosition() const
{
  return m_position.GetData();
}

const vec3& Transform::GetScale() const
{
  return m_scale.GetData();
}

const vec3& Transform::GetRotation() const
{
  return m_rotation.GetData();
}

void Transform::SetPosition(const vec3& pos)
{
  _update = vec3Compare(m_position.GetData(), pos);
  m_position.GetData() = pos;
}

void Transform::SetScale(const vec3& scale)
{
  _update = vec3Compare(m_scale.GetData(), scale);
  m_scale.GetData() = scale;
}

void Transform::SetRotation(const vec3& rotate)
{
  _update = vec3Compare(m_rotation.GetData(), rotate);
  m_rotation.GetData() = rotate;
}

const mat4& Transform::GetTransformMtx() const
{
  return m_transformation;
}

mat4 Transform::GetTransformMtx_Transpose() const
{
  return m_transformation.transposed();
}

const mat4& Transform::GetTranslationMtx() const
{
  return m_translationMtx;
}

mat4 Transform::GetTranslationMtx_Transpose() const
{
  return m_translationMtx.transposed();
}

const mat4& Transform::GetRotationMtx() const
{
  return m_rotationMtx;
}

mat4 Transform::GetRotationMtx_Transpose() const
{
  return m_rotationMtx.transposed();
}

const mat4& Transform::GetScaleMtx() const
{
  return m_scaleMtx;
}

mat4 Transform::GetScaleMtx_Transpose() const
{
  return m_scaleMtx.transposed();
}

bool Transform::GetRequiredUpdate() const
{
  return _update;
}

#pragma once
#include "glm/glm.hpp"
#include "OGLHeader.h"
#include "Shader.h"
#include "OGLBaseType.h"
#include "../../../tools/Utilities/MemoryStream/MemoryStream/MemoryStream.h"

enum UNIFORM_USAGE
{
  CAM_VIEW,
  CAM_PROJ,
  CAM_VIEWPROJ,
  TRANSFORM,
  USER_INPUT,
  DRIVER_TIME
};


class UniformResourceBase
{
public:
  UniformResourceBase(OGL_BASETYPE type);
  virtual void       BindUniform(GLint location, bool transpose = false) = 0; // TRANSPOSE ONLY APPPLICABLE TO MATRICES
  virtual void*      GetUniformData() = 0;
  OGL_BASETYPE       GetType() const;
  UNIFORM_USAGE      GetUsage() const;
  void               ChangeUsage(UNIFORM_USAGE newUsage);
  void               ChangeGroup(const unsigned& set);
  unsigned           GetGroup() const;

  // Creation of uniform
  static UniformResourceBase* CreateUniform(OGL_BASETYPE baseType, UNIFORM_USAGE usage, unsigned group);
  virtual void       Serialize(MemoryStream& ofs) = 0;
  virtual void       DeSerialize(MemoryStream& ifs) = 0;

private:
  const OGL_BASETYPE     m_type;
  UNIFORM_USAGE          m_usage;
  unsigned               m_groupSet;
};

class UniformInt : public UniformResourceBase
{
public:
  UniformInt();
  virtual void* GetUniformData() override;
  virtual void  BindUniform(GLint location, bool transpose = false) override;

  virtual void  Serialize(MemoryStream& ofs) override;
  virtual void  DeSerialize(MemoryStream& ifs) override;

private:
  int m_data;
};


class UniformInt4 : public UniformResourceBase
{
public:
  UniformInt4();
  virtual void* GetUniformData() override;
  virtual void  BindUniform(GLint location, bool transpose = false) override;

  virtual void  Serialize(MemoryStream& ofs) override;
  virtual void  DeSerialize(MemoryStream& ifs) override;

private:
  int m_data[4];
};


class UniformFloat : public UniformResourceBase
{
public:
  UniformFloat();
  virtual void* GetUniformData() override;
  virtual void  BindUniform(GLint location, bool transpose = false) override;

  virtual void  Serialize(MemoryStream& ofs) override;
  virtual void  DeSerialize(MemoryStream& ifs) override;

private:
  float m_data;
};

class UniformBool : public UniformResourceBase
{
public:
  UniformBool();
  virtual void* GetUniformData() override;
  virtual void  BindUniform(GLint location, bool transpose = false) override;

  virtual void  Serialize(MemoryStream& ofs) override;
  virtual void  DeSerialize(MemoryStream& ifs) override;

private:
  bool m_data;
};

class UniformVec2 : public UniformResourceBase
{
public:
  UniformVec2();
  virtual void* GetUniformData() override;
  virtual void  BindUniform(GLint location, bool transpose = false) override;

  virtual void  Serialize(MemoryStream& ofs) override;
  virtual void  DeSerialize(MemoryStream& ifs) override;

private:
  glm::vec2 m_data;
};


class UniformVec3 : public UniformResourceBase
{
public:
  UniformVec3();
  virtual void* GetUniformData() override;
  virtual void  BindUniform(GLint location, bool transpose = false) override;

  virtual void  Serialize(MemoryStream& ofs) override;
  virtual void  DeSerialize(MemoryStream& ifs) override;

private:
  glm::vec3 m_data;
};


class UniformVec4 : public UniformResourceBase
{
public:
  UniformVec4();
  virtual void* GetUniformData() override;
  virtual void  BindUniform(GLint location, bool transpose = false) override;

  virtual void  Serialize(MemoryStream& ofs) override;
  virtual void  DeSerialize(MemoryStream& ifs) override;

private:
  glm::vec4 m_data;
};


class UniformMat3 : public UniformResourceBase
{
public:
  UniformMat3();
  virtual void* GetUniformData() override;
  virtual void  BindUniform(GLint location, bool transpose = false) override;

  virtual void  Serialize(MemoryStream& ofs) override;
  virtual void  DeSerialize(MemoryStream& ifs) override;

private:
  glm::mat3 m_data;
};


class UniformMat4 : public UniformResourceBase
{
public:
  UniformMat4();
  virtual void* GetUniformData() override;
  virtual void  BindUniform(GLint location, bool transpose = false) override;

  virtual void  Serialize(MemoryStream& ofs) override;
  virtual void  DeSerialize(MemoryStream& ifs) override;

private:
  glm::mat4 m_data;
};

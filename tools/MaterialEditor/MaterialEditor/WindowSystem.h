#pragma once
#include <direct.h>
#include <assert.h>

namespace WinSystem
{
  extern HWND  hwnd;
  extern HDC   hdc;
  extern HGLRC hrc; //rendering context

  extern void             InitWindow(HINSTANCE instanceH);
  extern LRESULT CALLBACK WinProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
  extern void             Run();
  extern void             Cleanup();

  // cmd line / file system usage
  extern std::string glslangExe;
  extern std::string curDirectory;
  extern std::string GetWorkingDirectory();
  extern std::string GetTempFilePath();
  extern void InvokeProcess(const char* lpApplicationName, char* cmdLine);
  extern void ClearTempFile(const char* filePath);
  extern std::string SaveFileAs(LPCSTR filter, LPCSTR extToAppend);
  extern std::string OpenFile(LPCSTR filter);
}